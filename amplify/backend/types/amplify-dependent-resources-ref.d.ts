export type AmplifyDependentResourcesAttributes = {
    "analytics": {
        "amplifyinit": {
            "Region": "string",
            "Id": "string",
            "appName": "string"
        }
    }
}