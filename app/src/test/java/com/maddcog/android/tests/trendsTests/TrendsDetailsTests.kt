package com.maddcog.android.tests.trendsTests

import android.content.Context
import com.maddcog.android.data.extensions.toLocalName
import com.maddcog.android.domain.entities.activity.Match
import com.maddcog.android.domain.entities.activity.MyPlayer
import com.maddcog.android.domain.entities.activity.PerformanceParam
import com.maddcog.android.domain.entities.activity.UserActivity
import com.maddcog.android.fakes.usecases.FakeGetTrendsDetailsUseCaseNegative
import com.maddcog.android.fakes.usecases.FakeGetTrendsDetailsUseCasePositive
import com.maddcog.android.trends.TrendsDetailsViewModel
import com.maddcog.android.trends.utils.FiltersListHandler
import com.maddcog.android.trends.utils.FiltersListHandler.Companion.ALL
import com.maddcog.android.trends.utils.FiltersListHandler.Companion.ALL_ACTIVITIES
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.Mockito.mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class TrendsDetailsTests {

    val dispatcher = TestCoroutineDispatcher()

    private lateinit var positiveUseCase: FakeGetTrendsDetailsUseCasePositive
    private lateinit var negativeUseCase: FakeGetTrendsDetailsUseCaseNegative
    private lateinit var positiveViewModel: TrendsDetailsViewModel
    private lateinit var negativeViewModel: TrendsDetailsViewModel

    private val filtersListHandler = FiltersListHandler()
    val context: Context = mock(Context::class.java)

    @BeforeEach
    fun open() {
        Dispatchers.setMain(dispatcher)

        positiveUseCase = FakeGetTrendsDetailsUseCasePositive(filtersListHandler)
        positiveViewModel = TrendsDetailsViewModel(positiveUseCase, mockk(), filtersListHandler)

        negativeUseCase = FakeGetTrendsDetailsUseCaseNegative(filtersListHandler)
        negativeViewModel = TrendsDetailsViewModel(negativeUseCase, mockk(), filtersListHandler)
    }

    @AfterEach
    fun close() {
        Dispatchers.resetMain()
    }

    @Test
    fun negative_test_1() {
        Assertions.assertEquals(null, negativeViewModel.state.value.trendsDetailsData)
    }

    @Test
    fun negative_test_2() {
        negativeViewModel.getTrendsDetails(null)
        Assertions.assertEquals("Trends Details Error", negativeViewModel.state.value.error)
    }

    @Test
    fun positive_test_1() {
        positiveViewModel.getTrendsDetails(null)
        val data = positiveViewModel.state.value.trendsDetailsData

        Assertions.assertEquals("123", data?.trendsInsight)
    }

    @Test
    fun positive_test_2() {
        positiveViewModel.getTrendsDetails(null)
        val data = positiveViewModel.state.value.trendsDetailsData

        Assertions.assertEquals("All", data?.userNameFilter)
    }

    @Test
    fun filters_list_1() {
        val listToFilter = listOf("Messi", "Ronaldo", "Figu", "Zidane", "Ronaldo", "Messi")
        Assertions.assertEquals(
            listOf("All", "Messi", "Ronaldo", "Figu", "Zidane"),
            filtersListHandler.getFilterList(ALL, listToFilter)
        )
    }

    @Test
    fun filters_list_2() {
        val listToFilter = listOf("Ronaldo", "Figu", "Zidane")
        Assertions.assertEquals(
            listOf("All Activities", "Ronaldo", "Figu", "Zidane"),
            filtersListHandler.getFilterList(ALL_ACTIVITIES, listToFilter)
        )
    }

    @Test
    fun filters_duration() {
        val durationList = listOf("All", "Last month", "Last week", "Hourly", "Per game", "During game")
        Assertions.assertEquals(
            durationList,
            filtersListHandler.getDurationList()
        )
    }

    @Test
    fun filters_tags() {
        val userActivity1 = Mockito.mock(UserActivity::class.java)
        Mockito.`when`(userActivity1.tags).thenReturn(listOf("Tag_1", "Tag_1_2"))
        val userActivity2 = Mockito.mock(UserActivity::class.java)
        Mockito.`when`(userActivity2.tags).thenReturn(listOf("Tag_1", "Tag_2_2"))

        Assertions.assertEquals(
            listOf("All", "Tag_1", "Tag_1_2", "Tag_2_2"),
            filtersListHandler.getTagsList(listOf(userActivity1, userActivity2))
        )
    }

    @Test
    fun filters_metric() {
        val userActivity1 = Mockito.mock(UserActivity::class.java)
        val userActivity2 = Mockito.mock(UserActivity::class.java)

        val match = Mockito.mock(Match::class.java)
        val myPlayer = Mockito.mock(MyPlayer::class.java)
        val perfParam1 = Mockito.mock(PerformanceParam::class.java)
        val perfParam2 = Mockito.mock(PerformanceParam::class.java)
        val paramsList = listOf<PerformanceParam>(perfParam1, perfParam2)

        Mockito.`when`(perfParam1.name).thenReturn("kda")
        Mockito.`when`(perfParam2.name).thenReturn("creep")

        Mockito.`when`(userActivity1.match).thenReturn(match)
        Mockito.`when`(userActivity2.match).thenReturn(match)

        Mockito.`when`(match.myPlayer).thenReturn(myPlayer)
        Mockito.`when`(myPlayer.performanceParams).thenReturn(paramsList)

        Assertions.assertEquals(
            listOf("Mental performance", "Win rate", "Game grade", "kda", "creep"),
            filtersListHandler.getMetricList(listOf(userActivity1, userActivity2))
        )
    }
}
