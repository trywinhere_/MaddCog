package com.maddcog.android.tests.data

import android.content.Context
import com.maddcog.android.R
import com.maddcog.android.baseui.ui.theme.LOSS
import com.maddcog.android.baseui.ui.theme.WIN
import com.maddcog.android.data.extensions.SCORE_5_4_SKILL
import com.maddcog.android.data.extensions.SCORE_6_TEAM
import com.maddcog.android.data.extensions.roundNumbers
import com.maddcog.android.data.extensions.toLocalName
import com.maddcog.android.data.extensions.toScoreText
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.mockito.Mockito

class ExtensionsTest {

    val context: Context = Mockito.mock(Context::class.java)

    @Test
    fun roundTest_1() {
        val testDouble = 5.87657
        Assertions.assertEquals(5.9, testDouble.roundNumbers())
    }

    @Test
    fun roundTest_2() {
        val testDouble = 6.49171
        Assertions.assertEquals(6.5, testDouble.roundNumbers())
    }

    @Test
    fun score_text_test_1() {
        val scoreText = SCORE_6_TEAM
        Assertions.assertEquals(R.string.win_6_team, scoreText.toScoreText(WIN))
    }

    @Test
    fun score_text_test_2() {
        val scoreText = SCORE_5_4_SKILL
        Assertions.assertEquals(R.string.loss_5_4_skill, scoreText.toScoreText(LOSS))
    }

    @Test
    fun local_name_test_3() {
        val localText = "Some"
        Assertions.assertEquals("Some", localText.toLocalName(context))
    }
}
