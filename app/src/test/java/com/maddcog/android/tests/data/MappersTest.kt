package com.maddcog.android.tests.data

import com.maddcog.android.baseui.ui.theme.WIN
import com.maddcog.android.data.extensions.NO_NAME
import com.maddcog.android.data.network.dto.activity.RemoteActivity
import com.maddcog.android.data.network.dto.activity.RemoteActivitySummary
import com.maddcog.android.data.network.dto.activity.RemoteMatch
import com.maddcog.android.data.network.dto.activity.RemoteMentalPerformance
import com.maddcog.android.data.network.dto.activity.RemoteMyPlayer
import com.maddcog.android.data.network.dto.activity.RemotePerformanceParam
import com.maddcog.android.data.network.dto.activity.RemoteTeamMember
import com.maddcog.android.data.network.dto.activity.RemoteTimelineEvent
import com.maddcog.android.data.network.dto.activityType.ActivityTypeResponseItem
import com.maddcog.android.data.network.mappers.toDomain
import com.maddcog.android.domain.entities.activity.TimelineEvent
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.mockito.Mockito

class MappersTest {

    private fun getRemoteActivity(): RemoteActivity? {
        val remoteActivity = Mockito.mock(RemoteActivity::class.java)
        val remoteActivitySummary = Mockito.mock(RemoteActivitySummary::class.java)
        val remoteActivityType = Mockito.mock(ActivityTypeResponseItem::class.java)
        val remoteMatch = Mockito.mock(RemoteMatch::class.java)
        val remotePlayer = Mockito.mock(RemoteMyPlayer::class.java)
        Mockito.`when`(remoteActivity.duration).thenReturn(1600000)
        Mockito.`when`(remoteActivity.activityId).thenReturn("0")
        Mockito.`when`(remoteActivity.activitySummary).thenReturn(remoteActivitySummary)
        Mockito.`when`(remoteActivity.activityType).thenReturn(remoteActivityType)
        Mockito.`when`(remoteActivity.match).thenReturn(remoteMatch)
        Mockito.`when`(remoteActivity.match.myPlayer).thenReturn(remotePlayer)

        return remoteActivity
    }

    private fun getRemoteMentalPerformance(): RemoteMentalPerformance? {
        val remoteMentalPerformance = Mockito.mock(RemoteMentalPerformance::class.java)
        Mockito.`when`(remoteMentalPerformance.fatigue).thenReturn(2.0)

        return remoteMentalPerformance
    }

    private fun getRemoteActivitySummary(): RemoteActivitySummary? {
        val remoteActivitySummary = Mockito.mock(RemoteActivitySummary::class.java)
        Mockito.`when`(remoteActivitySummary.fatigueAvg).thenReturn(3.0)
        Mockito.`when`(remoteActivitySummary.flowAvg).thenReturn(4.0)

        return remoteActivitySummary
    }

    private fun getRemoteMatch(): RemoteMatch? {
        val remoteMatch = Mockito.mock(RemoteMatch::class.java)
        val remotePlayer = Mockito.mock(RemoteMyPlayer::class.java)
        Mockito.`when`(remoteMatch.myPlayer).thenReturn(remotePlayer)
        Mockito.`when`(remoteMatch.mapName).thenReturn("Aztec")
        Mockito.`when`(remoteMatch.stayedTillGameOver).thenReturn(true)

        return remoteMatch
    }

    private fun getRemoteTimelineEvent(): RemoteTimelineEvent? {
        val remoteTimelineEvent = Mockito.mock(RemoteTimelineEvent::class.java)
        Mockito.`when`(remoteTimelineEvent.name).thenReturn("kda")

        return remoteTimelineEvent
    }

    private fun getRemotePerformanceParam(): RemotePerformanceParam? {
        val remotePerformanceParam = Mockito.mock(RemotePerformanceParam::class.java)
        Mockito.`when`(remotePerformanceParam.order).thenReturn(2)

        return remotePerformanceParam
    }

    private fun getRemoteMyPlayer(): RemoteMyPlayer? {
        val remoteMyPlayer = Mockito.mock(RemoteMyPlayer::class.java)
        Mockito.`when`(remoteMyPlayer.gamerImage).thenReturn("none.jpg")
        Mockito.`when`(remoteMyPlayer.outcome).thenReturn(WIN)

        return remoteMyPlayer
    }

    private fun getRemoteTeamMember(): RemoteTeamMember? {
        val remoteTeamMember = Mockito.mock(RemoteTeamMember::class.java)
        Mockito.`when`(remoteTeamMember.flow).thenReturn(5.0)

        return remoteTeamMember
    }

    private fun getRemoteActivityTypeResponseItem(): ActivityTypeResponseItem? {
        val remoteItem = Mockito.mock(ActivityTypeResponseItem::class.java)
        Mockito.`when`(remoteItem.maddcogOwned).thenReturn(true)

        return remoteItem
    }

    @Test
    fun remote_activity_test_1() {
        val remoteActivity = getRemoteActivity()
        Assertions.assertEquals(1600000, remoteActivity?.toDomain()?.duration)
    }

    @Test
    fun remote_activity_test_2() {
        val remoteActivity = getRemoteActivity()
        Assertions.assertEquals("0", remoteActivity?.toDomain()?.activityId)
    }

    @Test
    fun remote_mental_performance_1() {
        val remoteMentalPerformance = getRemoteMentalPerformance()

        Assertions.assertEquals(2.0, remoteMentalPerformance?.toDomain()?.fatigue)
    }

    @Test
    fun remote_mental_performance_2() {
        val remoteMentalPerformance = getRemoteMentalPerformance()

        Assertions.assertEquals(0.0, remoteMentalPerformance?.toDomain()?.flow)
    }

    @Test
    fun remote_activity_summary_1() {
        val remoteActivitySummary = getRemoteActivitySummary()

        Assertions.assertEquals(4.0, remoteActivitySummary?.toDomain()?.flowAvg)
    }

    @Test
    fun remote_activity_summary_2() {
        val remoteActivitySummary = getRemoteActivitySummary()

        Assertions.assertEquals(3.0, remoteActivitySummary?.toDomain()?.fatigueAvg)
    }

    @Test
    fun remote_activity_summary_3() {
        val remoteActivitySummary = getRemoteActivitySummary()

        Assertions.assertEquals(0.0, remoteActivitySummary?.toDomain()?.flowZoneHigh)
    }

    @Test
    fun remote_match_1() {
        val remoteMatch = getRemoteMatch()

        Assertions.assertEquals("Aztec", remoteMatch?.toDomain()?.mapName)
    }

    @Test
    fun remote_match_2() {
        val remoteMatch = getRemoteMatch()

        Assertions.assertEquals(true, remoteMatch?.toDomain()?.stayedTillGameOver)
    }

    @Test
    fun remote_match_3() {
        val remoteMatch = getRemoteMatch()

        Assertions.assertEquals(emptyList<TimelineEvent>(), remoteMatch?.toDomain()?.timelineEvents)
    }

    @Test
    fun remote_timeline_event_1() {
        val remoteTimelineEvent = getRemoteTimelineEvent()

        Assertions.assertEquals("kda", remoteTimelineEvent?.toDomain()?.name)
    }

    @Test
    fun remote_performance_param_1() {
        val remotePerformanceParam = getRemotePerformanceParam()

        Assertions.assertEquals(2, remotePerformanceParam?.toDomain()?.order)
    }

    @Test
    fun remote_my_player_1() {
        val remoteMyPlayer = getRemoteMyPlayer()

        Assertions.assertEquals("none.jpg", remoteMyPlayer?.toDomain()?.gamerImage)
    }

    @Test
    fun remote_my_player_2() {
        val remoteMyPlayer = getRemoteMyPlayer()

        Assertions.assertEquals(NO_NAME, remoteMyPlayer?.toDomain()?.characterName)
    }

    @Test
    fun remote_team_member_1() {
        val remoteTeamMember = getRemoteTeamMember()

        Assertions.assertEquals(5.0, remoteTeamMember?.toDomain()?.flow)
    }

    @Test
    fun remote_activity_item_1() {
        val remoteActivityItem = getRemoteActivityTypeResponseItem()

        Assertions.assertEquals(true, remoteActivityItem?.toDomain()?.maddcogOwned)
    }

    @Test
    fun remote_activity_item_2() {
        val remoteActivityItem = getRemoteActivityTypeResponseItem()

        Assertions.assertEquals("Some game", remoteActivityItem?.toDomain()?.name)
    }
}
