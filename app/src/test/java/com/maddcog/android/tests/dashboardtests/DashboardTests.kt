package com.maddcog.android.tests.dashboardtests


import com.maddcog.android.auth.loggingIn.LoggingInViewModel
import com.maddcog.android.dashboard.DashboardViewModel
import com.maddcog.android.dashboard.utils.ActivityUtils
import com.maddcog.android.domain.entities.ActivityItem
import com.maddcog.android.domain.entities.activity.*
import com.maddcog.android.fakes.repository.FakeDashboardRepository
import com.maddcog.android.fakes.usecases.FakeDashboardUseCaseNegative
import com.maddcog.android.fakes.usecases.FakeDashboardUseCasePositive
import com.maddcog.android.fakes.repository.FakeUserDataRepositoryNegative
import com.maddcog.android.fakes.repository.FakeUserDataRepositoryPositive
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.joda.time.DateTime
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.Mockito.mock
import org.mockito.junit.MockitoJUnitRunner

private const val DURATION = 1688
private const val DURATIONHOURS = "28 min"

@RunWith(MockitoJUnitRunner::class)
class DashboardTest {

    @OptIn(ExperimentalCoroutinesApi::class)
    val dispatcher = StandardTestDispatcher()

    private lateinit var useCasePositive: FakeDashboardUseCasePositive
    private lateinit var useCaseNegative: FakeDashboardUseCaseNegative
    private lateinit var repository: FakeDashboardRepository
    private lateinit var viewModel: DashboardViewModel
    private lateinit var utils: ActivityUtils
    private lateinit var positiveViewModel: LoggingInViewModel
    private lateinit var negativeViewModel: LoggingInViewModel
    private lateinit var positiveUserDataRepository: FakeUserDataRepositoryPositive
    private lateinit var negativeUserDataRepository: FakeUserDataRepositoryNegative

    @OptIn(ExperimentalCoroutinesApi::class)
    @BeforeEach
    fun init() {
        Dispatchers.setMain(dispatcher)
        useCasePositive = FakeDashboardUseCasePositive()
        useCaseNegative = FakeDashboardUseCaseNegative()
        positiveUserDataRepository = FakeUserDataRepositoryPositive()
        negativeUserDataRepository = FakeUserDataRepositoryNegative()
        repository = FakeDashboardRepository()
        viewModel = DashboardViewModel(mockk(), repository, mockk(), mockk(), mockk())
        utils = ActivityUtils
        positiveViewModel = LoggingInViewModel(
            positiveUserDataRepository,
            mockk(),
            repository,
            mockk(),
            mockk(),
            useCasePositive
        )
        negativeViewModel = LoggingInViewModel(
            negativeUserDataRepository,
            mockk(),
            repository,
            mockk(),
            mockk(),
            useCaseNegative
        )
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @AfterEach
    fun close() {
        Dispatchers.resetMain()
    }

    fun getUserActivity(grade: Double?): UserActivity {
        return UserActivity(
            activityId = "1",
            duration = 1111,
            mentalPerformanceTimeline = listOf(
                MentalPerformanceTimeline(0.0, 0.0, 1234),
                MentalPerformanceTimeline(0.0, 0.0, 1234)
            ),
            activityType = ActivityItem(1, "name", "game", true, true, "1", "2", "3", true),
            timestamp = DateTime.now(),
            activitySummary = ActivitySummary(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0),
            match = Match(
                1,
                2,
                "mode",
                "map",
                MyPlayer("name", "1", 0.0, "fatigue", 0.0, 0.0, "flow", "name", "123", grade ?: 0.0, "win", emptyList(), 1, 2, "score"),
                false,
                emptyList(),
                1,
                2,
                "1",
                "2",
                "3",
                emptyList()
            ),
            tags = listOf("tag1", "tag2"),
            isForTest = true
        )
    }

    @Test
    fun getDataPositive() {
        Assertions.assertEquals(true, positiveViewModel.getUserInfo())
    }

    @Test
    fun getDataNegative() {
        Assertions.assertEquals(true, negativeViewModel.getUserInfo())
    }

    @Test
    fun gradeAvgTest_1() {
        val userActivity = getUserActivity(5.0)
        val userActivityList = mutableListOf<UserActivity>()
        userActivityList.add(userActivity)
        userActivityList.add(userActivity)
        userActivityList.add(userActivity)
        userActivityList.add(userActivity)
        userActivityList.add(userActivity)
        Assertions.assertEquals("5.0", viewModel.getGradeAvg(userActivityList))
    }

    @Test
    fun gradeAvgTest_2() {
        val userActivity = getUserActivity(0.0)
        val userActivityList = mutableListOf<UserActivity>()
        userActivityList.add(userActivity)
        userActivityList.add(userActivity)
        userActivityList.add(userActivity)
        userActivityList.add(userActivity)
        userActivityList.add(userActivity)
        Assertions.assertEquals("0.0", viewModel.getGradeAvg(userActivityList))
    }

    @Test
    fun gradeAvgTest_3() {
        val userActivityList = mutableListOf<UserActivity>()
        for (i in 1..5) {
            val userActivity = getUserActivity(i.toDouble())
            userActivityList.add(userActivity)
        }
        Assertions.assertEquals("3.0", viewModel.getGradeAvg(userActivityList))
    }

    @Test
    fun gradeAvgTest_4() {
        val userActivity = getUserActivity(-10.0)
        val userActivityList = mutableListOf<UserActivity>()
        userActivityList.add(userActivity)
        userActivityList.add(userActivity)
        userActivityList.add(userActivity)
        userActivityList.add(userActivity)
        userActivityList.add(userActivity)
        Assertions.assertEquals("-10.0", viewModel.getGradeAvg(userActivityList))
    }

    @Test
    fun gradeAvgTest_5() {
        val userActivityList = mutableListOf<UserActivity>()
        //попеременные зхначения грейда отриц/положительные
        for (i in 1..5) {
            val userActivity = getUserActivity(i.toDouble())
            userActivityList.add(userActivity)
        }
        Assertions.assertEquals("3.0", viewModel.getGradeAvg(userActivityList))
    }

//    @Test
//    fun gradeAvgTest_6() {
//        val myPlayer = mock(MyPlayer::class.java)
//        Mockito.`when`(myPlayer.grade).thenReturn(null)
//        val match = mock(Match::class.java)
//        Mockito.`when`(match.myPlayer).thenReturn(myPlayer)
//        val userActivity = mock(UserActivity::class.java)
//        Mockito.`when`(userActivity.match).thenReturn(match)
//        val userActivityList = mutableListOf<UserActivity>()
//        userActivityList.add(userActivity)
//        userActivityList.add(userActivity)
//        userActivityList.add(userActivity)
//        userActivityList.add(userActivity)
//        userActivityList.add(userActivity)
//        Assertions.assertEquals("0.0", viewModel.getGradeAvg(userActivityList))
//    }

    @Test
    fun flowAvgTest_1() {
        val myPlayer = mock(MyPlayer::class.java)
        val match = mock(Match::class.java)
        Mockito.`when`(match.myPlayer).thenReturn(myPlayer)
        val activitySummary = mock(ActivitySummary::class.java)
        val userActivity = mock(UserActivity::class.java)
        Mockito.`when`(userActivity.activityId).thenReturn("demoactivity")
        Mockito.`when`(userActivity.match).thenReturn(match)
        Mockito.`when`(userActivity.activitySummary).thenReturn(activitySummary)
        Mockito.`when`(userActivity.activitySummary.flowZoneHigh).thenReturn(5.0)
        val userActivityList = mutableListOf<UserActivity>()
        userActivityList.add(userActivity)
        userActivityList.add(userActivity)
        userActivityList.add(userActivity)
        userActivityList.add(userActivity)
        userActivityList.add(userActivity)

        Assertions.assertEquals("0 %", viewModel.getFlowAvg(userActivityList))
    }

    @Test
    fun flowAvgTest_2() {
        val myPlayer = mock(MyPlayer::class.java)
        val match = mock(Match::class.java)
        Mockito.`when`(match.myPlayer).thenReturn(myPlayer)
        val activitySummary = mock(ActivitySummary::class.java)
        val userActivity = mock(UserActivity::class.java)
        Mockito.`when`(userActivity.activityId).thenReturn("demoactivity")
        Mockito.`when`(userActivity.match).thenReturn(match)
        Mockito.`when`(userActivity.activitySummary).thenReturn(activitySummary)
        Mockito.`when`(userActivity.activitySummary.flowZoneHigh).thenReturn(0.0)
        val userActivityList = mutableListOf<UserActivity>()
        userActivityList.add(userActivity)
        userActivityList.add(userActivity)
        userActivityList.add(userActivity)
        userActivityList.add(userActivity)
        userActivityList.add(userActivity)

        Assertions.assertEquals("0 %", viewModel.getFlowAvg(userActivityList))
    }

    @Test
    fun flowAvgTest_3() {
        val userActivityList = mutableListOf<UserActivity>()
        for (i in 1..5) {
            val myPlayer = mock(MyPlayer::class.java)
            val match = mock(Match::class.java)
            val activitySummary = mock(ActivitySummary::class.java)
            Mockito.`when`(match.myPlayer).thenReturn(myPlayer)
            val userActivity = mock(UserActivity::class.java)
            Mockito.`when`(userActivity.activityId).thenReturn("activity")
            Mockito.`when`(userActivity.activitySummary).thenReturn(activitySummary)
            Mockito.`when`(userActivity.activitySummary.flowZoneHigh).thenReturn(i.toDouble())
            Mockito.`when`(userActivity.match).thenReturn(match)
            userActivityList.add(userActivity)
        }
        Assertions.assertEquals("3 %", viewModel.getFlowAvg(userActivityList))
    }

    @Test
    fun flowAvgTest_4() {
        val myPlayer = mock(MyPlayer::class.java)
        val match = mock(Match::class.java)
        val activitySummary = mock(ActivitySummary::class.java)
        Mockito.`when`(match.myPlayer).thenReturn(myPlayer)
        val userActivity = mock(UserActivity::class.java)
        Mockito.`when`(userActivity.activityId).thenReturn("activity")
        Mockito.`when`(userActivity.activitySummary).thenReturn(activitySummary)
        Mockito.`when`(userActivity.match).thenReturn(match)
        Mockito.`when`(userActivity.activitySummary.flowZoneHigh).thenReturn(-50.0)
        val userActivityList = mutableListOf<UserActivity>()
        userActivityList.add(userActivity)
        userActivityList.add(userActivity)
        userActivityList.add(userActivity)
        userActivityList.add(userActivity)
        userActivityList.add(userActivity)

        Assertions.assertEquals("-50 %", viewModel.getFlowAvg(userActivityList))
    }

    @Test
    fun fatigueAvgTest_1() {
        val myPlayer = mock(MyPlayer::class.java)
        val match = mock(Match::class.java)
        val activitySummary = mock(ActivitySummary::class.java)
        Mockito.`when`(match.myPlayer).thenReturn(myPlayer)
        val userActivity = mock(UserActivity::class.java)
        Mockito.`when`(userActivity.activityId).thenReturn("activity")
        Mockito.`when`(userActivity.activitySummary).thenReturn(activitySummary)
        Mockito.`when`(userActivity.match).thenReturn(match)
        Mockito.`when`(userActivity.activitySummary.fatigueZoneHigh).thenReturn(5.0)
        val userActivityList = mutableListOf<UserActivity>()
        userActivityList.add(userActivity)
        userActivityList.add(userActivity)
        userActivityList.add(userActivity)
        userActivityList.add(userActivity)
        userActivityList.add(userActivity)
        Assertions.assertEquals("5 %", viewModel.getFatigueAvg(userActivityList))
    }

    @Test
    fun fatigueAvgTest_2() {
        val myPlayer = mock(MyPlayer::class.java)
        val match = mock(Match::class.java)
        val activitySummary = mock(ActivitySummary::class.java)
        Mockito.`when`(match.myPlayer).thenReturn(myPlayer)
        val userActivity = mock(UserActivity::class.java)
        Mockito.`when`(userActivity.activityId).thenReturn("activity")
        Mockito.`when`(userActivity.activitySummary).thenReturn(activitySummary)
        Mockito.`when`(userActivity.match).thenReturn(match)
        Mockito.`when`(userActivity.activitySummary.fatigueZoneHigh).thenReturn(0.0)
        val userActivityList = mutableListOf<UserActivity>()
        userActivityList.add(userActivity)
        userActivityList.add(userActivity)
        userActivityList.add(userActivity)
        userActivityList.add(userActivity)
        userActivityList.add(userActivity)
        Assertions.assertEquals("0 %", viewModel.getFatigueAvg(userActivityList))
    }

    @Test
    fun fatigueAvgTest_3() {
        val userActivityList = mutableListOf<UserActivity>()
        for (i in 1..5) {
            val myPlayer = mock(MyPlayer::class.java)
            val match = mock(Match::class.java)
            val activitySummary = mock(ActivitySummary::class.java)
            Mockito.`when`(match.myPlayer).thenReturn(myPlayer)
            val userActivity = mock(UserActivity::class.java)
            Mockito.`when`(userActivity.activityId).thenReturn("activity")
            Mockito.`when`(userActivity.activitySummary).thenReturn(activitySummary)
            Mockito.`when`(userActivity.activitySummary.fatigueZoneHigh).thenReturn(i.toDouble())
            Mockito.`when`(userActivity.match).thenReturn(match)
            userActivityList.add(userActivity)
        }
        Assertions.assertEquals("3 %", viewModel.getFatigueAvg(userActivityList))
    }

    @Test
    fun fatigueAvgTest_4() {
        val myPlayer = mock(MyPlayer::class.java)
        val match = mock(Match::class.java)
        val activitySummary = mock(ActivitySummary::class.java)
        Mockito.`when`(match.myPlayer).thenReturn(myPlayer)
        val userActivity = mock(UserActivity::class.java)
        Mockito.`when`(userActivity.activityId).thenReturn("activity")
        Mockito.`when`(userActivity.match).thenReturn(match)
        Mockito.`when`(userActivity.activitySummary).thenReturn(activitySummary)
        Mockito.`when`(userActivity.activitySummary.fatigueAvg).thenReturn(-60.0)
        val userActivityList = mutableListOf<UserActivity>()
        userActivityList.add(userActivity)
        userActivityList.add(userActivity)
        userActivityList.add(userActivity)
        userActivityList.add(userActivity)
        userActivityList.add(userActivity)
        Assertions.assertEquals("0 %", viewModel.getFatigueAvg(userActivityList))
    }

    @Test
    fun winNumberAvgTest_1() {
        val myPlayer = mock(MyPlayer::class.java)
        val match = mock(Match::class.java)
        val activitySummary = mock(ActivitySummary::class.java)
        Mockito.`when`(match.myPlayer).thenReturn(myPlayer)
        val userActivity = mock(UserActivity::class.java)
        Mockito.`when`(userActivity.activityId).thenReturn("activity")
        Mockito.`when`(userActivity.match).thenReturn(match)
        Mockito.`when`(userActivity.match.myPlayer.outcome).thenReturn("win")
        val userActivityList = mutableListOf<UserActivity>()
        userActivityList.add(userActivity)
        userActivityList.add(userActivity)
        userActivityList.add(userActivity)
        userActivityList.add(userActivity)
        userActivityList.add(userActivity)

        Assertions.assertEquals("100 %", viewModel.getWinNumber(userActivityList))
    }

    @Test
    fun winNumberAvgTest_2() {
        val myPlayer = mock(MyPlayer::class.java)
        val match = mock(Match::class.java)
        Mockito.`when`(match.myPlayer).thenReturn(myPlayer)
        val userActivity = mock(UserActivity::class.java)
        Mockito.`when`(userActivity.activityId).thenReturn("activity")
        Mockito.`when`(userActivity.match).thenReturn(match)
        Mockito.`when`(userActivity.match.myPlayer.outcome).thenReturn("loose")
        val userActivityList = mutableListOf<UserActivity>()
        userActivityList.add(userActivity)
        userActivityList.add(userActivity)
        userActivityList.add(userActivity)
        userActivityList.add(userActivity)
        userActivityList.add(userActivity)

        Assertions.assertEquals("0 %", viewModel.getWinNumber(userActivityList))
    }

    @Test
    fun winNumberAvgTest_3() {
        val myPlayer = mock(MyPlayer::class.java)
        val match = mock(Match::class.java)
        Mockito.`when`(match.myPlayer).thenReturn(myPlayer)
        val userActivity = mock(UserActivity::class.java)
        Mockito.`when`(userActivity.activityId).thenReturn("activity")
        Mockito.`when`(userActivity.match).thenReturn(match)
        Mockito.`when`(userActivity.match.myPlayer.outcome).thenReturn("-100")
        val userActivityList = mutableListOf<UserActivity>()
        userActivityList.add(userActivity)
        userActivityList.add(userActivity)
        userActivityList.add(userActivity)
        userActivityList.add(userActivity)
        userActivityList.add(userActivity)

        Assertions.assertEquals("0 %", viewModel.getWinNumber(userActivityList))
    }

    @Test
    fun deleteActivityTest() {
        val myPlayer = mock(MyPlayer::class.java)
        val match = mock(Match::class.java)
        val activitySummary = mock(ActivitySummary::class.java)
        Mockito.`when`(match.myPlayer).thenReturn(myPlayer)
        val userActivity_1 = mock(UserActivity::class.java)
        Mockito.`when`(userActivity_1.match).thenReturn(match)
        Mockito.`when`(userActivity_1.activitySummary).thenReturn(activitySummary)
        Mockito.`when`(userActivity_1.activitySummary.fatigueAvg).thenReturn(80.0)
        val userActivity_2 = mock(UserActivity::class.java)
        Mockito.`when`(userActivity_2.activitySummary).thenReturn(activitySummary)
        Mockito.`when`(userActivity_2.match).thenReturn(match)
        Mockito.`when`(userActivity_2.activitySummary.fatigueAvg).thenReturn(100.0)

        val activityList = mutableListOf<UserActivity>()
        activityList.add(userActivity_1)
        activityList.add(userActivity_2)
        Assertions.assertEquals(true, viewModel.deleteActivityTest(activityList, userActivity_2))
    }

    @Test
    fun DurationPresenterTest() {
        val expectedDuration = DURATIONHOURS
        val actualDuration = utils.activityDurationPresenter(DURATION)
        Assertions.assertEquals(expectedDuration, actualDuration)
    }
}