package com.maddcog.android.tests.settingsTests

import com.maddcog.android.fakes.usecases.FakeUserDataUseCaseNegative
import com.maddcog.android.fakes.usecases.FakeUserDataUseCasePositive
import com.maddcog.android.settings.SettingsViewModel
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@OptIn(ExperimentalCoroutinesApi::class)
@RunWith(MockitoJUnitRunner::class)
class SettingsTest {

    val dispatcher = TestCoroutineDispatcher()

    private lateinit var positiveUseCase: FakeUserDataUseCasePositive
    private lateinit var negativeUseCase: FakeUserDataUseCaseNegative
    private lateinit var positiveViewModel: SettingsViewModel
    private lateinit var negativeViewModel: SettingsViewModel

    @BeforeEach
    fun open() {
        Dispatchers.setMain(dispatcher)

        positiveUseCase = FakeUserDataUseCasePositive()
        negativeUseCase = FakeUserDataUseCaseNegative()

        positiveViewModel = SettingsViewModel(positiveUseCase, mockk(), mockk(), mockk())
        negativeViewModel = SettingsViewModel(negativeUseCase, mockk(), mockk(), mockk())
    }

    @AfterEach
    fun close() {
        Dispatchers.resetMain()
    }

    @Test
    fun positiveTest_1() {
        val expectedEmail = "test@email.com"
        Assertions.assertEquals(expectedEmail, positiveViewModel.getUserEmail())
    }

    @Test
    fun positiveTest_2() {
        val expectedGameNumber = 4
        Assertions.assertEquals(expectedGameNumber, positiveViewModel.getGameCount())
    }

    @Test
    fun negativeTest_1() {
        Assertions.assertEquals(null, negativeViewModel.getUserEmail())
    }

    @Test
    fun negativeTest_2() {
        Assertions.assertEquals(null, negativeViewModel.getUserEmail())
    }

    @Test
    fun negativeTest_3() {
        Assertions.assertEquals(null, negativeViewModel.getLanguageList())
    }
}
