package com.maddcog.android.tests.authtests

import com.amplifyframework.auth.AuthException
import com.amplifyframework.auth.result.AuthSignInResult
import com.amplifyframework.core.Amplify
import com.amplifyframework.core.Consumer
import com.maddcog.android.auth.signIn.SignInViewModel
import com.maddcog.android.domain.api.IAuthRepository
import com.maddcog.android.domain.entities.User
import com.maddcog.android.fakes.repository.FakeAuthRepository
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.setMain
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class SignInTest {

    @OptIn(ExperimentalCoroutinesApi::class)
    val dispatcher = StandardTestDispatcher()

    private lateinit var repository: IAuthRepository
    private lateinit var viewmodel: SignInViewModel

    @OptIn(ExperimentalCoroutinesApi::class)
    @BeforeEach
    fun onSetup() {
        Dispatchers.setMain(dispatcher)
        repository = FakeAuthRepository()
        viewmodel = SignInViewModel(repository, mockk(), mockk())
    }

    @Test
    fun loginTest() {
        val auth = mock(Amplify.Auth::class.java)
        val authSignIn: Consumer<*>? = mock(Consumer::class.java)
        val authEx: Consumer<*> = mock(Consumer::class.java)
        `when`(
            auth.signIn(
                "username", "password",
                authSignIn as Consumer<AuthSignInResult>, authEx as Consumer<AuthException>
            )
        )
        Assertions.assertEquals(true, viewmodel.signIn(User("username", "password")))
    }

}