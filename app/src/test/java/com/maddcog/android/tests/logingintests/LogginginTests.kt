package com.maddcog.android.tests.logingintests


import com.maddcog.android.auth.loggingIn.LoggingInViewModel
import com.maddcog.android.fakes.repository.FakeDashboardRepository
import com.maddcog.android.fakes.usecases.FakeDashboardUseCaseNegative
import com.maddcog.android.fakes.usecases.FakeDashboardUseCasePositive
import com.maddcog.android.fakes.repository.FakeUserDataRepositoryNegative
import com.maddcog.android.fakes.repository.FakeUserDataRepositoryPositive
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class LogginginTests {

    @OptIn(ExperimentalCoroutinesApi::class)
    val dispatcher = StandardTestDispatcher()

    private lateinit var useCasePositive: FakeDashboardUseCasePositive
    private lateinit var useCaseNegative: FakeDashboardUseCaseNegative
    private lateinit var repository: FakeDashboardRepository
    private lateinit var positiveViewModel: LoggingInViewModel
    private lateinit var negativeViewModel: LoggingInViewModel
    private lateinit var positiveUserDataRepository: FakeUserDataRepositoryPositive
    private lateinit var negativeUserDataRepository: FakeUserDataRepositoryNegative

    @OptIn(ExperimentalCoroutinesApi::class)
    @BeforeEach
    fun init() {
        Dispatchers.setMain(dispatcher)
        useCasePositive = FakeDashboardUseCasePositive()
        useCaseNegative = FakeDashboardUseCaseNegative()
        positiveUserDataRepository = FakeUserDataRepositoryPositive()
        negativeUserDataRepository = FakeUserDataRepositoryNegative()
        repository = FakeDashboardRepository()
        positiveViewModel = LoggingInViewModel(positiveUserDataRepository, mockk(), repository, mockk(), mockk(), useCasePositive)
        negativeViewModel = LoggingInViewModel(negativeUserDataRepository, mockk(), repository, mockk(), mockk(), useCaseNegative)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @AfterEach
    fun close() {
        Dispatchers.resetMain()
    }

    @Test
    fun getDataPositive() {
        Assertions.assertEquals(true, positiveViewModel.getUserInfo())
    }

    @Test
    fun getDataNegative() {
        Assertions.assertEquals(true, negativeViewModel.getUserInfo())
    }
}