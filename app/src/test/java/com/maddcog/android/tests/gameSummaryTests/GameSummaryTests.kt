package com.maddcog.android.tests.gameSummaryTests

import com.maddcog.android.domain.entities.activity.Match
import com.maddcog.android.domain.entities.activity.MyPlayer
import com.maddcog.android.domain.entities.activity.PerformanceParam
import com.maddcog.android.domain.entities.activity.TeamMember
import com.maddcog.android.domain.entities.activity.TeamMemberParam
import com.maddcog.android.domain.entities.activity.UserActivity
import com.maddcog.android.gameSummary.GameSummaryViewModel
import com.maddcog.android.gameSummary.model.PlayerScreenSummary
import com.maddcog.android.gameSummary.model.getParamsData
import com.maddcog.android.gameSummary.model.getParamsList
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.Mockito.mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class GameSummaryTests {

    @OptIn(ExperimentalCoroutinesApi::class)
    val dispatcher = StandardTestDispatcher()

    private lateinit var viewModel: GameSummaryViewModel

    @OptIn(ExperimentalCoroutinesApi::class)
    @BeforeEach
    fun init() {
        Dispatchers.setMain(dispatcher)
        viewModel = GameSummaryViewModel(mockk(), mockk())
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @AfterEach
    fun close() {
        Dispatchers.resetMain()
    }

    @Test
    fun getParamsListTest_1() {
        val performanceParam1 = mock(PerformanceParam::class.java)
        Mockito.`when`(performanceParam1.name).thenReturn("kd")
        val performanceParam2 = mock(PerformanceParam::class.java)
        Mockito.`when`(performanceParam2.name).thenReturn("kda")
        val performanceParam3 = mock(PerformanceParam::class.java)
        Mockito.`when`(performanceParam3.name).thenReturn("kd")
        val performanceParam4 = mock(PerformanceParam::class.java)
        Mockito.`when`(performanceParam4.name).thenReturn("kill_participation")
        val performanceParam5 = mock(PerformanceParam::class.java)
        Mockito.`when`(performanceParam5.name).thenReturn("score_against")
        val performanceParam6 = mock(PerformanceParam::class.java)
        Mockito.`when`(performanceParam6.name).thenReturn("score_against")
        val performanceParam7 = mock(PerformanceParam::class.java)
        Mockito.`when`(performanceParam7.name).thenReturn("score_for")

        val myPlayer = mock(MyPlayer::class.java)
        val match = mock(Match::class.java)
        val userActivity = mock(UserActivity::class.java)

        Mockito.`when`(userActivity.match).thenReturn(match)

        Mockito.`when`(match.myPlayer).thenReturn(myPlayer)

        Mockito.`when`(myPlayer.performanceParams)
            .thenReturn(
                listOf(
                    performanceParam1,
                    performanceParam2,
                    performanceParam3,
                    performanceParam4,
                    performanceParam5,
                    performanceParam6,
                    performanceParam7
                )
            )


        Assertions.assertEquals(
            listOf(
                "kd",
                "kda",
                "kill_participation",
                "score_against",
                "score_for"
            ), getParamsList(userActivity)
        )
    }

    @Test
    fun getParamsListTest_2() {
        val performanceParam1 = mock(PerformanceParam::class.java)
        Mockito.`when`(performanceParam1.name).thenReturn("kda")
        val performanceParam2 = mock(PerformanceParam::class.java)
        Mockito.`when`(performanceParam2.name).thenReturn("kda")
        val performanceParam3 = mock(PerformanceParam::class.java)
        Mockito.`when`(performanceParam3.name).thenReturn("kill_participation")
        val performanceParam4 = mock(PerformanceParam::class.java)
        Mockito.`when`(performanceParam4.name).thenReturn("kill_participation")
        val performanceParam5 = mock(PerformanceParam::class.java)
        Mockito.`when`(performanceParam5.name).thenReturn("score_for")
        val performanceParam6 = mock(PerformanceParam::class.java)
        Mockito.`when`(performanceParam6.name).thenReturn("score_against")
        val performanceParam7 = mock(PerformanceParam::class.java)
        Mockito.`when`(performanceParam7.name).thenReturn("score_for")

        val myPlayer = mock(MyPlayer::class.java)
        val match = mock(Match::class.java)
        val userActivity = mock(UserActivity::class.java)

        Mockito.`when`(userActivity.match).thenReturn(match)

        Mockito.`when`(match.myPlayer).thenReturn(myPlayer)

        Mockito.`when`(myPlayer.performanceParams)
            .thenReturn(
                listOf(
                    performanceParam1,
                    performanceParam2,
                    performanceParam3,
                    performanceParam4,
                    performanceParam5,
                    performanceParam6,
                    performanceParam7
                )
            )


        Assertions.assertEquals(
            listOf(
                "kda",
                "kill_participation",
                "score_for",
                "score_against",
            ), getParamsList(userActivity)
        )
    }

    @Test
    fun getParamsDataTest_1() {
        val paramsList = listOf("kda", "kill_participation")

        //team member 1
        val teamMember1 = mock(TeamMember::class.java)
        val teamMember1kda = mock(TeamMemberParam::class.java)
        Mockito.`when`(teamMember1kda.name).thenReturn("kda")
        Mockito.`when`(teamMember1kda.value).thenReturn(3.0)
        val teamMember1kp = mock(TeamMemberParam::class.java)
        Mockito.`when`(teamMember1kp.name).thenReturn("kill_participation")
        Mockito.`when`(teamMember1kp.value).thenReturn(35.0)

        Mockito.`when`(teamMember1.performanceParams)
            .thenReturn(listOf(teamMember1kda, teamMember1kp))
        Mockito.`when`(teamMember1.grade).thenReturn(3.0)
        Mockito.`when`(teamMember1.fatigueText).thenReturn("high")
        Mockito.`when`(teamMember1.flowText).thenReturn("low")
        Mockito.`when`(teamMember1.gamerName).thenReturn("Ben")
        Mockito.`when`(teamMember1.characterImage).thenReturn("none.jpg")

        //team member 2
        val teamMember2 = mock(TeamMember::class.java)
        val teamMember2kda = mock(TeamMemberParam::class.java)
        Mockito.`when`(teamMember2kda.name).thenReturn("kda")
        Mockito.`when`(teamMember2kda.value).thenReturn(1.5)
        val teamMember2kp = mock(TeamMemberParam::class.java)
        Mockito.`when`(teamMember2kp.name).thenReturn("kill_participation")
        Mockito.`when`(teamMember2kp.value).thenReturn(27.0)

        Mockito.`when`(teamMember2.performanceParams)
            .thenReturn(listOf(teamMember2kda, teamMember2kp))
        Mockito.`when`(teamMember2.grade).thenReturn(4.0)
        Mockito.`when`(teamMember2.fatigueText).thenReturn("low")
        Mockito.`when`(teamMember2.flowText).thenReturn("normal")
        Mockito.`when`(teamMember2.gamerName).thenReturn("OldDude")
        Mockito.`when`(teamMember2.characterImage).thenReturn("none.jpg")

        //team members list
        val memberList = listOf<TeamMember>(teamMember1, teamMember2)

        //myplayer
        val myPlayer = mock(MyPlayer::class.java)
        Mockito.`when`(myPlayer.grade).thenReturn(2.5)
        Mockito.`when`(myPlayer.fatigueText).thenReturn("low")
        Mockito.`when`(myPlayer.flowText).thenReturn("low")
        Mockito.`when`(myPlayer.characterImage).thenReturn("my.jpg")

        val myPlayer1kda = mock(PerformanceParam::class.java)
        Mockito.`when`(myPlayer1kda.name).thenReturn("kda")
        Mockito.`when`(myPlayer1kda.value).thenReturn(6.0)
        val myPlayer1kp = mock(PerformanceParam::class.java)
        Mockito.`when`(myPlayer1kp.name).thenReturn("kill_participation")
        Mockito.`when`(myPlayer1kp.value).thenReturn(50.0)

        Mockito.`when`(myPlayer.performanceParams).thenReturn(listOf(myPlayer1kda, myPlayer1kp))

        val expectedMap = mutableMapOf<String, List<PlayerScreenSummary>>()
        val expectedGradeList = listOf<PlayerScreenSummary>(
            PlayerScreenSummary(
                "OldDude",
                "4.0",
                "none.jpg"
            ),
            PlayerScreenSummary(
                "Ben",
                "3.0",
                "none.jpg"
            ),
            PlayerScreenSummary(
                "You",
                "2.5",
                "my.jpg"
            )
        )
        val expectedFlowList = listOf<PlayerScreenSummary>(
            PlayerScreenSummary(
                "OldDude",
                "MID",
                "none.jpg"
            ),
            PlayerScreenSummary(
                "Ben",
                "LOW",
                "none.jpg"
            ),
            PlayerScreenSummary(
                "You",
                "LOW",
                "my.jpg"
            ),
        )
        val expectedFatigueList = listOf<PlayerScreenSummary>(
            PlayerScreenSummary(
                "OldDude",
                "LOW",
                "none.jpg"
            ),
            PlayerScreenSummary(
                "You",
                "LOW",
                "my.jpg"
            ),
            PlayerScreenSummary(
                "Ben",
                "HI",
                "none.jpg"
            ),
        )
        val expectedKdaList = listOf<PlayerScreenSummary>(
            PlayerScreenSummary(
                "You",
                "6.0",
                "my.jpg"
            ),
            PlayerScreenSummary(
                "Ben",
                "3.0",
                "none.jpg"
            ),
            PlayerScreenSummary(
                "OldDude",
                "1.5",
                "none.jpg"
            )
        )
        val expectedKillParticipationList = listOf<PlayerScreenSummary>(
            PlayerScreenSummary(
                "You",
                "50.0",
                "my.jpg"
            ),
            PlayerScreenSummary(
                "Ben",
                "35.0",
                "none.jpg"
            ),
            PlayerScreenSummary(
                "OldDude",
                "27.0",
                "none.jpg"
            )
        )
        expectedMap.put("Grade", expectedGradeList)
        expectedMap.put("Fatigue", expectedFatigueList)
        expectedMap.put("Flow", expectedFlowList)
        expectedMap.put("kda", expectedKdaList)
        expectedMap.put("kill_participation", expectedKillParticipationList)

        Assertions.assertEquals(expectedMap, getParamsData(paramsList, memberList, myPlayer))
    }

    @Test
    fun getParamsDataTest_2() {
        val paramsList = listOf("score_for", "score_against")

        //team member 1
        val teamMember1 = mock(TeamMember::class.java)
        val teamMember1kda = mock(TeamMemberParam::class.java)
        Mockito.`when`(teamMember1kda.name).thenReturn("score_for")
        Mockito.`when`(teamMember1kda.value).thenReturn(2.0)
        val teamMember1kp = mock(TeamMemberParam::class.java)
        Mockito.`when`(teamMember1kp.name).thenReturn("score_against")
        Mockito.`when`(teamMember1kp.value).thenReturn(3.0)

        Mockito.`when`(teamMember1.performanceParams)
            .thenReturn(listOf(teamMember1kda, teamMember1kp))
        Mockito.`when`(teamMember1.grade).thenReturn(2.5)
        Mockito.`when`(teamMember1.fatigueText).thenReturn("high")
        Mockito.`when`(teamMember1.flowText).thenReturn("high")
        Mockito.`when`(teamMember1.gamerName).thenReturn("Gandalf")
        Mockito.`when`(teamMember1.characterImage).thenReturn("none.jpg")

        //team member 2
        val teamMember2 = mock(TeamMember::class.java)
        val teamMember2kda = mock(TeamMemberParam::class.java)
        Mockito.`when`(teamMember2kda.name).thenReturn("score_for")
        Mockito.`when`(teamMember2kda.value).thenReturn(1.0)
        val teamMember2kp = mock(TeamMemberParam::class.java)
        Mockito.`when`(teamMember2kp.name).thenReturn("score_against")
        Mockito.`when`(teamMember2kp.value).thenReturn(0.0)

        Mockito.`when`(teamMember2.performanceParams)
            .thenReturn(listOf(teamMember2kda, teamMember2kp))
        Mockito.`when`(teamMember2.grade).thenReturn(4.0)
        Mockito.`when`(teamMember2.fatigueText).thenReturn("low")
        Mockito.`when`(teamMember2.flowText).thenReturn("low")
        Mockito.`when`(teamMember2.gamerName).thenReturn("Aragorn")
        Mockito.`when`(teamMember2.characterImage).thenReturn("none.jpg")

        //team members list
        val memberList = listOf<TeamMember>(teamMember1, teamMember2)

        //myplayer
        val myPlayer = mock(MyPlayer::class.java)
        Mockito.`when`(myPlayer.grade).thenReturn(4.5)
        Mockito.`when`(myPlayer.fatigueText).thenReturn("normal")
        Mockito.`when`(myPlayer.flowText).thenReturn("normal")
        Mockito.`when`(myPlayer.characterImage).thenReturn("my.jpg")

        val myPlayer1kda = mock(PerformanceParam::class.java)
        Mockito.`when`(myPlayer1kda.name).thenReturn("score_for")
        Mockito.`when`(myPlayer1kda.value).thenReturn(4.0)
        val myPlayer1kp = mock(PerformanceParam::class.java)
        Mockito.`when`(myPlayer1kp.name).thenReturn("score_against")
        Mockito.`when`(myPlayer1kp.value).thenReturn(1.0)

        Mockito.`when`(myPlayer.performanceParams).thenReturn(listOf(myPlayer1kda, myPlayer1kp))

        val expectedMap = mutableMapOf<String, List<PlayerScreenSummary>>()
        val expectedGradeList = listOf<PlayerScreenSummary>(
            PlayerScreenSummary(
                "You",
                "4.5",
                "my.jpg"
            ),
            PlayerScreenSummary(
                "Aragorn",
                "4.0",
                "none.jpg"
            ),
            PlayerScreenSummary(
                "Gandalf",
                "2.5",
                "none.jpg"
            ),
        )
        val expectedFlowList = listOf<PlayerScreenSummary>(
            PlayerScreenSummary(
                "You",
                "MID",
                "my.jpg"
            ),
            PlayerScreenSummary(
                "Aragorn",
                "LOW",
                "none.jpg"
            ),
            PlayerScreenSummary(
                "Gandalf",
                "HI",
                "none.jpg"
            ),
        )
        val expectedFatigueList = listOf<PlayerScreenSummary>(
            PlayerScreenSummary(
                "You",
                "MID",
                "my.jpg"
            ),
            PlayerScreenSummary(
                "Aragorn",
                "LOW",
                "none.jpg"
            ),
            PlayerScreenSummary(
                "Gandalf",
                "HI",
                "none.jpg"
            ),
        )
        val expectedForList = listOf<PlayerScreenSummary>(
            PlayerScreenSummary(
                "You",
                "4.0",
                "my.jpg"
            ),
            PlayerScreenSummary(
                "Gandalf",
                "2.0",
                "none.jpg"
            ),
            PlayerScreenSummary(
                "Aragorn",
                "1.0",
                "none.jpg"
            ),
        )
        val expectedAgainstList = listOf<PlayerScreenSummary>(
            PlayerScreenSummary(
                "Gandalf",
                "3.0",
                "none.jpg"
            ),
            PlayerScreenSummary(
                "You",
                "1.0",
                "my.jpg"
            ),
            PlayerScreenSummary(
                "Aragorn",
                "0.0",
                "none.jpg"
            ),
        )
        expectedMap.put("Grade", expectedGradeList)
        expectedMap.put("Fatigue", expectedFatigueList)
        expectedMap.put("Flow", expectedFlowList)
        expectedMap.put("score_for", expectedForList)
        expectedMap.put("score_against", expectedAgainstList)

        Assertions.assertEquals(expectedMap, getParamsData(paramsList, memberList, myPlayer))
    }
}
