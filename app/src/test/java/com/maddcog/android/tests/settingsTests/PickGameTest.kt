package com.maddcog.android.tests.settingsTests

import com.maddcog.android.domain.entities.ActivityItem
import com.maddcog.android.fakes.usecases.FakeGetGamesListUseCaseNegative
import com.maddcog.android.fakes.usecases.FakeGetGamesListUseCasePositive
import com.maddcog.android.settings.presentation.gameIntegration.presentation.pickGame.PickGameViewModel
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.jupiter.api.*
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class PickGameTest {

    val dispatcher = TestCoroutineDispatcher()

    private lateinit var positiveUseCase: FakeGetGamesListUseCasePositive
    private lateinit var negativeUseCase: FakeGetGamesListUseCaseNegative
    private lateinit var positiveViewModel: PickGameViewModel
    private lateinit var negativeViewModel: PickGameViewModel

    @BeforeEach
    fun open() {
        Dispatchers.setMain(dispatcher)

        positiveUseCase = FakeGetGamesListUseCasePositive()
        positiveViewModel = PickGameViewModel(positiveUseCase, mockk(), mockk())

        negativeUseCase = FakeGetGamesListUseCaseNegative()
        negativeViewModel = PickGameViewModel(negativeUseCase, mockk(), mockk())
    }

    @AfterEach
    fun close() {
        Dispatchers.resetMain()
    }

    @Test
    fun positiveTest_1() {
        val searchText = "_1"
        val fullList = positiveViewModel.state.value.items
        val resultList = mutableListOf<ActivityItem>()
        resultList.add(fullList[0])
        resultList.add(fullList[2])
        resultList.add(fullList[4])
        Assertions.assertEquals(resultList, positiveViewModel.getFilteredList(searchText))
    }

    @Test
    fun positiveTest_2() {
        val searchText = "Te"
        val fullList = positiveViewModel.state.value.items
        val resultList = mutableListOf<ActivityItem>()
        resultList.add(fullList[0])
        resultList.add(fullList[1])
        resultList.add(fullList[2])
        resultList.add(fullList[3])
        resultList.add(fullList[4])
        Assertions.assertEquals(resultList, positiveViewModel.getFilteredList(searchText))
    }

    @Test
    fun positiveTest_3() {
        val searchText = "t_"
        val fullList = positiveViewModel.state.value.items
        val resultList = mutableListOf<ActivityItem>()
        resultList.add(fullList[0])
        resultList.add(fullList[1])
        resultList.add(fullList[2])
        resultList.add(fullList[3])
        resultList.add(fullList[4])
        Assertions.assertEquals(resultList, positiveViewModel.getFilteredList(searchText))
    }

    @Test
    fun negativeTest_1() {
        val searchText = "t_"
        Assertions.assertEquals(emptyList<ActivityItem>(), negativeViewModel.getFilteredList(searchText))
    }
}
