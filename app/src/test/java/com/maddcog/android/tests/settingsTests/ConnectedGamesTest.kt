package com.maddcog.android.tests.settingsTests

import com.maddcog.android.domain.entities.UserGame
import com.maddcog.android.fakes.usecases.FakeGetUserGamesUseCaseNegative
import com.maddcog.android.fakes.usecases.FakeGetUserGamesUseCasePositive
import com.maddcog.android.settings.presentation.gameIntegration.presentation.connectedGames.ConnectedGamesListViewModel
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class ConnectedGamesTest {

    val dispatcher = TestCoroutineDispatcher()

    private lateinit var positiveUseCase: FakeGetUserGamesUseCasePositive
    private lateinit var negativeUseCase: FakeGetUserGamesUseCaseNegative
    private lateinit var positiveViewModel: ConnectedGamesListViewModel
    private lateinit var negativeViewModel: ConnectedGamesListViewModel

    @BeforeEach
    fun open() {
        Dispatchers.setMain(dispatcher)

        positiveUseCase = FakeGetUserGamesUseCasePositive()
        positiveViewModel = ConnectedGamesListViewModel(positiveUseCase, mockk(), mockk(), mockk())

        negativeUseCase = FakeGetUserGamesUseCaseNegative()
        negativeViewModel = ConnectedGamesListViewModel(negativeUseCase, mockk(), mockk(), mockk())
    }

    @AfterEach
    fun close() {
        Dispatchers.resetMain()
    }

    @Test
    fun positiveTest_1() {
        val fullList = positiveViewModel.state.value.games
        val resultList = mutableListOf<UserGame>()
        resultList.addAll(fullList)

        Assertions.assertEquals("FIFA", resultList.first().gameName)
    }

    @Test
    fun positiveTest_2() {
        val fullList = positiveViewModel.state.value.games
        val resultList = mutableListOf<UserGame>()
        resultList.addAll(fullList)

        Assertions.assertEquals("Dota 2", resultList.last().gameName)
    }

    @Test
    fun negativeTest_1() {
        Assertions.assertEquals(emptyList<UserGame>(), negativeViewModel.state.value.games)
    }

    @Test
    fun negativeTest_2() {
        Assertions.assertEquals("Test Error Message", negativeViewModel.state.value.error)
    }
}
