package com.maddcog.android.tests.livegamedatatests

import com.maddcog.android.domain.entities.ActivityItem
import com.maddcog.android.domain.entities.UserGame
import com.maddcog.android.domain.entities.activity.*
import com.maddcog.android.domain.entities.user.UserAverage
import com.maddcog.android.fakes.repository.FakeLiveGameDataRepository
import com.maddcog.android.livegamedata.startnewsession.StartNewSessionViewModel
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.setMain
import org.joda.time.DateTime
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class StartNewSessionTests {

    @OptIn(ExperimentalCoroutinesApi::class)
    val dispatcher = StandardTestDispatcher()

    private lateinit var startNewSessionViewModel: StartNewSessionViewModel
    private lateinit var repository: FakeLiveGameDataRepository

    @OptIn(ExperimentalCoroutinesApi::class)
    @BeforeEach
    fun init() {
        Dispatchers.setMain(dispatcher)
        repository = FakeLiveGameDataRepository()
        startNewSessionViewModel =
            StartNewSessionViewModel(mockk(), repository, mockk(), mockk(), mockk())
    }

    @Test
    fun addActivityTest() {
        val myPlayer = Mockito.mock(MyPlayer::class.java)
        val match = Mockito.mock(Match::class.java)
        Mockito.`when`(match.myPlayer).thenReturn(myPlayer)
        val userActivity = Mockito.mock(UserActivity::class.java)
        Mockito.`when`(userActivity.match).thenReturn(match)

        Assertions.assertEquals(true, startNewSessionViewModel.addActivity(userActivity))
    }

    @Test
    fun addTagsTest() {
        val perfomanceParams = listOf(PerformanceParam("", 0.0, 0))
        val myPlayer = MyPlayer(
            "", "", 0.0, "",
            0.0, 5.0, "", "", "", 0.0, "",
            perfomanceParams, 0, 0, ""
        )
        val teamMemberParams = listOf(TeamMemberParam("", 0.0))
        val teamMember = listOf(TeamMember("", "", 0.0, "", 0.0, "", "", "", 0.0, teamMemberParams))
        val timeLineEvent = listOf(TimelineEvent("", listOf(0, 0, 0)))
        val match =
            Match(0, 0, "", "", myPlayer, false, teamMember, 0, 0, "", "", "", timeLineEvent)
        val mentalPerformanceTimeline = listOf(MentalPerformanceTimeline(0.0, 0.0, 0))
        val activityType = ActivityItem(0, "", "", false, false, "", "", "", false)
        val activitySummary = ActivitySummary(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0)
        val activity = UserActivity(
            "",
            0,
            mentalPerformanceTimeline,
            activityType,
            DateTime(1),
            activitySummary,
            match,
            listOf("", "", ""),
            true
        )
        val tags = listOf("tag_1", "tag_2", "tag_3")

        Assertions.assertEquals(
            listOf("tag_1", "tag_2", "tag_3"),
            startNewSessionViewModel.addTags(tags, activity)
        )
    }

    @Test
    fun addGameNameTest() {
        val perfomanceParams = listOf(PerformanceParam("", 0.0, 0))
        val myPlayer = MyPlayer(
            "", "", 0.0, "",
            0.0, 5.0, "", "", "", 0.0, "",
            perfomanceParams, 0, 0, ""
        )
        val teamMemberParams = listOf(TeamMemberParam("", 0.0))
        val teamMember = listOf(TeamMember("", "", 0.0, "", 0.0, "", "", "", 0.0, teamMemberParams))
        val timeLineEvent = listOf(TimelineEvent("", listOf(0, 0, 0)))
        val match =
            Match(0, 0, "", "", myPlayer, false, teamMember, 0, 0, "", "", "", timeLineEvent)
        val mentalPerformanceTimeline = listOf(MentalPerformanceTimeline(0.0, 0.0, 0))
        val activityType = ActivityItem(0, "", "", false, false, "", "", "", false)
        val activitySummary = ActivitySummary(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0)
        val activity = UserActivity(
            "",
            0,
            mentalPerformanceTimeline,
            activityType,
            DateTime(1),
            activitySummary,
            match,
            listOf("", "", ""),
            true
        )
        val gameName = "DOTA"

        Assertions.assertEquals("DOTA", startNewSessionViewModel.addGameName(gameName, activity))
    }

    @Test
    fun getImageUrlTest_1() {
        val activityItem_1 = Mockito.mock(ActivityItem::class.java)
        Mockito.`when`(activityItem_1.imageSmall).thenReturn("some_url_1")
        Mockito.`when`(activityItem_1.name).thenReturn("some_game_1")
        val activityItem_2 = Mockito.mock(ActivityItem::class.java)
        Mockito.`when`(activityItem_2.imageSmall).thenReturn("some_url_2")
        Mockito.`when`(activityItem_2.name).thenReturn("some_game_2")
        val activityItem_3 = Mockito.mock(ActivityItem::class.java)
        Mockito.`when`(activityItem_3.imageSmall).thenReturn("some_url_3")
        Mockito.`when`(activityItem_3.name).thenReturn("some_game_3")
        val list = mutableListOf<ActivityItem>()
        list.add(activityItem_1)
        list.add(activityItem_2)
        list.add(activityItem_3)
        val game = Mockito.mock(UserGame::class.java)
        Mockito.`when`(game.gameName).thenReturn("some_game_1")

        Assertions.assertEquals("some_url_1", startNewSessionViewModel.getImageUrl(list, game.gameName))
    }

    @Test
    fun getImageUrlTest_2() {
        val activityItem_1 = Mockito.mock(ActivityItem::class.java)
        Mockito.`when`(activityItem_1.imageSmall).thenReturn("some_url_1")
        Mockito.`when`(activityItem_1.name).thenReturn("some_game_1")
        val activityItem_2 = Mockito.mock(ActivityItem::class.java)
        Mockito.`when`(activityItem_2.imageSmall).thenReturn("some_url_2")
        Mockito.`when`(activityItem_2.name).thenReturn("some_game_2")
        val activityItem_3 = Mockito.mock(ActivityItem::class.java)
        Mockito.`when`(activityItem_3.imageSmall).thenReturn("some_url_3")
        Mockito.`when`(activityItem_3.name).thenReturn("some_game_3")
        val list = mutableListOf<ActivityItem>()
        list.add(activityItem_1)
        list.add(activityItem_2)
        list.add(activityItem_3)
        val game = Mockito.mock(UserGame::class.java)
        Mockito.`when`(game.gameName).thenReturn("")

        Assertions.assertEquals("", startNewSessionViewModel.getImageUrl(list, game.gameName))
    }

    @Test
    fun getImageUrlTest_3() {
        val activityItem_1 = Mockito.mock(ActivityItem::class.java)
        Mockito.`when`(activityItem_1.imageSmall).thenReturn("")
        Mockito.`when`(activityItem_1.name).thenReturn("some_game_1")
        val activityItem_2 = Mockito.mock(ActivityItem::class.java)
        Mockito.`when`(activityItem_2.imageSmall).thenReturn("some_url_2")
        Mockito.`when`(activityItem_2.name).thenReturn("some_game_2")
        val activityItem_3 = Mockito.mock(ActivityItem::class.java)
        Mockito.`when`(activityItem_3.imageSmall).thenReturn("some_url_3")
        Mockito.`when`(activityItem_3.name).thenReturn("some_game_3")
        val list = mutableListOf<ActivityItem>()
        list.add(activityItem_1)
        list.add(activityItem_2)
        list.add(activityItem_3)
        val game = Mockito.mock(UserGame::class.java)
        Mockito.`when`(game.gameName).thenReturn("some_game_1")

        Assertions.assertEquals("", startNewSessionViewModel.getImageUrl(list, game.gameName))
    }

    @Test
    fun getImageUrlTest_4() {
        val list = mutableListOf<ActivityItem>()
        val game = Mockito.mock(UserGame::class.java)
        Mockito.`when`(game.gameName).thenReturn("some_game_1")

        Assertions.assertEquals("", startNewSessionViewModel.getImageUrl(list, game.gameName))
    }
}