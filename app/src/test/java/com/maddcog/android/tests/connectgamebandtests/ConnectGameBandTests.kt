package com.maddcog.android.tests.connectgamebandtests

import android.bluetooth.BluetoothDevice
import com.maddcog.android.domain.entities.Sensors
import com.maddcog.android.domain.utils.BluetoothUtils
import com.maddcog.android.fakes.repository.FakeScanningForGamBandRepository
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.setMain
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class ConnectGameBandTests {

    @OptIn(ExperimentalCoroutinesApi::class)
    val dispatcher = StandardTestDispatcher()

    private lateinit var utils: BluetoothUtils
    private lateinit var viewModel: ScanningForGameBandViewModel
    private lateinit var repository: FakeScanningForGamBandRepository

    @OptIn(ExperimentalCoroutinesApi::class)
    @BeforeEach
    fun init() {
        Dispatchers.setMain(dispatcher)
        utils = BluetoothUtils
        repository = FakeScanningForGamBandRepository()
        viewModel = ScanningForGameBandViewModel(mockk(), repository)
    }

    val deviceSet = setOf<BluetoothDevice>()
    val ourAddress = "00:NF:52:85:444"

    @Test
    fun checkPairedDevices() {
        Assertions.assertEquals(false, utils.checkPaired(deviceSet, ourAddress))
    }

    @Test
    fun addSensorsTest() {
        val sensors = Sensors(
            "Game Band",
            "TTP PPG"
        )
        Assertions.assertEquals(true, viewModel.addSensors(sensors))
    }

}