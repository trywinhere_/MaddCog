package com.maddcog.android.tests.authtests

import com.maddcog.android.auth.utils.CheckPassword
import com.maddcog.android.auth.utils.CheckSignUpNameEmail
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.setMain
import org.junit.Assert.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

private const val VALIDPASSWORD = "Testicle1!"
private const val INVALIDPASSWORD = "test"

private const val VALIDEMAIL = "email@test.com"
private const val INVALIDEMAIL = "email@test.c"

private const val VALIDFIRSTNAME = "Test"
private const val INVALIDFIRSTNAME = "T"

private const val VALIDLASTTNAME = "Test"
private const val INVALIDLASTNAME = "T"

@RunWith(MockitoJUnitRunner::class)
class SignUpCheckPasswordTest {

    @OptIn(ExperimentalCoroutinesApi::class)
    val dispatcher = StandardTestDispatcher()

    private lateinit var checkPassword: CheckPassword
    private var checkEmail = CheckSignUpNameEmail

    @OptIn(ExperimentalCoroutinesApi::class)
    @BeforeEach
    fun init() {
        Dispatchers.setMain(dispatcher)
        checkPassword = CheckPassword
        checkEmail = CheckSignUpNameEmail
    }

    @Test
    fun checkIsUserPasswordValid() {
        val inputValidPassword = VALIDPASSWORD

        Assertions.assertEquals(false, CheckPassword.isPasswordFalse(inputValidPassword))
    }

    @Test
    fun checkIsUserPasswordTooShort() {
        val inputInvalidPassword = INVALIDPASSWORD

        Assertions.assertEquals(true, CheckPassword.isPasswordFalse(inputInvalidPassword))
    }

    @Test
    fun checkIsUserPasswordNoSigns() {
        val inputInvalidPassword = "1234dD78"

        Assertions.assertEquals(true, CheckPassword.isPasswordFalse(inputInvalidPassword))
    }

    @Test
    fun checkIsUserPasswordNoLetters() {
        val inputInvalidPassword = "12345f&!8"

        Assertions.assertEquals(true, CheckPassword.isPasswordFalse(inputInvalidPassword))
    }

    @Test
    fun checkIsUserPasswordNoLetters_2() {
        val inputInvalidPassword = "12345F&!8"

        Assertions.assertEquals(true, CheckPassword.isPasswordFalse(inputInvalidPassword))
    }

    @Test
    fun checkIsUserPasswordNoLetters_3() {
        val inputInvalidPassword = "123459&!8"

        Assertions.assertEquals(true, CheckPassword.isPasswordFalse(inputInvalidPassword))
    }

    @Test
    fun checkIsUserPasswordNoNumbers() {
        val inputInvalidPassword = "!Tehhhhhhhhh"

        Assertions.assertEquals(true, CheckPassword.isPasswordFalse(inputInvalidPassword))
    }

    @Test
    fun checkIsEmailValid() {
        val inputValidEmail = VALIDEMAIL

        Assertions.assertEquals(false, CheckSignUpNameEmail.isEmailValid(inputValidEmail))
    }

    @Test
    fun checkIsEmailTooShort_1() {
        val inputInvalidEmail = INVALIDEMAIL

        Assertions.assertEquals(true, CheckSignUpNameEmail.isEmailValid(inputInvalidEmail))
    }

    @Test
    fun checkIsEmailContainsSimbol() {
        val inputInvalidEmail = "test_test.com"

        Assertions.assertEquals(true, CheckSignUpNameEmail.isEmailValid(inputInvalidEmail))
    }

    @Test
    fun checkIsEmailTooShort_2() {
        val inputInvalidEmail = "@test.com"

        Assertions.assertEquals(true, CheckSignUpNameEmail.isEmailValid(inputInvalidEmail))
    }

    @Test
    fun checkIsEmailHasDot() {
        val inputInvalidEmail = "test@test!com"

        Assertions.assertEquals(true, CheckSignUpNameEmail.isEmailValid(inputInvalidEmail))
    }

    @Test
    fun checkIsEmailTooShort_4() {
        val inputInvalidEmail = "test@.com"

        Assertions.assertEquals(true, CheckSignUpNameEmail.isEmailValid(inputInvalidEmail))
    }

    @Test
    fun checkIsEmailTooShort_5() {
        val inputInvalidEmail = "test@test.c"

        Assertions.assertEquals(true, CheckSignUpNameEmail.isEmailValid(inputInvalidEmail))
    }

    @Test
    fun checkIsFirstNameValid() {
        val inputValidFirstName = VALIDFIRSTNAME

        Assertions.assertEquals(false, CheckSignUpNameEmail.isNameLongEnough(inputValidFirstName))
    }

    @Test
    fun checkIsFirstNemInValid() {
        val inputInvalidFirstName = INVALIDFIRSTNAME

        Assertions.assertEquals(true, CheckSignUpNameEmail.isNameLongEnough(inputInvalidFirstName))
    }

    @Test
    fun checkIsFirstNemInValid_2() {
        val inputInvalidFirstName = ""

        Assertions.assertEquals(true, CheckSignUpNameEmail.isNameLongEnough(inputInvalidFirstName))
    }

    @Test
    fun checkIsFirstNemInValid_3() {
        val inputInvalidFirstName = "TE"

        Assertions.assertEquals(false, CheckSignUpNameEmail.isNameLongEnough(inputInvalidFirstName))
    }


    @Test
    fun checkIsLastNameValid() {
        val inputValidLastName = VALIDLASTTNAME

        Assertions.assertEquals(false, CheckSignUpNameEmail.isNameLongEnough(inputValidLastName))
    }

    @Test
    fun checkIsLastNemInValid() {
        val inputInvalidLastName = INVALIDLASTNAME

        Assertions.assertEquals(true, CheckSignUpNameEmail.isNameLongEnough(inputInvalidLastName))
    }

    @Test
    fun checkIsLastNemInValid_2() {
        val inputInvalidLastName = ""

        Assertions.assertEquals(true, CheckSignUpNameEmail.isNameLongEnough(inputInvalidLastName))
    }

    @Test
    fun checkIsLastNemInValid_3() {
        val inputInvalidLastName = "TE"

        Assertions.assertEquals(false, CheckSignUpNameEmail.isNameLongEnough(inputInvalidLastName))
    }

}