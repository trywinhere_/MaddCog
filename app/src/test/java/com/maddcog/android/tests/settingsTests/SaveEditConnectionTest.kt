package com.maddcog.android.tests.settingsTests

import com.maddcog.android.settings.presentation.gameIntegration.presentation.saveEditConnection.SaveEditConnectionViewModel
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class SaveEditConnectionTest {

    val dispatcher = TestCoroutineDispatcher()

    private lateinit var viewModel: SaveEditConnectionViewModel

    @BeforeEach
    fun open() {
        Dispatchers.setMain(dispatcher)
        viewModel = SaveEditConnectionViewModel(mockk(), mockk())
        viewModel.initContent(1, "SomeName", "none.jpg",)
    }

    @AfterEach
    fun close() {
        Dispatchers.resetMain()
    }

    @Test
    fun region_test_1() {
        val newRegion = "RU1"
        viewModel.saveRegion(newRegion)
        Assertions.assertEquals(newRegion, viewModel.savedGame.region)
    }

    @Test
    fun region_test_2() {
        val newRegion = "NA1"
        viewModel.saveRegion(newRegion)
        Assertions.assertEquals(newRegion, viewModel.savedGame.region)
    }

    @Test
    fun username_test_1() {
        val newUserName = "Dude"
        viewModel.saveText(newUserName)
        Assertions.assertEquals(newUserName, viewModel.savedGame.username)
    }

    @Test
    fun gamename_test_2() {
        val gameName = "SomeName"
        Assertions.assertEquals(gameName, viewModel.savedGame.name)
    }
}
