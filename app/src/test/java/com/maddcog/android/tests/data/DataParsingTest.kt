package com.maddcog.android.tests.data

import com.maddcog.android.data.network.dto.rawActivity.RawMentalPerformance
import com.maddcog.android.data.sensordatarecorder.RawDataStorage
import com.maddcog.android.data.sensordatarecorder.SensorDataRecorder
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class DataParsingTest {

    @OptIn(ExperimentalCoroutinesApi::class)
    val dispatcher = StandardTestDispatcher()

    private val rawDataStorage = RawDataStorage()
    private val sensorDataRecorder = SensorDataRecorder(rawDataStorage)

    @OptIn(ExperimentalCoroutinesApi::class)
    @BeforeEach
    fun init() {
        Dispatchers.setMain(dispatcher)
    }

    // RawDataStorage
    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun raw_data_storage_test_1() = runBlockingTest {
        rawDataStorage.saveBatteryLevel(50)
        Assertions.assertEquals("50", rawDataStorage.getBatteryLevel())
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun raw_data_storage_test_2() = runBlockingTest {
        rawDataStorage.saveMentalPerformance(RawMentalPerformance(1.1, 2.2, 3.3, 1234))
        Assertions.assertEquals(1234, rawDataStorage.getMentalPerformanceList().last().timestamp)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun raw_data_storage_test_3() = runBlockingTest {
        rawDataStorage.saveMentalPerformance(RawMentalPerformance(1.1, 2.2, 3.3, 1234))
        Assertions.assertEquals(2.2, rawDataStorage.getMentalPerformanceList().last().flow)
    }

    //SensorDataRecorder
    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun sensor_data_recorder_test_1() = runBlockingTest {
        val testBytes = byteArrayOf(0, 0, 234.toByte(), 0, 0, 2, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0 ,0 ,0, 0, 0)
        sensorDataRecorder.parseEEGMaddcog(testBytes)
        val signal = sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.signal
        Assertions.assertEquals(10, signal)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun sensor_data_recorder_test_2() = runBlockingTest {
        val testBytes = byteArrayOf(0, 0, 234.toByte(), 0, 0, 2, 20, 0, 0, 0, 0, 0, 0, 0, 0, 0 ,0 ,0, 0, 0)
        sensorDataRecorder.parseEEGMaddcog(testBytes)
        val signal = sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.signal
        Assertions.assertEquals(20, signal)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun sensor_data_recorder_test_3() = runBlockingTest {
        val testBytes = byteArrayOf(0, 0, 235.toByte(), 0, 6, 2, 20, 0, 0, 0, 0, 0, 0, 0, 0, 0 ,0 ,0, 0, 0)
        sensorDataRecorder.parseEEGMaddcog(testBytes)
        val delta = sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.delta
        Assertions.assertNotNull(delta)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun sensor_data_recorder_test_4() = runBlockingTest {
        val testBytes = byteArrayOf(0, 0, 235.toByte(), 0, 6, 2, 20, 7, 7, 0, 0, 0, 0, 0, 0, 0 ,0 ,0, 0, 0)
        sensorDataRecorder.parseEEGMaddcog(testBytes)
        val theta = sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.theta
        Assertions.assertNotNull(theta)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun sensor_data_recorder_test_5() = runBlockingTest {
        val testBytes = byteArrayOf(0, 0, 235.toByte(), 0, 6, 2, 20, 8, 0, 0, 0, 0, 0, 0, 0, 0 ,0 ,0, 0, 0)
        sensorDataRecorder.parseEEGMaddcog(testBytes)
        val theta = sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.theta
        Assertions.assertNull(theta)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun sensor_data_recorder_test_6() = runBlockingTest {
        val testBytes = byteArrayOf(0, 0, 235.toByte(), 0, 6, 2, 20, 8, 0, 0, 0, 0, 8, 0, 0, 0 ,0 ,0, 0, 0)
        sensorDataRecorder.parseEEGMaddcog(testBytes)
        val lowAlpha = sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.lowAlpha
        Assertions.assertNotNull(lowAlpha)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun sensor_data_recorder_test_7() = runBlockingTest {
        val testBytes = byteArrayOf(0, 0, 235.toByte(), 0, 6, 2, 20, 8, 0, 0, 0, 0, 8, 0, 0, 0 ,9 ,0, 0, 0)
        sensorDataRecorder.parseEEGMaddcog(testBytes)
        val highAlpha = sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.highAlpha
        Assertions.assertNotNull(highAlpha)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun sensor_data_recorder_test_8() = runBlockingTest {
        val testBytes = byteArrayOf(0, 0, 236.toByte(), 0, 10, 2, 20, 8, 0, 0, 0, 0, 8, 0, 0, 0 ,9 ,0, 0, 0)
        sensorDataRecorder.parseEEGMaddcog(testBytes)
        val lowBeta = sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.lowBeta
        Assertions.assertNotNull(lowBeta)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun sensor_data_recorder_test_9() = runBlockingTest {
        val testBytes = byteArrayOf(0, 0, 236.toByte(), 0, 10, 2, 20, 8, 11, 0, 0, 0, 8, 0, 0, 0 ,9 ,0, 0, 0)
        sensorDataRecorder.parseEEGMaddcog(testBytes)
        val highBeta = sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.highBeta
        Assertions.assertNotNull(highBeta)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun sensor_data_recorder_test_10() = runBlockingTest {
        val testBytes = byteArrayOf(0, 0, 236.toByte(), 0, 10, 2, 20, 8, 11, 0, 0, 0, 8, 0, 0, 0 ,13 ,0, 0, 0)
        sensorDataRecorder.parseEEGMaddcog(testBytes)
        val midGamma = sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.midGamma
        Assertions.assertNotNull(midGamma)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun sensor_data_recorder_test_11() = runBlockingTest {
        sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.timestamp = 12341234
        sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.delta = 200
        sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.theta = 200
        sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.lowAlpha = 100
        sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.highAlpha = 100
        sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.lowBeta = 100
        sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.highBeta = 100
        sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.lowGamma = 200
        sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.midGamma = 200
        sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.signal = 10
        sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.let {
            sensorDataRecorder.computeMetrics(it)
        }
        Assertions.assertNotNull(sensorDataRecorder.fatigue)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun sensor_data_recorder_test_12() = runBlockingTest {
        sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.timestamp = 12341234
        sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.delta = 200
        sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.theta = 200
        sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.lowAlpha = 100
        sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.highAlpha = 100
        sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.lowBeta = 100
        sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.highBeta = 100
        sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.lowGamma = 200
        sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.midGamma = 200
        sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.signal = 10
        sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.let {
            sensorDataRecorder.computeMetrics(it)
        }
        Assertions.assertNotNull(sensorDataRecorder.flow)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun sensor_data_recorder_test_13() = runBlockingTest {
        sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.timestamp = 12341234
        sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.delta = 200
        sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.theta = 200
        sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.lowAlpha = 100
        sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.highAlpha = 100
        sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.lowBeta = 100
        sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.highBeta = 100
        sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.lowGamma = 200
        sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.midGamma = 200
        sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.signal = 10
        sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.let {
            sensorDataRecorder.computeMetrics(it)
        }
        Assertions.assertNotNull(sensorDataRecorder.focus)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun sensor_data_recorder_test_14() = runBlockingTest {
        sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.timestamp = 12341234
        sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.delta = 10
        sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.theta = 10
        sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.lowAlpha = 100
        sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.highAlpha = 100
        sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.lowBeta = 100
        sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.highBeta = 100
        sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.lowGamma = 10
        sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.midGamma = 10
        sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.signal = 10
        sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.let {
            sensorDataRecorder.computeMetrics(it)
        }
        Assertions.assertNull(sensorDataRecorder.focus)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun sensor_data_recorder_test_15() = runBlockingTest {
        sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.timestamp = 12341234
        sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.delta = 10
        sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.theta = 10
        sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.lowAlpha = 100
        sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.highAlpha = 100
        sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.lowBeta = 100
        sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.highBeta = 100
        sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.lowGamma = 10
        sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.midGamma = 10
        sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.signal = 10
        sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.let {
            sensorDataRecorder.computeMetrics(it)
        }
        Assertions.assertNull(sensorDataRecorder.flow)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun sensor_data_recorder_test_16() = runBlockingTest {
        sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.timestamp = 12341234
        sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.delta = 10
        sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.theta = 10
        sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.lowAlpha = 100
        sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.highAlpha = 100
        sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.lowBeta = 100
        sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.highBeta = 100
        sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.lowGamma = 10
        sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.midGamma = 10
        sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.signal = 10
        sensorDataRecorder.currentSampleEeg[sensorDataRecorder.peripheral]?.let {
            sensorDataRecorder.computeMetrics(it)
        }
        Assertions.assertNull(sensorDataRecorder.fatigue)
    }
}