package com.maddcog.android.tests.livegamedatatests

import com.maddcog.android.data.bluetoothle.BLEReceiveManager
import com.maddcog.android.data.bluetoothle.ConnectionState
import com.maddcog.android.data.sensordatarecorder.RawDataStorage
import com.maddcog.android.data.sensordatarecorder.SensorDataRecorder
import com.maddcog.android.domain.entities.activity.*
import com.maddcog.android.livegamedata.liveGameStats.LiveGameStatsViewModel
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.setMain
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class PreparedToStartTests {

    @OptIn(ExperimentalCoroutinesApi::class)
    val dispatcher = StandardTestDispatcher()

    private lateinit var viewModel: LiveGameStatsViewModel

    private val bleManager = Mockito.mock(BLEReceiveManager::class.java)
    private val rawDataStorage = Mockito.mock(RawDataStorage::class.java)
    private val sensorDataRecorder = Mockito.mock(SensorDataRecorder::class.java)

    @OptIn(ExperimentalCoroutinesApi::class)
    @BeforeEach
    fun init() {
        Dispatchers.setMain(dispatcher)
        viewModel = LiveGameStatsViewModel(mockk(), bleManager, rawDataStorage, mockk(), sensorDataRecorder)
    }

    @Test
    fun getDisplayedFlowTest_1() {
        val myPlayer = Mockito.mock(MyPlayer::class.java)
        val match = Mockito.mock(Match::class.java)
        Mockito.`when`(match.myPlayer).thenReturn(myPlayer)
        val activitySummary = Mockito.mock(ActivitySummary::class.java)
        val userActivity = Mockito.mock(UserActivity::class.java)
        Mockito.`when`(userActivity.match).thenReturn(match)
        Mockito.`when`(userActivity.activitySummary).thenReturn(activitySummary)
        Mockito.`when`(userActivity.activitySummary.flowAvg).thenReturn(35.0)

        Assertions.assertEquals("LO", viewModel.getDisplayedFlow(userActivity))
    }

    @Test
    fun getDisplayedFlowTest_2() {
        val myPlayer = Mockito.mock(MyPlayer::class.java)
        val match = Mockito.mock(Match::class.java)
        Mockito.`when`(match.myPlayer).thenReturn(myPlayer)
        val activitySummary = Mockito.mock(ActivitySummary::class.java)
        val userActivity = Mockito.mock(UserActivity::class.java)
        Mockito.`when`(userActivity.match).thenReturn(match)
        Mockito.`when`(userActivity.activitySummary).thenReturn(activitySummary)
        Mockito.`when`(userActivity.activitySummary.flowAvg).thenReturn(45.0)

        Assertions.assertEquals("MID", viewModel.getDisplayedFlow(userActivity))
    }

    @Test
    fun getDisplayedFlowTest_3() {
        val myPlayer = Mockito.mock(MyPlayer::class.java)
        val match = Mockito.mock(Match::class.java)
        Mockito.`when`(match.myPlayer).thenReturn(myPlayer)
        val activitySummary = Mockito.mock(ActivitySummary::class.java)
        val userActivity = Mockito.mock(UserActivity::class.java)
        Mockito.`when`(userActivity.match).thenReturn(match)
        Mockito.`when`(userActivity.activitySummary).thenReturn(activitySummary)
        Mockito.`when`(userActivity.activitySummary.flowAvg).thenReturn(80.0)

        Assertions.assertEquals("HI", viewModel.getDisplayedFlow(userActivity))
    }

    @Test
    fun getDisplayedFlowTest_4() {
        val myPlayer = Mockito.mock(MyPlayer::class.java)
        val match = Mockito.mock(Match::class.java)
        Mockito.`when`(match.myPlayer).thenReturn(myPlayer)
        val activitySummary = Mockito.mock(ActivitySummary::class.java)
        val userActivity = Mockito.mock(UserActivity::class.java)
        Mockito.`when`(userActivity.match).thenReturn(match)
        Mockito.`when`(userActivity.activitySummary).thenReturn(activitySummary)
        Mockito.`when`(userActivity.activitySummary.flowAvg).thenReturn(-80.0)

        Assertions.assertEquals("LO", viewModel.getDisplayedFlow(userActivity))
    }

    @Test
    fun getDisplayedFatigueTest_1() {
        val myPlayer = Mockito.mock(MyPlayer::class.java)
        val match = Mockito.mock(Match::class.java)
        Mockito.`when`(match.myPlayer).thenReturn(myPlayer)
        val activitySummary = Mockito.mock(ActivitySummary::class.java)
        val userActivity = Mockito.mock(UserActivity::class.java)
        Mockito.`when`(userActivity.match).thenReturn(match)
        Mockito.`when`(userActivity.activitySummary).thenReturn(activitySummary)
        Mockito.`when`(userActivity.activitySummary.fatigueAvg).thenReturn(60.0)

        Assertions.assertEquals("MID", viewModel.getDisplayedFatigue(userActivity))
    }

    @Test
    fun getDisplayedFatigueTest_2() {
        val myPlayer = Mockito.mock(MyPlayer::class.java)
        val match = Mockito.mock(Match::class.java)
        Mockito.`when`(match.myPlayer).thenReturn(myPlayer)
        val activitySummary = Mockito.mock(ActivitySummary::class.java)
        val userActivity = Mockito.mock(UserActivity::class.java)
        Mockito.`when`(userActivity.match).thenReturn(match)
        Mockito.`when`(userActivity.activitySummary).thenReturn(activitySummary)
        Mockito.`when`(userActivity.activitySummary.fatigueAvg).thenReturn(31.0)

        Assertions.assertEquals("LO", viewModel.getDisplayedFatigue(userActivity))
    }

    @Test
    fun getDisplayedFatigueTest_3() {
        val myPlayer = Mockito.mock(MyPlayer::class.java)
        val match = Mockito.mock(Match::class.java)
        Mockito.`when`(match.myPlayer).thenReturn(myPlayer)
        val activitySummary = Mockito.mock(ActivitySummary::class.java)
        val userActivity = Mockito.mock(UserActivity::class.java)
        Mockito.`when`(userActivity.match).thenReturn(match)
        Mockito.`when`(userActivity.activitySummary).thenReturn(activitySummary)
        Mockito.`when`(userActivity.activitySummary.fatigueAvg).thenReturn(-31.0)

        Assertions.assertEquals("LO", viewModel.getDisplayedFatigue(userActivity))
    }

    @Test
    fun getDisplayedFatigueTest_4() {
        val myPlayer = Mockito.mock(MyPlayer::class.java)
        val match = Mockito.mock(Match::class.java)
        Mockito.`when`(match.myPlayer).thenReturn(myPlayer)
        val activitySummary = Mockito.mock(ActivitySummary::class.java)
        val userActivity = Mockito.mock(UserActivity::class.java)
        Mockito.`when`(userActivity.match).thenReturn(match)
        Mockito.`when`(userActivity.activitySummary).thenReturn(activitySummary)
        Mockito.`when`(userActivity.activitySummary.fatigueAvg).thenReturn(150.0)

        Assertions.assertEquals("HI", viewModel.getDisplayedFatigue(userActivity))
    }
}