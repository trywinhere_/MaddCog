package com.maddcog.android.ui

import com.maddcog.android.data.network.dto.activityType.ActivityTypeResponseItem
import com.maddcog.android.data.network.dto.userProfile.RemoteUserGame
import com.maddcog.android.data.network.mappers.toDomain
import com.maddcog.android.domain.entities.UserGame
import com.maddcog.android.util.extensions.shouldEqual
import org.junit.Test

class SettingsDataClassesTest {

    @Test
    fun `UserGame gameName should return correct name`() {

        val expectedGameName = "FIFA"
        val userGame = UserGame("1", "FIFA", 1, "Messi", "NA1", emptyList())
        val actualGameName = userGame.gameName

        actualGameName shouldEqual expectedGameName
    }

    @Test
    fun `RemoteUserGame toDomain mapper should work correct with generatedIds`() {

        val remoteList = listOf<RemoteUserGame>(
            RemoteUserGame(1, "FIFA", "", "Messi", emptyList()),
            RemoteUserGame(2, "Dota 2", null, "Pugna", emptyList()),
            RemoteUserGame(3, "League of Legends", null, "SomeName", emptyList()),
            RemoteUserGame(4, "Assassins Creed", null, "Altair", emptyList())
        )

        val actualDomainList = remoteList.toDomain()

        val actualId = actualDomainList.find { it.gameName == "Assassins Creed" }?.generatedId
        val expectedId =
            remoteList.indexOf(RemoteUserGame(4, "Assassins Creed", null, "Altair", emptyList())).toString()

        actualId shouldEqual expectedId
    }

    @Test
    fun `RemoteActivityType toDomain mapper should add correct gameName in case it is null`() {

        val remoteItem = ActivityTypeResponseItem(
            1,
            null,
            "image.com",
            true,
            true,
            null,
            null,
            null,
            false
        )

        val domainItem = remoteItem.toDomain()

        val expectedItemName = "Some game"
        val actualItemName = domainItem.name

        actualItemName shouldEqual expectedItemName
    }
}
