package com.maddcog.android.ui.auth

import com.maddcog.android.domain.entities.User
import org.junit.Test
import org.junit.Assert.*

private const val EMAIL = "test@test.com"
private const val PASSWORD = "Test@34test"

class SignInDataClassTest {

    @Test
    fun checkUserEmail() {
        val expectedUserEmail = EMAIL
        val user = User("test@test.com", PASSWORD)
        val actualUserEmail = user.email

        assertEquals(expectedUserEmail, actualUserEmail)
    }

    @Test
    fun checkUserPassword() {
        val expectedUserPassword = PASSWORD
        val user = User(EMAIL, "Test@34test")
        val actualUserPassword = user.password

        assertEquals(expectedUserPassword, actualUserPassword)
    }


}