package com.maddcog.android.ui.dashboard

import com.maddcog.android.dashboard.utils.ActivityUtils
import org.junit.Assert.*
import org.junit.Test

private const val DATEINSEC = 1672685606L
private const val DATE = "02.01.2023"
private const val DURATION = 1688
private const val DURATIONHOURS = "28 min"

class DashboardUserActivityPresenterTest {

    @Test
    fun checkTimePresenter() {
        val expectedDate = DATE
        val actualDate = ActivityUtils.timePresenter(DATEINSEC)

        assertEquals(expectedDate, actualDate)
    }

    @Test
    fun checkDurationPresenter() {
        val expectedDuration = DURATIONHOURS
        val actualDuration = ActivityUtils.activityDurationPresenter(DURATION)

        assertEquals(expectedDuration, actualDuration)
    }


}