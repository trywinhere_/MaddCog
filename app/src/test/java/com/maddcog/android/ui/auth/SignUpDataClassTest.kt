package com.maddcog.android.ui.auth

import com.maddcog.android.domain.entities.UserSignUp
import org.junit.Test
import org.junit.Assert.*

private const val EMAIL = "test@test.com"
private const val PASSWORD = "Test@34test"
private const val FIRSTNAME = "Test"
private const val LASTNAME = "Test"

class SignUpDataClassTest {

    private val name = "$FIRSTNAME $LASTNAME"

    @Test
    fun checkUserSignUpEmail() {
        val expectedUserEmail = EMAIL
        val user = UserSignUp(email = "test@test.com", password =  PASSWORD, name = name)
        val actualUserEmail = user.email

        assertEquals(expectedUserEmail, actualUserEmail)
    }

    @Test
    fun checkUserSignUpPassword() {
        val expectedUserPassword = PASSWORD
        val user = UserSignUp(email = EMAIL, password = "Test@34test", name = name)
        val actualUserPassword = user.password

        assertEquals(expectedUserPassword, actualUserPassword)
    }

    @Test
    fun checkUserSignUpFullName() {
        val expectedUserName = name
        val user = UserSignUp(email = EMAIL, password = PASSWORD, name = "Test" + " " + "Test")
        val actualUserName = user.name

        assertEquals(expectedUserName, actualUserName)
    }
}