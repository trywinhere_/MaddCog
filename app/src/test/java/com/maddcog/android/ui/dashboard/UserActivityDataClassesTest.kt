package com.maddcog.android.ui.dashboard

import com.maddcog.android.domain.entities.activity.ActivitySummary
import com.maddcog.android.domain.entities.activity.Match
import com.maddcog.android.domain.entities.activity.MyPlayer
import com.maddcog.android.domain.entities.activity.PerformanceParam
import com.maddcog.android.domain.entities.activity.TeamMember
import com.maddcog.android.domain.entities.activity.TeamMemberParam
import org.junit.Test
import org.junit.Assert.*

private const val fatigue_zone_high = 0.0
private const val fatigue_zone_normal = 14.09068746952706
private const val flow_zone_normal = 35.153583617747444
private const val fatigue_zone_low = 85.90931253047293
private const val flow_zone_high = 32.13066796684544
private const val fatigue_end_avg = 27.61631092268041
private const val flow_avg = 49.90843189105808
private const val flow_zone_low = 32.71574841540712
private const val fatigue_avg = 32.132721121404195

private const val outcome = "win"
private const val gamer_name = "PunkHeadOldDude"
private const val gamer_image =
    "https://media.valorant-api.com/agents/9f0d8ba9-4140-b941-57d3-a7ad57c6b417/displayicon.png"
private const val character_name = "Brimstone"
private const val character_image =
    "https://media.valorant-api.com/agents/9f0d8ba9-4140-b941-57d3-a7ad57c6b417/displayicon.png"
private const val grade = 5.3
private const val score = 4
private const val score_text = "5_4_equal"
private const val flow = 49.90843189105808
private const val flow_text = "normal"
private const val fatigue = 32.132721121404195
private const val fatigue_text = "low"
private const val fatigue_end = 27.61631092268041
private const val name = "kd"
private const val valuePerfomanceParams = 1.29
private const val order = 2

private const val startMatch = 162252
private const val endMatch = 555689
private const val tile_image =
    "https://media.valorant-api.com/agents/9f0d8ba9-4140-b941-57d3-a7ad57c6b417/displayicon.png"

private const val nameTeam = "Team Name"
private const val puuid = "puuid"
private const val image =
    "https://media.valorant-api.com/agents/9f0d8ba9-4140-b941-57d3-a7ad57c6b417/displayicon.png"
private const val champion_name = "Champion name"
private const val champion_image =
    "https://media.valorant-api.com/agents/9f0d8ba9-4140-b941-57d3-a7ad57c6b417/displayicon.png"
private const val flow_avg_match = 1.454
private const val fatigue_avg_match = 4.45787
private const val fatigue_avg_end_match = 4.45787
private const val grade_match = 5.0
private const val kda_match = 7.8888
private const val kill_participation_match = 0.0
private const val creep_score_min = 4.0
private const val position = 0
private const val kda = "kda"
private const val kda_param = 0.0
private const val kd = "kd"
private const val kd_param = 0.0
private const val teamCTScore = 0
private const val game_mode = "mode"
private const val map_name = "map"
private const val teamCTscore = 0
private const val teamTscore = 0
private const val stayedTillGameOver = true
private const val tileMetricDescription = "123"
private const val tileMetricText = "321"


class UserActivityDataClassesTest {

    val expectedMyPlayer = MyPlayer(
        outcome = outcome,
        gamerName = gamer_name,
        gamerImage = gamer_image,
        characterName = character_name,
        characterImage = character_image,
        grade = grade,
        score = score,
        scoreText = score_text,
        flow = flow,
        flowText = flow_text,
        fatigue = fatigue,
        fatigueText = fatigue_text,
        fatigueEnd = fatigue_end,
        performanceParams = arrayListOf(
            PerformanceParam(
                name = name,
                value = valuePerfomanceParams,
                order = order
            )
        ),
        position = position,
    )

    @Test
    fun checkFatigueAvg() {
        val expectedFatigueAvg = fatigue_avg
        val activitySummary = ActivitySummary(
            fatigueZoneHigh = fatigue_zone_high,
            fatigueZoneNormal = fatigue_zone_normal,
            flowZoneNormal = flow_zone_normal,
            fatigueZoneLow = fatigue_zone_low,
            flowZoneHigh = flow_zone_high,
            fatigueEndAvg = fatigue_end_avg,
            flowAvg = flow_avg,
            flowZoneLow = flow_zone_low,
            fatigueAvg = 32.132721121404195
        )

        val actualFatigueAvg = activitySummary.fatigueAvg

        assertEquals(actualFatigueAvg, expectedFatigueAvg)
    }

    @Test
    fun checkActivitySummary() {
        val expectedActivitySummary = ActivitySummary(
            fatigueZoneHigh = fatigue_zone_high,
            fatigueZoneNormal = fatigue_zone_normal,
            flowZoneNormal = flow_zone_normal,
            fatigueZoneLow = fatigue_zone_low,
            flowZoneHigh = flow_zone_high,
            fatigueEndAvg = fatigue_end_avg,
            flowAvg = flow_avg,
            flowZoneLow = flow_zone_low,
            fatigueAvg = fatigue_avg
        )

        val actualActivitySummary = ActivitySummary(
            fatigueZoneHigh = 0.0,
            fatigueZoneNormal = 14.09068746952706,
            flowZoneNormal = 35.153583617747444,
            fatigueZoneLow = 85.90931253047293,
            flowZoneHigh = 32.13066796684544,
            fatigueEndAvg = 27.61631092268041,
            flowAvg = 49.90843189105808,
            flowZoneLow = 32.71574841540712,
            fatigueAvg = 32.132721121404195
        )

        assertEquals(expectedActivitySummary, actualActivitySummary)
    }

    @Test
    fun checkMyPlayer() {
        val expectedMyPlayer = expectedMyPlayer

        val actualMyPlayer = MyPlayer(
            outcome = "win",
            gamerName = "PunkHeadOldDude",
            gamerImage = "https://media.valorant-api.com/agents/9f0d8ba9-4140-b941-57d3-a7ad57c6b417/displayicon.png",
            characterName = "Brimstone",
            characterImage = "https://media.valorant-api.com/agents/9f0d8ba9-4140-b941-57d3-a7ad57c6b417/displayicon.png",
            grade = 5.3,
            score = 4,
            scoreText = "5_4_equal",
            flow = 49.90843189105808,
            flowText = "normal",
            fatigue = 32.132721121404195,
            fatigueText = "low",
            fatigueEnd = 27.61631092268041,
            performanceParams = arrayListOf(
                PerformanceParam(
                    name = "kd",
                    value = 1.29,
                    order = 2
                )
            ),
            position = 0,
        )

        assertEquals(actualMyPlayer, expectedMyPlayer)
    }

    @Test
    fun checkMatch() {
        val expectedMyPlayer = expectedMyPlayer

        val expectedMatch = Match(
            start = startMatch,
            end = endMatch,
            tileImage = tile_image,
            myPlayer = expectedMyPlayer,
            team = arrayListOf(
                TeamMember(
                    gamerName = nameTeam,
                    gamerImage = image,
                    characterImage = champion_image,
                    characterName = champion_name,
                    flow = flow_avg_match,
                    flowText = puuid,
                    fatigue = fatigue_avg_match,
                    fatigueText = puuid,
                    grade = grade_match,
                    performanceParams = listOf(
                        TeamMemberParam(kd, kd_param),
                        TeamMemberParam(kda, kda_param)
                    )
                )
            ),
            gameMode = game_mode,
            mapName = map_name,
            teamTScore = teamTscore,
            teamCtScore = teamCTscore,
            stayedTillGameOver = stayedTillGameOver,
            tileMetricDescription = tileMetricDescription,
            tileMetricText = tileMetricText,
            timelineEvents = emptyList(),
        )

        val actualMyPlayer = MyPlayer(
            outcome = "win",
            gamerName = "PunkHeadOldDude",
            gamerImage = "https://media.valorant-api.com/agents/9f0d8ba9-4140-b941-57d3-a7ad57c6b417/displayicon.png",
            characterName = "Brimstone",
            characterImage = "https://media.valorant-api.com/agents/9f0d8ba9-4140-b941-57d3-a7ad57c6b417/displayicon.png",
            grade = 5.3,
            score = 4,
            scoreText = "5_4_equal",
            flow = 49.90843189105808,
            flowText = "normal",
            fatigue = 32.132721121404195,
            fatigueText = "low",
            fatigueEnd = 27.61631092268041,
            performanceParams = arrayListOf(
                PerformanceParam(
                    name = "kd",
                    value = 1.29,
                    order = 2,
                )
            ),
            position = 0,
        )

        val actualMatch = Match(
            start = 162252,
            end = 555689,
            tileImage = "https://media.valorant-api.com/agents/9f0d8ba9-4140-b941-57d3-a7ad57c6b417/displayicon.png",
            myPlayer = actualMyPlayer,
            team = arrayListOf(
                TeamMember(
                    gamerName = "Team Name",
                    fatigueText = "puuid",
                    gamerImage = "https://media.valorant-api.com/agents/9f0d8ba9-4140-b941-57d3-a7ad57c6b417/displayicon.png",
                    characterImage = "https://media.valorant-api.com/agents/9f0d8ba9-4140-b941-57d3-a7ad57c6b417/displayicon.png",
                    characterName = "Champion name",
                    flow = 1.454,
                    flowText = "puuid",
                    fatigue = 4.45787,
                    grade = 5.0,
                    performanceParams = listOf(
                        TeamMemberParam("kd", 0.0),
                        TeamMemberParam("kda", 0.0)
                    )
                )
            ),
            gameMode = "mode",
            mapName = "map",
            stayedTillGameOver = true,
            teamCtScore = 0,
            teamTScore = 0,
            tileMetricDescription = "123",
            tileMetricText = "321",
            timelineEvents = emptyList(),
        )

        assertEquals(actualMatch, expectedMatch)

    }
}
