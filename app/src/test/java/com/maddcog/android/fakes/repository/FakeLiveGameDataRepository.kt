package com.maddcog.android.fakes.repository

import com.maddcog.android.domain.repository.LiveGameDataRepositoryImpl
import io.mockk.mockk
import javax.inject.Inject

class FakeLiveGameDataRepository @Inject constructor() : LiveGameDataRepositoryImpl(mockk(), mockk(), mockk(), mockk())