package com.maddcog.android.fakes.usecases

import com.maddcog.android.baseui.model.Resource
import com.maddcog.android.domain.entities.user.UserProfile
import com.maddcog.android.domain.entities.user.UserProfileData
import com.maddcog.android.domain.usecase.UserDataUseCase
import io.mockk.mockk
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class FakeRepoUserDataUseCaseNegative @Inject constructor() : UserDataUseCase(mockk()) {
    override operator fun invoke(): Flow<Resource<UserProfileData?>> = flow {
        val userData = UserProfileData(
            profile = mockk<UserProfile>(),
            languageList = mockk()
        )
        emit(Resource.Error("Test Error Message"))
    }
}