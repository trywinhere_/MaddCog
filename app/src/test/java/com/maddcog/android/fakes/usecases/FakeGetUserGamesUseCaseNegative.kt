package com.maddcog.android.fakes.usecases

import com.maddcog.android.baseui.model.Resource
import com.maddcog.android.domain.entities.UserGame
import com.maddcog.android.domain.usecase.GetUserGamesUseCase
import io.mockk.mockk
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class FakeGetUserGamesUseCaseNegative @Inject constructor() : GetUserGamesUseCase(mockk(), mockk(), mockk()) {
    override operator fun invoke(): Flow<Resource<List<UserGame>?>> =
        flow {
            emit(Resource.Error(message = "Test Error Message"))
        }
}
