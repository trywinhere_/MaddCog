package com.maddcog.android.fakes.usecases

import com.maddcog.android.domain.entities.ActivityItem
import com.maddcog.android.domain.entities.activity.UserActivity
import com.maddcog.android.domain.entities.team.Team
import com.maddcog.android.domain.usecase.DashboardUseCase
import org.mockito.kotlin.mock
import javax.inject.Inject

class FakeDashboardUseCaseNegative @Inject constructor() : DashboardUseCase(mock(), mock(), mock()) {

    override suspend fun getActivities(): List<UserActivity> {
        throw Exception("Test user activity error message")
    }

    override suspend fun getActivitiesTypes(): List<ActivityItem> {
        throw Exception("Test activity types error message")
    }

    override suspend fun getTeams(): List<Team> {
        throw Exception("Test teams error message")
    }

}
