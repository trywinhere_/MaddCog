package com.maddcog.android.fakes.repository

import com.maddcog.android.domain.repository.AuthRepositoryImpl
import io.mockk.mockk
import javax.inject.Inject

class FakeAuthRepository @Inject constructor() : AuthRepositoryImpl(mockk(), mockk()) {

}