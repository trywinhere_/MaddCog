package com.maddcog.android.fakes.usecases

import com.maddcog.android.baseui.model.Resource
import com.maddcog.android.domain.entities.UserGame
import com.maddcog.android.domain.entities.user.UserProfile
import com.maddcog.android.domain.entities.user.UserProfileData
import com.maddcog.android.domain.usecase.UserDataUseCase
import io.mockk.mockk
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import org.mockito.Mockito
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.mock
import javax.inject.Inject

class FakeUserDataUseCasePositive @Inject constructor() : UserDataUseCase(mockk()) {
    override operator fun invoke(): Flow<Resource<UserProfileData?>> = flow {

        val userData = UserProfileData(
            profile = getProfile(),
            languageList = mockk()
        )
        emit(Resource.Success(userData))
    }

    private fun getProfile(): UserProfile {
        val profile = mock<UserProfile>()

        val gameList = mutableListOf<UserGame>()
        val useGame_1 = mock<UserGame>()
        val useGame_2 = mock<UserGame>()
        val useGame_3 = mock<UserGame>()
        val useGame_4 = mock<UserGame>()
        gameList.add(useGame_1)
        gameList.add(useGame_2)
        gameList.add(useGame_3)
        gameList.add(useGame_4)

        Mockito.`when`(profile.userGames).thenReturn(gameList)
        Mockito.`when`(profile.email).thenReturn("test@email.com")
        return profile
    }
}
