package com.maddcog.android.fakes.usecases

import com.maddcog.android.baseui.model.Resource
import com.maddcog.android.domain.entities.ActivityItem
import com.maddcog.android.domain.entities.activity.ActivitySummary
import com.maddcog.android.domain.entities.activity.Match
import com.maddcog.android.domain.entities.activity.MyPlayer
import com.maddcog.android.domain.entities.activity.PerformanceParam
import com.maddcog.android.domain.entities.activity.UserActivity
import com.maddcog.android.domain.entities.trends.TrendsDetailsData
import com.maddcog.android.domain.usecase.GetTrendsDetailsUseCase
import com.maddcog.android.trends.utils.FiltersListHandler
import io.mockk.mockk
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import org.mockito.Mockito
import org.mockito.Mockito.mock
import javax.inject.Inject

class FakeGetTrendsDetailsUseCasePositive @Inject constructor(
    private val filtersListHandler: FiltersListHandler,
) :
    GetTrendsDetailsUseCase(
        mockk(),
        mockk(),
        filtersListHandler
    ) {

    fun getUserActivityList(
        gameName: String,
        characterName: String,
        gamerName: String,
        paramsList: List<PerformanceParam>,
        grade: Double,
    ): UserActivity? {
        val userActivity1 = mock(UserActivity::class.java)
        val activityItem = mock(ActivityItem::class.java)
        val activitySummary = mock(ActivitySummary::class.java)
        val match = mock(Match::class.java)
        val myPlayer = mock(MyPlayer::class.java)

        Mockito.`when`(myPlayer.characterName).thenReturn(characterName)
        Mockito.`when`(myPlayer.gamerName).thenReturn(gamerName)
        Mockito.`when`(myPlayer.characterName).thenReturn(characterName)
        Mockito.`when`(myPlayer.gamerName).thenReturn(gamerName)
        Mockito.`when`(myPlayer.performanceParams).thenReturn(paramsList)
        Mockito.`when`(myPlayer.grade).thenReturn(grade)

        Mockito.`when`(match.myPlayer).thenReturn(myPlayer)
        Mockito.`when`(userActivity1.match).thenReturn(match)

        Mockito.`when`(activitySummary.flowZoneHigh).thenReturn(2.0)
        Mockito.`when`(activitySummary.fatigueZoneHigh).thenReturn(3.0)
        Mockito.`when`(userActivity1.activitySummary).thenReturn(activitySummary)

        Mockito.`when`(activityItem.name).thenReturn(gameName)
        Mockito.`when`(userActivity1.activityType).thenReturn(activityItem)

        Mockito.`when`(userActivity1.tags).thenReturn(listOf("Tag_1", "Tag_2", "Tag_3"))

        return userActivity1
    }

    fun getPerformanceParamsList1(): List<PerformanceParam> {
        val performanceParam1 = mock(PerformanceParam::class.java)
        Mockito.`when`(performanceParam1.name).thenReturn("KDA")
        Mockito.`when`(performanceParam1.value).thenReturn(5.0)

        val performanceParam2 = mock(PerformanceParam::class.java)
        Mockito.`when`(performanceParam2.name).thenReturn("Score_against")
        Mockito.`when`(performanceParam2.value).thenReturn(2.0)

        val performanceParam3 = mock(PerformanceParam::class.java)
        Mockito.`when`(performanceParam3.name).thenReturn("Score_for")
        Mockito.`when`(performanceParam3.value).thenReturn(1.0)

        return listOf<PerformanceParam>(performanceParam1, performanceParam2, performanceParam3)
    }

    fun getPerformanceParamsList2(): List<PerformanceParam> {
        val performanceParam1 = mock(PerformanceParam::class.java)
        Mockito.`when`(performanceParam1.name).thenReturn("KDA")
        Mockito.`when`(performanceParam1.value).thenReturn(5.0)

        val performanceParam2 = mock(PerformanceParam::class.java)
        Mockito.`when`(performanceParam2.name).thenReturn("Creep_score")
        Mockito.`when`(performanceParam2.value).thenReturn(37.0)

        val performanceParam3 = mock(PerformanceParam::class.java)
        Mockito.`when`(performanceParam3.name).thenReturn("Kill_participation")
        Mockito.`when`(performanceParam3.value).thenReturn(50.1)

        return listOf<PerformanceParam>(performanceParam1, performanceParam2, performanceParam3)
    }

    fun getPerformanceParamsList3(): List<PerformanceParam> {
        val performanceParam1 = mock(PerformanceParam::class.java)
        Mockito.`when`(performanceParam1.name).thenReturn("Score_for")
        Mockito.`when`(performanceParam1.value).thenReturn(4.0)

        val performanceParam2 = mock(PerformanceParam::class.java)
        Mockito.`when`(performanceParam2.name).thenReturn("Creep_score")
        Mockito.`when`(performanceParam2.value).thenReturn(21.0)

        val performanceParam3 = mock(PerformanceParam::class.java)
        Mockito.`when`(performanceParam3.name).thenReturn("Kill_participation")
        Mockito.`when`(performanceParam3.value).thenReturn(10.5)

        return listOf<PerformanceParam>(performanceParam1, performanceParam2, performanceParam3)
    }

    override operator fun invoke(): Flow<Resource<TrendsDetailsData?>> = flow {
        val userActivityList = mutableListOf<UserActivity>()
        val activity1 = getUserActivityList("FIFA", "Messi", "Ben", getPerformanceParamsList1(), 5.0)
        activity1?.let { userActivityList.add(it) }
        val activity2 = getUserActivityList("CS-GO", "Cops", "Ben", getPerformanceParamsList2(), 6.0)
        activity2?.let { userActivityList.add(it) }
        val activity3 = getUserActivityList("LoL", "Pugna", "OldDude", getPerformanceParamsList3(), 1.0)
        activity3?.let { userActivityList.add(it) }

        val trendsDetailsData = mock(TrendsDetailsData::class.java)
        Mockito.`when`(trendsDetailsData.initialData).thenReturn(userActivityList)
        Mockito.`when`(trendsDetailsData.championsList).thenReturn(mockk())
        Mockito.`when`(trendsDetailsData.durationList).thenReturn(mockk())
        Mockito.`when`(trendsDetailsData.tagList).thenReturn(mockk())
        Mockito.`when`(trendsDetailsData.gameList).thenReturn(mockk())
        Mockito.`when`(trendsDetailsData.metricList).thenReturn(mockk())
        Mockito.`when`(trendsDetailsData.userNameList).thenReturn(mockk())
        Mockito.`when`(trendsDetailsData.userNameFilter).thenReturn("All")
        Mockito.`when`(trendsDetailsData.durationFilter).thenReturn("All")
        Mockito.`when`(trendsDetailsData.metricFilter).thenReturn("All")
        Mockito.`when`(trendsDetailsData.gameFilter).thenReturn("All")
        Mockito.`when`(trendsDetailsData.tagFilter).thenReturn("All")
        Mockito.`when`(trendsDetailsData.championFilter).thenReturn("All")
        Mockito.`when`(trendsDetailsData.trendsInsight).thenReturn("123")

        emit(Resource.Success(trendsDetailsData))
    }
}
