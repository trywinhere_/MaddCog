package com.maddcog.android.fakes.usecases

import com.maddcog.android.baseui.model.Resource
import com.maddcog.android.domain.entities.ActivityItem
import com.maddcog.android.domain.usecase.GetGamesListUseCase
import io.mockk.mockk
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import org.mockito.Mockito
import org.mockito.kotlin.mock
import javax.inject.Inject

class FakeGetGamesListUseCasePositive @Inject constructor() : GetGamesListUseCase(mockk(), mockk()) {
    override operator fun invoke(): Flow<Resource<List<ActivityItem>>> =
        flow {
            val activityItem_1 = mock<ActivityItem>()
            Mockito.`when`(activityItem_1.name).thenReturn("Test_1")
            val activityItem_2 = mock<ActivityItem>()
            Mockito.`when`(activityItem_2.name).thenReturn("Test_2")
            val activityItem_3 = mock<ActivityItem>()
            Mockito.`when`(activityItem_3.name).thenReturn("Text_1")
            val activityItem_4 = mock<ActivityItem>()
            Mockito.`when`(activityItem_4.name).thenReturn("Text_2")
            val activityItem_5 = mock<ActivityItem>()
            Mockito.`when`(activityItem_5.name).thenReturn("tet_1")
            val fullList = mutableListOf<ActivityItem>()
            fullList.add(activityItem_1)
            fullList.add(activityItem_2)
            fullList.add(activityItem_3)
            fullList.add(activityItem_4)
            fullList.add(activityItem_5)
            emit(Resource.Success(fullList))
        }
}
