package com.maddcog.android.fakes.api

import com.maddcog.android.data.network.services.ActivityListApiServiceImpl
import com.maddcog.android.domain.entities.ActivityItem
import com.maddcog.android.domain.entities.activity.UserActivity
import io.mockk.mockk
import javax.inject.Inject

class FakeActivityListApiServiceImpl @Inject constructor() : ActivityListApiServiceImpl(mockk()) {
    override suspend fun getUserActivities(): List<UserActivity> {
        return super.getUserActivities()
    }

    override suspend fun loadAllActivitiesList(): List<ActivityItem> {
        return super.loadAllActivitiesList()
    }

    override suspend fun getUserActivityById(userActivityId: String): UserActivity {
        return super.getUserActivityById(userActivityId)
    }
}
