package com.maddcog.android.fakes.usecases

import com.maddcog.android.baseui.model.Resource
import com.maddcog.android.domain.entities.UserGame
import com.maddcog.android.domain.usecase.GetUserGamesUseCase
import io.mockk.mockk
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import org.mockito.Mockito
import org.mockito.kotlin.mock
import javax.inject.Inject

class FakeGetUserGamesUseCasePositive @Inject constructor() : GetUserGamesUseCase(mockk(), mockk(), mockk()) {
    override operator fun invoke(): Flow<Resource<List<UserGame>?>> =
        flow {
            val userGame_1 = mock<UserGame>()
            Mockito.`when`(userGame_1.gameName).thenReturn("FIFA")
            val userGame_2 = mock<UserGame>()
            Mockito.`when`(userGame_2.gameName).thenReturn("CS_GO")
            val userGame_3 = mock<UserGame>()
            Mockito.`when`(userGame_3.gameName).thenReturn("Dota 2")

            val fullList = mutableListOf<UserGame>()
            fullList.add(userGame_1)
            fullList.add(userGame_2)
            fullList.add(userGame_3)
            emit(Resource.Success(fullList))
        }
}
