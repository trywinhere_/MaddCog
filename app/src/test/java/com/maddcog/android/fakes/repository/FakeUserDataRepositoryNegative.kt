package com.maddcog.android.fakes.repository

import com.maddcog.android.domain.entities.user.UserProfile
import com.maddcog.android.domain.repository.UserDataRepositoryImpl
import io.mockk.mockk
import javax.inject.Inject

class FakeUserDataRepositoryNegative @Inject constructor() : UserDataRepositoryImpl(mockk(), mockk()) {

    override suspend fun getProfile(): UserProfile? {
        throw Exception("Test error message")
    }

    override suspend fun loadAndSaveProfile() {
        throw Exception("Test error message")
    }
}