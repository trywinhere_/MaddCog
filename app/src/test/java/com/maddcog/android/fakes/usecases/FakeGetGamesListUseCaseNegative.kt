package com.maddcog.android.fakes.usecases

import com.maddcog.android.baseui.model.Resource
import com.maddcog.android.domain.entities.ActivityItem
import com.maddcog.android.domain.usecase.GetGamesListUseCase
import io.mockk.mockk
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class FakeGetGamesListUseCaseNegative @Inject constructor() : GetGamesListUseCase(mockk(), mockk()) {
    override operator fun invoke(): Flow<Resource<List<ActivityItem>>> =
        flow {
            emit(Resource.Error("Test Error Message"))
        }
}
