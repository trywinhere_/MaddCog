package com.maddcog.android.fakes.usecases

import com.maddcog.android.baseui.model.Resource
import com.maddcog.android.domain.entities.trends.TrendsDetailsData
import com.maddcog.android.domain.usecase.GetTrendsDetailsUseCase
import com.maddcog.android.trends.utils.FiltersListHandler
import io.mockk.mockk
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import org.mockito.Mockito
import javax.inject.Inject

class FakeGetTrendsDetailsUseCaseNegative @Inject constructor(
    private val filtersListHandler: FiltersListHandler,
    ) :
    GetTrendsDetailsUseCase(
        mockk(),
        mockk(),
        filtersListHandler
    ) {
    override operator fun invoke(): Flow<Resource<TrendsDetailsData?>> = flow {

        emit(Resource.Error("Trends Details Error"))
    }
}
