package com.maddcog.android.fakes.usecases

import com.maddcog.android.domain.usecase.DashboardUseCase
import org.mockito.kotlin.mock
import javax.inject.Inject

class FakeDashboardUseCasePositive @Inject constructor(): DashboardUseCase(mock(), mock(), mock()) {
}
