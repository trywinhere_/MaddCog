package com.maddcog.android.fakes.repository

import com.maddcog.android.domain.repository.UserDataRepositoryImpl
import io.mockk.mockk
import javax.inject.Inject

class FakeUserDataRepositoryPositive @Inject constructor() : UserDataRepositoryImpl(mockk(), mockk()) {
}