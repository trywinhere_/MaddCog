package com.maddcog.android.fakes.repository

import androidx.lifecycle.MutableLiveData
import com.maddcog.android.domain.entities.ActivityItem
import com.maddcog.android.domain.entities.ActivityType
import com.maddcog.android.domain.repository.DashboardRepositoryImpl
import io.mockk.mockk
import javax.inject.Inject

class FakeDashboardRepository @Inject constructor() : DashboardRepositoryImpl(mockk(), mockk()) {

    override val activityTypeInfo: MutableLiveData<List<ActivityItem>>
        get() = MutableLiveData()

}
