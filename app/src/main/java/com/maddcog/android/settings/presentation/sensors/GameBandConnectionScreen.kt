package com.maddcog.android.settings.presentation.sensors

import android.bluetooth.BluetoothAdapter
import android.os.Bundle
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.lifecycle.LifecycleOwner
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import com.maddcog.android.R
import com.maddcog.android.baseui.components.BasicTopAppBar
import com.maddcog.android.baseui.ui.theme.*
import com.maddcog.android.data.bluetoothle.ConnectionState
import com.maddcog.android.domain.utils.ble.PermissionUtils
import com.maddcog.android.domain.utils.ble.SystemBroadcastReceiver
import com.maddcog.android.settings.components.MenuButtonWithBottomValue
import com.maddcog.android.settings.components.ScanningProgressGameBand
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.rememberMultiplePermissionsState

@OptIn(ExperimentalPermissionsApi::class)
@Composable
fun GameBandConnectionScreen(
    wasDeviceSaved: Boolean,
    navigateBack: () -> Unit,
    saveBand: () -> Unit,
    onBluetoothStateChanged: () -> Unit,
    onConnectedState: () -> Unit,
    viewModel: GameBandConnectionViewModel,
    lifeCycleOwner: LifecycleOwner,
) {
    val progressVisibility = remember { mutableStateOf(false) }

    SystemBroadcastReceiver(systemAction = BluetoothAdapter.ACTION_STATE_CHANGED) { bluetoothState ->
        val action = bluetoothState?.action ?: return@SystemBroadcastReceiver
        if (action == BluetoothAdapter.ACTION_STATE_CHANGED) {
            onBluetoothStateChanged()
        }
    }

    val permissionState =
        rememberMultiplePermissionsState(permissions = PermissionUtils.permissions)
    val lifecycleOwner = lifeCycleOwner
    val bleConnectionState = viewModel.connectionState

    DisposableEffect(
        key1 = lifeCycleOwner,
        effect = {
            val observer = LifecycleEventObserver { _, event ->
                if (event == Lifecycle.Event.ON_START) {
                    permissionState.launchMultiplePermissionRequest()
                    if (permissionState.allPermissionsGranted && bleConnectionState == ConnectionState.Disconnected) {
                        viewModel.reconnect()
                    }
                }
            }
            lifecycleOwner.lifecycle.addObserver(observer)

            onDispose {
                lifecycleOwner.lifecycle.removeObserver(observer)
            }
        }
    )

    LaunchedEffect(key1 = permissionState.allPermissionsGranted) {
        if (permissionState.allPermissionsGranted) {
            if (bleConnectionState == ConnectionState.Uninitialized) {
                progressVisibility.value = true
                viewModel.initializeConnection()
            }
        }
    }

    LaunchedEffect(key1 = bleConnectionState == ConnectionState.Connected) {
        if (bleConnectionState == ConnectionState.Connected) {
            progressVisibility.value = false
            onConnectedState()
        }
    }

    LaunchedEffect(key1 = bleConnectionState == ConnectionState.DeviceFound) {
        if (bleConnectionState == ConnectionState.Connected) {
            progressVisibility.value = false
        }
    }

    val firebaseAnalytics: FirebaseAnalytics = Firebase.analytics

    Column(
        Modifier
            .background(Black)
    ) {
        BasicTopAppBar(title = R.string.scanning_game_band) {
            navigateBack()
        }
        Box(
            Modifier
                .fillMaxSize()
                .padding(Spacing_12)
        ) {
            if (bleConnectionState == ConnectionState.DeviceFound || bleConnectionState == ConnectionState.Connected ) {
                progressVisibility.value = false
                MenuButtonWithBottomValue(
                    icon = if (bleConnectionState == ConnectionState.Connected || wasDeviceSaved) R.drawable.ic_circle_checked else R.drawable.ic_arrow_forward,
                    name = game_band_text,
                    value = "EEG PPG",
                    {}
                ) {
                    firebaseAnalytics.logEvent(
                        "settings_game_band_selected",
                        Bundle().apply {
                            this.putString(
                                SETTINGS_GAME_BAND_SELECTED, "settings_game_band_selected"
                            )
                        }
                    )
                    saveBand()
                }
            } else {
                progressVisibility.value = true
            }
        }
    }
    if (progressVisibility.value) {
        ScanningProgressGameBand(
            stringResource(
                id = if (viewModel.connectionState != ConnectionState.DeviceFound) R.string.scanning else R.string.saving_sensor
            )
        )
    }
}