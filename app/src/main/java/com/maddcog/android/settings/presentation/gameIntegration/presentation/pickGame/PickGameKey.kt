package com.maddcog.android.settings.presentation.gameIntegration.presentation.pickGame

enum class PickGameKey {
    StartNewGame, NewConnection
}