package com.maddcog.android.settings.presentation.sensors

import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.ui.platform.ComposeView
import androidx.fragment.app.viewModels
import com.maddcog.android.baseui.presentation.BaseFragment
import com.maddcog.android.baseui.presentation.BaseViewModel
import com.maddcog.android.baseui.ui.theme.MaddcogTheme
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class GameBandConnectionFragment : BaseFragment() {

    companion object {
        const val CONNECTION_SUCCESS = "Device connected!"
    }

    private val viewModel: GameBandConnectionViewModel by viewModels()

    @Inject
    lateinit var bluetoothAdapter: BluetoothAdapter

    private var isBluetoothDialogAlreadyShown = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = ComposeView(requireContext())
        view.setContent {
            MaddcogTheme {
                GameBandConnectionScreen(
                    wasDeviceSaved = viewModel.isDeviceConnected,
                    navigateBack = {
                        viewModel.navigateBack()
                    },
                    saveBand = { viewModel.connectToGameBand() },
                    viewModel = viewModel,
                    onBluetoothStateChanged = { showBluetoothDialog() },
                    onConnectedState = {
                        viewModel.saveDevice()
                        Toast.makeText(requireContext(), CONNECTION_SUCCESS, Toast.LENGTH_SHORT).show()
                    },
                    lifeCycleOwner = viewLifecycleOwner
                )
            }
        }

        return view
    }

    override fun onStart() {
        super.onStart()
        showBluetoothDialog()
    }

    override fun withViewModel(): BaseViewModel = viewModel.apply {
    }

    private fun showBluetoothDialog() {
        if (!bluetoothAdapter.isEnabled) {
            val enabledBluetoothIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            startBluetoothIntentForResult.launch(enabledBluetoothIntent)
            isBluetoothDialogAlreadyShown = true
        }
    }

    private val startBluetoothIntentForResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            isBluetoothDialogAlreadyShown = false
            if (result.resultCode != Activity.RESULT_OK) {
                showBluetoothDialog()
            }
        }
}