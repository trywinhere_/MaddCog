package com.maddcog.android.settings.presentation.gameIntegration.presentation.selectRegion

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.ui.platform.ComposeView
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.viewModels
import com.maddcog.android.baseui.presentation.BaseFragment
import com.maddcog.android.baseui.presentation.BaseViewModel
import com.maddcog.android.baseui.ui.theme.MaddcogTheme
import com.maddcog.android.settings.presentation.gameIntegration.presentation.saveEditConnection.SaveEditConnectionFragment.Companion.SELECT_REGION_DATA_KEY
import com.maddcog.android.settings.presentation.gameIntegration.presentation.saveEditConnection.SaveEditConnectionFragment.Companion.SELECT_REGION_KEY
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SelectRegionFragment : BaseFragment() {

    private val viewModel: SelectRegionViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.initContent()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = ComposeView(requireContext())
        view.setContent {
            MaddcogTheme {
                SelectRegionScreen(
                    viewModel,
                    selectRegion = { region ->
                        setFragmentResult(
                            SELECT_REGION_KEY,
                            Bundle().apply {
                                putString(SELECT_REGION_DATA_KEY, region)
                            }
                        )
                        viewModel.navigateBack()
                    }
                )
            }
        }

        return view
    }

    override fun withViewModel(): BaseViewModel = viewModel.apply {
    }
}