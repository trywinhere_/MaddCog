package com.maddcog.android.settings.components

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.BlendMode
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.layout.ContentScale
import coil.compose.AsyncImage
import com.maddcog.android.baseui.ui.theme.BlackSemiTransparent
import com.maddcog.android.baseui.ui.theme.FlowText
import com.maddcog.android.baseui.ui.theme.Spacing_0
import com.maddcog.android.baseui.ui.theme.Spacing_10
import com.maddcog.android.baseui.ui.theme.Spacing_12
import com.maddcog.android.baseui.ui.theme.Spacing_2
import com.maddcog.android.baseui.ui.theme.Spacing_250
import com.maddcog.android.baseui.ui.theme.Spacing_6
import com.maddcog.android.baseui.ui.theme.WhiteText16Bold

@Composable
fun GridGameCard(
    imageUrl: String,
    contentDescription: String,
    title: String,
    isSelected: Boolean,
    selectGame: () -> Unit,
) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .height(Spacing_250)
            .padding(Spacing_6)
            .clickable {
                selectGame()
            },
        shape = RoundedCornerShape(Spacing_10),
        elevation = Spacing_0,
        border = if (isSelected) BorderStroke(Spacing_2, FlowText) else null
    ) {
        AsyncImage(
            model = imageUrl,
            contentDescription = contentDescription,
            contentScale = ContentScale.Crop,
            colorFilter = ColorFilter.tint(BlackSemiTransparent, BlendMode.Darken)
        )
        Box(
            modifier = Modifier
                .fillMaxSize()
                .padding(Spacing_12),
            contentAlignment = Alignment.Center
        ) {
            Text(title, style = WhiteText16Bold)
        }
    }
}