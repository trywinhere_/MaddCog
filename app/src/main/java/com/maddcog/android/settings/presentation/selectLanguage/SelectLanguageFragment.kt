package com.maddcog.android.settings.presentation.selectLanguage

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.ui.platform.ComposeView
import androidx.fragment.app.viewModels
import com.maddcog.android.baseui.presentation.BaseFragment
import com.maddcog.android.baseui.presentation.BaseViewModel
import com.maddcog.android.baseui.ui.theme.MaddcogTheme
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SelectLanguageFragment : BaseFragment() {

    private val viewModel: SelectLanguageViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.initContent()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = ComposeView(requireContext())
        view.setContent {
            MaddcogTheme {
                SelectLanguageScreen(
                    viewModel
                )
            }
        }

        return view
    }

    override fun withViewModel(): BaseViewModel = viewModel.apply {
    }
}