package com.maddcog.android.settings.components

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.input.TextFieldValue
import com.maddcog.android.R
import com.maddcog.android.baseui.ui.theme.*

@Composable
fun SearchBoxWithPen(
    state: MutableState<TextFieldValue?>
) {
    val focusManager = LocalFocusManager.current

    Row(modifier = Modifier.fillMaxWidth()) {

        state.value?.let {
            TextField(
                value = it,
                onValueChange = { text ->
                    state.value = text
                },
                modifier = Modifier
                    .fillMaxWidth()
                    .height(Spacing_50),
                shape = RoundedCornerShape(15),
                colors = TextFieldDefaults.textFieldColors(
                    backgroundColor = BoxDarkGrey,
                    textColor = White, placeholderColor = HintLightGrey,
                    cursorColor = Color.White
                ),
                keyboardActions = KeyboardActions(onDone = { focusManager.clearFocus() }),
                singleLine = true,
                textStyle = WhiteText14,
                trailingIcon = {
                    if (state.value != TextFieldValue("")) {
                        IconButton(
                            onClick = { }
                        ) {
                            Icon(
                                painter = painterResource(id = R.drawable.ic_outline_edit_24),
                                contentDescription = "",
                                modifier = Modifier
                                    .padding(end = Spacing_5)
                                    .size(Spacing_21),
                                tint = Color.White
                            )
                        }
                    }
                }
            )
        }
    }
}