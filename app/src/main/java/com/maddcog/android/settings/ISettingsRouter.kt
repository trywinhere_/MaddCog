package com.maddcog.android.settings

import com.maddcog.android.baseui.model.NavCommand
import com.maddcog.android.settings.presentation.gameIntegration.presentation.pickGame.PickGameKey

interface ISettingsRouter {

    fun openPrivacyPolice(): NavCommand

    fun openGameBandConnection(): NavCommand

    fun openGameIntegration(): NavCommand

    fun openPickGame(key: PickGameKey): NavCommand

    fun openPickGameById(gameId: Int, key: PickGameKey): NavCommand

    fun openSelectRegion(): NavCommand

    fun openSaveConnection(gameNumber: Int, gameName: String, poster: String): NavCommand

    fun openChangePassword(): NavCommand

    fun openDeleteAccount(): NavCommand

    fun openSelectLanguage(): NavCommand

    fun goToAuth(): NavCommand

    fun navigateToScanningForGameBand(): NavCommand

    fun navigateToStartNewSession(): NavCommand

    fun navigateToDashboard(): NavCommand

    fun navigateToCreateTeam(): NavCommand

    fun navigateToAddTeam(): NavCommand

    fun navigateToOneTeam(): NavCommand
}