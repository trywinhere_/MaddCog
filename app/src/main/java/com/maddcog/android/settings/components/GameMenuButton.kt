package com.maddcog.android.settings.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.layout.layoutId
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.ConstraintSet
import coil.compose.AsyncImage
import com.maddcog.android.R
import com.maddcog.android.baseui.ui.theme.BoxDarkGrey
import com.maddcog.android.baseui.ui.theme.Spacing_0
import com.maddcog.android.baseui.ui.theme.Spacing_10
import com.maddcog.android.baseui.ui.theme.Spacing_16
import com.maddcog.android.baseui.ui.theme.Spacing_6
import com.maddcog.android.baseui.ui.theme.WhiteText16
import com.maddcog.android.baseui.ui.theme.arrow_right

@Composable
fun GameMenuButton(
    name: String,
    imageUrl: String,
    navigateTo: () -> Unit
) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .clickable {
                navigateTo()
            },
        shape = RoundedCornerShape(Spacing_10),
        elevation = Spacing_0,
        backgroundColor = BoxDarkGrey,
    ) {
        val constraints = ConstraintSet {
            val name = createRefFor("name")
            val icon = createRefFor("icon")
            val arrow = createRefFor("arrow")

            constrain(icon) {
                top.linkTo(parent.top)
                start.linkTo(parent.start)
                bottom.linkTo(parent.bottom)
            }
            constrain(name) {
                top.linkTo(parent.top)
                bottom.linkTo(parent.bottom)
                start.linkTo(icon.end)
            }
            constrain(arrow) {
                top.linkTo(parent.top)
                end.linkTo(parent.end)
                bottom.linkTo(parent.bottom)
            }
        }
        ConstraintLayout(
            constraints,
            modifier = Modifier
                .fillMaxWidth()
                .background(color = BoxDarkGrey)
                .padding(start = Spacing_16, top = Spacing_6, end = Spacing_10, bottom = Spacing_6)
        ) {
            Text(
                text = name,
                style = WhiteText16,
                modifier = Modifier
                    .layoutId("name")
                    .padding(start = Spacing_16)
            )
            AsyncImage(
                model = imageUrl,
                contentDescription = null,
                modifier = Modifier
                    .clip(RoundedCornerShape(Spacing_10))
                    .layoutId("icon")
                    .height(45.dp)
                    .aspectRatio(1f),
                contentScale = ContentScale.Crop
            )
            Image(
                painterResource(id = R.drawable.ic_arrow_forward),
                contentDescription = arrow_right,
                modifier = Modifier.layoutId("arrow")
            )
        }
    }
}