package com.maddcog.android.settings.presentation.changePassword

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.compose.ui.platform.ComposeView
import androidx.fragment.app.viewModels
import com.maddcog.android.R
import com.maddcog.android.baseui.presentation.BaseFragment
import com.maddcog.android.baseui.presentation.BaseViewModel
import com.maddcog.android.baseui.ui.theme.MaddcogTheme
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ChangePasswordFragment : BaseFragment() {

    private val viewModel: ChangePasswordViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.initContent()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        viewModel.clearLiveData()

        val view = ComposeView(requireContext())
        view.setContent {
            MaddcogTheme {
                ChangePasswordScreen(
                    viewModel
                )
            }
        }

        return view
    }

    override fun withViewModel(): BaseViewModel = viewModel.apply {
        responseErrorChange.observe(viewLifecycleOwner) { error ->
            if (!error.isNullOrEmpty()) {
                Toast.makeText(requireContext(), error.toString(), Toast.LENGTH_SHORT).show()
            }
        }
        responseSuccessChange.observe(viewLifecycleOwner) {
            if (it) {
                viewModel.navigateBack()
                Toast.makeText(
                    requireContext(),
                    requireContext().resources.getString(R.string.success_password_change),
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }
}