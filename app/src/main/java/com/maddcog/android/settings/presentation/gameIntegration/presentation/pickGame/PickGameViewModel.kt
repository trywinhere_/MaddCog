package com.maddcog.android.settings.presentation.gameIntegration.presentation.pickGame

import androidx.compose.runtime.State
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.viewModelScope
import com.maddcog.android.baseui.model.Resource
import com.maddcog.android.baseui.presentation.BaseViewModel
import com.maddcog.android.data.bluetoothle.ConnectionState
import com.maddcog.android.data.bluetoothle.IBleReceiveManager
import com.maddcog.android.domain.entities.ActivityItem
import com.maddcog.android.domain.usecase.GetGamesListUseCase
import com.maddcog.android.settings.ISettingsRouter
import com.maddcog.android.settings.presentation.gameIntegration.states.ActivityItemsState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class PickGameViewModel @Inject constructor(
    private val useCase: GetGamesListUseCase,
    private val router: ISettingsRouter,
    private val bleReceiveManager: IBleReceiveManager
) : BaseViewModel() {

    private val _state = mutableStateOf(ActivityItemsState())
    val state: State<ActivityItemsState> = _state
    var connectionState by mutableStateOf<ConnectionState>(ConnectionState.Uninitialized)

    init {
        getActivityItems()
    }

    private fun getActivityItems() {
        useCase().onEach { result ->
            when (result) {
                is Resource.Success -> {
                    _state.value = ActivityItemsState(items = result.data ?: emptyList())
                }
                is Resource.Error -> {
                    _state.value = ActivityItemsState(error = result.message ?: "An unexpected error occurred")
                }
                is Resource.Loading -> {
                    _state.value = ActivityItemsState(isLoading = true)
                }
            }
        }.launchIn(viewModelScope)
    }

    fun openSaveConnection(gameNumber: Int, gameName: String, poster: String) {
        navigateTo(router.openSaveConnection(gameNumber, gameName, poster))
    }

    fun getFilteredList(query: String): List<ActivityItem> {
        return if (query.isEmpty()) {
            state.value.items
        } else {
            state.value.items.filter { it.name.contains(query, true) }
        }
    }

    fun saveGameForNewSession(item: ActivityItem) {
        useCase.saveGameForNewSession(item)
    }

    fun navigateToStartNewSession() {
        navigateTo(router.navigateToStartNewSession())
    }

    fun subscribeToChanges() {
        viewModelScope.launch {
            bleReceiveManager.data.collect { result ->
                when (result) {
                    is com.maddcog.android.domain.utils.ble.Resource.Success -> {
                        connectionState = result.data.connectionState
                    }

                    is com.maddcog.android.domain.utils.ble.Resource.Loading -> {
                        connectionState = ConnectionState.CurrentlyInitializing
                    }

                    is com.maddcog.android.domain.utils.ble.Resource.Error -> {
                        connectionState = ConnectionState.Uninitialized
                    }
                }
            }
        }
    }
}