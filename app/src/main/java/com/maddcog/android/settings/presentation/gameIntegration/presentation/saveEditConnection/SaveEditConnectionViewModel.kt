package com.maddcog.android.settings.presentation.gameIntegration.presentation.saveEditConnection

import androidx.compose.runtime.mutableStateOf
import com.maddcog.android.baseui.model.StatefulData
import com.maddcog.android.baseui.presentation.BaseViewModel
import com.maddcog.android.domain.entities.RegionType
import com.maddcog.android.domain.usecase.SettingsUseCase
import com.maddcog.android.settings.ISettingsRouter
import com.maddcog.android.settings.model.PresentationGame
import com.maddcog.android.settings.presentation.gameIntegration.presentation.pickGame.PickGameKey
import com.maddcog.android.settings.presentation.gameIntegration.presentation.saveEditConnection.SaveEditConnectionFragment.Companion.SELECT_REGION
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

@HiltViewModel
class SaveEditConnectionViewModel @Inject constructor(
    private val useCase: SettingsUseCase,
    private val router: ISettingsRouter,
) : BaseViewModel() {

    val errorDialog = mutableStateOf(false)

    private val _saveConnection = statefulSharedFlow<Unit>()
    val saveConnection: Flow<StatefulData<Unit>>
        get() = _saveConnection

    private var _savedGame: PresentationGame? = null
    val savedGame: PresentationGame
        get() = _savedGame!!

    fun initContent(number: Int, gameName: String, poster: String) {
        _savedGame = PresentationGame(
            name = gameName,
            number = number,
            poster = poster,
            username = "",
            region = SELECT_REGION
        )
    }

    fun saveText(text: String) {
        savedGame.username = text
    }

    fun saveRegion(region: String) {
        savedGame.region = region
    }

    fun saveGame() {
        _saveConnection.fetch {
            useCase.saveConnectedGame(
                savedGame.number,
                savedGame.username,
                region = if (savedGame.region != SELECT_REGION) savedGame.region else RegionType.NA1.name
            )
        }
    }

    fun openPickGame(gameId: Int) {
        navigateTo(router.openPickGameById(gameId, PickGameKey.NewConnection))
    }

    fun openSelectRegion() {
        navigateTo(router.openSelectRegion())
    }
}