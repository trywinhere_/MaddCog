package com.maddcog.android.settings.presentation.teams.oneteam

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.maddcog.android.baseui.presentation.BaseViewModel
import com.maddcog.android.data.model.StateViewModel
import com.maddcog.android.domain.api.IDashboardRepository
import com.maddcog.android.domain.entities.team.TeamMembers
import com.maddcog.android.domain.entities.team.Team
import com.maddcog.android.domain.usecase.TeamUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

private const val OWNER = "owner"

@HiltViewModel
class OneTeamViewModel @Inject constructor(
    private val useCase: TeamUseCase,
    private val repository: IDashboardRepository
) : BaseViewModel() {

    val teams = repository.teamInfo.value?.toMutableList()

    fun getTeammates(teams: MutableList<Team>, team: Team): ArrayList<TeamMembers>? {
        val teamNeeded = teams.find { it -> it == team }
        return teamNeeded?.teamMembers
    }

    fun deleteTeamMember(teamMembersList: MutableList<TeamMembers>, teamMember: TeamMembers) {
        teamMembersList.remove(teamMember)
    }

    fun getTeamName(): String? {
        return useCase.getTeamName()
    }

    fun removeTeammate(userId: String) {
        viewModelScope.launch {
            useCase.removeTeammate(userId)
        }
    }

    fun isUserOwner(teammate: TeamMembers): String {
        if (teammate.owner == true) return OWNER
        else return ""
    }

    fun leaveTeam(teamId: String) {
        viewModelScope.launch {
            useCase.leaveTeam(teamId)
        }
    }

    private val _dataState = MutableLiveData<StateViewModel>()
    val dataState: LiveData<StateViewModel>
        get() = _dataState

    fun inviteUser(userEmail: String) {
        try {
            viewModelScope.launch {
                _dataState.value = StateViewModel(loading = true)
                useCase.inviteUser(userEmail)
                _dataState.value = StateViewModel()
            }
        } catch (e: Exception) {
            _dataState.value = StateViewModel(error = true)
        }
    }
}