package com.maddcog.android.settings

import androidx.compose.runtime.State
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.viewModelScope
import com.maddcog.android.baseui.model.Resource
import com.maddcog.android.baseui.presentation.BaseViewModel
import com.maddcog.android.baseui.ui.theme.UNEXPECTED_ERROR
import com.maddcog.android.data.bluetoothle.ConnectionState
import com.maddcog.android.data.bluetoothle.IBleReceiveManager
import com.maddcog.android.domain.entities.user.UserProfile
import com.maddcog.android.domain.entities.user.UserProfileData
import com.maddcog.android.domain.usecase.SettingsUseCase
import com.maddcog.android.domain.usecase.UserDataUseCase
import com.maddcog.android.settings.model.SettingsState
import com.maddcog.android.settings.presentation.selectLanguage.model.Language
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class SettingsViewModel @Inject constructor(
    private val userDataUseCase: UserDataUseCase,
    private val useCase: SettingsUseCase,
    private val router: ISettingsRouter,
    private val bleReceiveManager: IBleReceiveManager
) : BaseViewModel() {

    private val _state = mutableStateOf(SettingsState())
    val state: State<SettingsState> = _state
    var isDeviceSaved: Boolean = true

    var connectionState by mutableStateOf<ConnectionState>(ConnectionState.Uninitialized)

    init {
        getUserData()
        isDeviceSaved = useCase.getDeviceSavedStatus()
    }

    fun subscribeToChanges() {
        viewModelScope.launch {
            bleReceiveManager.data.collect { result ->
                when (result) {
                    is com.maddcog.android.domain.utils.ble.Resource.Success -> {
                        connectionState = result.data.connectionState
                    }

                    is com.maddcog.android.domain.utils.ble.Resource.Loading -> {
                        connectionState = ConnectionState.CurrentlyInitializing
                    }

                    is com.maddcog.android.domain.utils.ble.Resource.Error -> {
                        connectionState = ConnectionState.Uninitialized
                    }
                }
            }
        }
    }

    fun getUserData() {
        userDataUseCase().onEach { result ->
            when (result) {
                is Resource.Success -> {
                    _state.value = SettingsState(userData = result.data)
                }
                is Resource.Error -> {
                    _state.value = SettingsState(error = result.message ?: UNEXPECTED_ERROR)
                }
                is Resource.Loading -> {
                    _state.value = SettingsState(isLoading = true)
                }
            }
        }.launchIn(viewModelScope)
    }

    fun openPrivacyPolicy() {
        navigateTo(router.openPrivacyPolice())
    }

    fun openGameBandConnection() {
        navigateTo(router.openGameBandConnection())
    }

    fun openGameIntegration() {
        navigateTo(router.openGameIntegration())
    }

    fun openChangePassword() {
        navigateTo(router.openChangePassword())
    }

    fun openDeleteAccount() {
        navigateTo(router.openDeleteAccount())
    }

    fun openSelectLanguage() {
        navigateTo(router.openSelectLanguage())
    }

    fun performLogout() {
        viewModelScope.launch {
            useCase.performLogout()
            withContext(Dispatchers.Main) {
                navigateTo(router.goToAuth())
            }
        }
    }

    fun navigateToScanningForGameBand() {
        navigateTo(router.navigateToScanningForGameBand())
    }

    fun getGameCount(userProfile: UserProfile? = state.value.userData?.profile): Int? {
        return userProfile?.userGames?.size
    }

    fun getUserEmail(userProfile: UserProfile? = state.value.userData?.profile): String? {
        return userProfile?.email
    }

    fun getLanguageList(userData: UserProfileData? = state.value.userData): List<Language>? {
        return userData?.languageList
    }

    fun navigateToCreateTeam() {
        navigateTo(router.navigateToCreateTeam())
    }

    fun navigateToStartNewSession() {
        navigateTo(router.navigateToStartNewSession())
    }

    fun getSavedDeviceStatus(): Boolean {
        return useCase.getDeviceSavedStatus()
    }
}