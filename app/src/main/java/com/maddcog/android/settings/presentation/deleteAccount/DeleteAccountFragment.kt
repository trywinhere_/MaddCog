package com.maddcog.android.settings.presentation.deleteAccount

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.ui.platform.ComposeView
import androidx.fragment.app.viewModels
import com.maddcog.android.baseui.presentation.BaseFragment
import com.maddcog.android.baseui.presentation.BaseViewModel
import com.maddcog.android.baseui.ui.theme.MaddcogTheme
import com.maddcog.android.domain.extensions.showErrorType
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DeleteAccountFragment : BaseFragment() {

    private val viewModel: DeleteAccountViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = ComposeView(requireContext())
        view.setContent {
            MaddcogTheme {
                DeleteAccountScreen(
                    viewModel,
                    deleteUser = {
                        viewModel.deleteUser()
                    }
                )
            }
        }

        return view
    }

    override fun withViewModel(): BaseViewModel = viewModel.apply {
        deleteUser.collectWithState {
            it.isSuccessful {
                viewModel.dropToAuth()
            }
            it.isError { error ->
                showToastMessage(requireContext().resources.getString(error.showErrorType()))
            }
        }
    }
}