package com.maddcog.android.settings.presentation.selectLanguage

import android.os.Bundle
import androidx.appcompat.app.AppCompatDelegate
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.selection.selectable
import androidx.compose.foundation.selection.selectableGroup
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.semantics.Role
import androidx.core.os.LocaleListCompat
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import com.maddcog.android.R
import com.maddcog.android.baseui.components.BasicTopAppBar
import com.maddcog.android.baseui.components.SelectableButton
import com.maddcog.android.baseui.ui.theme.*
import com.maddcog.android.settings.presentation.selectLanguage.extensions.displayName

@Composable
fun SelectLanguageScreen(
    viewModel: SelectLanguageViewModel,
) {
    val languageList = viewModel.data
    val currentLocale = languageList.firstOrNull {
        it.name.lowercase() == AppCompatDelegate.getApplicationLocales().toLanguageTags()
    } ?: languageList.first()
    var selectedItem by remember {
        mutableStateOf(
            languageList.find {
                it == currentLocale
            }
        )
    }
    val firebaseAnalytics: FirebaseAnalytics = Firebase.analytics

    Column(
        Modifier
            .background(Black)
    ) {
        BasicTopAppBar(title = R.string.select_language) {
            viewModel.navigateBack()
        }
        Column(
            modifier = Modifier
                .background(Color.Black)
                .fillMaxSize()
                .padding(Spacing_12)
                .selectableGroup()
        ) {
            languageList.forEach { label ->
                SelectableButton(
                    name = label.displayName(LocalContext.current),
                    selectionTerm = (selectedItem == label),
                    modifier = Modifier.selectable(
                        selected = (selectedItem == label),
                        onClick = {
                            selectedItem = label
                            AppCompatDelegate.setApplicationLocales(
                                LocaleListCompat.forLanguageTags(label.name.lowercase())
                            )
                            firebaseAnalytics.logEvent(
                                "settings_change_language",
                                Bundle().apply {
                                    this.putString(
                                        SETTINGS_CHANGE_LANGUAGE, "settings_change_language"
                                    )
                                }
                            )
                        },
                        role = Role.RadioButton,
                    )
                )
                Spacer(modifier = Modifier.height(Spacing_16))
            }
        }
    }
}