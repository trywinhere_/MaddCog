package com.maddcog.android.settings

import android.os.Bundle
import androidx.appcompat.app.AppCompatDelegate
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import com.maddcog.android.R
import com.maddcog.android.baseui.components.BasicTopAppBar
import com.maddcog.android.baseui.components.PrivacyPolicyText
import com.maddcog.android.baseui.ui.theme.*
import com.maddcog.android.data.bluetoothle.ConnectionState
import com.maddcog.android.settings.components.MenuButtonWithValue
import com.maddcog.android.settings.components.SectionHeader
import com.maddcog.android.settings.components.SimpleMenuButton
import com.maddcog.android.settings.presentation.selectLanguage.extensions.displayName

@Composable
fun SettingsScreen(
    wasDeviceSaved: Boolean,
    viewModel: SettingsViewModel,
    navigateToCreateTeam: () -> Unit,
) {
    val bleConnectionState = viewModel.connectionState
    val firebaseAnalytics: FirebaseAnalytics = Firebase.analytics

    Column() {
        BasicTopAppBar(title = R.string.settings_title) {
            viewModel.navigateBack()
        }
        Box(Modifier.fillMaxWidth()) {
            LazyColumn(
                modifier = Modifier
                    .background(Color.Black)
                    .fillMaxSize()
                    .padding(Spacing_12)
            ) {
                item {
                    SectionHeader(name = R.string.sensors_header)
                    SimpleMenuButton(
                        textStyle = if (bleConnectionState == ConnectionState.Connected || wasDeviceSaved) WhiteText16 else RedText16,
                        name = if (bleConnectionState == ConnectionState.Connected || wasDeviceSaved) R.string.game_band_text else R.string.scan_game_band,
                    ) {
                        viewModel.openGameBandConnection()
                    }
                    SectionHeader(name = R.string.teams_header)
                    SimpleMenuButton(name = R.string.create_team_text) {
                        navigateToCreateTeam()
                    }
                    SectionHeader(name = R.string.game_integration_header)
                    MenuButtonWithValue(
                        name = R.string.connected_games_text,
                        value = viewModel.getGameCount().toString()
                    ) {
                        viewModel.openGameIntegration()
                    }
                    SectionHeader(name = R.string.language_header)
                    MenuButtonWithValue(
                        name = R.string.language_text,
                        value = (
                            viewModel.getLanguageList()?.firstOrNull() {
                                it.name.lowercase() == AppCompatDelegate.getApplicationLocales()
                                    .toLanguageTags()
                            } ?: viewModel.getLanguageList()?.first()
                            )?.displayName(LocalContext.current)
                            .toString()
                    ) {
                        viewModel.openSelectLanguage()
                    }
                    SectionHeader(name = R.string.account_header)
                    SimpleMenuButton(name = R.string.change_password_text) {
                        viewModel.openChangePassword()
                    }
                    Spacer(modifier = Modifier.height(Spacing_16))
                    SimpleMenuButton(name = R.string.delete_account_text) {
                        viewModel.openDeleteAccount()
                    }
                    Text(
                        text = viewModel.getUserEmail().toString(),
                        style = HintGreyText12,
                        modifier = Modifier
                            .padding(top = Spacing_10)
                            .fillMaxWidth(),
                        textAlign = TextAlign.Center
                    )
                    Text(
                        text = stringResource(id = R.string.log_out_text),
                        style = RedText16,
                        textAlign = TextAlign.Center,
                        modifier = Modifier
                            .fillMaxWidth()
                            .clickable {
                                firebaseAnalytics.logEvent(
                                    "settings_sign_out",
                                    Bundle().apply {
                                        this.putString(
                                            SETTINGS_SIGN_OUT, "settings_sign_out"
                                        )
                                    }
                                )
                                viewModel.performLogout()
                            },
                    )
                    Spacer(modifier = Modifier.height(Spacing_16))
                    PrivacyPolicyText {
                        viewModel.openPrivacyPolicy()
                    }
                    Spacer(modifier = Modifier.height(Spacing_12))
                    Text(
                        text = "1.10(27)",
                        style = HintGreyText12,
                        modifier = Modifier
                            .padding(top = Spacing_10)
                            .fillMaxWidth(),
                        textAlign = TextAlign.Center
                    ) // FIXME: hardcoded version number will be fixed
                    Spacer(modifier = Modifier.height(Spacing_16))
                }
            }

            if (viewModel.state.value.isLoading) {
                CircularProgressIndicator(
                    modifier = Modifier
                        .height(Spacing_40)
                        .width(Spacing_40)
                        .align(Alignment.Center),
                    color = Color.Red
                )
            }
        }
    }
}