package com.maddcog.android.settings.model

data class PresentationPasswordChange(
    var oldPassword: String,
    var newPassword: String
)