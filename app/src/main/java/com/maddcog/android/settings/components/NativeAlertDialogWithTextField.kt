package com.maddcog.android.settings.components

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.TextFieldValue
import com.maddcog.android.R
import com.maddcog.android.baseui.ui.theme.*

@Composable
fun NativeAlertDialogWithTextField(
    onExit: () -> Unit,
    onSuccess: () -> Unit,
    successName: Int,
    disclaimerFirst: Int,
    disclaimerSecond: Int,
    inputText: MutableState<TextFieldValue>,
    hintText: Int
) {

    val focusManager = LocalFocusManager.current

    AlertDialog(
        backgroundColor = BoxDarkGrey,
        shape = RoundedCornerShape(Spacing_10),
        onDismissRequest = {
            onExit()
        },
        title = {
            Text(text = stringResource(id = disclaimerFirst), color = Color.White)
            Text(text = stringResource(id = disclaimerSecond), color = Color.White)
        },
        text = {
            TextField(
                value = inputText.value,
                onValueChange = { text ->
                    inputText.value = text
                },
                placeholder = {
                    Text(
                        text = stringResource(id = hintText),
                        color = HintLightGrey,
                        fontSize = Text_18
                    )
                },
                modifier = Modifier
                    .padding(horizontal = Spacing_15, vertical = Spacing_20)
                    .fillMaxWidth(),
                shape = RoundedCornerShape(15),
                colors = TextFieldDefaults.textFieldColors(
                    backgroundColor = BoxDarkGrey,
                    textColor = White, placeholderColor = HintLightGrey,
                    cursorColor = White
                ),
                singleLine = true,
                visualTransformation = PasswordVisualTransformation(),
                keyboardActions = KeyboardActions(onDone = { focusManager.clearFocus() })
            )
        },
        confirmButton = {
            TextButton(
                onClick = { onSuccess() }
            ) {
                Text(
                    text = stringResource(id = successName),
                    color = Color.White,
                )
            }
        },
        dismissButton = {
            TextButton(
                onClick = { onExit() }
            ) {
                Text(
                    text = stringResource(id = R.string.cancel_btn),
                    color = Color.White,
                )
            }
        }
    )
}