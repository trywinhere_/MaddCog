package com.maddcog.android.settings.components

import androidx.compose.animation.animateContentSize
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import com.maddcog.android.baseui.components.GifImage
import com.maddcog.android.baseui.ui.theme.*

@Composable
fun ScanningProgressGameBand(text: String) {

    Surface(color = DialogBackgroundColor) {
        Row(
            modifier = Modifier.fillMaxSize(),
            verticalAlignment = Alignment.CenterVertically,
        ) {
            Card(
                modifier = Modifier
                    .animateContentSize()
                    .fillMaxWidth()
                    .wrapContentHeight(align = Alignment.CenterVertically)
                    .padding(all = Spacing_30),
                shape = RoundedCornerShape(10),
                backgroundColor = BoxDarkGrey
            ) {
                Column(
                    verticalArrangement = Arrangement.Center,
                    horizontalAlignment = Alignment.CenterHorizontally,
                    modifier = Modifier
                        .padding(all = Spacing_15)
                        .background(BoxDarkGrey)
                ) {
                    Text(
                        text = text,
                        fontSize = Text_18,
                        fontWeight = FontWeight.Bold,
                        color = White
                    )
                    Spacer(modifier = Modifier.height(Spacing_20))
                    GifImage()
                }
            }
        }
    }
}