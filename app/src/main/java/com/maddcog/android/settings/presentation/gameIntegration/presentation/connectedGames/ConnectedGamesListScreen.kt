package com.maddcog.android.settings.presentation.gameIntegration.presentation.connectedGames

import android.os.Bundle
import androidx.compose.animation.animateColorAsState
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.DismissDirection
import androidx.compose.material.DismissValue
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.Icon
import androidx.compose.material.SwipeToDismiss
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.filled.Done
import androidx.compose.material.rememberDismissState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import com.maddcog.android.R
import com.maddcog.android.baseui.components.BasicTopAppBarWithRightButton
import com.maddcog.android.baseui.presentation.NativeAlertDialog
import com.maddcog.android.baseui.ui.theme.*
import com.maddcog.android.domain.entities.UserGame
import com.maddcog.android.settings.components.MenuButtonWithBottomValue
import com.maddcog.android.settings.components.SectionHeader
import com.maddcog.android.settings.components.SimpleMenuButton

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun ConnectedGamesListScreen(
    viewModel: ConnectedGamesListViewModel,
    removeItem: (UserGame) -> Unit,
) {
    val state = viewModel.state.value
    var openDialog = remember { mutableStateOf(false) }
    val firebaseAnalytics: FirebaseAnalytics = Firebase.analytics
    Column(
        Modifier
            .background(Black)
    ) {
        BasicTopAppBarWithRightButton(
            title = R.string.connected_games_text,
            navigateBack = {
                viewModel.navigateBack()
            }, rightButtonClick = {
            viewModel.openPickGame()
        },
            rightButtonText = R.string.add_btn,
            state.games.isNotEmpty()
        )
        Box(Modifier.fillMaxWidth()) {
            LazyColumn(
                modifier = Modifier
                    .background(Color.Black)
                    .fillMaxSize()
                    .padding(Spacing_12)
            ) {
                item {
                    if (state.games.isEmpty() && !state.isLoading) {
                        SectionHeader(name = R.string.pick_game_text)
                        SimpleMenuButton(name = R.string.pick_game_text) {
                            viewModel.openPickGame()
                        }
                    }
                }
                items(state.games, { listItem: UserGame -> listItem.generatedId }) { game ->
                    val dismissState = rememberDismissState()
                    LaunchedEffect(dismissState.currentValue) {
                        if (dismissState.currentValue == DismissValue.DismissedToStart) {
                            openDialog.value = true
                            dismissState.snapTo(DismissValue.Default)
                        }
                    }
                    if (openDialog.value) {
                        NativeAlertDialog(
                            onExit = {
                                openDialog.value = false
                            },
                            onSuccess = {
                                removeItem(game)
                                firebaseAnalytics.logEvent(
                                    "settings_delete_game_integration",
                                    Bundle().apply {
                                        this.putString(
                                            SETTINGS_DELETE_GAME_INTEGRATION, "settings_delete_game_integration"
                                        )
                                    }
                                )
                                openDialog.value = false
                            },
                            disclaimerFirst = R.string.delete_integration_disclaimer_first,
                            disclaimerSecond = R.string.delete_integration_disclaimer_second,
                            successName = R.string.delete_btn
                        )
                    }

                    SwipeToDismiss(
                        state = dismissState,
                        directions = setOf(DismissDirection.EndToStart),
                        background = {
                            val direction = dismissState.dismissDirection ?: return@SwipeToDismiss
                            val color by animateColorAsState(
                                targetValue = when (dismissState.targetValue) {
                                    DismissValue.Default -> Color.Black
                                    DismissValue.DismissedToEnd -> Color.Green
                                    DismissValue.DismissedToStart -> Color.Red
                                }
                            )
                            val icon = when (direction) {
                                DismissDirection.StartToEnd -> Icons.Default.Done
                                DismissDirection.EndToStart -> Icons.Default.Delete
                            }

                            val alignment = when (direction) {
                                DismissDirection.StartToEnd -> Alignment.CenterStart
                                DismissDirection.EndToStart -> Alignment.CenterEnd
                            }

                            Box(
                                modifier = Modifier
                                    .fillMaxSize()
                                    .background(color)
                                    .padding(horizontal = Spacing_12),
                                contentAlignment = alignment
                            ) {
                                Icon(icon, contentDescription = "Icon")
                            }
                        },
                        dismissContent = {
                            MenuButtonWithBottomValue(
                                icon = R.drawable.ic_arrow_forward,
                                name = game.gameName.toString(),
                                value = game.userName.toString(),
                                navigateTo = { viewModel.goToStartNewSession() }
                            ) {
                                viewModel.saveGameName(game.gameName)
                                viewModel.goToStartNewSession()
                            }
                        }
                    )
                    Spacer(modifier = Modifier.height(Spacing_16))
                }
            }

            if (state.isLoading) {
                CircularProgressIndicator(
                    modifier = Modifier
                        .height(Spacing_40)
                        .width(Spacing_40)
                        .align(Alignment.Center),
                    color = Color.Red
                )
            }
        }
    }
}