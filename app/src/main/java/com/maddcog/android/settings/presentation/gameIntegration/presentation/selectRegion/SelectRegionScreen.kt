package com.maddcog.android.settings.presentation.gameIntegration.presentation.selectRegion

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.selection.selectableGroup
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import com.maddcog.android.R
import com.maddcog.android.baseui.components.BasicTopAppBar
import com.maddcog.android.baseui.components.ChangeableMenuButton
import com.maddcog.android.baseui.ui.theme.Black
import com.maddcog.android.baseui.ui.theme.Spacing_12
import com.maddcog.android.baseui.ui.theme.Spacing_8

@Composable
fun SelectRegionScreen(
    viewModel: SelectRegionViewModel,
    selectRegion: (String) -> Unit,
) {
    val regionList = viewModel.data

    Column(
        Modifier
            .background(Black)
    ) {
        BasicTopAppBar(title = R.string.select_region_text) {
            viewModel.navigateBack()
        }
        Column(
            modifier = Modifier
                .background(Color.Black)
                .fillMaxSize()
                .padding(Spacing_12)
                .selectableGroup()
        ) {
            LazyColumn() {
                item {
                    regionList.forEach { region ->
                        ChangeableMenuButton(name = region.name) {
                            selectRegion(region.name)
                        }
                        Spacer(modifier = Modifier.height(Spacing_8))
                    }
                }
            }
        }
    }
}