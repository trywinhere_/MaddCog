package com.maddcog.android.settings.presentation.changePassword

import com.maddcog.android.baseui.model.StatefulData
import com.maddcog.android.baseui.presentation.BaseViewModel
import com.maddcog.android.domain.api.IAuthRepository
import com.maddcog.android.domain.usecase.SettingsUseCase
import com.maddcog.android.settings.ISettingsRouter
import com.maddcog.android.settings.model.PresentationPasswordChange
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

@HiltViewModel
class ChangePasswordViewModel @Inject constructor(
    private val useCase: SettingsUseCase,
    private val router: ISettingsRouter,
    private val repository: IAuthRepository,
) : BaseViewModel() {

    private val _changePassword = statefulSharedFlow<Unit>()
    val changePassword: Flow<StatefulData<Unit>>
        get() = _changePassword

    private var _passwordData: PresentationPasswordChange? = null
    val passwordData: PresentationPasswordChange
        get() = _passwordData!!

    val responseSuccessChange = repository.successChangePassword

    val responseErrorChange = repository.errorChangePassword

    fun initContent() {
        _passwordData = PresentationPasswordChange(
            oldPassword = "",
            newPassword = "",
        )
    }

    fun changePassword() {
        _changePassword.fetch {
            useCase.changePassword(passwordData.oldPassword, passwordData.newPassword)
        }
    }

    fun saveOldPassword(text: String) {
        passwordData.oldPassword = text
    }

    fun saveNewPassword(text: String) {
        passwordData.newPassword = text
    }
    fun clearLiveData() {
        repository.clearChangePassword()
    }
}