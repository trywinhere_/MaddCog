package com.maddcog.android.settings.model

import com.maddcog.android.domain.entities.user.UserProfileData

data class SettingsState(
    val isLoading: Boolean = false,
    val userData: UserProfileData? = null,
    val error: String = "",
)