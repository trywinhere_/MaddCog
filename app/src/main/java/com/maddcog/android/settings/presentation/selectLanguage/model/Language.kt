package com.maddcog.android.settings.presentation.selectLanguage.model

enum class Language {
    EN, FR, ES
}