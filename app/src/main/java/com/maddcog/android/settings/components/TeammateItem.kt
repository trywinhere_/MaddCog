package com.maddcog.android.settings.components

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.ui.Modifier
import com.maddcog.android.baseui.ui.theme.*

@Composable
fun TeammateItem(
    name: String?,
    status: String?,
    visibleDialog: MutableState<Boolean>
) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .clickable {
                visibleDialog.value = true
            },
        shape = RoundedCornerShape(Spacing_10),
        elevation = Spacing_0,
        backgroundColor = BoxDarkGrey,
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .background(color = BoxDarkGrey)
                .padding(horizontal = Spacing_12, vertical = Spacing_16),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            if (name != null) {
                Text(
                    text = name,
                    style = WhiteText16
                )
            }
            if (status != null) {
                Text(
                    text = status,
                    style = HintGreyText12
                )
            }
        }
    }
}