package com.maddcog.android.settings.components

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.Checkbox
import androidx.compose.material.CheckboxDefaults
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import com.maddcog.android.baseui.ui.theme.*
import com.maddcog.android.domain.entities.UserGame

@Composable
fun UserGameRegionCard(game: UserGame) {
    val checkedState = remember { mutableStateOf(false) }

    Card(
        shape = RoundedCornerShape(15),
        backgroundColor = BoxDarkGrey,
        modifier = Modifier
            .fillMaxWidth()
            .height(Spacing_60)
            .clickable {
                checkedState.value = !checkedState.value
            }
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = Spacing_20)
        ) {
            Column(verticalArrangement = Arrangement.Center) {
                game.userName?.let {
                    Text(
                        text = it,
                        fontSize = Text_14,
                        fontWeight = FontWeight.Bold,
                        color = White
                    )
                }
                Spacer(modifier = Modifier.height(Spacing_3))
                Text(
                    text = game.region,
                    fontSize = Text_12,
                    color = White
                )
            }

            Spacer(modifier = Modifier.weight(1f))

            Checkbox(
                checked = checkedState.value, onCheckedChange = {
                    checkedState.value = it
                },
                colors = CheckboxDefaults.colors(
                    checkedColor = BoxDarkGrey,
                    checkmarkColor = White,
                    uncheckedColor = White
                )
            )
        }
    }
}