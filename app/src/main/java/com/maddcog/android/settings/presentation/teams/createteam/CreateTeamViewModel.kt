package com.maddcog.android.settings.presentation.teams.createteam

import androidx.lifecycle.viewModelScope
import com.maddcog.android.baseui.presentation.BaseViewModel
import com.maddcog.android.domain.api.IDashboardRepository
import com.maddcog.android.domain.entities.team.Team
import com.maddcog.android.domain.usecase.TeamUseCase
import com.maddcog.android.settings.ISettingsRouter
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CreateTeamViewModel @Inject constructor(
    private val useCase: TeamUseCase,
    private val repository: IDashboardRepository,
    private val router: ISettingsRouter
) : BaseViewModel() {

    val teamsInfo = repository.teamInfo

    var teamList = teamsInfo.value?.toMutableList()

    fun leaveTeam(team: Team) {
        viewModelScope.launch {
            team.teamId?.let { useCase.leaveTeam(it) }
        }
        teamList?.remove(team)
    }

    fun saveTeamName(name: String) {
        useCase.saveTeamName(name)
    }

    fun navigateToAddTeam() {
        navigateTo(router.navigateToAddTeam())
    }

    fun navigateToOneTeam() {
        navigateTo(router.navigateToOneTeam())
    }

    fun getTeams() {
        viewModelScope.launch {
            useCase.getTeams()
        }
    }
}