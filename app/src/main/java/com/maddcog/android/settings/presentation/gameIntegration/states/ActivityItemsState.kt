package com.maddcog.android.settings.presentation.gameIntegration.states

import com.maddcog.android.domain.entities.ActivityItem

data class ActivityItemsState(
    val isLoading: Boolean = false,
    val items: List<ActivityItem> = emptyList(),
    val error: String = "",
)