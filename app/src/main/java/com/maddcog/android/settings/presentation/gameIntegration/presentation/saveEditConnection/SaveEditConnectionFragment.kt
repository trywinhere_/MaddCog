package com.maddcog.android.settings.presentation.gameIntegration.presentation.saveEditConnection

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.compose.ui.platform.ComposeView
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.setFragmentResultListener
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.maddcog.android.R
import com.maddcog.android.baseui.extensions.HTTP_404
import com.maddcog.android.baseui.presentation.BaseFragment
import com.maddcog.android.baseui.presentation.BaseViewModel
import com.maddcog.android.baseui.ui.theme.MaddcogTheme
import com.maddcog.android.settings.SettingsFragment.Companion.GAME_ADDED_KEY
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SaveEditConnectionFragment : BaseFragment() {

    companion object {
        const val LOL = "League of Legends"
        const val SELECT_REGION = "Select Region"

        const val SELECT_REGION_KEY = "select:region:key"
        const val SELECT_REGION_DATA_KEY = "select:region:data:key"
    }

    private val viewModel: SaveEditConnectionViewModel by viewModels()
    private val args by navArgs<SaveEditConnectionFragmentArgs>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.initContent(args.gameNumber, args.gameName, args.poster)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = ComposeView(requireContext())
        view.setContent {
            MaddcogTheme {
                SaveEditConnectionScreen(
                    viewModel
                )
            }
        }

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setFragmentResultListener(SELECT_REGION_KEY) { _, bundle ->
            val content = bundle.getString(SELECT_REGION_DATA_KEY)
            viewModel.saveRegion(content.toString())
        }
    }

    override fun withViewModel(): BaseViewModel = viewModel.apply {
        saveConnection.collectWithState {
            it.isSuccessful {
                setFragmentResult(GAME_ADDED_KEY, Bundle())
                Toast.makeText(requireContext(), "Game saved!", Toast.LENGTH_SHORT).show()
                viewModel.navigateBack(R.id.add_integration_graph)
            }
            it.isError { error ->
                if (error.message == HTTP_404) {
                    viewModel.errorDialog.value = true
                } else {
                    Toast.makeText(requireContext(), error.message, Toast.LENGTH_SHORT).show()
                }
            }
        }
    }
}