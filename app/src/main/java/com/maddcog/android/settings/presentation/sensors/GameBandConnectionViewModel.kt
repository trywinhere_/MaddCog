package com.maddcog.android.settings.presentation.sensors

import androidx.compose.runtime.getValue
import com.maddcog.android.baseui.presentation.BaseViewModel
import com.maddcog.android.data.bluetoothle.IBleReceiveManager
import com.maddcog.android.settings.ISettingsRouter
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.viewModelScope
import com.maddcog.android.data.bluetoothle.ConnectionState
import com.maddcog.android.domain.api.ISharedPreferencesStorage
import com.maddcog.android.domain.utils.ble.Resource
import kotlinx.coroutines.launch

@HiltViewModel
class GameBandConnectionViewModel @Inject constructor(
    private val router: ISettingsRouter,
    private val bleReceiveManager: IBleReceiveManager,
    private val storage: ISharedPreferencesStorage,
) : BaseViewModel() {

    var isDeviceConnected: Boolean = false

    init {
        isDeviceConnected = storage.getDeviceStatus()
    }

    fun navigateToStartNewSession() {
        navigateTo(router.navigateToStartNewSession())
    }

    var initializingMessage by mutableStateOf<String?>(null)
        private set

    var errorMessage by mutableStateOf<String?>(null)
        private set

    var connectionState by mutableStateOf<ConnectionState>(ConnectionState.Uninitialized)

    private fun subscribeToChanges() {
        viewModelScope.launch {
            bleReceiveManager.data.collect { result ->
                when (result) {
                    is Resource.Success -> {
                        connectionState = result.data.connectionState
                    }

                    is Resource.Loading -> {
                        initializingMessage = result.message
                        connectionState = ConnectionState.CurrentlyInitializing
                    }

                    is Resource.Error -> {
                        errorMessage = result.errorMessage
                        connectionState = ConnectionState.Uninitialized
                    }
                }
            }
        }
    }

    fun disconnect() {
        bleReceiveManager.disconnect()
    }

    fun reconnect() {
        bleReceiveManager.reconnect()
    }

    fun initializeConnection() {
        errorMessage = null
        subscribeToChanges()
        bleReceiveManager.startReceiving()
    }

    fun connectToGameBand() {
        bleReceiveManager.connectToGameBand()
    }

    fun saveDevice() {
        storage.saveDeviceStatus(true)
    }
}