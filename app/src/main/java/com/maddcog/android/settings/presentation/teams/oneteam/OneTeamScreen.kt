package com.maddcog.android.settings.presentation.teams.oneteam

import android.os.Bundle
import androidx.compose.animation.animateColorAsState
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.filled.Done
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.TextFieldValue
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import com.maddcog.android.R
import com.maddcog.android.baseui.components.BasicTopAppBar
import com.maddcog.android.baseui.presentation.NativeAlertDialog
import com.maddcog.android.baseui.ui.theme.*
import com.maddcog.android.settings.components.*

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun OneTeamScreen(viewModel: OneTeamViewModel) {

    val teamName = viewModel.getTeamName()
    val textState = remember { mutableStateOf(teamName?.let { TextFieldValue(it) }) }
    val team = viewModel.teams?.find { it.teamName == teamName }
    val teammembersList = team?.teamMembers
    val visibleDeleteDialog = remember { mutableStateOf(false) }
    val visibleAddNewTeammateDialog = remember { mutableStateOf(false) }
    val visibleLeaveTeamDialog = remember { mutableStateOf(false) }
    val summonerName = remember { mutableStateOf(TextFieldValue("")) }
    val firebaseAnalytics: FirebaseAnalytics = Firebase.analytics

    Column(modifier = Modifier.fillMaxSize()) {
        BasicTopAppBar(title = R.string.teams_header) {
            viewModel.navigateBack()
        }
        LazyColumn(
            modifier = Modifier
                .background(Color.Black)
                .fillMaxSize()
                .padding(Spacing_12)
        ) {

            item {
                SectionHeader(name = R.string.team_name_text)
                Spacer(modifier = Modifier.height(Spacing_10))
                SearchBoxWithPen(state = textState)
                Spacer(modifier = Modifier.height(Spacing_30))
                SectionHeader(name = R.string.teammates_header)
            }

            items(teammembersList?.size!!) { index ->
                val teamMemberItem = teammembersList.getOrNull(index)

                if (teamMemberItem != null) {
                    key(teamMemberItem) {

                        val dismissState = rememberDismissState()
                        LaunchedEffect(dismissState.currentValue) {
                            if (dismissState.currentValue == DismissValue.DismissedToStart) {
                                println("ACT ITEM " + teamMemberItem.username)
                                visibleDeleteDialog.value = true
                                dismissState.snapTo(DismissValue.Default)
                            }
                        }

                        if (visibleDeleteDialog.value) {
                            NativeAlertDialog(
                                onExit = {
                                    visibleDeleteDialog.value = false
                                },
                                onSuccess = {
                                    teamMemberItem.teamUserId?.let { viewModel.removeTeammate(it) }
                                    firebaseAnalytics.logEvent(
                                        "settings_teams_remove_teammate",
                                        Bundle().apply {
                                            this.putString(
                                                SETTINGS_TEAMS_REMOVE_TEAMMATE, "settings_teams_remove_teammate"
                                            )
                                        }
                                    )
                                    teammembersList.remove(teamMemberItem)
                                    visibleDeleteDialog.value = false
                                },
                                disclaimerFirst = R.string.teammates_remove_header,
                                disclaimerSecond = R.string.teammates_remove_message,
                                successName = R.string.teammates_remove_button
                            )
                        }

                        SwipeToDismiss(
                            state = dismissState,
                            directions = setOf(DismissDirection.EndToStart),
                            background = {
                                val direction =
                                    dismissState.dismissDirection ?: return@SwipeToDismiss
                                val color by animateColorAsState(
                                    targetValue = when (dismissState.targetValue) {
                                        DismissValue.Default -> Color.Black
                                        DismissValue.DismissedToEnd -> Color.Green
                                        DismissValue.DismissedToStart -> Color.Red
                                    }
                                )
                                val icon = when (direction) {
                                    DismissDirection.StartToEnd -> Icons.Default.Done
                                    DismissDirection.EndToStart -> Icons.Default.Delete
                                }

                                val alignment = when (direction) {
                                    DismissDirection.StartToEnd -> Alignment.CenterStart
                                    DismissDirection.EndToStart -> Alignment.CenterEnd
                                }

                                Box(
                                    modifier = Modifier
                                        .fillMaxSize()
                                        .background(color)
                                        .padding(horizontal = Spacing_12),
                                    contentAlignment = alignment
                                ) {
                                    Icon(icon, contentDescription = "Icon")
                                }
                            },
                            dismissContent = {
                                TeammateItem(
                                    name = teamMemberItem.username,
                                    status = viewModel.isUserOwner(teamMemberItem),
                                    visibleDialog = visibleDeleteDialog
                                )
                            }
                        )
                        Spacer(modifier = Modifier.height(Spacing_5))
                    }
                }
            }
            item {
                Spacer(modifier = Modifier.height(Spacing_5))
                SimpleMenuButton(name = R.string.add_new_teammate) {
                    visibleAddNewTeammateDialog.value = true
                }
                Spacer(modifier = Modifier.height(Spacing_30))
                TextButton(
                    onClick = { visibleLeaveTeamDialog.value = true },
                    modifier = Modifier.fillMaxWidth()
                ) {
                    Text(
                        text = stringResource(id = R.string.leave_team),
                        color = Red,
                        fontSize = Text_16
                    )
                }
            }
        }
        if (visibleLeaveTeamDialog.value) {
            NativeAlertDialog(
                onExit = {
                    visibleDeleteDialog.value = false
                },
                onSuccess = {
                    team?.teamId?.let { viewModel.leaveTeam(it) }
                    firebaseAnalytics.logEvent(
                        "settings_teams_leave_team",
                        Bundle().apply {
                            this.putString(
                                SETTINGS_TEAMS_LEAVE_TEAM, "settings_teams_leave_team"
                            )
                        }
                    )
                    visibleDeleteDialog.value = false
                },
                disclaimerFirst = R.string.confirm_leave_team,
                disclaimerSecond = R.string.confirm_leave_team_text,
                successName = R.string.confirm_leave_team_button
            )
        }
        if (visibleAddNewTeammateDialog.value) {
            NativeAlertDialogWithTextField(
                onExit = { visibleAddNewTeammateDialog.value = false },
                onSuccess = { viewModel.inviteUser(summonerName.value.text) },
                successName = R.string.invite,
                disclaimerFirst = R.string.enter_user_name_title,
                disclaimerSecond = R.string.enter_user_name_message,
                inputText = summonerName,
                hintText = R.string.invite_teammate_hint
            )
        }
    }
}