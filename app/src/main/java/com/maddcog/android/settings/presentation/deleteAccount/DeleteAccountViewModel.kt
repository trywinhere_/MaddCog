package com.maddcog.android.settings.presentation.deleteAccount

import androidx.lifecycle.viewModelScope
import com.maddcog.android.baseui.model.StatefulData
import com.maddcog.android.baseui.presentation.BaseViewModel
import com.maddcog.android.domain.usecase.SettingsUseCase
import com.maddcog.android.settings.ISettingsRouter
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class DeleteAccountViewModel @Inject constructor(
    private val useCase: SettingsUseCase,
    private val router: ISettingsRouter,
) : BaseViewModel() {

    private val _deleteUser = statefulSharedFlow<Unit>()
    val deleteUser: Flow<StatefulData<Unit>>
        get() = _deleteUser

    fun deleteUser() {
        _deleteUser.fetch {
            useCase.deleteUser()
        }
    }

    fun dropToAuth() {
        viewModelScope.launch {
            useCase.performLogout()
            withContext(Dispatchers.Main) {
                navigateTo(router.goToAuth())
            }
        }
    }
}