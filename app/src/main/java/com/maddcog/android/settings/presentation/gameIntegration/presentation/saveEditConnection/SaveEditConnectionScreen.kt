package com.maddcog.android.settings.presentation.gameIntegration.presentation.saveEditConnection

import android.os.Bundle
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import com.maddcog.android.R
import com.maddcog.android.baseui.components.BaseTextField
import com.maddcog.android.baseui.components.BasicTopAppBarWithRightButton
import com.maddcog.android.baseui.components.ChangeableMenuButton
import com.maddcog.android.baseui.presentation.NativeErrorDialog
import com.maddcog.android.baseui.ui.theme.Black
import com.maddcog.android.baseui.ui.theme.SETTINGS_ADD_GAME_INTEGRATION
import com.maddcog.android.baseui.ui.theme.Spacing_12
import com.maddcog.android.settings.components.GameMenuButton
import com.maddcog.android.settings.components.SectionHeader
import com.maddcog.android.settings.presentation.gameIntegration.presentation.saveEditConnection.SaveEditConnectionFragment.Companion.LOL

@Composable
fun SaveEditConnectionScreen(
    viewModel: SaveEditConnectionViewModel
) {
    val firebaseAnalytics: FirebaseAnalytics = Firebase.analytics
    Column(
        Modifier
            .background(Black)
    ) {
        BasicTopAppBarWithRightButton(
            title = R.string.connected_games_text,
            navigateBack = {
                viewModel.navigateBack()
            }, rightButtonClick = {
            viewModel.saveGame()
            firebaseAnalytics.logEvent(
                "settings_add_game_integration",
                Bundle().apply {
                    this.putString(
                        SETTINGS_ADD_GAME_INTEGRATION, "settings_add_game_integration"
                    )
                }
            )
        },
            rightButtonText = R.string.save_btn,
            true
        )
        LazyColumn(
            modifier = Modifier
                .background(Color.Black)
                .fillMaxSize()
                .padding(Spacing_12)
        ) {
            item {
                SectionHeader(name = R.string.pick_game_text)
                GameMenuButton(
                    name = viewModel.savedGame.name,
                    imageUrl = viewModel.savedGame.poster
                ) {
                    viewModel.openPickGame(viewModel.savedGame.number)
                }
                SectionHeader(name = R.string.username_text)
                BaseTextField(placeholder = R.string.user_name_placeholder, saveText = {
                    viewModel.saveText(it)
                })
                if (viewModel.savedGame.name == LOL) {
                    SectionHeader(name = R.string.region_text)
                    ChangeableMenuButton(name = viewModel.savedGame.region) {
                        viewModel.openSelectRegion()
                    }
                }
                if (viewModel.errorDialog.value) {
                    NativeErrorDialog(
                        onExit = {
                            viewModel.errorDialog.value = false
                        },
                        disclaimerFirst = R.string.invalid_username_error,
                        disclaimerSecond = R.string.invalid_username_error_message,
                    )
                }
            }
        }
    }
}