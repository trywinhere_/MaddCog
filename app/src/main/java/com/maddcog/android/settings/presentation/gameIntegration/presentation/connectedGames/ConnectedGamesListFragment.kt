package com.maddcog.android.settings.presentation.gameIntegration.presentation.connectedGames

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.ui.platform.ComposeView
import androidx.fragment.app.setFragmentResultListener
import androidx.fragment.app.viewModels
import com.maddcog.android.baseui.presentation.BaseFragment
import com.maddcog.android.baseui.presentation.BaseViewModel
import com.maddcog.android.baseui.ui.theme.MaddcogTheme
import com.maddcog.android.settings.SettingsFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ConnectedGamesListFragment : BaseFragment() {

    private val viewModel: ConnectedGamesListViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = ComposeView(requireContext())
        view.setContent {
            MaddcogTheme {
                ConnectedGamesListScreen(
                    viewModel,
                    removeItem = {
                        viewModel.deleteGame(it)
                    }
                )
            }
        }

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setFragmentResultListener(SettingsFragment.GAME_ADDED_KEY) { _, _ ->
            viewModel.getGames()
        }
    }

    override fun withViewModel(): BaseViewModel = viewModel.apply {
        deleteGame.collectWithState {
            it.isSuccessful {
                getGames()
            }
        }
    }
}