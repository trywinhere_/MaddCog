package com.maddcog.android.settings.presentation.gameIntegration.presentation.connectedGames

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.viewModelScope
import com.maddcog.android.baseui.model.Resource
import com.maddcog.android.baseui.model.StatefulData
import com.maddcog.android.baseui.presentation.BaseViewModel
import com.maddcog.android.domain.api.ISharedPreferencesStorage
import com.maddcog.android.domain.entities.UserGame
import com.maddcog.android.domain.usecase.GetUserGamesUseCase
import com.maddcog.android.domain.usecase.SettingsUseCase
import com.maddcog.android.settings.ISettingsRouter
import com.maddcog.android.settings.presentation.gameIntegration.presentation.pickGame.PickGameKey
import com.maddcog.android.settings.presentation.gameIntegration.states.ConnectedGamesState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@HiltViewModel
class ConnectedGamesListViewModel @Inject constructor(
    private val useCase: GetUserGamesUseCase,
    private val settingsUseCase: SettingsUseCase,
    private val router: ISettingsRouter,
    private val storage: ISharedPreferencesStorage
) : BaseViewModel() {

    private val _state = mutableStateOf(ConnectedGamesState())
    val state: State<ConnectedGamesState> = _state

    private val _deleteGame = statefulSharedFlow<Unit>()
    val deleteGame: Flow<StatefulData<Unit>>
        get() = _deleteGame

    init {
        getGames()
    }

    fun getGames() {
        useCase().onEach { result ->
            when (result) {
                is Resource.Success -> {
                    _state.value = ConnectedGamesState(games = result.data?.toMutableList() ?: mutableListOf())
                }
                is Resource.Error -> {
                    _state.value = ConnectedGamesState(error = result.message ?: "An unexpected error occurred")
                }
                is Resource.Loading -> {
                    _state.value = ConnectedGamesState(isLoading = true)
                }
            }
        }.launchIn(viewModelScope)
    }

    fun openPickGame() {
        navigateTo(router.openPickGame(PickGameKey.NewConnection))
    }

    fun deleteGame(game: UserGame) {
        _deleteGame.fetch {
            settingsUseCase.deleteConnectedGame(game)
        }
    }

    fun saveGameName(gameName: String) {
        storage.saveGameName(gameName)
    }

    fun goToStartNewSession() {
        navigateTo(router.navigateToStartNewSession())
    }
}