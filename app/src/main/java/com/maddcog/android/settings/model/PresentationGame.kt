package com.maddcog.android.settings.model

data class PresentationGame(
    val number: Int,
    val name: String,
    val poster: String,
    var username: String,
    var region: String,
)