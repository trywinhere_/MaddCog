package com.maddcog.android.settings.presentation.selectLanguage

import com.maddcog.android.baseui.model.StatefulData
import com.maddcog.android.baseui.presentation.BaseViewModel
import com.maddcog.android.domain.api.ISharedPreferencesStorage
import com.maddcog.android.settings.presentation.selectLanguage.model.Language
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

@HiltViewModel
class SelectLanguageViewModel @Inject constructor(
    private val storage: ISharedPreferencesStorage,
) : BaseViewModel() {

    private val _languages = statefulSharedFlow<List<Language>>()
    val languages: Flow<StatefulData<List<Language>>>
        get() = _languages

    private var _data: List<Language>? = null
    val data: List<Language>
        get() = _data!!

    fun initContent() {
        _languages.fetch {
            val newData = storage.getLanguageList()
            _data = newData
            newData
        }
    }
}