package com.maddcog.android.settings.presentation.gameIntegration.presentation.selectRegion

import com.maddcog.android.baseui.model.StatefulData
import com.maddcog.android.baseui.presentation.BaseViewModel
import com.maddcog.android.domain.api.ISharedPreferencesStorage
import com.maddcog.android.domain.entities.RegionType
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

@HiltViewModel
class SelectRegionViewModel @Inject constructor(
    private val storage: ISharedPreferencesStorage,
) : BaseViewModel() {

    private val _regions = statefulSharedFlow<List<RegionType>>()
    val region: Flow<StatefulData<List<RegionType>>>
        get() = _regions

    private var _data: List<RegionType>? = null
    val data: List<RegionType>
        get() = _data!!

    fun initContent() {
        _regions.fetch {
            val newData = storage.getRegionList()
            _data = newData
            newData
        }
    }
}