package com.maddcog.android.settings.presentation.gameIntegration.states

import com.maddcog.android.domain.entities.UserGame

data class ConnectedGamesState(
    val isLoading: Boolean = false,
    val games: MutableList<UserGame> = mutableListOf(),
    val error: String = "",
)