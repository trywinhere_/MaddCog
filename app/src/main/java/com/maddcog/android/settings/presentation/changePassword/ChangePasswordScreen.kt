package com.maddcog.android.settings.presentation.changePassword

import android.os.Bundle
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import com.maddcog.android.R
import com.maddcog.android.baseui.components.BasePasswordTextField
import com.maddcog.android.baseui.components.BasicTopAppBar
import com.maddcog.android.baseui.components.SimpleButton
import com.maddcog.android.baseui.ui.theme.*
import com.maddcog.android.settings.components.SectionHeader

@Composable
fun ChangePasswordScreen(
    viewModel: ChangePasswordViewModel,
) {
    val firebaseAnalytics: FirebaseAnalytics = Firebase.analytics
    Column(
        Modifier
            .background(Black)
    ) {
        BasicTopAppBar(title = R.string.change_password_text) {
            viewModel.navigateBack()
        }
        LazyColumn(
            modifier = Modifier
                .background(Color.Black)
                .fillMaxSize()
                .padding(Spacing_12)
        ) {
            item {
                SectionHeader(name = R.string.current_password)
                BasePasswordTextField(placeholder = R.string.password_placeholder, saveText = {
                    viewModel.saveOldPassword(it)
                })
                SectionHeader(name = R.string.new_password)
                BasePasswordTextField(placeholder = R.string.password_placeholder, saveText = {
                    viewModel.saveNewPassword(it)
                    firebaseAnalytics.logEvent(
                        "settings_change_password",
                        Bundle().apply {
                            this.putString(
                                SETTINGS_CHANGE_PASSWORD, "settings_change_password"
                            )
                        }
                    )
                })
                Spacer(modifier = Modifier.height(Spacing_16))
                SimpleButton(name = R.string.change_password_btn) {
                    viewModel.changePassword()
                    firebaseAnalytics.logEvent(
                        "settings_change_password",
                        Bundle().apply {
                            this.putString(
                                SETTINGS_CHANGE_PASSWORD, "settings_change_password"
                            )
                        }
                    )
                }
            }
        }
    }
}