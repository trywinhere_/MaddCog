package com.maddcog.android.settings.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.layoutId
import androidx.compose.ui.res.painterResource
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.ConstraintSet
import com.maddcog.android.baseui.ui.theme.BoxDarkGrey
import com.maddcog.android.baseui.ui.theme.HintGreyText16
import com.maddcog.android.baseui.ui.theme.Spacing_0
import com.maddcog.android.baseui.ui.theme.Spacing_10
import com.maddcog.android.baseui.ui.theme.Spacing_12
import com.maddcog.android.baseui.ui.theme.Spacing_20
import com.maddcog.android.baseui.ui.theme.Spacing_8
import com.maddcog.android.baseui.ui.theme.WhiteText16
import com.maddcog.android.baseui.ui.theme.arrow_right

@Composable
fun MenuButtonWithBottomValue(
    icon: Int,
    name: String,
    value: String,
    navigateTo: () -> Unit,
    saveGameName: () -> Unit
) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .clickable {
                saveGameName()
                navigateTo()
            },
        shape = RoundedCornerShape(Spacing_10),
        elevation = Spacing_0,
        backgroundColor = BoxDarkGrey
    ) {
        val constraints = ConstraintSet {
            val menuOption = createRefFor("menu_option")
            val menuValue = createRefFor("menu_value")
            val arrow = createRefFor("arrow")

            constrain(menuOption) {
                top.linkTo(parent.top)
                start.linkTo(parent.start)
                bottom.linkTo(menuValue.top)
            }
            constrain(menuValue) {
                top.linkTo(menuOption.bottom)
                bottom.linkTo(parent.bottom)
                start.linkTo(menuOption.start)
            }
            constrain(arrow) {
                top.linkTo(parent.top)
                end.linkTo(parent.end)
                bottom.linkTo(parent.bottom)
            }
        }
        ConstraintLayout(
            constraints,
            modifier = Modifier
                .fillMaxWidth()
                .background(color = BoxDarkGrey)
                .padding(horizontal = Spacing_12, vertical = Spacing_8)
        ) {
            Text(
                text = name,
                style = WhiteText16,
                modifier = Modifier.layoutId("menu_option")
            )
            Text(
                text = value,
                style = HintGreyText16,
                modifier = Modifier.layoutId("menu_value").padding(end = Spacing_20)
            )
            Image(
                painterResource(id = icon),
                contentDescription = arrow_right,
                modifier = Modifier.layoutId("arrow")
            )
        }
    }
}