package com.maddcog.android.settings.presentation.deleteAccount

import android.os.Bundle
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import com.maddcog.android.R
import com.maddcog.android.baseui.components.BasicTopAppBar
import com.maddcog.android.baseui.components.SimpleButton
import com.maddcog.android.baseui.presentation.NativeAlertDialog
import com.maddcog.android.baseui.ui.theme.*

@Composable
fun DeleteAccountScreen(
    viewModel: DeleteAccountViewModel,
    deleteUser: () -> Unit,
) {
    var openDialog = remember { mutableStateOf(false) }
    val firebaseAnalytics: FirebaseAnalytics = Firebase.analytics

    Column(
        Modifier
            .background(Black)
    ) {
        BasicTopAppBar(title = R.string.delete_account_text) {
            viewModel.navigateBack()
        }
        LazyColumn(
            modifier = Modifier
                .background(Color.Black)
                .fillMaxSize()
                .padding(Spacing_12)
        ) {
            item {
                Text(
                    text = stringResource(id = R.string.delete_account_main_disclaimer),
                    modifier = Modifier.align(Alignment.Start),
                    style = RedText16
                )
                Spacer(modifier = Modifier.height(Spacing_16))
                SimpleButton(name = R.string.delete_account) {
                    openDialog.value = true
                }
                if (openDialog.value) {
                    NativeAlertDialog(
                        onExit = {
                            openDialog.value = false
                        },
                        onSuccess = {
                            deleteUser()
                            firebaseAnalytics.logEvent(
                                "settings_delete_account",
                                Bundle().apply {
                                    this.putString(
                                        SETTINGS_DELETE_ACCOUNT, "settings_delete_account"
                                    )
                                }
                            )
                        },
                        disclaimerFirst = R.string.delete_account_disclaimer_first,
                        disclaimerSecond = R.string.delete_account_disclaimer_second,
                        successName = R.string.delete_btn
                    )
                }
            }
        }
    }
}