package com.maddcog.android.settings.presentation.gameIntegration.presentation.pickGame

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.input.TextFieldValue
import com.maddcog.android.R
import com.maddcog.android.baseui.components.BasicTopAppBar
import com.maddcog.android.baseui.components.SearchBox
import com.maddcog.android.baseui.ui.theme.Black
import com.maddcog.android.baseui.ui.theme.Spacing_40
import com.maddcog.android.baseui.ui.theme.Spacing_6
import com.maddcog.android.settings.components.GridGameCard
import com.maddcog.android.settings.presentation.gameIntegration.states.ActivityItemsState

@Composable
fun PickGameScreen(
    pickGameKey: PickGameKey,
    viewModel: PickGameViewModel,
    state: ActivityItemsState,
    selectedGame: String?,
) {
    val textState = remember { mutableStateOf(TextFieldValue("")) }

    Column(
        Modifier
            .background(Black)
    ) {
        BasicTopAppBar(
            title = R.string.pick_game_text,
            navigateBack = {
                viewModel.navigateBack()
            }
        )
        SearchBox(textState)
        Box(Modifier.fillMaxSize()) {
            LazyVerticalGrid(
                columns = GridCells.Fixed(2),
                modifier = Modifier.padding(Spacing_6)
            ) {
                val filteredItems = viewModel.getFilteredList(textState.value.text)
                items(filteredItems) { item ->
                    GridGameCard(
                        imageUrl = item.imageSmall,
                        contentDescription = "",
                        title = item.name,
                        isSelected = item.id == selectedGame?.toInt()
                    ) {
                        when (pickGameKey) {
                            PickGameKey.StartNewGame -> {
                                viewModel.saveGameForNewSession(item)
                                viewModel.navigateBack()
                            }
                            PickGameKey.NewConnection -> { viewModel.openSaveConnection(item.id, item.name, item.imageSmall) }
                        }
                    }
                }
            }
            if (state.isLoading) {
                CircularProgressIndicator(
                    modifier = Modifier
                        .height(Spacing_40)
                        .width(Spacing_40)
                        .align(Alignment.Center),
                    color = Color.Red
                )
            }
        }
    }
}