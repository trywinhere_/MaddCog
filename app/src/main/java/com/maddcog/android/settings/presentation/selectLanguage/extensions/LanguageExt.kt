package com.maddcog.android.settings.presentation.selectLanguage.extensions

import android.content.Context
import com.maddcog.android.R
import com.maddcog.android.settings.presentation.selectLanguage.model.Language

fun Language.displayName(context: Context): String {
    return when (this) {
        Language.EN -> context.resources.getString(R.string.english_language)
        Language.FR -> context.resources.getString(R.string.french_language)
        Language.ES -> context.resources.getString(R.string.spanish_language)
    }
}