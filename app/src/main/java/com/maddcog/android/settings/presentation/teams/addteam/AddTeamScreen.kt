package com.maddcog.android.settings.presentation.teams.addteam

import android.os.Bundle
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.input.TextFieldValue
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import com.maddcog.android.R
import com.maddcog.android.baseui.components.BasicTopAppBarWithRightButton
import com.maddcog.android.baseui.ui.theme.*
import com.maddcog.android.settings.components.*

private const val ENTERTEAMNAME = "Enter a team name"

@Composable
fun AddTeamScreen(viewModel: AddTeamViewModel) {

    val state = viewModel.state.value
    val textState = remember { mutableStateOf(""?.let { TextFieldValue(ENTERTEAMNAME) }) }
    val firebaseAnalytics: FirebaseAnalytics = Firebase.analytics

    Column(modifier = Modifier.fillMaxSize()) {
        BasicTopAppBarWithRightButton(
            title = R.string.teams_header,
            navigateBack = {
                viewModel.navigateBack()
            }, rightButtonClick = {
            firebaseAnalytics.logEvent(
                "settings_teams_create_team",
                Bundle().apply {
                    this.putString(
                        SETTINGS_TEAMS_CREATE_TEAMS, "settings_teams_create_team"
                    )
                }
            )
        },
            rightButtonText = R.string.create_team_text,
            isVisible = true
        )
        LazyColumn(
            modifier = Modifier
                .background(Color.Black)
                .fillMaxSize()
                .padding(horizontal = Spacing_12)
                .weight(1f)
        ) {
            item {
                SectionHeader(name = R.string.team_name_text)
                Spacer(modifier = Modifier.height(Spacing_10))
                SearchBoxWithPen(state = textState)
                Spacer(modifier = Modifier.height(Spacing_30))
                SectionHeader(name = R.string.username_text)
            }
            itemsIndexed(items = state.games) { _, item ->
                UserGameRegionCard(item)
                Spacer(modifier = Modifier.height(Spacing_5))
            }
        }

        if (state.isLoading) {
            Row(
                modifier = Modifier.fillMaxSize(),
                horizontalArrangement = Arrangement.Center,
                verticalAlignment = Alignment.CenterVertically
            ) {
                CircularProgressIndicator(
                    modifier = Modifier
                        .height(Spacing_40)
                        .width(Spacing_40)
                        .fillMaxSize(),
                    color = Color.Red
                )
            }
        }
    }
}