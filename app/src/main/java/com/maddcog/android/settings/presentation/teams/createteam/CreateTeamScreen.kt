package com.maddcog.android.settings.presentation.teams.createteam

import androidx.compose.animation.animateColorAsState
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.filled.Done
import androidx.compose.material3.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import com.maddcog.android.R
import com.maddcog.android.baseui.components.BasicTopAppBarWithRightButton
import com.maddcog.android.baseui.presentation.NativeAlertDialog
import com.maddcog.android.baseui.ui.theme.*
import com.maddcog.android.settings.components.MenuButtonWithString
import com.maddcog.android.settings.components.SimpleMenuButton

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun CreateTeamScreen(viewModel: CreateTeamViewModel) {

    val visibleDeleteDialog = remember { mutableStateOf(false) }

    Column(modifier = Modifier.fillMaxSize()) {
        BasicTopAppBarWithRightButton(
            title = R.string.teams_header,
            navigateBack = {
                viewModel.navigateBack()
            },
            rightButtonClick = {
                viewModel.navigateToAddTeam()
            },
            rightButtonText = R.string.add_btn,
            isVisible = true
        )
        LazyColumn(
            modifier = Modifier
                .background(Color.Black)
                .fillMaxSize()
                .weight(1f)
                .padding(Spacing_12)
        ) {
            if (viewModel.teamsInfo.value != null) {
                items(viewModel.teamList?.size!!) { index ->
                    val teamItem = viewModel.teamList!!.getOrNull(index)

                    if (teamItem != null) {
                        key(teamItem) {

                            val dismissState = rememberDismissState()
                            LaunchedEffect(dismissState.currentValue) {
                                if (dismissState.currentValue == DismissValue.DismissedToStart) {
                                    println("ACT ITEM " + teamItem.teamId)
                                    visibleDeleteDialog.value = true
                                    dismissState.snapTo(DismissValue.Default)
                                }
                            }

                            if (visibleDeleteDialog.value) {
                                NativeAlertDialog(
                                    onExit = {
                                        visibleDeleteDialog.value = false
                                    },
                                    onSuccess = {
                                        viewModel.leaveTeam(teamItem)
                                        visibleDeleteDialog.value = false
                                    },
                                    disclaimerFirst = R.string.confirm_leave_team,
                                    disclaimerSecond = R.string.confirm_leave_team_text,
                                    successName = R.string.confirm_leave_team_button
                                )
                            }

                            SwipeToDismiss(
                                state = dismissState,
                                directions = setOf(DismissDirection.EndToStart),
                                background = {
                                    val direction =
                                        dismissState.dismissDirection ?: return@SwipeToDismiss
                                    val color by animateColorAsState(
                                        targetValue = when (dismissState.targetValue) {
                                            DismissValue.Default -> Color.Black
                                            DismissValue.DismissedToEnd -> Color.Green
                                            DismissValue.DismissedToStart -> Color.Red
                                        }
                                    )
                                    val icon = when (direction) {
                                        DismissDirection.StartToEnd -> Icons.Default.Done
                                        DismissDirection.EndToStart -> Icons.Default.Delete
                                    }

                                    val alignment = when (direction) {
                                        DismissDirection.StartToEnd -> Alignment.CenterStart
                                        DismissDirection.EndToStart -> Alignment.CenterEnd
                                    }

                                    Box(
                                        modifier = Modifier
                                            .fillMaxSize()
                                            .background(color)
                                            .padding(horizontal = Spacing_12),
                                        contentAlignment = alignment
                                    ) {
                                        Icon(icon, contentDescription = "Icon")
                                    }
                                },
                                dismissContent = {
                                    MenuButtonWithString(name = teamItem.teamName, { }) {
                                        teamItem.teamName?.let { viewModel.saveTeamName(it) }
                                        viewModel.navigateToOneTeam()
                                    }
                                }
                            )
                            Spacer(modifier = Modifier.height(Spacing_16))
                        }
                    }
                }
            } else {
                item {
                    SimpleMenuButton(name = R.string.create_team_text) {
                        viewModel.navigateToAddTeam()
                    }
                }
            }
        }
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .background(Black)
        ) {
            Text(
                text = stringResource(id = R.string.join_team_text),
                style = WhiteText12Bold,
                modifier = Modifier.padding(Spacing_12)
            )
        }
    }
}