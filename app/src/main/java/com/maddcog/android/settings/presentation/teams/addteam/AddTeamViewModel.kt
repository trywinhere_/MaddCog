package com.maddcog.android.settings.presentation.teams.addteam

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.viewModelScope
import com.maddcog.android.baseui.model.Resource
import com.maddcog.android.baseui.presentation.BaseViewModel
import com.maddcog.android.domain.usecase.GetUserGamesUseCase
import com.maddcog.android.settings.presentation.gameIntegration.states.ConnectedGamesState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@HiltViewModel
class AddTeamViewModel @Inject constructor(
    private val useCase: GetUserGamesUseCase
) : BaseViewModel() {

    private val _state = mutableStateOf(ConnectedGamesState())
    val state: State<ConnectedGamesState> = _state

    fun getGames() {
        useCase().onEach { result ->
            when (result) {
                is Resource.Success -> {
                    _state.value = ConnectedGamesState(games = result.data?.toMutableList() ?: mutableListOf())
                }
                is Resource.Error -> {
                    _state.value = ConnectedGamesState(error = result.message ?: "An unexpected error occurred")
                }
                is Resource.Loading -> {
                    _state.value = ConnectedGamesState(isLoading = true)
                }
            }
        }.launchIn(viewModelScope)
    }
}