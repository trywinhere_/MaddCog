package com.maddcog.android.settings.components

import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import com.maddcog.android.baseui.ui.theme.Spacing_12
import com.maddcog.android.baseui.ui.theme.WhiteText16
import com.maddcog.android.baseui.ui.theme.WhiteText16Bold

@Composable
fun SectionHeader(
    name: Int
) {
    Text(
        text = stringResource(id = name),
        style = WhiteText16,
        modifier = Modifier.padding(vertical = Spacing_12)
    )
}

@Composable
fun BoldSectionHeader(
    modifier: Modifier = Modifier,
    name: Int
) {
    Text(
        text = stringResource(id = name),
        style = WhiteText16Bold,
        modifier = modifier.padding(vertical = Spacing_12)
    )
}