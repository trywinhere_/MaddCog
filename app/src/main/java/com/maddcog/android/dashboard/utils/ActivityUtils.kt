package com.maddcog.android.dashboard.utils

import android.text.format.DateFormat
import com.maddcog.android.R
import java.time.LocalDate
import java.util.*

private const val PATTERN_DAY_MONTH_YEAR = "dd.MM.yyyy"
private const val PATTERN_DAY_MONTH = "dd MMMM"
private const val PATTERN_YEAR_MONTH_DAY = "yyyy-MM-dd"
private const val CONVERT_MS = 1000
private const val WEEK = 7
private const val HOUR = "h"
private const val MIN = "min"

object ActivityUtils {

    var calendar: Calendar = Calendar.getInstance()

    fun timePresenter(activityDate: Long): String {
        val currentDate = DateFormat.format(PATTERN_DAY_MONTH_YEAR, calendar).toString()

        val date = Calendar.getInstance()
        val weekAgo = date.add(Calendar.DATE, -WEEK).toString()

        val activityDateString = DateFormat.format(PATTERN_DAY_MONTH_YEAR, activityDate * CONVERT_MS).toString()
        val isYestedayResult = isYesterday(activityDate)
        val activityDateTimeString = DateFormat.format(PATTERN_DAY_MONTH, activityDate * CONVERT_MS).toString()

        val formatted = DateFormat.format(PATTERN_YEAR_MONTH_DAY, activityDate * CONVERT_MS).toString()

        val myDate = LocalDate.parse(formatted)
        val dayOfWeek = myDate.dayOfWeek

        val lowerCase = dayOfWeek.toString().lowercase(Locale.getDefault())
        val firstCapitalize = lowerCase.substring(0, 1).uppercase() + lowerCase.substring(1)

        if (activityDateString == currentDate) {
            return (R.string.today).toString()
        } else if (isYestedayResult) {
            return (R.string.yesterday).toString()
        } else if (activityDateString >= weekAgo) {
            return firstCapitalize
        } else {
            return activityDateTimeString
        }
    }

    fun activityDurationPresenter(activityDuration: Int): String {
        val minutes = activityDuration / 60
        val hours = minutes / 60
        val minutesWithHour = minutes - 60

        if (hours >= 1) {
            return hours.toString() + " " + HOUR + " " + minutesWithHour.toString() + " " +
                MIN
        } else {
            return minutes.toString() + " " + MIN
        }
    }

    private fun isYesterday(date: Long): Boolean {
        val now = Calendar.getInstance()
        val cdate = Calendar.getInstance()
        cdate.setTimeInMillis(date)

        now.add(Calendar.DATE, -1)

        return now.get(Calendar.YEAR) == cdate.get(Calendar.YEAR) &&
            now.get(Calendar.MONTH) == cdate.get(Calendar.MONTH) &&
            now.get(Calendar.DATE) == cdate.get(Calendar.DATE)
    }
}