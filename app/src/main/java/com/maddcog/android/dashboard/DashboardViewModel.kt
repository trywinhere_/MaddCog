package com.maddcog.android.dashboard

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.viewModelScope
import com.maddcog.android.baseui.model.StatefulData
import com.maddcog.android.baseui.presentation.BaseViewModel
import com.maddcog.android.baseui.ui.theme.WIN
import com.maddcog.android.data.bluetoothle.ConnectionState
import com.maddcog.android.data.bluetoothle.IBleReceiveManager
import com.maddcog.android.data.extensions.roundNumbers
import com.maddcog.android.domain.api.IDashboardRepository
import com.maddcog.android.domain.entities.activity.UserActivity
import com.maddcog.android.domain.entities.user.UserProfileData
import com.maddcog.android.domain.usecase.DashboardUseCase
import com.maddcog.android.domain.usecase.SettingsUseCase
import com.maddcog.android.domain.utils.ble.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import javax.inject.Inject
import kotlin.math.roundToInt

private const val DEMOACTIVITY = "demoactivity"

@HiltViewModel
class DashboardViewModel @Inject constructor(
    private val useCase: SettingsUseCase,
    private val repository: IDashboardRepository,
    private val router: IDashboardRouter,
    private val dashboardUseCase: DashboardUseCase,
    private val bleReceiveManager: IBleReceiveManager
) : BaseViewModel() {

    var userActivityForDelete: UserActivity? = null

    val activityAllInfo = repository.allActivitiesInfo

    val userActivitiesNameForFilter = mutableListOf<String>()

    var chooseActivityName = ""

    val listActivities = activityAllInfo.value?.toMutableList()
    val listActivitiesForTrends = activityAllInfo.value?.toMutableList()

    private val _loadUser = statefulSharedFlow<UserProfileData?>()
    val loadUser: Flow<StatefulData<UserProfileData?>>
        get() = _loadUser

    private var _data: UserProfileData? = null
    val data: UserProfileData?
        get() = _data

    var connectionState by mutableStateOf<ConnectionState>(ConnectionState.Uninitialized)

    fun subscribeToChanges() {
        viewModelScope.launch {
            bleReceiveManager.data.collect { result ->
                when (result) {
                    is Resource.Success -> {
                        connectionState = result.data.connectionState
                    }

                    is Resource.Loading -> {
                        connectionState = ConnectionState.CurrentlyInitializing
                    }

                    is Resource.Error -> {
                        connectionState = ConnectionState.Uninitialized
                    }
                }
            }
        }
    }

    fun initContent() {
        _loadUser.fetch {
            val newData = useCase.loadUserProfileData()
            _data = newData
            newData
        }
    }

    fun getGradeAvg(listActivities: List<UserActivity>?): String {
        val listGrades = mutableListOf<Double>()
        listActivities?.forEach { activity ->
            if (activity.activityId.contains(DEMOACTIVITY)) {
                listGrades.add(0.0)
            } else {
                activity.match.myPlayer.grade.let { grade -> listGrades.add(grade) }
            }
        }
        val gradeAvg = listGrades.average()
        val result = gradeAvg.roundNumbers()
        return result.toString()
    }

    fun getFlowAvg(listActivities: List<UserActivity>?): String {
        val listFlow = mutableListOf<Double>()
        listActivities?.forEach { activity ->
            if (activity.activityId.contains(DEMOACTIVITY)) {
                listFlow.add(0.0)
            } else {
                activity.activitySummary.flowZoneHigh.let { flow -> listFlow.add(flow) }
            }
        }
        val flowAvg = listFlow.average()
        val result = flowAvg.roundNumbers()
        return result.roundToInt().toString() + " %"
    }

    fun getFatigueAvg(listActivities: List<UserActivity>?): String {
        val listFatigue = mutableListOf<Double>()
        listActivities?.forEach { activity ->
            if (activity.activityId.contains(DEMOACTIVITY)) {
                listFatigue.add(0.0)
            } else {
                activity.activitySummary.fatigueZoneHigh.let { fatigue -> listFatigue.add(fatigue) }
            }
        }
        val fatigueAvg = listFatigue.average()
        val result = fatigueAvg.roundNumbers()
        return result.roundToInt().toString() + " %"
    }

    fun getWinNumber(listActivities: List<UserActivity>?): String {
        val listAll = mutableListOf<String>()
        listActivities?.forEach { activity ->
            if (activity.activityId.contains(DEMOACTIVITY)) {
                listAll.add("loose")
            } else {
                activity.match.myPlayer.outcome.let { winNumber -> listAll.add(winNumber) }
            }
        }
        val winsOnly = listAll.filter { it == WIN }

        val all = listAll.size
        val winsCount = winsOnly.size

        val result = winsCount / all * 100
        val returnNumber = "$result %"

        return returnNumber
    }

    fun deleteActivity() {
        viewModelScope.launch {
            listActivities?.remove(userActivityForDelete)
            userActivityForDelete?.let { repository.deleteActivity(it) }
        }
    }

    fun deleteActivityTest(
        listActivities: MutableList<UserActivity>,
        userActivityForDelete: UserActivity
    ): Boolean {
        listActivities.remove(userActivityForDelete)
        return !listActivities.contains(userActivityForDelete)
    }

    fun navigateToSettings() {
        navigateTo(router.navigateToSettings())
    }

    fun navigateToTrendsDetails() {
        navigateTo(router.navigateToTrendsDetails())
    }

    fun navigateToTrendsDetailsWithFilter(filter: String) {
        navigateTo(router.navigateToTrendsDetailsWithFilter(filter))
    }

    fun navigateToGameSummary(userActivityId: String) {
        navigateTo(router.navigateToGameSummary(userActivityId))
    }

    fun navigateToGameBandConnection() {
        navigateTo(router.navigateToOpenGameBandConnection())
    }

    fun navigateToStartNewSession() {
        navigateTo(router.navigateToStartNewSession())
    }

    fun saveGameName(title: String) {
        dashboardUseCase.saveGameName(title)
    }

    fun saveGameUrl(url: String) {
        dashboardUseCase.saveGameUrl(url)
    }

    fun getSavedDeviceStatus(): Boolean {
        return dashboardUseCase.getSavedStatus()
    }
}