package com.maddcog.android.dashboard

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion.CenterEnd
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import com.maddcog.android.R
import com.maddcog.android.baseui.presentation.NativeAlertDialog
import com.maddcog.android.baseui.ui.theme.*
import com.maddcog.android.dashboard.components.AdviceText
import com.maddcog.android.dashboard.components.ConnectGameBandButton
import com.maddcog.android.dashboard.components.FilterOnDialog
import com.maddcog.android.dashboard.components.GameCard
import com.maddcog.android.dashboard.components.Header
import com.maddcog.android.dashboard.components.TrendCard
import com.maddcog.android.data.bluetoothle.ConnectionState
import com.maddcog.android.data.extensions.getDashboardInsightText

private const val CSName = "Counter-Strike: Global Offensive"

@SuppressLint("CoroutineCreationDuringComposition")
@OptIn(
    ExperimentalFoundationApi::class,
    ExperimentalAnimationApi::class, ExperimentalMaterialApi::class
)
@Composable
fun Dashboard(
    wasDeviceSaved: Boolean,
    connectionState: ConnectionState,
    dashboardViewModel: DashboardViewModel,
    navigateToSettings: () -> Unit,
    removeActivity: () -> Unit,
    onGradeClick: () -> Unit,
    onWinRateClick: () -> Unit,
    onFatigueClick: () -> Unit,
    onFlowClick: () -> Unit,
    onGameButtonClicked: () -> Unit
) {
    val firebaseAnalytics: FirebaseAnalytics = Firebase.analytics
    val visibleDeleteDialog = remember { mutableStateOf(false) }
    val visibleDialog = remember { mutableStateOf(false) }
    val checkedStateAll = remember { mutableStateOf(false) }

    val flowPicture =
        if (dashboardViewModel.getFlowAvg(dashboardViewModel.listActivitiesForTrends) == "0 %") painterResource(
            id = R.drawable.ic_minus_flow
        )
        else painterResource(id = R.drawable.ic_flow)

    val fatiguePicture =
        if (dashboardViewModel.getFatigueAvg(dashboardViewModel.listActivitiesForTrends) == "0 %") painterResource(
            id = R.drawable.ic_minus_fatigue
        )
        else painterResource(id = R.drawable.ic_fatique)

    val winRatePicture =
        if (dashboardViewModel.getWinNumber(dashboardViewModel.listActivitiesForTrends) == "0 %") painterResource(
            id = R.drawable.ic_minus_win_rate
        )
        else painterResource(id = R.drawable.ic_minus_win_rate)

    val gradePicture =
        if (dashboardViewModel.getGradeAvg(dashboardViewModel.listActivitiesForTrends) == "0.0") painterResource(
            id = R.drawable.ic_minus_grade
        )
        else painterResource(id = R.drawable.ic_minus_grade)

    val textForFilter =
        if (dashboardViewModel.chooseActivityName == "") stringResource(id = R.string.all_activities)
        else dashboardViewModel.chooseActivityName

    val gradeTrendText =
        if (dashboardViewModel.getGradeAvg(dashboardViewModel.listActivitiesForTrends) == "0.0") ""
        else dashboardViewModel.getGradeAvg(dashboardViewModel.listActivitiesForTrends)

    val flowTrendText =
        if (dashboardViewModel.getFlowAvg(dashboardViewModel.listActivitiesForTrends) == "0 %") ""
        else dashboardViewModel.getFlowAvg(dashboardViewModel.listActivitiesForTrends)

    val fatigueTrendText =
        if (dashboardViewModel.getFatigueAvg(dashboardViewModel.listActivitiesForTrends) == "0 %") ""
        else dashboardViewModel.getFatigueAvg(dashboardViewModel.listActivitiesForTrends)

    val winRateTrendText =
        if (dashboardViewModel.getWinNumber(dashboardViewModel.listActivitiesForTrends) == "0 %") ""
        else dashboardViewModel.getWinNumber(dashboardViewModel.listActivitiesForTrends)

    Surface(
        color = Black,
        modifier = Modifier
            .fillMaxSize()
    ) {

        Column(Modifier.fillMaxSize()) {
            LazyColumn(
                modifier = Modifier
                    .fillMaxSize()
                    .weight(1f)
            ) {

                stickyHeader {
                    Header(navigateToSettings)
                }

                item {
                    Spacer(
                        modifier = Modifier
                            .height(Spacing_5)
                            .background(Black)
                    )

                    Column(
                        modifier = Modifier
                            .fillMaxSize()
                            .background(Black),
                        verticalArrangement = Arrangement.SpaceBetween
                    ) {

                        Row(
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(horizontal = Spacing_15)
                        ) {
                            Text(
                                text = stringResource(id = R.string.trends),
                                fontSize = Text_20,
                                fontWeight = FontWeight.Bold,
                                color = White
                            )

                            Spacer(modifier = Modifier.weight(1f))

                            Text(
                                text = stringResource(id = R.string.see_more),
                                fontSize = Text_16,
                                color = subHeadGrayText,
                                modifier = Modifier.clickable {
                                    firebaseAnalytics.logEvent(
                                        "dashboard_open_trends",
                                        Bundle().apply {
                                            this.putString(
                                                DASHBOARD_OPEN_TRENDS, "source_see_more_button"
                                            )
                                        }
                                    )
                                    dashboardViewModel.navigateToTrendsDetails()
                                }
                            )
                        }

                        Spacer(modifier = Modifier.height(Spacing_3))

                        Row(
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(horizontal = Spacing_15),
                            horizontalArrangement = Arrangement.spacedBy(Spacing_10)
                        ) {
                            Column(
                                modifier = Modifier
                                    .weight(1f)
                            ) {
                                TrendCard(
                                    trendName = stringResource(id = R.string.grade),
                                    trendScore = gradeTrendText,
                                    trendIcon = gradePicture,
                                    trendColor = GameGrade,
                                    onClicked = onGradeClick
                                )
                                Spacer(modifier = Modifier.height(Spacing_10))
                                TrendCard(
                                    trendName = stringResource(id = R.string.high_fatigue),
                                    trendScore = fatigueTrendText,
                                    trendIcon = fatiguePicture,
                                    trendColor = FatigueText,
                                    onClicked = onFatigueClick
                                )
                            }

                            Column(
                                modifier = Modifier
                                    .weight(1f)
                            ) {
                                TrendCard(
                                    trendName = stringResource(id = R.string.win_rate),
                                    trendScore = winRateTrendText,
                                    trendIcon = winRatePicture,
                                    trendColor = WinRate,
                                    onClicked = onWinRateClick
                                )
                                Spacer(modifier = Modifier.height(Spacing_10))
                                TrendCard(
                                    trendName = stringResource(id = R.string.high_flow),
                                    trendScore = flowTrendText,
                                    trendIcon = flowPicture,
                                    trendColor = FlowText,
                                    onClicked = onFlowClick
                                )
                            }
                        }

                        Spacer(modifier = Modifier.height(Spacing_15))

                        Card(
                            shape = RoundedCornerShape(Spacing_10), backgroundColor = BoxDarkGrey,
                            modifier = Modifier.padding(horizontal = Spacing_15)
                        ) {
                            val insight = dashboardViewModel.data?.profile?.userData?.userInsight
                            AdviceText(text = insight.getDashboardInsightText(LocalContext.current))
                        }

                        Spacer(modifier = Modifier.height(Spacing_10))

                        Row(
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(start = Spacing_15),
                            verticalAlignment = Alignment.CenterVertically,
                        ) {
                            Text(
                                text = stringResource(id = R.string.games),
                                fontSize = Text_20,
                                fontWeight = FontWeight.Bold,
                                color = White,
                                modifier = Modifier.padding(top = Spacing_5)
                            )

                            Spacer(modifier = Modifier.weight(1f))

                            Button(
                                onClick = { visibleDialog.value = true },
                                colors = ButtonDefaults.buttonColors(
                                    backgroundColor = Black,
                                    contentColor = subHeadGrayText
                                )
                            ) {
                                if (textForFilter == CSName) {
                                    Text(
                                        text = stringResource(id = R.string.CSGO),
                                        fontSize = Text_16,
                                        color = subHeadGrayText
                                    )
                                } else {
                                    Text(
                                        text = textForFilter,
                                        fontSize = Text_16,
                                        color = subHeadGrayText
                                    )
                                }
                                Image(
                                    painter = painterResource(id = R.drawable.ic_baseline_keyboard_arrow_down_24),
                                    contentDescription = stringResource(
                                        id = R.string.all_activities
                                    ),
                                    alignment = CenterEnd
                                )
                            }
                        }
                    }
                }

                var listactivity = if (dashboardViewModel.chooseActivityName == "") {
                    dashboardViewModel.listActivities
                } else {
                    dashboardViewModel.listActivities?.filter {
                        it.activityType.name == dashboardViewModel.chooseActivityName
                    }?.toMutableList()
                }

                items(listactivity?.size!!) { index ->
                    val activityItem = listactivity!!.getOrNull(index)

                    if (activityItem != null) {
                        key(activityItem) {

                            val dismissState = rememberDismissState()
                            LaunchedEffect(dismissState.currentValue) {
                                if (dismissState.currentValue == DismissValue.DismissedToStart) {
                                    dashboardViewModel.userActivityForDelete = activityItem
                                    println("ACT ITEM " + activityItem.activityType.name)
                                    visibleDeleteDialog.value = true
                                    dismissState.snapTo(DismissValue.Default)
                                }
                            }

                            if (visibleDeleteDialog.value) {
                                NativeAlertDialog(
                                    onExit = {
                                        visibleDeleteDialog.value = false
                                    },
                                    onSuccess = {
                                        removeActivity()
                                        visibleDeleteDialog.value = false
                                        if (dashboardViewModel.chooseActivityName != "") {
                                            listactivity =
                                                dashboardViewModel.listActivities?.filter {
                                                    it.activityType.name == dashboardViewModel.chooseActivityName
                                                }?.toMutableList()
                                            listactivity?.remove(dashboardViewModel.userActivityForDelete)
                                        }
                                    },
                                    disclaimerFirst = R.string.confirm_delete_activity,
                                    disclaimerSecond = R.string.confirm_delete_activity_text,
                                    successName = R.string.delete_session
                                )
                            }

                            SwipeToDismiss(
                                state = dismissState,
                                directions = setOf(DismissDirection.EndToStart),
                                background = {
                                    val direction =
                                        dismissState.dismissDirection ?: return@SwipeToDismiss

                                    val alignment = when (direction) {
                                        DismissDirection.StartToEnd -> CenterEnd
                                        DismissDirection.EndToStart -> CenterEnd
                                    }

                                    Box(
                                        modifier = Modifier
                                            .fillMaxSize()
                                            .background(Black)
                                            .padding(horizontal = Spacing_12),
                                        contentAlignment = alignment
                                    ) {
                                        Text(
                                            text = stringResource(id = R.string.delete),
                                            color = Color.Red,
                                            fontSize = Text_16,
                                            fontWeight = FontWeight.Bold
                                        )
                                    }
                                },
                                dismissContent = {
                                    GameCard(activityItem) {
                                        if (connectionState == ConnectionState.Connected) {
                                            dashboardViewModel.saveGameName(activityItem.activityType.name)
                                            dashboardViewModel.saveGameUrl(activityItem.match.tileImage)
                                            dashboardViewModel.navigateToGameSummary(activityItem.activityId)
                                        } else {
                                            firebaseAnalytics.logEvent(
                                                "dashboard_open_game_tile",
                                                Bundle().apply {
                                                    this.putString(
                                                        DASHBOARD_OPEN_GAME_TILE,
                                                        "dashboard_open_game_tile ${activityItem.activityType.name}"
                                                    )
                                                }
                                            )
                                            dashboardViewModel.navigateToGameSummary(activityItem.activityId)
                                        }
                                    }
                                }
                            )
                        }
                    }
                }
            }
            ConnectGameBandButton(
                wasDeviceSaved = wasDeviceSaved,
                onGameButtonClicked = {
                    onGameButtonClicked()
                },
                connectionState = connectionState
            )
        }
    }

    if (visibleDialog.value) {
        FilterOnDialog(visibleDialog, dashboardViewModel)
    }
}