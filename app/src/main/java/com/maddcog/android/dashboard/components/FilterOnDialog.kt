package com.maddcog.android.dashboard.components

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.compose.animation.animateContentSize
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.CheckCircle
import androidx.compose.material.icons.outlined.AddCircle
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion.Center
import androidx.compose.ui.Alignment.Companion.CenterVertically
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import com.maddcog.android.R
import com.maddcog.android.baseui.ui.theme.*
import com.maddcog.android.dashboard.DashboardViewModel

private const val CSName = "Counter-Strike: Global Offensive"

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun FilterOnDialog(visibleDialog: MutableState<Boolean>, dashboardViewModel: DashboardViewModel) {

    val checkedStateAll = remember { mutableStateOf(false) }
    val firebaseAnalytics: FirebaseAnalytics = Firebase.analytics

    Scaffold(
        backgroundColor = DialogBackgroundColor,
        modifier = Modifier
            .fillMaxSize()
            .clickable {
                visibleDialog.value = false
            }
    ) {
        Row(
            modifier = Modifier.fillMaxSize(),
            verticalAlignment = Alignment.Bottom,
        ) {
            Card(
                modifier = Modifier
                    .animateContentSize()
                    .fillMaxWidth()
                    .wrapContentHeight(align = Alignment.Bottom),
                backgroundColor = Black
            ) {
                Column(verticalArrangement = Arrangement.Bottom) {
                    Row(
                        modifier = Modifier.padding(
                            top = Spacing_15,
                            start = Spacing_18,
                            end = Spacing_18
                        )
                    ) {
                        Text(
                            text = stringResource(id = R.string.filter_on),
                            fontSize = Text_16,
                            color = White,
                            modifier = Modifier.align(CenterVertically)
                        )

                        Spacer(modifier = Modifier.weight(1f))

                        Box() {
                            Button(
                                onClick = { visibleDialog.value = false },
                                shape = RoundedCornerShape(100),
                                colors = ButtonDefaults.buttonColors(
                                    backgroundColor = BoxDarkGrey
                                ),
                                modifier = Modifier.size(Spacing_20)
                            ) { }

                            Image(
                                painter = painterResource(id = R.drawable.ic_cancel),
                                contentDescription = stringResource(
                                    id = R.string.cancel_btn
                                ),
                                modifier = Modifier
                                    .size(Spacing_18)
                                    .align(Center)
                            )
                        }
                    }

                    Spacer(modifier = Modifier.height(Spacing_5))

                    Card(
                        backgroundColor = BoxDarkGrey,
                        modifier = Modifier
                            .fillMaxWidth()
                            .heightIn()
                            .padding(start = Spacing_15, end = Spacing_15, top = Spacing_15)
                    ) {
                        LazyColumn(
                            Modifier
                                .background(Color.Black)
                        ) {
                            item {

                                Card(
                                    backgroundColor = BoxDarkGrey,
                                    modifier = Modifier
                                        .fillMaxWidth()
                                        .height(Spacing_60)
                                        .padding(
                                            start = Spacing_15,
                                            end = Spacing_15,
                                            top = Spacing_15
                                        )
                                        .clickable {
                                            checkedStateAll.value != checkedStateAll.value
                                            firebaseAnalytics.logEvent(
                                                "dashboard_filter_game",
                                                Bundle().apply {
                                                    this.putString(
                                                        DASHBOARD_FILTER_GAME,
                                                        "dashboard_filter_game_all_activity"
                                                    )
                                                }
                                            )
                                            visibleDialog.value = false
                                            dashboardViewModel.chooseActivityName = ""
                                        }
                                ) {
                                    Row(
                                        modifier = Modifier
                                            .fillMaxWidth()
                                            .padding(horizontal = Spacing_20)
                                    ) {

                                        Text(
                                            text = stringResource(
                                                id = R.string.all_activities
                                            ),
                                            fontSize = Text_14,
                                            fontWeight = FontWeight.Bold,
                                            color = White,
                                            modifier = Modifier.align(CenterVertically)
                                        )

                                        Spacer(modifier = Modifier.weight(1f))

                                        Checkbox(
                                            checked = checkedStateAll.value,
                                            onCheckedChange = {
                                                checkedStateAll.value = it
                                                firebaseAnalytics.logEvent(
                                                    "dashboard_filter_game",
                                                    Bundle().apply {
                                                        this.putString(
                                                            DASHBOARD_FILTER_GAME,
                                                            "dashboard_filter_game_all_activity"
                                                        )
                                                    }
                                                )
                                                visibleDialog.value = false
                                                dashboardViewModel.chooseActivityName = ""
                                            },
                                            colors = CheckboxDefaults.colors(
                                                checkedColor = BoxDarkGrey,
                                                checkmarkColor = White,
                                                uncheckedColor = White
                                            )
                                        )
                                    }
                                }
                            }
                            itemsIndexed(items = dashboardViewModel.userActivitiesNameForFilter) { _, item ->
                                ActivityCard(
                                    activityName = item,
                                    dashboardViewModel = dashboardViewModel,
                                    visibleDialog = visibleDialog,
                                    firebaseAnalytics = firebaseAnalytics
                                )
                            }
                        }
                    }
                    Spacer(modifier = Modifier.height(Spacing_5))
                }
            }
        }
    }
}

@Composable
fun ActivityCard(
    visibleDialog: MutableState<Boolean>,
    activityName: String,
    dashboardViewModel: DashboardViewModel,
    firebaseAnalytics: FirebaseAnalytics
) {

    val checkedState = remember { mutableStateOf(false) }

    Card(
        shape = RoundedCornerShape(15),
        backgroundColor = BoxDarkGrey,
        modifier = Modifier
            .fillMaxWidth()
            .height(Spacing_60)
            .padding(start = Spacing_15, end = Spacing_15, top = Spacing_15)
            .clickable {
                dashboardViewModel.chooseActivityName = activityName
                firebaseAnalytics.logEvent(
                    "dashboard_filter_game",
                    Bundle().apply {
                        this.putString(
                            DASHBOARD_FILTER_GAME, "dashboard_filter_game $activityName"
                        )
                    }
                )
                checkedState.value = !checkedState.value
                visibleDialog.value = false
            }
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = Spacing_20)
        ) {
            if (activityName == CSName) {
                Text(
                    text = stringResource(id = R.string.CSGO),
                    fontSize = Text_14,
                    fontWeight = FontWeight.Bold,
                    color = White,
                    modifier = Modifier.align(CenterVertically)
                )
            } else {
                Text(
                    text = activityName,
                    fontSize = Text_14,
                    fontWeight = FontWeight.Bold,
                    color = White,
                    modifier = Modifier.align(CenterVertically)
                )
            }

            Spacer(modifier = Modifier.weight(1f))

            Checkbox(
                checked = checkedState.value, onCheckedChange = {
                    checkedState.value = it
                    firebaseAnalytics.logEvent(
                        "dashboard_filter_game",
                        Bundle().apply {
                            this.putString(
                                DASHBOARD_FILTER_GAME, "dashboard_filter_game $activityName"
                            )
                        }
                    )
                    visibleDialog.value = false
                    dashboardViewModel.chooseActivityName = activityName
                },
                colors = CheckboxDefaults.colors(
                    checkedColor = BoxDarkGrey,
                    checkmarkColor = White,
                    uncheckedColor = White
                )
            )
        }
    }
}

@Composable
fun CircleCheckbox(selected: Boolean, enabled: Boolean = true, onChecked: () -> Unit) {

    val imageVector = if (selected) Icons.Filled.CheckCircle else Icons.Outlined.AddCircle
    val tint = HintLightGrey
    val background = White

    IconButton(
        onClick = { onChecked() },
        modifier = Modifier.offset(x = Spacing_5, y = Spacing_5),
        enabled = enabled
    ) {

        Icon(
            imageVector = imageVector, tint = tint,
            modifier = Modifier.background(background, shape = CircleShape),
            contentDescription = "checkbox"
        )
    }
}