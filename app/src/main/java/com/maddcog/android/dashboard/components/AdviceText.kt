package com.maddcog.android.dashboard.components

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import com.maddcog.android.baseui.ui.theme.*

@Composable
fun AdviceText(text: String) {
    Card(
        shape = RoundedCornerShape(10), backgroundColor = BoxDarkGrey,
        modifier = Modifier.padding(horizontal = Spacing_15)
    ) {
        Text(
            text = text,
            fontSize = Text_18,
            color = White,
            modifier = Modifier
                .padding(Spacing_18)
                .fillMaxWidth(),
            textAlign = TextAlign.Center
        )
    }
}