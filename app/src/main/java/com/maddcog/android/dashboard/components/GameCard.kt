package com.maddcog.android.dashboard.components

import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material3.Card
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.*
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.Dp
import coil.compose.AsyncImage
import coil.request.ImageRequest
import com.maddcog.android.R
import com.maddcog.android.baseui.ui.theme.*
import com.maddcog.android.dashboard.utils.ActivityUtils
import com.maddcog.android.data.extensions.toLocalName
import com.maddcog.android.domain.entities.activity.UserActivity
import com.maddcog.android.gameSummary.components.SummaryHeaderDoubleText
import com.maddcog.android.gameSummary.extensions.DEMO_ACTIVITY_NAME
import com.maddcog.android.gameSummary.extensions.toSummaryTile

private const val FILTER = "kd"
private const val ZEROGRADE = 0.0

@ExperimentalAnimationApi
@Composable
fun GameCard(
    userActivity: UserActivity,
    onCardClick: () -> Unit,
) {
    Surface(modifier = Modifier.fillMaxWidth(), color = Black) {
        Box(
            modifier = Modifier
                .fillMaxSize()
                .padding(start = Spacing_18, end = Spacing_18, top = Spacing_18)
        ) {
            Card(
                shape = RoundedCornerShape(Spacing_15),
                modifier = Modifier
                    .fillMaxSize()
                    .clickable {
                        onCardClick()
                    }
            ) {
                AsyncImage(
                    model = ImageRequest.Builder(LocalContext.current)
                        .data(userActivity.match.tileImage)
                        .crossfade(true)
                        .placeholder(R.color.white)
                        .build(),
                    contentDescription = "",
                    contentScale = ContentScale.Crop,
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(Spacing_250),
                    colorFilter = ColorFilter.tint(
                        ImageTileTint,
                        blendMode = BlendMode.Multiply
                    )
                )
            }

            Column() {
                Row() {
                    Column(modifier = Modifier.padding(start = Spacing_10, top = Spacing_30)) {
                        val hourText = (userActivity.match.end - userActivity.match.start) / 60
                        if (userActivity.activityId.contains(DEMO_ACTIVITY_NAME)) {
                            Text(
                                text = stringResource(
                                    id = R.string.demoactivity_date_text,
                                    hourText
                                ),
                                fontSize = Text_14,
                                color = White
                            )
                        } else {
                            val dateText =
                                userActivity.timestamp.let { ActivityUtils.timePresenter(it.millis / 1000) }
                            if (dateText != "") {

                                Text(
                                    text = "$dateText - $hourText",
                                    fontSize = Text_14,
                                    color = White
                                )
                            }
                        }
                        userActivity.activityType.name.let {
                            Text(
                                text = it,
                                fontSize = Text_18,
                                color = White,
                                fontWeight = FontWeight.Bold,
                                maxLines = oneLine,
                                overflow = TextOverflow.Ellipsis,
                                modifier = Modifier.fillMaxWidth(0.7f)
                            )
                        }
                    }

                    Spacer(modifier = Modifier.weight(1f))

                    Box(modifier = Modifier.padding(Spacing_20)) {

                        if (userActivity.match.myPlayer.grade == 0.0) {
                            Circle(value = ZEROGRADE)
                            Text(
                                text = ZEROGRADE.toString(),
                                fontSize = Text_18,
                                color = White,
                                modifier = Modifier.align(
                                    Alignment.Center
                                )
                            )
                        } else {
                            userActivity.match.myPlayer.grade.let { Circle(value = it) }

                            val number = userActivity.match.myPlayer.grade
                            val number3digits = number.times(1000.0).let { Math.round(it) }
                                .div(1000.0)
                            val number2digits =
                                number3digits.times(100.0).let { Math.round(it) }
                                    .div(100.0)
                            val solution =
                                number2digits.times(10.0).let { Math.round(it) }.div(10.0)

                            Text(
                                text = solution.toString(),
                                fontSize = Text_18,
                                color = White,
                                modifier = Modifier.align(
                                    Alignment.Center
                                )
                            )
                        }
                    }
                }

                Spacer(modifier = Modifier.height(Spacing_30))

                Row(
                    horizontalArrangement = Arrangement.Center,
                    modifier = Modifier.fillMaxWidth()
                ) {
                    userActivity.toSummaryTile(LocalContext.current).forEach {
                        SummaryHeaderDoubleText(
                            upperText = it.upperText.toLocalName(LocalContext.current),
                            downText = it.lowerText.toLocalName(LocalContext.current),
                        )
                    }
                }
            }
        }
    }
}

@Composable
fun Circle(value: Double) {

    val backgroundCircleColor = GameGrade
    val size: Dp = Spacing_50
    val thickness: Dp = Spacing_7

    val sweepAngles = value * Number_for_angle / Number_for_div_for_angle

    Canvas(
        modifier = Modifier
            .size(size)
    ) {

        val arcRadius = size.toPx()

        drawCircle(
            color = backgroundCircleColor,
            alpha = 0.2F,
            radius = arcRadius / 2,
            style = Stroke(width = thickness.toPx(), cap = StrokeCap.Butt)
        )

        drawArc(
            color = GameGrade,
            startAngle = -90f,
            sweepAngle = sweepAngles.toFloat(),
            useCenter = false,
            style = Stroke(width = thickness.toPx(), cap = StrokeCap.Round),
            size = Size(arcRadius, arcRadius),
            topLeft = Offset(
                x = (size.toPx() - arcRadius) / 2,
                y = (size.toPx() - arcRadius) / 2
            )
        )
    }
}