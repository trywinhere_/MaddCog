package com.maddcog.android.dashboard.components

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import com.maddcog.android.R
import com.maddcog.android.baseui.ui.theme.*
import com.maddcog.android.data.bluetoothle.ConnectionState

@Composable
fun ConnectGameBandButton(
    wasDeviceSaved: Boolean,
    modifier: Modifier = Modifier,
    onGameButtonClicked: () -> Unit,
    connectionState: ConnectionState
) {
    Column() {
        Card(
            modifier = modifier
                .fillMaxWidth()
                .height(Spacing_70), backgroundColor = Black
        ) {
            Button(
                onClick = {
                    onGameButtonClicked()
                },
                shape = RoundedCornerShape(20),
                colors = ButtonDefaults.buttonColors(
                    backgroundColor = White,
                    contentColor = Black
                ),
                modifier = Modifier
                    .fillMaxWidth()
                    .height(Spacing_50)
                    .padding(horizontal = Spacing_15, vertical = Spacing_5)
            ) {
                if (connectionState == ConnectionState.Connected || wasDeviceSaved) {
                    Text(
                        text = stringResource(id = R.string.changed_button_connect_game_band),
                        color = Black,
                        textAlign = TextAlign.Center,
                        modifier = Modifier.fillMaxWidth(),
                        fontSize = Text_16,
                        fontWeight = FontWeight.Bold
                    )
                } else {
                    Text(
                        text = stringResource(id = R.string.connect_game_band),
                        color = Black,
                        textAlign = TextAlign.Center,
                        modifier = Modifier.fillMaxWidth(),
                        fontSize = Text_16,
                        fontWeight = FontWeight.Bold
                    )
                }
            }
        }
    }
}