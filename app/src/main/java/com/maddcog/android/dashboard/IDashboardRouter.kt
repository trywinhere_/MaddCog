package com.maddcog.android.dashboard

import com.maddcog.android.baseui.model.NavCommand

interface IDashboardRouter {

    fun navigateToSettings(): NavCommand

    fun navigateToTrendsDetails(): NavCommand

    fun navigateToTrendsDetailsWithFilter(filter: String): NavCommand

    fun navigateToScanningForGameBand(): NavCommand

    fun navigateToGameSummary(userActivityId: String): NavCommand

    fun navigateToOpenGameBandConnection(): NavCommand

    fun navigateToStartNewSession(): NavCommand
}