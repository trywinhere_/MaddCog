package com.maddcog.android.dashboard.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import com.maddcog.android.R
import com.maddcog.android.baseui.ui.theme.*

@Composable
fun Header(navigateTo: () -> Unit) {
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .background(Black)
    ) {
        Text(
            text = stringResource(id = R.string.dashboard_title),
            fontSize = Text_20,
            fontWeight = FontWeight.Bold,
            color = White,
            modifier = Modifier
                .padding(top = Spacing_12)
                .fillMaxWidth(),
            textAlign = TextAlign.Center
        )

        Button(
            onClick = { navigateTo() },
            colors = ButtonDefaults.buttonColors(
                backgroundColor = Black,
                contentColor = White
            ),
            modifier = Modifier
                .align(Alignment.CenterEnd)
                .padding(top = Spacing_5)
        ) {
            Image(
                painter = painterResource(id = R.drawable.ic_baseline_settings_24),
                contentDescription = stringResource(
                    id = R.string.back_button
                ),
                modifier = Modifier.size(Spacing_30)
            )
        }
    }
}