package com.maddcog.android.dashboard.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.text.font.FontWeight
import com.maddcog.android.baseui.ui.theme.*

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun TrendCard(
    trendName: String,
    trendScore: String,
    trendIcon: Painter,
    trendColor: Color,
    onClicked: () -> Unit
) {
    Card(
        backgroundColor = BoxDarkGrey,
        shape = RoundedCornerShape(Spacing_10),
        modifier = Modifier.height(Spacing_90).fillMaxWidth(),
        onClick = onClicked
    ) {
        Row(verticalAlignment = Alignment.CenterVertically) {
            Spacer(modifier = Modifier.width(Spacing_10))
            Card(
                backgroundColor = Black,
                shape = RoundedCornerShape(100),
                modifier = Modifier.size(Spacing_50)
            ) {
                Image(
                    painter = trendIcon,
                    contentDescription = trendName,
                    modifier = Modifier
                        .align(Alignment.CenterVertically)
                        .size(Spacing_30)
                )
            }

            Column(
                modifier = Modifier.padding(
                    start = Spacing_7,
                    top = Spacing_15,
                    bottom = Spacing_15
                )
            ) {
                if (trendScore == "") {
                    Text(
                        text = trendName,
                        fontSize = Text_16,
                        color = White,
                        maxLines = Int_1
                    )
                } else {
                    Text(
                        text = trendName,
                        fontSize = Text_16,
                        color = White,
                        maxLines = Int_1
                    )
                    Text(
                        text = trendScore, fontSize = Text_20,
                        fontWeight = FontWeight.Bold,
                        color = trendColor
                    )
                }
            }
        }
    }
}