package com.maddcog.android.dashboard

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.ui.platform.ComposeView
import androidx.fragment.app.viewModels
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import com.maddcog.android.baseui.presentation.BaseFragment
import com.maddcog.android.baseui.presentation.BaseViewModel
import com.maddcog.android.baseui.ui.theme.*
import com.maddcog.android.data.bluetoothle.ConnectionState
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DashboardFragment : BaseFragment() {

    companion object {
        const val WIN_RATE = "Win rate"
        const val GAME_GRADE = "Game grade"
    }

    var wasDeviceSaved: Boolean = false

    private val dashboardViewModel: DashboardViewModel by viewModels()
    private lateinit var firebaseAnalytics: FirebaseAnalytics

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        firebaseAnalytics = Firebase.analytics
        dashboardViewModel.subscribeToChanges()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        val bleConnectionState = dashboardViewModel.connectionState
        wasDeviceSaved = dashboardViewModel.getSavedDeviceStatus()

        dashboardViewModel.initContent()

        firebaseAnalytics.logEvent("Dashboard", Bundle().apply { this.putString(DASHBOARD, "Dashboard") })

        val view = ComposeView(requireContext())
        view.setContent {
            MaddcogTheme {
                Dashboard(
                    wasDeviceSaved = wasDeviceSaved,
                    connectionState = dashboardViewModel.connectionState,
                    dashboardViewModel,
                    navigateToSettings = {
                        firebaseAnalytics.logEvent("dashboard_open_settings", Bundle()
                            .apply { this.putString(DASHBOARD_OPEN_TRENDS, "dashboard_open_settings") })
                        dashboardViewModel.navigateToSettings()
                    },
                    removeActivity = { dashboardViewModel.deleteActivity() },
                    onFatigueClick = {
                        firebaseAnalytics.logEvent("dashboard_open_trends_source_fatigue", Bundle()
                                .apply { this.putString(DASHBOARD_OPEN_TRENDS, "dashboard_open_trends_source_fatigue") })
                        dashboardViewModel.navigateToTrendsDetails()
                    },
                    onFlowClick = {
                        firebaseAnalytics.logEvent("dashboard_open_trends_source_flow", Bundle()
                                .apply { this.putString(DASHBOARD_OPEN_TRENDS, "dashboard_open_trends_source_flow") })
                        dashboardViewModel.navigateToTrendsDetails()
                    },
                    onGradeClick = {
                            firebaseAnalytics.logEvent("dashboard_open_trends_source_grade", Bundle()
                                .apply { this.putString(DASHBOARD_OPEN_TRENDS, "dashboard_open_trends_source_grade") })
                            dashboardViewModel.navigateToTrendsDetailsWithFilter(GAME_GRADE)
                    },
                    onWinRateClick = {
                        firebaseAnalytics.logEvent("dashboard_open_trends_source_winrate", Bundle()
                            .apply { this.putString(DASHBOARD_OPEN_TRENDS, "dashboard_open_trends_source_winrate") })
                        dashboardViewModel.navigateToTrendsDetailsWithFilter(WIN_RATE)
                    },
                    onGameButtonClicked = {
                        if(bleConnectionState == ConnectionState.Connected || wasDeviceSaved) {
                            firebaseAnalytics.logEvent("dashboard_start_playing", Bundle()
                                    .apply { this.putString(DASHBOARD_START_PLAYING, "dashboard_start_playing") })
                            dashboardViewModel.navigateToStartNewSession()
                        } else {
                            firebaseAnalytics.logEvent("dashboard_connect_game_band", Bundle()
                                .apply { this.putString(DASHBOARD_CONNECT_GAME_BAND, "dashboard_connect_game_band") })
                            dashboardViewModel.navigateToGameBandConnection()
                        }
                    }
                )
            }
        }

        return view
    }

    override fun withViewModel(): BaseViewModel = dashboardViewModel.apply {
    }
}