package com.maddcog.android.auth.signUp

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.compose.ui.platform.ComposeView
import androidx.fragment.app.viewModels
import com.maddcog.android.R
import com.maddcog.android.baseui.presentation.BaseFragment
import com.maddcog.android.baseui.presentation.BaseViewModel
import com.maddcog.android.baseui.ui.theme.MaddcogTheme
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SignUpFragment : BaseFragment() {

    private val viewModel: SignUpViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        viewModel.clearErrorsSignUp()

        val view = ComposeView(requireContext())

        view.setContent {
            MaddcogTheme {
                SignUpScreen(
                    navigateBack = {
                        viewModel.navigateBack()
                    },
                    openPrivacyPolicy = {
                        viewModel.openPrivacyPolicy()
                    },
                    viewModel = viewModel
                )
            }
        }

        return view
    }

    override fun withViewModel(): BaseViewModel = viewModel.apply {
        responseErrorData.observe(viewLifecycleOwner) { error ->
            if (error == null) {
            } else {
                val errormes = error.replaceAfter("(", "")
                val errormessage = errormes.replace("(", "")
                context?.let { context ->
                    AlertDialog.Builder(context).setMessage(errormessage)
                        .setNegativeButton(
                            R.string.dismiss
                        ) { _, _ ->
                            return@setNegativeButton
                        }
                        .show()
                }
            }
        }
        responseTokenData.observe(viewLifecycleOwner) { token ->
            if (token == null) {
//
            } else {
                viewModel.saveStateLogginingInSignIn(false)
                viewModel.navigateToLoggingIn()
            }
        }
        errorMessageForPassword.observe(viewLifecycleOwner) { containsError ->
            if (containsError) {
                context?.let { context ->
                    AlertDialog.Builder(context).setTitle(R.string.password).setMessage(R.string.error_message_for_password)
                        .setNegativeButton(
                            R.string.dismiss
                        ) { _, _ ->
                            return@setNegativeButton
                        }
                        .show()
                }
            }
        }
        errorMessageForEmail.observe(viewLifecycleOwner) { emailError ->
            if (emailError) {
                context?.let { context ->
                    AlertDialog.Builder(context).setTitle(R.string.email).setMessage(R.string.email_valid_error)
                        .setNegativeButton(
                            R.string.dismiss
                        ) { _, _ ->
                            return@setNegativeButton
                        }
                        .show()
                }
            }
        }
        errorMessageForName.observe(viewLifecycleOwner) { nameError ->
            if (nameError) {
                context?.let { context ->
                    AlertDialog.Builder(context).setTitle(R.string.first_name).setMessage(R.string.first_name_error)
                        .setNegativeButton(
                            R.string.dismiss
                        ) { _, _ ->
                            return@setNegativeButton
                        }
                        .show()
                }
            }
        }
        errorMessageForLastName.observe(viewLifecycleOwner) { lastNameError ->
            if (lastNameError) {
                context?.let { context ->
                    AlertDialog.Builder(context).setTitle(R.string.last_name).setMessage(R.string.last_name_error)
                        .setNegativeButton(
                            R.string.dismiss
                        ) { _, _ ->
                            return@setNegativeButton
                        }
                        .show()
                }
            }
        }
    }
}