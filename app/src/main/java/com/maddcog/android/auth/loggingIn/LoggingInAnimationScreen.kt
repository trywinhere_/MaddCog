package com.maddcog.android.auth.loggingIn

import android.net.Uri
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import com.maddcog.android.R
import com.maddcog.android.auth.components.VideoPlayer
import com.maddcog.android.auth.components.VideoPlayerSpinner
import com.maddcog.android.baseui.ui.theme.*

@Composable
fun LoggingInAnimationScreen() {

    val logo = Uri.parse("android.resource://com.maddcog.android/" + R.raw.maddcog)
    val spinner = Uri.parse("android.resource://com.maddcog.android/" + R.raw.logo_reveal)

    Surface(
        color = Black,
        modifier = Modifier
            .fillMaxSize()
    ) {

        Column(modifier = Modifier.fillMaxSize(), verticalArrangement = Arrangement.SpaceBetween) {
            Column(
                modifier = Modifier.fillMaxWidth(),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                VideoPlayer(videoUri = logo)
            }

            Column(
                modifier = Modifier.fillMaxWidth(),
                verticalArrangement = Arrangement.Bottom,
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                VideoPlayerSpinner(videoUri = spinner, width = Spacing_90, height = Spacing_50)
                Spacer(modifier = Modifier.height(Spacing_15))
                Row() {
                    Text(
                        text = stringResource(id = R.string.logging_in),
                        color = White,
                        fontWeight = FontWeight.Bold
                    )
                }
            }
        }
    }
}