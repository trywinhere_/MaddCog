package com.maddcog.android.auth.notifications

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.amazonaws.mobile.client.AWSMobileClient
import com.amazonaws.mobile.client.Callback
import com.amazonaws.mobile.client.UserStateDetails
import com.amazonaws.mobile.config.AWSConfiguration
import com.amazonaws.mobileconnectors.pinpoint.PinpointConfiguration
import com.amazonaws.mobileconnectors.pinpoint.PinpointManager
import com.amazonaws.mobileconnectors.pinpoint.targeting.notification.NotificationClient
import com.amazonaws.mobileconnectors.pinpoint.targeting.notification.NotificationClient.CampaignPushResult
import com.amazonaws.mobileconnectors.pinpoint.targeting.notification.NotificationDetails
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import java.lang.Exception

class PushListenerService : FirebaseMessagingService() {

    var pinpointManager: PinpointManager? = null
    val TAG = "pinpoint"

    /** Intent action used in local broadcast */
    val ACTION_PUSH_NOTIFICATION = "push-notification"

    /** Intent keys */
    val INTENT_SNS_NOTIFICATION_FROM = "from"
    val INTENT_SNS_NOTIFICATION_DATA = "data"

    override fun onNewToken(token: String) {
        super.onNewToken(token)
        Log.d("TAG", "Registering push notifications token: $token")
        getPinpointManager(applicationContext).notificationClient
            .registerDeviceToken(token)
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        Log.d("TAG", "Message: " + remoteMessage.data)
        val notificationClient: NotificationClient = getPinpointManager(
            applicationContext
        ).notificationClient
        val notificationDetails = NotificationDetails.builder()
            .from(remoteMessage.from)
            .mapData(remoteMessage.data)
            .intentAction(NotificationClient.FCM_INTENT_ACTION)
            .build()
        val pushResult = notificationClient.handleCampaignPush(notificationDetails)
        if (CampaignPushResult.NOT_HANDLED != pushResult) {
            /**
             * The push message was due to a Pinpoint campaign.
             * If the app was in the background, a local notification was added
             * in the notification center. If the app was in the foreground, an
             * event was recorded indicating the app was in the foreground,
             * for the demo, you will broadcast the notification to let the main
             * activity display it in a dialog.
             */
            if (CampaignPushResult.APP_IN_FOREGROUND == pushResult) {
                /* Create a message that will display the raw data of the campaign push in a dialog. */
                val dataMap = HashMap(remoteMessage.data)
                broadcast(remoteMessage.from, dataMap)
            }
            return
        }
    }

    private fun broadcast(from: String?, dataMap: HashMap<String, String>) {
        val intent = Intent(ACTION_PUSH_NOTIFICATION)
        intent.putExtra(INTENT_SNS_NOTIFICATION_FROM, from)
        intent.putExtra(INTENT_SNS_NOTIFICATION_DATA, dataMap)
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
    }

    /**
     * Helper method to extract push message from bundle.
     *
     * @param data bundle
     * @return message string from push notification
     */
    fun getMessage(data: Bundle): String {
        return (data["data"] as HashMap<*, *>?).toString()
    }

    fun getPinpointManager(applicationContext: Context?): PinpointManager {
        if (pinpointManager == null) {
            val awsConfig = AWSConfiguration(applicationContext)
            AWSMobileClient.getInstance()
                .initialize(
                    applicationContext, awsConfig,
                    object : Callback<UserStateDetails?> {
                        override fun onResult(result: UserStateDetails?) {
                            println("NOTIFICATIONS RESULT OK ")
                        }

                        override fun onError(e: Exception?) {
                            println("NOTIFICATIONS RESULT ERROR ")
                        }
                    }
                )
            val pinpointConfig = PinpointConfiguration(
                applicationContext,
                AWSMobileClient.getInstance(),
                awsConfig
            )
            pinpointManager = PinpointManager(pinpointConfig)
            FirebaseMessaging.getInstance().token
                .addOnCompleteListener(object : OnCompleteListener<String?> {
                    override fun onComplete(task: Task<String?>) {
                        if (!task.isSuccessful()) {
                            Log.w(
                                TAG,
                                "Fetching FCM registration token failed",
                                task.getException()
                            )
                            return
                        }
                        val token: String? = task.result
                        Log.d(TAG, "Registering push notifications token: $token")
                        pinpointManager!!.notificationClient.registerDeviceToken(token)
                    }
                })
        }
        return pinpointManager as PinpointManager
    }
}