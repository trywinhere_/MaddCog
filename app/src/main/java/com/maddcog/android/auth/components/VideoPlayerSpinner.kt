package com.maddcog.android.auth.components

import android.annotation.SuppressLint
import android.net.Uri
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.width
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.viewinterop.AndroidView
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.Player.REPEAT_MODE_ONE
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout.RESIZE_MODE_FILL
import com.google.android.exoplayer2.ui.StyledPlayerView

@SuppressLint("UseCompatLoadingForDrawables", "ResourceAsColor")
@Composable
fun VideoPlayerSpinner(videoUri: Uri, width: Dp, height: Dp) {

    val context = LocalContext.current

    val mediaItem = MediaItem.Builder()
        .setUri(videoUri)
        .setMediaMetadata(MediaMetadata.Builder().build())
        .build()

    val exoPlayer = remember {
        ExoPlayer.Builder(context).build().apply {
            this.setMediaItem(mediaItem)
            this.prepare()
            this.playWhenReady = true
            this.repeatMode = REPEAT_MODE_ONE
        }
    }

    DisposableEffect(
        AndroidView(
            modifier = Modifier
                .width(width).height(height).background(Color.Transparent),
            factory = {
                StyledPlayerView(context).apply {
                    player = exoPlayer
                    useController = false
                    resizeMode = RESIZE_MODE_FILL
                }
            }
        )
    ) {
        onDispose {
            exoPlayer.release()
        }
    }
}