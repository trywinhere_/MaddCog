package com.maddcog.android.auth.notifications

import android.Manifest
import android.content.pm.PackageManager
import android.os.Build
import android.util.Log
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.RequiresApi
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.core.content.ContextCompat
import com.maddcog.android.R
import com.maddcog.android.auth.components.NotificationsAlertDialog
import com.maddcog.android.baseui.ui.theme.Black

@RequiresApi(Build.VERSION_CODES.TIRAMISU)
@Composable
fun NotificationsScreen(
    pressAllow: () -> Unit,
    pressExit: () -> Unit
) {
    val launcher = rememberLauncherForActivityResult(
        ActivityResultContracts.RequestPermission()
    ) { isGranted: Boolean ->
        if (isGranted) {
            // Permission Accepted: Do something
            Log.d("ExampleScreen", "PERMISSION GRANTED")
        } else {
            // Permission Denied: Do something
            Log.d("ExampleScreen", "PERMISSION DENIED")
        }
    }
    val context = LocalContext.current

    Surface(color = Black, modifier = Modifier.fillMaxSize()) {
        Row(
            modifier = Modifier.fillMaxSize(),
            horizontalArrangement = Arrangement.Center,
            verticalAlignment = Alignment.CenterVertically
        ) {
            NotificationsAlertDialog(
                onExit = { pressExit() },
                title = stringResource(id = R.string.notification_dialog_title),
                text = stringResource(id = R.string.notification_dialog_message),
                onConfirm = {
                    pressAllow()
                    when (PackageManager.PERMISSION_GRANTED) {
                        ContextCompat.checkSelfPermission(
                            context,
                            Manifest.permission.POST_NOTIFICATIONS
                        ) -> {
                            // Some works that require permission
                            Log.d("ExampleScreen", "Code requires permission")
                        }
                        else -> {
                            // Asking for permission
                            launcher.launch(Manifest.permission.POST_NOTIFICATIONS)
                        }
                    }
                },
                confirmButtonText = stringResource(id = R.string.notification_dialog_allow),
                dismissButtonText = stringResource(id = R.string.notification_dialog_not_allow)
            )
        }
    }
}