package com.maddcog.android.auth.components

import androidx.compose.material.*
import androidx.compose.runtime.Composable
import com.maddcog.android.baseui.ui.theme.*

@Composable
fun NotificationsAlertDialog(
    onExit: () -> Unit,
    title: String,
    text: String,
    onConfirm: () -> Unit,
    confirmButtonText: String,
    dismissButtonText: String,
) {
    AlertDialog(
        onDismissRequest = { onExit() },
        title = {
            Text(text = title)
        },
        text = {
            Text(text)
        },
        confirmButton = {
            Button(
                onClick = { onConfirm() },
                colors = ButtonDefaults.buttonColors(
                    backgroundColor = HintLightGrey,
                    contentColor = White
                )
            ) {
                Text(text = confirmButtonText, color = White)
            }
        },
        dismissButton = {
            Button(
                onClick = { onExit() },
                colors = ButtonDefaults.buttonColors(
                    backgroundColor = HintLightGrey,
                    contentColor = White
                )
            ) {
                Text(text = dismissButtonText, color = White)
            }
        },
        backgroundColor = HintLightGrey
    )
}