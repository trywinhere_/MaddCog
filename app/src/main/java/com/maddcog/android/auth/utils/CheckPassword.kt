package com.maddcog.android.auth.utils

object CheckPassword {

    fun isPasswordFalse(password: String): Boolean {
        val regex = """[A-Z]""".toRegex()
        val regexLower = """[a-z]""".toRegex()
        val regexNumber = """[0-9]""".toRegex()
        if (!password.containsSymbol(listOf("$", "@", "-", "#", "!", "%", "*", "?", "&"))) {
            return true
        } else if (password.length < 8) {
            return true
        } else if (!password.contains(regex)) {
            return true
        } else if (!password.contains(regexNumber)) {
            return true
        } else if (!password.contains(regexLower)) {
            return true
        } else {
            return false
        }
    }

    private fun String.containsSymbol(
        symbols: List<String>
    ): Boolean {
        return this.findAnyOf(symbols, 0, false) != null
    }
}