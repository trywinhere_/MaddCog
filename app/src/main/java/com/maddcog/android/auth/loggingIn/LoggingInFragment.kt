package com.maddcog.android.auth.loggingIn

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.ui.platform.ComposeView
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.maddcog.android.baseui.presentation.BaseFragment
import com.maddcog.android.baseui.presentation.BaseViewModel
import com.maddcog.android.baseui.ui.theme.MaddcogTheme
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@AndroidEntryPoint
class LoggingInFragment : BaseFragment() {

    private val loggingInViewModel: LoggingInViewModel by viewModels()

    private var timerJob: Job? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        val state = loggingInViewModel.getState()

        if (!state) {
            loggingInViewModel.getUserInfo()
        } else {
            loggingInViewModel.signIn()
            startGetUserInfo()
        }

        val stateGoogleSignIn = loggingInViewModel.getStateGoogleSignIn()

        if (!stateGoogleSignIn) {
            loggingInViewModel.saveGoogleSignIn(true)
            loggingInViewModel.getUserInfo()
        }

        val view = ComposeView(requireContext())

        view.setContent {
            MaddcogTheme {
                LoggingInAnimationScreen()
            }
        }

        return view
    }

    override fun withViewModel(): BaseViewModel = loggingInViewModel.apply {
        val stateNotifications = loggingInViewModel.getStateNotifications()
        dataState.observe(viewLifecycleOwner) { state ->
            if (!state.loading) {
                if (!stateNotifications) {
                    loggingInViewModel.saveStateLogginingInSignIn(true)
                    loggingInViewModel.navigateToDashboard()
                } else {
                    loggingInViewModel.saveStateLogginingInSignIn(true)
                    loggingInViewModel.navigateToNotifications()
                }
            }
        }
    }

    private fun startGetUserInfo() {
        timerJob?.cancel()
        timerJob = viewLifecycleOwner.lifecycleScope.launch(Dispatchers.Main) {
            delay(9000)
            loggingInViewModel.getUserInfo()
        }
    }
}