package com.maddcog.android.auth.signUp

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.maddcog.android.auth.IAuthRouter
import com.maddcog.android.baseui.presentation.BaseViewModel
import com.maddcog.android.data.model.StateViewModel
import com.maddcog.android.domain.api.IAuthRepository
import com.maddcog.android.domain.entities.UserSignUp
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SignUpViewModel @Inject constructor(
    private val repository: IAuthRepository,
    private val router: IAuthRouter,
) : BaseViewModel() {

    private val _dataState = MutableLiveData<StateViewModel>()
    val dataState: LiveData<StateViewModel>
        get() = _dataState

    val responseErrorData = repository.errorResponseSignUp

    val responseTokenData = repository.tokenResponse

    val errorMessageForPassword = MutableLiveData<Boolean>()

    val errorMessageForName = MutableLiveData<Boolean>()

    val errorMessageForLastName = MutableLiveData<Boolean>()

    val errorMessageForEmail = MutableLiveData<Boolean>()

    var emailFromStorage: String? = null

    var passwordFromStorage: String? = null

    val userWithNoValue = UserSignUp(email = "", password = "", name = "")

    fun signUp(user: UserSignUp): Boolean {
        try {
            viewModelScope.launch {
                _dataState.value = StateViewModel(loading = true)
                repository.signUp(user)
                _dataState.value = StateViewModel()
            }
        } catch (e: Exception) {
            _dataState.value = StateViewModel(error = true)
        }
        return _dataState.value?.error != true
    }

    fun navigateToLoggingIn() {
        navigateTo(router.navigateToLoggingInFromSignUp())
    }

    fun openPrivacyPolicy() {
        navigateTo(router.openPrivacyPolice())
    }

    fun clearErrorsSignUp() {
        repository.clearErrorsSignUp()
    }

    fun saveStateLogginingInSignIn(showing: Boolean) {
        repository.saveStateSignIn(showing)
    }
}