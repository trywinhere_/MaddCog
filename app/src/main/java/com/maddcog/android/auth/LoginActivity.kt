package com.maddcog.android.auth

import android.content.ContentValues
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.amplifyframework.auth.cognito.AWSCognitoAuthSession
import com.amplifyframework.core.Amplify
import com.maddcog.android.app.presentation.MainActivity
import com.maddcog.android.app.presentation.MainActivityViewModel
import com.maddcog.android.auth.signIn.GoogleSignIn
import com.maddcog.android.auth.signIn.SignInViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

@AndroidEntryPoint
class LoginActivity : AppCompatActivity() {

    private val viewModel: SignInViewModel by viewModels()
    private val mainViewModel: MainActivityViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val coroutineScope = CoroutineScope(Dispatchers.Main)

        setContent {
            GoogleSignIn()
        }

        Amplify.Auth.fetchAuthSession(
            { result ->
                Log.i("RESULT FETCH SESSION ", result.toString())
                val cognitoAuthSession = result as AWSCognitoAuthSession
                if (cognitoAuthSession.isSignedIn) {
                    coroutineScope.launch {
                        mainViewModel.getToken()
                        viewModel.saveStateGoogleSignin(false)
                        viewModel.saveGoogleSignIn(false)
                        val intent = Intent(applicationContext, MainActivity::class.java)
                        startActivity(intent)
                        finish()
                    }
                } else {
                    coroutineScope.launch {
                        viewModel.signInGoogle(this@LoginActivity)

                        viewModel.googleError.observe(
                            this@LoginActivity
                        ) { error ->
                            if (error) {
                                val intent = Intent(applicationContext, MainActivity::class.java)
                                startActivity(intent)
                                finish()
                            } else {
                                viewModel.googleToken.observe(
                                    this@LoginActivity,
                                    Observer { googleToken ->
                                        if (googleToken == null) {
                                        } else {
                                            viewModel.saveStateGoogleSignin(false)
                                            viewModel.saveGoogleSignIn(false)
                                            val intent =
                                                Intent(applicationContext, MainActivity::class.java)
                                            startActivity(intent)
                                            finish()
                                        }
                                    }
                                )
                            }
                        }
                    }

                    viewModel.googleError.observe(
                        this
                    ) { error ->
                        if (error) {
                            val intent = Intent(applicationContext, MainActivity::class.java)
                            startActivity(intent)
                            finish()
                        } else {
                            viewModel.googleToken.observe(
                                this,
                                Observer { googleToken ->
                                    if (googleToken == null) {
                                    } else {
                                        viewModel.saveStateGoogleSignin(false)
                                        viewModel.saveGoogleSignIn(false)
                                        val intent =
                                            Intent(applicationContext, MainActivity::class.java)
                                        startActivity(intent)
                                        finish()
                                    }
                                }
                            )
                        }
                    }
                }
            },
            { error -> Log.i(ContentValues.TAG, error.toString()) }
        )
    }
}