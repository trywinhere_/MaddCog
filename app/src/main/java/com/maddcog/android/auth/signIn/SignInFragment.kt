package com.maddcog.android.auth.signIn

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.ui.platform.ComposeView
import androidx.fragment.app.viewModels
import com.maddcog.android.R
import com.maddcog.android.baseui.presentation.BaseFragment
import com.maddcog.android.baseui.presentation.BaseViewModel
import com.maddcog.android.baseui.ui.theme.MaddcogTheme
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi

@AndroidEntryPoint
class SignInFragment : BaseFragment() {

    private val viewModel: SignInViewModel by viewModels()

    @OptIn(ExperimentalMaterialApi::class, ExperimentalCoroutinesApi::class)
    @SuppressLint("SuspiciousIndentation")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        viewModel.clearErrorsSignIn()

        val view = ComposeView(requireContext())

        view.setContent {
            MaddcogTheme {
                LogInScreen(
                    viewModel = viewModel,
                    navigateToSignUp = {
                        viewModel.navigateToSignUp()
                    }
                )
            }
        }

        return view
    }

    override fun withViewModel(): BaseViewModel = viewModel.apply {
        responseErrorData.observe(viewLifecycleOwner) { error ->
            if (error == null) {
            } else {
                val errormes = error.replaceAfter("(", "")
                val errormessage = errormes.replace("(", "")
                context?.let { context ->
                    AlertDialog.Builder(context).setMessage(errormessage)
                        .setNegativeButton(
                            R.string.dismiss
                        ) { _, _ ->
                            return@setNegativeButton
                        }
                        .show()
                }
            }
        }
        responseTokenData.observe(viewLifecycleOwner) { token ->
            if (token == null) {
//
            } else {
                viewModel.saveStateLogginingInSignIn(false)
                viewModel.navigateToLoggingIn()
            }
        }
    }
}