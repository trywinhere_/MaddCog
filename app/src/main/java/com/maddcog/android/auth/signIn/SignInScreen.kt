package com.maddcog.android.auth.signIn

import android.content.Intent
import android.os.Bundle
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusDirection
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.style.TextAlign
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import com.maddcog.android.R
import com.maddcog.android.auth.LoginActivity
import com.maddcog.android.auth.components.AuthAlertDialog
import com.maddcog.android.baseui.ui.theme.*
import com.maddcog.android.domain.entities.User
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay

private const val DURATION: Long = 4000

@ExperimentalCoroutinesApi
@ExperimentalMaterialApi
@Composable
fun LogInScreen(
    navigateToSignUp: () -> Unit,
    viewModel: SignInViewModel
) {
    val visibleDialog = remember { mutableStateOf(false) }
    val firebaseAnalytics: FirebaseAnalytics = Firebase.analytics
    var emailText by remember { mutableStateOf(TextFieldValue("")) }
    var passwordText by remember { mutableStateOf(TextFieldValue("")) }
    val focusManager = LocalFocusManager.current
    val context = LocalContext.current
    val intent = Intent(context, LoginActivity::class.java)

    Surface(
        color = Black,
        modifier = Modifier.fillMaxSize().verticalScroll(rememberScrollState())
    ) {
        Column(
            modifier = Modifier.fillMaxSize(),
            verticalArrangement = Arrangement.SpaceBetween
        ) {
            Row(
                horizontalArrangement = Arrangement.Center,
                modifier = Modifier.fillMaxWidth()
            ) {
                Image(
                    painter = painterResource(id = R.drawable.maddcog_orange_white),
                    contentDescription = stringResource(id = R.string.logo),
                    modifier = Modifier.padding(top = Spacing_60)
                )
            }

            Spacer(modifier = Modifier.height(Spacing_20))

            Column() {
                Text(
                    text = stringResource(R.string.email),
                    color = White,
                    modifier = Modifier
                        .align(Alignment.Start)
                        .padding(start = Spacing_15),
                    fontSize = Text_18
                )

                TextField(
                    value = emailText,
                    onValueChange = { email ->
                        emailText = email
                    },
                    placeholder = {
                        Text(
                            text = stringResource(id = R.string.email_hint),
                            color = HintLightGrey,
                            fontSize = Text_18
                        )
                    },
                    modifier = Modifier
                        .padding(horizontal = Spacing_15, vertical = Spacing_20)
                        .fillMaxWidth(),
                    shape = RoundedCornerShape(15),
                    colors = TextFieldDefaults.textFieldColors(
                        backgroundColor = BoxDarkGrey,
                        textColor = White, placeholderColor = HintLightGrey,
                        cursorColor = White
                    ),
                    singleLine = true,
                    keyboardActions = KeyboardActions(onDone = {
                        focusManager.moveFocus(
                            FocusDirection.Down
                        )
                    })
                )

                Text(
                    text = stringResource(R.string.password),
                    color = White,
                    modifier = Modifier
                        .align(Alignment.Start)
                        .padding(start = Spacing_15),
                    fontSize = Text_18
                )

                TextField(
                    value = passwordText,
                    onValueChange = { password ->
                        passwordText = password
                    },
                    placeholder = {
                        Text(
                            text = stringResource(id = R.string.password_hint),
                            color = HintLightGrey,
                            fontSize = Text_18
                        )
                    },
                    modifier = Modifier
                        .padding(horizontal = Spacing_15, vertical = Spacing_20)
                        .fillMaxWidth(),
                    shape = RoundedCornerShape(15),
                    colors = TextFieldDefaults.textFieldColors(
                        backgroundColor = BoxDarkGrey,
                        textColor = White, placeholderColor = HintLightGrey,
                        cursorColor = White
                    ),
                    singleLine = true,
                    visualTransformation = PasswordVisualTransformation(),
                    keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Password),
                    keyboardActions = KeyboardActions(onDone = { focusManager.clearFocus() })
                )
            }

            Spacer(modifier = Modifier.height(Spacing_20))

            Column(verticalArrangement = Arrangement.Bottom) {
                Button(
                    onClick = {
                        val user = User(
                            email = emailText.text,
                            password = passwordText.text
                        )

                        firebaseAnalytics.logEvent(
                            "Sign_in",
                            Bundle().apply {
                                this.putString(
                                    SIGN_IN, "sign_in"
                                )
                            }
                        )

                        if (user != viewModel.userWithNoValue && user.password != "") {
                            visibleDialog.value = true
                            viewModel.signIn(user)
                        }
                    }, shape = RoundedCornerShape(20),
                    colors = ButtonDefaults.buttonColors(
                        backgroundColor = White,
                        contentColor = Black
                    ),
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(Spacing_50)
                        .padding(horizontal = Spacing_15)
                ) {
                    Text(
                        text = stringResource(id = R.string.sign_in),
                        color = Black,
                        textAlign = TextAlign.Center,
                        modifier = Modifier.fillMaxWidth(),
                        fontSize = Text_18
                    )
                }

                Spacer(modifier = Modifier.height(Spacing_20))

                Button(
                    onClick = {
                        context.startActivity(intent)
                    }, shape = RoundedCornerShape(20),
                    colors = ButtonDefaults.buttonColors(
                        backgroundColor = White,
                        contentColor = Black
                    ),
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(Spacing_50)
                        .padding(horizontal = Spacing_15)
                ) {
                    Image(
                        painter = painterResource(id = R.drawable.google),
                        contentDescription = stringResource(
                            id = R.string.sign_in_with_google
                        ),
                        alignment = Alignment.Center,
                        modifier = Modifier.size(Spacing_21)
                    )
                    Spacer(modifier = Modifier.width(Spacing_5))
                    Text(
                        text = stringResource(id = R.string.sign_in_with_google),
                        color = Black,
                        textAlign = TextAlign.Center,
                        fontSize = Text_18
                    )
                }

                Spacer(modifier = Modifier.height(Spacing_20))

                Button(
                    onClick = {
                        navigateToSignUp()
                    },
                    shape = RoundedCornerShape(20),
                    colors = ButtonDefaults.buttonColors(
                        backgroundColor = Black,
                        contentColor = White
                    ),
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(Spacing_50)
                        .padding(horizontal = Spacing_15)
                ) {
                    Text(
                        text = stringResource(id = R.string.sign_up),
                        color = White,
                        textAlign = TextAlign.Center,
                        modifier = Modifier.fillMaxWidth(),
                        fontSize = Text_18
                    )
                }
                Spacer(modifier = Modifier.height(Spacing_20))
            }
        }

        if (visibleDialog.value) {
            AuthAlertDialog()
            LaunchedEffect(true) {
                delay(DURATION)
                visibleDialog.value = false
            }
        }
    }
}