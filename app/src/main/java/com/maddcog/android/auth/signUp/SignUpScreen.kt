package com.maddcog.android.auth.signUp

import android.os.Bundle
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusDirection
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.style.TextAlign
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import com.maddcog.android.R
import com.maddcog.android.auth.components.AuthAlertDialog
import com.maddcog.android.auth.utils.CheckPassword
import com.maddcog.android.auth.utils.CheckSignUpNameEmail
import com.maddcog.android.baseui.components.PrivacyPolicyText
import com.maddcog.android.baseui.ui.theme.*
import com.maddcog.android.domain.entities.UserSignUp
import kotlinx.coroutines.delay

private const val DURATION: Long = 2000

@Composable
fun SignUpScreen(
    navigateBack: () -> Unit,
    openPrivacyPolicy: () -> Unit,
    viewModel: SignUpViewModel
) {

    val firebaseAnalytics: FirebaseAnalytics = Firebase.analytics
    val visibleDialog = remember { mutableStateOf(false) }
    var firstNameText by remember { mutableStateOf(TextFieldValue("")) }
    var lastNameText by remember { mutableStateOf(TextFieldValue("")) }
    var emailText by remember { mutableStateOf(TextFieldValue("")) }
    var passwordText by remember { mutableStateOf(TextFieldValue("")) }
    val focusManager = LocalFocusManager.current

    Surface(
        color = Black,
        modifier = Modifier.fillMaxSize().verticalScroll(rememberScrollState())
    ) {
        Column(modifier = Modifier.fillMaxSize(), verticalArrangement = Arrangement.SpaceBetween) {
            Box(
                modifier = Modifier.fillMaxWidth()
            ) {
                Button(
                    onClick = {
                        navigateBack()
                    },
                    colors = ButtonDefaults.buttonColors(
                        backgroundColor = Black,
                        contentColor = White
                    )
                ) {
                    Image(
                        painter = painterResource(id = R.drawable.ic_arrow_back),
                        contentDescription = stringResource(
                            id = R.string.back_button
                        ),
                        modifier = Modifier.size(Spacing_30)
                    )
                }

                Text(
                    text = stringResource(id = R.string.sign_up),
                    fontSize = Text_20,
                    fontWeight = FontWeight.Bold,
                    color = White,
                    modifier = Modifier
                        .padding(top = Spacing_12)
                        .fillMaxWidth(),
                    textAlign = TextAlign.Center
                )
            }

            Row(
                horizontalArrangement = Arrangement.Center,
                modifier = Modifier.fillMaxWidth()
            ) {
                Image(
                    painter = painterResource(id = R.drawable.maddcog_orange_white),
                    contentDescription = stringResource(id = R.string.logo),
                    modifier = Modifier
                        .padding(top = Spacing_10)
                        .size(Spacing_90)
                )
            }

            Spacer(modifier = Modifier.height(Spacing_20))

            Column() {

                Text(
                    text = stringResource(R.string.first_name),
                    color = White,
                    modifier = Modifier
                        .align(Alignment.Start)
                        .padding(start = Spacing_15),
                    fontSize = Text_18
                )

                TextField(
                    value = firstNameText,
                    onValueChange = { firstName ->
                        firstNameText = firstName
                    },
                    placeholder = {
                        Text(
                            text = stringResource(id = R.string.first_name_hint),
                            color = HintLightGrey,
                            fontSize = Text_18
                        )
                    },
                    modifier = Modifier
                        .padding(horizontal = Spacing_15, vertical = Spacing_15)
                        .fillMaxWidth(),
                    shape = RoundedCornerShape(15),
                    colors = TextFieldDefaults.textFieldColors(
                        backgroundColor = BoxDarkGrey,
                        textColor = White, placeholderColor = HintLightGrey,
                        cursorColor = White
                    ),
                    singleLine = true,
                    keyboardActions = KeyboardActions(onDone = {
                        focusManager.moveFocus(
                            FocusDirection.Down
                        )
                    })
                )

                Text(
                    text = stringResource(R.string.last_name),
                    color = White,
                    modifier = Modifier
                        .align(Alignment.Start)
                        .padding(start = Spacing_15),
                    fontSize = Text_18
                )

                TextField(
                    value = lastNameText,
                    onValueChange = { lastName ->
                        lastNameText = lastName
                    },
                    placeholder = {
                        Text(
                            text = stringResource(id = R.string.last_name_hint),
                            color = HintLightGrey,
                            fontSize = Text_18
                        )
                    },
                    modifier = Modifier
                        .padding(horizontal = Spacing_15, vertical = Spacing_15)
                        .fillMaxWidth(),
                    shape = RoundedCornerShape(15),
                    colors = TextFieldDefaults.textFieldColors(
                        backgroundColor = BoxDarkGrey,
                        textColor = White, placeholderColor = HintLightGrey,
                        cursorColor = White
                    ),
                    singleLine = true,
                    keyboardActions = KeyboardActions(onDone = {
                        focusManager.moveFocus(
                            FocusDirection.Down
                        )
                    })
                )

                Text(
                    text = stringResource(R.string.email),
                    color = White,
                    modifier = Modifier
                        .align(Alignment.Start)
                        .padding(start = Spacing_15),
                    fontSize = Text_18
                )

                TextField(
                    value = emailText,
                    onValueChange = { email ->
                        emailText = email
                    },
                    placeholder = {
                        Text(
                            text = stringResource(id = R.string.email_hint),
                            color = HintLightGrey,
                            fontSize = Text_18
                        )
                    },
                    modifier = Modifier
                        .padding(horizontal = Spacing_15, vertical = Spacing_15)
                        .fillMaxWidth(),
                    shape = RoundedCornerShape(15),
                    colors = TextFieldDefaults.textFieldColors(
                        backgroundColor = BoxDarkGrey,
                        textColor = White, placeholderColor = HintLightGrey,
                        cursorColor = White
                    ),
                    singleLine = true,
                    keyboardActions = KeyboardActions(onDone = {
                        focusManager.moveFocus(
                            FocusDirection.Down
                        )
                    })
                )

                Text(
                    text = stringResource(R.string.password),
                    color = White,
                    modifier = Modifier
                        .align(Alignment.Start)
                        .padding(start = Spacing_15),
                    fontSize = Text_18
                )

                TextField(
                    value = passwordText,
                    onValueChange = { password ->
                        passwordText = password
                    },
                    placeholder = {
                        Text(
                            text = stringResource(id = R.string.password_hint),
                            color = HintLightGrey,
                            fontSize = Text_18
                        )
                    },
                    modifier = Modifier
                        .padding(horizontal = Spacing_15, vertical = Spacing_15)
                        .fillMaxWidth(),
                    shape = RoundedCornerShape(15),
                    colors = TextFieldDefaults.textFieldColors(
                        backgroundColor = BoxDarkGrey,
                        textColor = White, placeholderColor = HintLightGrey,
                        cursorColor = White
                    ),
                    singleLine = true,
                    visualTransformation = PasswordVisualTransformation(),
                    keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Password),
                    keyboardActions = KeyboardActions(onDone = { focusManager.clearFocus() })
                )
            }

            Spacer(modifier = Modifier.height(Spacing_15))

            Column(verticalArrangement = Arrangement.Bottom) {
                val name = firstNameText.text + " " + lastNameText.text
                Button(
                    onClick = {
                        val userReg = UserSignUp(
                            name = name,
                            email = emailText.text,
                            password = passwordText.text
                        )

                        if (CheckSignUpNameEmail.isNameLongEnough(firstNameText.text)) {
                            viewModel.errorMessageForName.value = true
                        } else if (CheckSignUpNameEmail.isLastNameLongEnough(lastNameText.text)) {
                            viewModel.errorMessageForLastName.value = true
                        } else if (CheckSignUpNameEmail.isEmailValid(emailText.text)) {
                            viewModel.errorMessageForEmail.value = true
                        } else if (CheckPassword.isPasswordFalse(passwordText.text)) {
                            viewModel.errorMessageForPassword.value = true
                        } else if (viewModel.userWithNoValue != userReg) {
                            visibleDialog.value = true
                            viewModel.signUp(userReg)
                        }

                        firebaseAnalytics.logEvent(
                            "Sign_up",
                            Bundle().apply {
                                this.putString(
                                    SIGN_UP, "sign_up"
                                )
                            }
                        )
                    }, shape = RoundedCornerShape(20),
                    colors = ButtonDefaults.buttonColors(
                        backgroundColor = White,
                        contentColor = Black
                    ),
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(Spacing_50)
                        .padding(horizontal = Spacing_15)
                ) {
                    Text(
                        text = stringResource(id = R.string.sign_up),
                        color = Black,
                        textAlign = TextAlign.Center,
                        modifier = Modifier.fillMaxWidth(),
                        fontSize = Text_18
                    )
                }

                Spacer(modifier = Modifier.height(Spacing_10))

                PrivacyPolicyText {
                    openPrivacyPolicy()
                }
            }
        }

        if (visibleDialog.value) {
            AuthAlertDialog()
            LaunchedEffect(true) {
                delay(DURATION)
                visibleDialog.value = false
            }
        }
    }
}