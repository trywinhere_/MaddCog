package com.maddcog.android.auth.utils

import android.content.Context
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.maddcog.android.R

fun getGoogleSignInClient(context: Context): GoogleSignInClient {
    /** Request id token if you intend to verify google user from your backend server */
    val signInOptions = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
        .requestIdToken(context.getString(R.string.default_web_client_id_google))
        .requestEmail()
        .build()

    return GoogleSignIn.getClient(context, signInOptions)
}