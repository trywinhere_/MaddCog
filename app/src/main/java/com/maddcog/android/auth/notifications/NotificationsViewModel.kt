package com.maddcog.android.auth.notifications

import com.maddcog.android.auth.IAuthRouter
import com.maddcog.android.baseui.presentation.BaseViewModel
import com.maddcog.android.domain.api.ISharedPreferencesStorage
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class NotificationsViewModel @Inject constructor(
    private val router: IAuthRouter,
    private val storage: ISharedPreferencesStorage
) : BaseViewModel() {

    fun navigateToDashboard() {
        navigateTo(router.navigateToDashboard())
    }

    fun saveStateNotifications(state: Boolean) {
        storage.saveStateNotifications(state)
    }
}