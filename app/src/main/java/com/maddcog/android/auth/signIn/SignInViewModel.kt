package com.maddcog.android.auth.signIn

import android.app.Activity
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.maddcog.android.auth.IAuthRouter
import com.maddcog.android.baseui.presentation.BaseViewModel
import com.maddcog.android.data.model.StateViewModel
import com.maddcog.android.domain.api.IAuthRepository
import com.maddcog.android.domain.api.ISharedPreferencesStorage
import com.maddcog.android.domain.entities.User
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SignInViewModel @Inject constructor(
    private val repository: IAuthRepository,
    private val router: IAuthRouter,
    private val storage: ISharedPreferencesStorage
) : BaseViewModel() {

    private val _dataState = MutableLiveData<StateViewModel>()
    val dataState: LiveData<StateViewModel>
        get() = _dataState

    val responseErrorData = repository.errorResponseSignIn

    val responseTokenData = repository.tokenResponse

    val userWithNoValue = User(email = "", password = "")

    val googleToken = repository.googleToken

    val googleError = repository.googleSignInError

    fun signInGoogle(activity: Activity) {
        try {
            _dataState.value = StateViewModel(loading = true)
            repository.googleSignIn(activity)
            _dataState.value = StateViewModel()
        } catch (e: Exception) {
            _dataState.value = StateViewModel(error = true)
        }
    }

    fun signIn(user: User): Boolean {
        try {
            viewModelScope.launch {
                _dataState.value = StateViewModel(loading = true)
                repository.signIn(user)
                _dataState.value = StateViewModel()
            }
        } catch (e: Exception) {
            _dataState.value = StateViewModel(error = true)
        }
        return _dataState.value?.error != true
    }

    fun navigateToSignUp() {
        navigateTo(router.navigateToSignUp())
    }

    fun navigateToLoggingIn() {
        navigateTo(router.navigateToLoggingIn())
    }

    fun goToLoggingIn() {
        navigateTo(router.goToLoggingin())
    }

    fun clearErrorsSignIn() {
        repository.clearErrorsSignIn()
    }

    fun saveStateLogginingInSignIn(showing: Boolean) {
        repository.saveStateSignIn(showing)
    }

    fun saveStateGoogleSignin(showing: Boolean) {
        storage.saveStateGoogleSignIn(false)
    }

    fun clearGoogleError() {
        repository.clearGoogleError()
    }

    fun saveGoogleSignIn(act: Boolean) {
        storage.saveGoogleSignIn(act)
    }
}