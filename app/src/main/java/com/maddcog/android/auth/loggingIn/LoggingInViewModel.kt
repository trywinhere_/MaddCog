package com.maddcog.android.auth.loggingIn

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.maddcog.android.auth.IAuthRouter
import com.maddcog.android.baseui.model.StatefulData
import com.maddcog.android.baseui.presentation.BaseViewModel
import com.maddcog.android.data.model.StateViewModel
import com.maddcog.android.domain.api.IAuthRepository
import com.maddcog.android.domain.api.IDashboardRepository
import com.maddcog.android.domain.api.ISharedPreferencesStorage
import com.maddcog.android.domain.api.IUserDataRepository
import com.maddcog.android.domain.entities.User
import com.maddcog.android.domain.usecase.DashboardUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class LoggingInViewModel @Inject constructor(
    private val repository: IUserDataRepository,
    private val router: IAuthRouter,
    private val dashboardRepository: IDashboardRepository,
    private val authRepository: IAuthRepository,
    private val storage: ISharedPreferencesStorage,
    private val dashboardUseCase: DashboardUseCase
) : BaseViewModel() {

    private val _loadProfile = statefulSharedFlow<Unit>()
    val loadProfile: Flow<StatefulData<Unit>>
        get() = _loadProfile

    private val _dataState = MutableLiveData<StateViewModel>()
    val dataState: LiveData<StateViewModel>
        get() = _dataState

    fun loadProfile() {
        _loadProfile.fetch {
            repository.loadAndSaveProfile()
        }
    }

    fun getUserInfo(): Boolean {
        try {
            viewModelScope.launch {
                _dataState.value = StateViewModel(loading = true)
                loadProfile()
                dashboardRepository.getTeams()
                dashboardRepository.getActivityTypes()
                dashboardRepository.getAllActivitiesInfo()
                _dataState.value = StateViewModel()
            }
            return true
        } catch (e: Exception) {
            _dataState.value = StateViewModel(error = true)
            return false
        }
    }

    fun navigateToDashboard() {
        navigateTo(router.navigateToDashboard())
    }

    fun getState(): Boolean {
        return authRepository.getStateSignInLoggingingIn()
    }

    fun saveStateLogginingInSignIn(showing: Boolean) {
        authRepository.saveStateSignInLoggingingIn(showing)
    }

    fun signIn() {
        try {
            viewModelScope.launch {
                val email = authRepository.getEmail()
                val password = authRepository.getPassword()
                if (email != null && password != null) {
                    val user = User(email, password)
                    authRepository.signIn(user)
                }
            }
        } catch (_: Exception) {
            _dataState.value = StateViewModel(error = true)
        }
    }

    fun navigateToNotifications() {
        navigateTo(router.navigateToNotifications())
    }

    fun getStateNotifications(): Boolean {
        return storage.getStateNotifications()
    }

    fun getStateGoogleSignIn(): Boolean {
        return storage.getStateGoogleSignIn()
    }

    fun saveGoogleSignIn(act: Boolean) {
        storage.saveGoogleSignIn(act)
    }
}