package com.maddcog.android.auth.signIn

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import com.maddcog.android.auth.components.AuthAlertDialog
import com.maddcog.android.baseui.ui.theme.Black

@Composable
fun GoogleSignIn() {
    Surface(color = Black, modifier = Modifier.fillMaxSize()) {
        AuthAlertDialog()
    }
}