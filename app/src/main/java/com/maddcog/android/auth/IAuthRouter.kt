package com.maddcog.android.auth

import com.maddcog.android.baseui.model.NavCommand

interface IAuthRouter {

    fun navigateToSignUp(): NavCommand

    fun navigateToLoggingIn(): NavCommand

    fun navigateToLoggingInFromSignUp(): NavCommand

    fun openPrivacyPolice(): NavCommand

    fun goToLoggingin(): NavCommand

    fun navigateToDashboard(): NavCommand

    fun navigateToNotifications(): NavCommand
}