package com.maddcog.android.auth.signIn

import androidx.appcompat.app.AppCompatActivity
import com.amazonaws.mobile.client.AWSMobileClient

class HandleActivity : AppCompatActivity() {
    override fun onResume() {
        super.onResume()
        val activityIntent = intent
        if (activityIntent.data != null && "maddcog" == activityIntent.data!!.scheme) {
            AWSMobileClient.getInstance().handleAuthResponse(activityIntent)
        }
    }
}