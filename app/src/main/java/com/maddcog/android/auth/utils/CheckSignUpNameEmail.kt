package com.maddcog.android.auth.utils

object CheckSignUpNameEmail {

    fun isNameLongEnough(name: String): Boolean {
        if (name.length < 2) {
            return true
        } else {
            return false
        }
    }

    fun isLastNameLongEnough(name: String): Boolean {
        if (name.length < 2) {
            return true
        } else {
            return false
        }
    }

    fun isEmailValid(email: String): Boolean {
        val before = email.substringBefore("@")
        val last = email.substringAfterLast("@")
        val checkDot = email.substringAfterLast("@").substringBeforeLast(".")
        val afterDot = email.substringAfter(".")
        if (!email.contains("@")) {
            return true
        } else if (before.length < 1) {
            return true
        } else if (!last.contains(".")) {
            return true
        } else if (checkDot.length < 1) {
            return true
        } else if (afterDot.length < 2) {
            return true
        } else {
            return false
        }
    }
}