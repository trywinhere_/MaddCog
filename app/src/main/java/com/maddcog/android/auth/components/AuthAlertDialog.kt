package com.maddcog.android.auth.components

import android.net.Uri
import androidx.compose.animation.animateContentSize
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import com.maddcog.android.R
import com.maddcog.android.baseui.ui.theme.*

@Composable
fun AuthAlertDialog() {

    val spinner = Uri.parse("android.resource://com.maddcog.android/" + R.raw.logo_reveal)

    Surface(color = DialogBackgroundColor) {
        Row(
            modifier = Modifier.fillMaxSize(),
            verticalAlignment = Alignment.CenterVertically,
        ) {
            Card(
                modifier = Modifier
                    .animateContentSize()
                    .fillMaxWidth()
                    .wrapContentHeight(align = Alignment.CenterVertically)
                    .padding(all = Spacing_30),
                shape = RoundedCornerShape(10),
                backgroundColor = Black
            ) {
                Column(
                    verticalArrangement = Arrangement.Center,
                    horizontalAlignment = Alignment.CenterHorizontally,
                    modifier = Modifier
                        .padding(all = Spacing_15)
                        .background(Black)
                ) {
                    Text(
                        text = stringResource(id = R.string.authentication),
                        fontSize = Text_18,
                        fontWeight = FontWeight.Bold,
                        color = White
                    )
                    Spacer(modifier = Modifier.height(Spacing_20))
                    VideoPlayerSpinner(videoUri = spinner, width = Spacing_90, height = Spacing_50)
                }
            }
        }
    }
}