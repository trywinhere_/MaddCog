package com.maddcog.android.app.navigation

import android.content.Context
import androidx.core.net.toUri
import androidx.navigation.NavDeepLinkRequest
import com.maddcog.android.R
import com.maddcog.android.baseui.extensions.openWebLink
import com.maddcog.android.baseui.model.NavCommand
import com.maddcog.android.baseui.ui.theme.PRIVACY_POLICY
import com.maddcog.android.settings.ISettingsRouter
import com.maddcog.android.settings.SettingsFragmentDirections
import com.maddcog.android.settings.presentation.gameIntegration.presentation.pickGame.PickGameFragmentDirections
import com.maddcog.android.settings.presentation.gameIntegration.presentation.pickGame.PickGameKey
import com.maddcog.android.settings.presentation.gameIntegration.presentation.saveEditConnection.SaveEditConnectionFragmentDirections
import com.maddcog.android.settings.presentation.teams.createteam.CreateTeamFragmentDirections

class SettingsRouterImpl(private val context: Context) : ISettingsRouter {

    override fun openPrivacyPolice(): NavCommand = PRIVACY_POLICY.openWebLink()

    override fun openGameBandConnection(): NavCommand {
        return NavCommand.Navigate(SettingsFragmentDirections.openGameBandConnection())
    }

    override fun openGameIntegration(): NavCommand {
        return NavCommand.Navigate(SettingsFragmentDirections.openGameIntegration())
    }

    override fun openPickGame(key: PickGameKey): NavCommand = NavCommand.Deeplink(
        NavDeepLinkRequest.Builder.fromUri(
            context.getString(R.string.deep_link_go_to_pick_game_key_value, key).toUri()
        ).build()
    )

    override fun openPickGameById(gameId: Int, key: PickGameKey): NavCommand = NavCommand.Deeplink(
        NavDeepLinkRequest.Builder.fromUri(
            context.getString(R.string.deep_link_go_to_pick_game_value, gameId.toString(), key).toUri()
        ).build()
    )

    override fun openSelectRegion(): NavCommand {
        return NavCommand.Navigate(SaveEditConnectionFragmentDirections.openSelectRegion())
    }

    override fun openSaveConnection(gameNumber: Int, gameName: String, poster: String): NavCommand {
        return NavCommand.Navigate(PickGameFragmentDirections.openSaveConnection(gameNumber, gameName, poster))
    }

    override fun openChangePassword(): NavCommand {
        return NavCommand.Navigate(SettingsFragmentDirections.openChangePassword())
    }

    override fun openDeleteAccount(): NavCommand {
        return NavCommand.Navigate(SettingsFragmentDirections.openDeleteAccount())
    }

    override fun openSelectLanguage(): NavCommand {
        return NavCommand.Navigate(SettingsFragmentDirections.openSelectLanguage())
    }

    override fun goToAuth(): NavCommand = NavCommand.Deeplink(
        NavDeepLinkRequest.Builder.fromUri(
            context.getString(R.string.deep_link_go_to_auth).toUri()
        ).build(),
        R.id.nav_graph
    )

    override fun navigateToScanningForGameBand(): NavCommand = NavCommand.Deeplink(
        NavDeepLinkRequest.Builder.fromUri(
            context.getString(R.string.deep_link_go_to_scanning_for_game_band).toUri()
        ).build()
    )

    override fun navigateToStartNewSession(): NavCommand = NavCommand.Deeplink(
        NavDeepLinkRequest.Builder.fromUri(
            context.getString(R.string.deep_link_go_to_start_new_session).toUri()
        ).build()
    )

    override fun navigateToCreateTeam(): NavCommand = NavCommand.Deeplink(
        NavDeepLinkRequest.Builder.fromUri(
            context.getString(R.string.deep_link_open_create_teams).toUri()
        ).build()
    )

    override fun navigateToAddTeam(): NavCommand {
        return NavCommand.Navigate(CreateTeamFragmentDirections.actionCreateTeamFragmentToAddTeamFragment())
    }

    override fun navigateToOneTeam(): NavCommand {
        return NavCommand.Navigate(CreateTeamFragmentDirections.actionCreateTeamFragmentToOneTeamFragment())
    }

    override fun navigateToDashboard(): NavCommand = NavCommand.Deeplink(
        NavDeepLinkRequest.Builder.fromUri(
            context.getString(R.string.deep_link_go_to_dashboard).toUri()
        ).build()
    )
}