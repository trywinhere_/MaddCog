package com.maddcog.android.app.navigation

import android.content.Context
import androidx.core.net.toUri
import androidx.navigation.NavDeepLinkRequest
import com.maddcog.android.R
import com.maddcog.android.baseui.model.NavCommand
import com.maddcog.android.dashboard.IDashboardRouter

class DashboardsRouterImpl(private val context: Context) : IDashboardRouter {

    override fun navigateToSettings(): NavCommand = NavCommand.Deeplink(
        NavDeepLinkRequest.Builder.fromUri(
            context.getString(R.string.deep_link_go_to_settings).toUri()
        ).build()
    )

    override fun navigateToTrendsDetails(): NavCommand = NavCommand.Deeplink(
        NavDeepLinkRequest.Builder.fromUri(
            context.getString(R.string.deep_link_go_to_trends_details).toUri()
        ).build()
    )

    override fun navigateToTrendsDetailsWithFilter(filter: String): NavCommand = NavCommand.Deeplink(
        NavDeepLinkRequest.Builder.fromUri(
            context.getString(R.string.deep_link_go_to_trends_details_value, filter).toUri()
        ).build()
    )

    override fun navigateToScanningForGameBand(): NavCommand = NavCommand.Deeplink(
        NavDeepLinkRequest.Builder.fromUri(
            context.getString(R.string.deep_link_go_to_scanning_for_game_band).toUri()
        ).build()
    )

    override fun navigateToGameSummary(userActivityId: String): NavCommand = NavCommand.Deeplink(
        NavDeepLinkRequest.Builder.fromUri(
            context.getString(R.string.deep_link_open_game_summary_value, userActivityId).toUri()
        ).build()
    )

    override fun navigateToStartNewSession(): NavCommand = NavCommand.Deeplink(
        NavDeepLinkRequest.Builder.fromUri(
            context.getString(R.string.deep_link_go_to_start_new_session).toUri()
        ).build()
    )

    override fun navigateToOpenGameBandConnection(): NavCommand = NavCommand.Deeplink(
        NavDeepLinkRequest.Builder.fromUri(
            context.getString(R.string.deep_link_go_to_game_band_connection_screen).toUri()
        ).build()
    )
}