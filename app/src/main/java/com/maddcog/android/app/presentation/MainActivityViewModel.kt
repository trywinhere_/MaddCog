package com.maddcog.android.app.presentation

import android.content.ContentValues
import android.util.Log
import androidx.lifecycle.viewModelScope
import com.amplifyframework.auth.cognito.AWSCognitoAuthSession
import com.amplifyframework.core.Amplify
import com.maddcog.android.auth.IAuthRouter
import com.maddcog.android.baseui.presentation.BaseViewModel
import com.maddcog.android.domain.api.IAuthRepository
import com.maddcog.android.domain.api.ISharedPreferencesStorage
import com.maddcog.android.domain.entities.User
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainActivityViewModel @Inject constructor(
    private val authRepository: IAuthRepository,
    private val router: IAuthRouter,
    private val storage: ISharedPreferencesStorage
) : BaseViewModel() {

    fun getToken() {
        Amplify.Auth.fetchAuthSession(
            { result ->
                Log.i("RESULT FETCH SESSION ", result.toString())
                val cognitoAuthSession = result as AWSCognitoAuthSession
                val token = cognitoAuthSession.userPoolTokensResult.value?.idToken
                if (token != null) {
                    storage.saveToken(token)
                    storage.saveCognitoToken(token)
                }
            },
            { error -> Log.i(ContentValues.TAG, error.toString()) }
        )
    }

    fun getState(): Boolean {
        return authRepository.getStateSignIn()
    }

    fun signIn() {
        try {
            viewModelScope.launch {
                val email = authRepository.getEmail()
                val password = authRepository.getPassword()
                if (email != null && password != null) {
                    val user = User(email, password)
                    authRepository.signIn(user)
                }
            }
        } catch (_: Exception) {
        }
    }

    fun getStateGoogleSignIn(): Boolean {
        return storage.getStateGoogleSignIn()
    }

    fun getGoogleSignIn(): Boolean {
        return storage.getGoogleSignIn()
    }

    fun clearGoogleError() {
        authRepository.clearGoogleError()
    }
}