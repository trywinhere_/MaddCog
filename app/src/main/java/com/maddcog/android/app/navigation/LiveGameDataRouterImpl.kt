package com.maddcog.android.app.navigation

import android.content.Context
import androidx.core.net.toUri
import androidx.navigation.NavDeepLinkRequest
import com.maddcog.android.R
import com.maddcog.android.baseui.extensions.openWebLink
import com.maddcog.android.baseui.model.NavCommand
import com.maddcog.android.baseui.ui.theme.MADDCOG_WEBSITE
import com.maddcog.android.livegamedata.ILiveGameDataRouter
import com.maddcog.android.livegamedata.preparingtostart.PreparingToStartFragmentDirections
import com.maddcog.android.livegamedata.startnewsession.StartNewSessionFragmentDirections
import com.maddcog.android.settings.presentation.gameIntegration.presentation.pickGame.PickGameKey

class LiveGameDataRouterImpl(private val context: Context) : ILiveGameDataRouter {

    override fun openMaddcogWebsite(): NavCommand = MADDCOG_WEBSITE.openWebLink()

    override fun navigateToPreparingToStart(gameName: String?): NavCommand {
        return NavCommand.Navigate(StartNewSessionFragmentDirections.actionStartNewSessionFragmentToPreparingToStartFragment(gameName))
    }

    override fun navigateToLiveGameStats(): NavCommand {
        return NavCommand.Navigate(PreparingToStartFragmentDirections.actionPreparingToStartFragmentToPreparedToStartGameFragment())
    }

    override fun navigateToWarmUp(): NavCommand {
        return NavCommand.Navigate(PreparingToStartFragmentDirections.actionPreparingToStartFragmentToWarmUpFragment())
    }

    override fun navigateToPickGameDeepLink(key: PickGameKey): NavCommand = NavCommand.Deeplink(
        NavDeepLinkRequest.Builder.fromUri(
            context.getString(R.string.deep_link_go_to_pick_game_key_value, key).toUri()
        ).build()
    )

    override fun navigateToConnectedGameDeepLink(): NavCommand = NavCommand.Deeplink(
        NavDeepLinkRequest.Builder.fromUri(
            context.getString(R.string.deep_link_go_to_connected_games).toUri()
        ).build()
    )

    override fun navigateToLiveGameSummary(gameName: String): NavCommand = NavCommand.Deeplink(
        NavDeepLinkRequest.Builder.fromUri(
            context.getString(R.string.deep_link_go_to_live_game_summary_value, gameName).toUri()
        ).build()
    )

    override fun navigateToGameStatsAdding(gameName: String): NavCommand = NavCommand.Deeplink(
        NavDeepLinkRequest.Builder.fromUri(
            context.getString(R.string.deep_link_go_to_game_stats_value, gameName).toUri()
        ).build()
    )

    override fun navigateToDashboards(): NavCommand = NavCommand.Deeplink(
        NavDeepLinkRequest.Builder.fromUri(
            context.getString(R.string.deep_link_go_to_dashboard).toUri()
        ).build()
    )
}