package com.maddcog.android.app.navigation

import android.content.Context
import com.maddcog.android.baseui.model.NavCommand
import com.maddcog.android.trends.ITrendsDetailsRouter
import com.maddcog.android.trends.TrendsDetailsFragmentDirections

class TrendsDetailsRouterImpl(private val context: Context) : ITrendsDetailsRouter {

    override fun navigateToDurationFilters(duration: String, list: List<String>): NavCommand {
        return NavCommand.Navigate(TrendsDetailsFragmentDirections.goToDuration(duration, list.toTypedArray()))
    }

    override fun navigateToMetricFilters(metric: String, list: List<String>): NavCommand {
        return NavCommand.Navigate(TrendsDetailsFragmentDirections.goToMetric(metric, list.toTypedArray()))
    }

    override fun navigateToGameFilters(game: String, list: List<String>): NavCommand {
        return NavCommand.Navigate(
            TrendsDetailsFragmentDirections.goToGame(
                game,
                list.toTypedArray()
            )
        )
    }

    override fun navigateToTagFilters(tag: String, list: List<String>): NavCommand {
        return NavCommand.Navigate(TrendsDetailsFragmentDirections.goToTag(tag, list.toTypedArray()))
    }

    override fun navigateToChampionFilters(champion: String, list: List<String>): NavCommand {
        return NavCommand.Navigate(
            TrendsDetailsFragmentDirections.goToChampion(
                champion,
                list.toTypedArray()
            )
        )
    }

    override fun navigateToUsernameFilters(username: String, list: List<String>): NavCommand {
        return NavCommand.Navigate(TrendsDetailsFragmentDirections.goToUsername(username, list.toTypedArray()))
    }
}