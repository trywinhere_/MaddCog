package com.maddcog.android.app

import android.app.Application
import android.content.Context
import android.util.Log
import com.amazonaws.mobile.client.AWSMobileClient
import com.amazonaws.mobile.client.Callback
import com.amazonaws.mobile.client.UserStateDetails
import com.amazonaws.mobile.config.AWSConfiguration
import com.amazonaws.mobileconnectors.pinpoint.PinpointConfiguration
import com.amazonaws.mobileconnectors.pinpoint.PinpointManager
import com.amplifyframework.AmplifyException
import com.amplifyframework.auth.cognito.AWSCognitoAuthPlugin
import com.amplifyframework.core.Amplify
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.messaging.FirebaseMessaging
import dagger.hilt.android.HiltAndroidApp
import java.lang.Exception

@HiltAndroidApp
class MaddcogApp : Application() {

    var pinpointManager: PinpointManager? = null
    val TAG = "pinpoint"

    override fun onCreate() {
        super.onCreate()

        getPinpointManager(applicationContext)

        Amplify.addPlugin(AWSCognitoAuthPlugin())
        try {
            Amplify.configure(getApplicationContext())
            println("MyAmplifyApp" + "Initialized Amplify")
        } catch (error: AmplifyException) {
            println("MyAmplifyApp" + "Could not initialize Amplify" + error.toString())
        }
    }

    private fun getPinpointManager(applicationContext: Context?): PinpointManager {
        if (pinpointManager == null) {
            val awsConfig = AWSConfiguration(applicationContext)
            AWSMobileClient.getInstance()
                .initialize(
                    applicationContext, awsConfig,
                    object : Callback<UserStateDetails?> {
                        override fun onResult(result: UserStateDetails?) {
                            println("NOTIFICATIONS RESULT OK ")
                        }

                        override fun onError(e: Exception?) {
                            println("NOTIFICATIONS RESULT ERROR ")
                        }
                    }
                )
            val pinpointConfig = PinpointConfiguration(
                applicationContext,
                AWSMobileClient.getInstance(),
                awsConfig
            )
            pinpointManager = PinpointManager(pinpointConfig)
            FirebaseMessaging.getInstance().token
                .addOnCompleteListener(object : OnCompleteListener<String?> {
                    override fun onComplete(task: Task<String?>) {
                        if (!task.isSuccessful()) {
                            Log.w(
                                TAG,
                                "Fetching FCM registration token failed",
                                task.getException()
                            )
                            return
                        }
                        val token: String? = task.result
                        Log.d(TAG, "Registering push notifications token: $token")
                        pinpointManager!!.notificationClient.registerDeviceToken(token)
                    }
                })
        }
        return pinpointManager as PinpointManager
    }
}