package com.maddcog.android.app.navigation

import android.content.Context
import androidx.core.net.toUri
import androidx.navigation.NavDeepLinkRequest
import com.maddcog.android.R
import com.maddcog.android.auth.IAuthRouter
import com.maddcog.android.auth.loggingIn.LoggingInFragmentDirections
import com.maddcog.android.auth.signIn.SignInFragmentDirections
import com.maddcog.android.auth.signUp.SignUpFragmentDirections
import com.maddcog.android.baseui.extensions.openWebLink
import com.maddcog.android.baseui.model.NavCommand
import com.maddcog.android.baseui.ui.theme.PRIVACY_POLICY

class AuthRouterImpl(private val context: Context) : IAuthRouter {

    override fun navigateToSignUp(): NavCommand {
        return NavCommand.Navigate(SignInFragmentDirections.openSignUpFragment())
    }

    override fun navigateToLoggingIn(): NavCommand {
        return NavCommand.Navigate(SignInFragmentDirections.openLoggingInFragment())
    }

    override fun openPrivacyPolice(): NavCommand = PRIVACY_POLICY.openWebLink()

    override fun navigateToDashboard(): NavCommand = NavCommand.Deeplink(
        NavDeepLinkRequest.Builder.fromUri(
            context.getString(R.string.deep_link_go_to_dashboard).toUri()
        ).build(),
        R.id.nav_graph
    )

    override fun goToLoggingin(): NavCommand = NavCommand.Deeplink(
        NavDeepLinkRequest.Builder.fromUri(
            context.getString(R.string.deep_link_go_to_logginingin).toUri()
        ).build(),
        R.id.nav_graph
    )

    override fun navigateToLoggingInFromSignUp(): NavCommand {
        return NavCommand.Navigate(SignUpFragmentDirections.actionFragmentSignUpToFragmentLoggingIn())
    }

    override fun navigateToNotifications(): NavCommand {
        return NavCommand.Navigate(LoggingInFragmentDirections.actionFragmentLoggingInToNotificationsFragment())
    }
}