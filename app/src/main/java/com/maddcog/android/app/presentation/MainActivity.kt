package com.maddcog.android.app.presentation

import android.content.ContentValues.TAG
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.NavHostFragment
import com.amazonaws.auth.CognitoCachingCredentialsProvider
import com.amazonaws.mobile.client.*
import com.amazonaws.regions.Regions
import com.amplifyframework.auth.AuthChannelEventName
import com.amplifyframework.core.Amplify
import com.amplifyframework.core.InitializationStatus
import com.amplifyframework.hub.HubChannel
import com.amplifyframework.hub.HubEvent
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import com.maddcog.android.R
import com.maddcog.android.auth.LoginActivity
import dagger.hilt.android.AndroidEntryPoint
import java.lang.Exception

private const val POOL_ID = "us-west-2:acd00e8e-79df-4a34-8cfa-b43388f701e0"

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private val viewModel: MainActivityViewModel by viewModels()
    private lateinit var firebaseAnalytics: FirebaseAnalytics

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main_compose)

        /**
        If user is successfully authorized, his credentials will be saved in SharedPreferences.
        Also, getStateSignInScreen = false, so SignInScreen won't be shown, and app going straight
        to LoggingInScreen and loading user data.
         **/

        val state = viewModel.getState()

        val googleSignInState = viewModel.getStateGoogleSignIn()

        val googleSignIn = viewModel.getGoogleSignIn()

        viewModel.clearGoogleError()

        if (googleSignIn) {
            val intent = Intent(applicationContext, LoginActivity::class.java)
            startActivity(intent)
        } else if (!state || !googleSignInState) {
            val navHostFragment =
                supportFragmentManager.findFragmentById(R.id.container) as NavHostFragment

            val navController = navHostFragment.navController
            val authGraph = navController.navInflater.inflate(R.navigation.auth_graph)
            authGraph.setStartDestination(R.id.fragment_logging_in)

            val navGraph = navController.navInflater.inflate(R.navigation.nav_graph)
            navGraph.setStartDestination(R.id.auth_graph)

            navGraph.addDestination(authGraph)

            navController.graph = navGraph
        }

        /** Initialize the Amazon Cognito credentials provider */
        CognitoCachingCredentialsProvider(
            applicationContext,
            POOL_ID, // Identity pool ID
            Regions.US_WEST_2 // Region
        )

        firebaseAnalytics = Firebase.analytics

        Amplify.Hub.subscribe(HubChannel.AUTH) { hubEvent: HubEvent<*> ->
            when (hubEvent.name) {
                InitializationStatus.SUCCEEDED.toString() -> {
                    Log.i(TAG, "Amplify successfully initialized")
                }
                InitializationStatus.FAILED.toString() -> {
                    Log.i(TAG, "Amplify initialization failed")
                }
                else -> {
                    when (AuthChannelEventName.valueOf(hubEvent.name)) {
                        AuthChannelEventName.SIGNED_IN -> {
                            Log.i(TAG, "HUB : SIGNED_IN")
                        }
                        AuthChannelEventName.SIGNED_OUT -> {
                            Log.i(TAG, "HUB : SIGNED_OUT")
                        }
                        else -> Log.i(TAG, """HUB EVENT:${hubEvent.name}""")
                    }
                }
            }
        }

        Amplify.Auth.fetchUserAttributes(
            { attributes -> Log.i("AuthDemo ", "User attributes = $attributes") },
            { Log.e("AuthDemo ", "Failed to fetch user attributes", it) }
        )

        AWSMobileClient.getInstance().initialize(
            this,
            object : Callback<UserStateDetails> {
                override fun onResult(userStateDetails: UserStateDetails) {
                    println("INIT " + userStateDetails.userState)
                }

                override fun onError(e: Exception) {
                    println("INIT Error during initialization$e")
                }
            }
        )
    }
}