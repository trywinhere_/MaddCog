package com.maddcog.android.app.navigation

import android.content.Context
import com.maddcog.android.gameSummary.IGameSummaryRouter

class GameSummaryRouterImpl(private val context: Context) : IGameSummaryRouter
