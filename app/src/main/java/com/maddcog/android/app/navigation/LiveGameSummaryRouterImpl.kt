package com.maddcog.android.app.navigation

import android.content.Context
import androidx.core.net.toUri
import androidx.navigation.NavDeepLinkRequest
import com.maddcog.android.R
import com.maddcog.android.baseui.model.NavCommand
import com.maddcog.android.liveGameSummary.ILiveGameSummaryRouter
import com.maddcog.android.liveGameSummary.LiveGameSummaryFragmentDirections

class LiveGameSummaryRouterImpl(private val context: Context) : ILiveGameSummaryRouter {

    override fun navigateToBreakTime(): NavCommand {
        return NavCommand.Navigate(LiveGameSummaryFragmentDirections.openBreakTime())
    }

    override fun navigateToDashboards(): NavCommand = NavCommand.Deeplink(
        NavDeepLinkRequest.Builder.fromUri(
            context.getString(R.string.deep_link_go_to_dashboard).toUri()
        ).build()
    )

    override fun navigateToGameStats(gameName: String): NavCommand {
        return NavCommand.Navigate(LiveGameSummaryFragmentDirections.openGameStats(gameName))
    }
}