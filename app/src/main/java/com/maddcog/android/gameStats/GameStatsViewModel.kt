package com.maddcog.android.gameStats

import com.maddcog.android.baseui.model.StatefulData
import com.maddcog.android.baseui.presentation.BaseViewModel
import com.maddcog.android.domain.api.ILiveGameDataRepository
import com.maddcog.android.domain.api.ISharedPreferencesStorage
import com.maddcog.android.livegamedata.ILiveGameDataRouter
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

@HiltViewModel
class GameStatsViewModel @Inject constructor(
    private val repository: ILiveGameDataRepository,
    private val router: ILiveGameDataRouter,
    private val storage: ISharedPreferencesStorage,
) : BaseViewModel() {

    private val _sendData = statefulSharedFlow<Unit>()
    val sendData: Flow<StatefulData<Unit>>
        get() = _sendData

    val gameStats  = GameStats()

    fun saveStat(key: String, value: String) {
        val newMap = gameStats.performanceParams
        newMap[key] = value

        gameStats.performanceParams = newMap
    }

    fun saveMainValue(value: String) {
        gameStats.outcome = value
    }

    fun sendSensorData() {
        _sendData.fetch {
            repository.sendRawActivity(gameStats)
        }
    }

    fun navigateToLiveGameSummary() {
        storage.getNewSessionInfo()?.name?.let {
            navigateTo(router.navigateToLiveGameSummary(it))
        }
    }
}

data class GameStats(
    var outcome: String = "",
    var performanceParams: MutableMap<String, String> = mutableMapOf()
)