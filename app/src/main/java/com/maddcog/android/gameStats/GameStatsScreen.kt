package com.maddcog.android.gameStats

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.selection.selectable
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.semantics.Role
import com.maddcog.android.R
import com.maddcog.android.baseui.components.BaseTextField
import com.maddcog.android.baseui.components.BasicTopAppBar
import com.maddcog.android.baseui.components.SelectableButton
import com.maddcog.android.baseui.components.SimpleButton
import com.maddcog.android.baseui.ui.theme.Spacing_12
import com.maddcog.android.baseui.ui.theme.Spacing_16
import com.maddcog.android.gameSummary.extensions.MainStatType
import com.maddcog.android.gameSummary.extensions.toGameStatsList
import com.maddcog.android.gameSummary.extensions.toMainStatType
import com.maddcog.android.settings.components.SectionHeader

@Composable
fun GameStatsScreen(
    viewModel: GameStatsViewModel,
    gameName: String,
) {
    val localContext = LocalContext.current
    val resultList = listOf("Win", "Loss", "Draw")
    val currentLocale = resultList.first()
    var selectedItem by remember {
        mutableStateOf(
            resultList.find {
                it == currentLocale
            }
        )
    }

    Column() {
        BasicTopAppBar(title = R.string.enter_game_stats) {
            viewModel.navigateBack()
        }
        Box(Modifier.fillMaxWidth()) {
            LazyColumn(
                modifier = Modifier
                    .background(Color.Black)
                    .fillMaxSize()
                    .padding(Spacing_12)
            ) {
                item {
                    gameName.toMainStatType().apply {
                        when (this) {
                            MainStatType.WIN_LOSS -> {
                                SectionHeader(name = R.string.game_filter)
                                resultList.forEach { label ->
                                    SelectableButton(
                                        name = label,
                                        selectionTerm = (selectedItem == label),
                                        modifier = Modifier.selectable(
                                            selected = (selectedItem == label),
                                            onClick = {
                                                selectedItem = label
                                            },
                                            role = Role.RadioButton,
                                        )
                                    )
                                    Spacer(modifier = Modifier.height(Spacing_16))
                                }
                            }
                            MainStatType.POSITION -> {
                                SectionHeader(name = R.string.position)
                                BaseTextField(
                                    placeholder = R.string.user_name_placeholder,
                                    saveText = {
                                        viewModel.saveMainValue(it)
                                    }
                                )
                                Spacer(modifier = Modifier.height(Spacing_16))
                            }
                            MainStatType.NONE -> { /* nothing */ }
                        }
                    }
                    gameName.toGameStatsList().forEach { label ->
                        SectionHeader(name = label)
                        BaseTextField(
                            placeholder = label,
                            saveText = {
                                viewModel.saveStat(localContext.resources.getResourceName(label), it)
                            }
                        )
                    }
                    Spacer(modifier = Modifier.height(Spacing_16))
                    SimpleButton(name = R.string.save_btn) {
                        viewModel.sendSensorData()
                    }
                    Spacer(modifier = Modifier.height(Spacing_16))
                }
            }
        }
    }
}