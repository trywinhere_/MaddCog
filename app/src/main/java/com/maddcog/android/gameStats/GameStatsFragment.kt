package com.maddcog.android.gameStats

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.ui.platform.ComposeView
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.maddcog.android.baseui.presentation.BaseFragment
import com.maddcog.android.baseui.presentation.BaseViewModel
import com.maddcog.android.baseui.ui.theme.MaddcogTheme
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class GameStatsFragment : BaseFragment() {

    private val viewModel: GameStatsViewModel by viewModels()
    private val args by navArgs<GameStatsFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = ComposeView(requireContext())
        view.setContent {
            MaddcogTheme {
                GameStatsScreen(
                    viewModel = viewModel,
                    gameName = args.gameName,
                )
            }
        }

        return view
    }

    override fun withViewModel(): BaseViewModel = viewModel.apply {
        sendData.collectWithState { state ->
            state.isSuccessful {
                showToastMessage("Data was successfully sended!")
                viewModel.navigateToLiveGameSummary()
            }
            state.isError {
                showToastMessage(message = it.message.toString())
            }
        }
    }
}