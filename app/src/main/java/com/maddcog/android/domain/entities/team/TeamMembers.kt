package com.maddcog.android.domain.entities.team

data class TeamMembers(
    var username: String? = null,
    var status: Int? = null,
    var owner: Boolean? = null,
    var imageUrl: String? = null,
    var teamUserId: String? = null
)