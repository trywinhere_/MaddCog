package com.maddcog.android.domain.entities.activity

import com.maddcog.android.domain.entities.user.UserProfile

data class UserActivityData(
    val userActivity: UserActivity,
    val profile: UserProfile?,
)