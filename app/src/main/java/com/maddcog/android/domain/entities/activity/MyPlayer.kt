package com.maddcog.android.domain.entities.activity

data class MyPlayer(
    val characterName: String,
    val characterImage: String,
    val fatigue: Double,
    val fatigueText: String,
    val fatigueEnd: Double,
    val flow: Double,
    val flowText: String,
    val gamerName: String,
    val gamerImage: String,
    var grade: Double,
    var outcome: String,
    val performanceParams: List<PerformanceParam>,
    val position: Int,
    val score: Int,
    val scoreText: String,
)