package com.maddcog.android.domain.api

import android.app.Activity
import androidx.lifecycle.LiveData
import com.maddcog.android.domain.entities.User
import com.maddcog.android.domain.entities.UserSignUp

interface IAuthRepository {

    val googleToken: LiveData<String?>

    val googleSignInError: LiveData<Boolean>

    val errorResponseSignIn: LiveData<String?>

    val tokenResponseSignIn: LiveData<String?>

    val errorResponseSignUp: LiveData<String?>

    val tokenResponse: LiveData<String?>

    val errorResponse: LiveData<String?>

    val successChangePassword: LiveData<Boolean>

    val errorChangePassword: LiveData<String?>

    fun signIn(user: User)

    suspend fun signUp(userInput: UserSignUp)

    suspend fun changePassword(oldPassword: String, newPassword: String)

    fun saveEmail(email: String)
    fun getEmail(): String?

    fun savePassword(password: String)
    fun getPassword(): String?

    fun getStateSignIn(): Boolean

    fun saveStateSignIn(showing: Boolean)

    fun googleSignIn(activity: Activity)

    fun clearTokenResponse()

    fun signOut()

    fun clearErrorsSignIn()

    fun clearErrorsSignUp()

    fun clearChangePassword()

    fun getStateSignInLoggingingIn(): Boolean

    fun saveStateSignInLoggingingIn(showing: Boolean)

    fun clearGoogleError()
}