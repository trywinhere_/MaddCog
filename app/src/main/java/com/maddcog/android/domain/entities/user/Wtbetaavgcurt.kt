package com.maddcog.android.domain.entities.user

data class Wtbetaavgcurt(
    val count: Int? = null,
    val sd: Double? = null,
    val startingSd: Double? = null,
    val mean: Double? = null,
    val startingMean: Double? = null
)