package com.maddcog.android.domain.api

import com.maddcog.android.domain.entities.ActivityItem
import com.maddcog.android.domain.entities.activity.UserActivity

interface IActivitiesApiService {

    suspend fun loadAllActivitiesList(): List<ActivityItem>

    suspend fun getUserActivities(): List<UserActivity>

    suspend fun getUserActivityById(userActivityId: String): UserActivity
}