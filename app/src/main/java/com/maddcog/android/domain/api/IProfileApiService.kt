package com.maddcog.android.domain.api

import com.maddcog.android.domain.entities.UserGame
import com.maddcog.android.domain.entities.user.UserProfile

interface IProfileApiService {

    suspend fun loadUserProfile(): UserProfile?

    suspend fun deleteUser()

    suspend fun deleteConnectedGame(game: UserGame)

    suspend fun saveConnection(number: Int, username: String, region: String)
}