package com.maddcog.android.domain.usecase

import com.maddcog.android.domain.api.IAuthRepository
import com.maddcog.android.domain.api.IProfileApiService
import com.maddcog.android.domain.api.ISharedPreferencesStorage
import com.maddcog.android.domain.api.IUserDataRepository
import com.maddcog.android.domain.entities.UserGame
import com.maddcog.android.domain.entities.user.UserProfileData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import javax.inject.Inject

class SettingsUseCase @Inject constructor(
    private val profileApi: IProfileApiService,
    private val authRepository: IAuthRepository,
    private val userRepository: IUserDataRepository,
    private val localStorage: ISharedPreferencesStorage,
) {

    suspend fun loadUserProfileData(): UserProfileData {
        return coroutineScope {
            val profileAsync = async(Dispatchers.IO) {
                localStorage.getUserProfile()
            }
            val languagesAsync = async(Dispatchers.IO) {
                localStorage.getLanguageList()
            }

            UserProfileData(
                profile = profileAsync.await(),
                languageList = languagesAsync.await(),
            )
        }
    }

    suspend fun performLogout() {
        authRepository.clearTokenResponse()
        authRepository.signOut()
        localStorage.clearOnLogout()
    }

    suspend fun deleteUser() {
        profileApi.deleteUser()
    }

    suspend fun deleteConnectedGame(game: UserGame) {
        profileApi.deleteConnectedGame(game)
        userRepository.loadAndSaveProfile()
    }

    suspend fun saveConnectedGame(number: Int, username: String, region: String) {
        profileApi.saveConnection(number, username, region)
        userRepository.loadAndSaveProfile()
    }

    suspend fun changePassword(oldPassword: String, newPassword: String) {
        authRepository.changePassword(oldPassword, newPassword)
    }

    fun getDeviceSavedStatus(): Boolean {
        return localStorage.getDeviceStatus()
    }
}