package com.maddcog.android.domain.entities.activity

data class MentalPerformanceTimeline(
    val fatigue: Double,
    val flow: Double,
    val timestamp: Int,
)