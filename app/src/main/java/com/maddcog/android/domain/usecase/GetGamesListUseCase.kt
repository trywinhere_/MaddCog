package com.maddcog.android.domain.usecase

import com.maddcog.android.baseui.model.Resource
import com.maddcog.android.baseui.ui.theme.INTERNET_CONNECTION_ERROR
import com.maddcog.android.baseui.ui.theme.UNEXPECTED_ERROR
import com.maddcog.android.domain.api.IActivitiesApiService
import com.maddcog.android.domain.api.ISharedPreferencesStorage
import com.maddcog.android.domain.entities.ActivityItem
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import java.io.IOException
import java.util.*
import javax.inject.Inject

open class GetGamesListUseCase @Inject constructor(
    private val api: IActivitiesApiService,
    private val storage: ISharedPreferencesStorage
) {
    open operator fun invoke(): Flow<Resource<List<ActivityItem>>> = flow {
        try {
            emit(Resource.Loading())
            val activities = api.loadAllActivitiesList()

            emit(Resource.Success(activities))
        } catch (e: HttpException) {
            emit(Resource.Error(e.localizedMessage ?: UNEXPECTED_ERROR))
        } catch (e: IOException) {
            emit(Resource.Error(INTERNET_CONNECTION_ERROR))
        }
    }

    fun saveGameForNewSession(item: ActivityItem) {
        storage.saveNewSessionInfo(item)
        storage.saveNewSessionUUID(UUID.randomUUID().toString())
    }
}