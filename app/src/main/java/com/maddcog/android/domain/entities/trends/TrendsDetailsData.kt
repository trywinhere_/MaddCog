package com.maddcog.android.domain.entities.trends

import com.maddcog.android.domain.entities.activity.UserActivity

data class TrendsDetailsData(
    val trendsInsight: String,
    val trendsInsightValue: Int,
    var durationFilter: String,
    var durationList: List<String>,
    var metricFilter: String,
    var metricList: List<String>,
    var gameFilter: String,
    var gameList: List<String>,
    var tagFilter: String,
    var tagList: List<String>,
    var championFilter: String,
    var championsList: List<String>,
    var userNameFilter: String,
    var userNameList: List<String>,
    val initialData: List<UserActivity>,
)