package com.maddcog.android.domain.api

import com.maddcog.android.domain.entities.activity.UserActivity
import com.maddcog.android.gameStats.GameStats

interface ILiveGameDataRepository {

    fun addActivity(activity: UserActivity): Boolean

    fun addTags(tags: List<String>, activity: UserActivity): List<String>

    fun addGame(activityTypeName: String, activity: UserActivity): String

    fun addTagsToStorage(tags: Set<String>)

    suspend fun sendFakeSensorData()

    suspend fun sendFakeRawActivity()

    suspend fun sendRawActivity(gameStats: GameStats)
}