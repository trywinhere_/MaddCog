package com.maddcog.android.domain.entities.activity

data class TimelineEvent(
    val name: String,
    val timestamps: List<Int>,
)