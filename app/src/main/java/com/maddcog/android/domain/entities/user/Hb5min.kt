package com.maddcog.android.domain.entities.user

data class Hb5min(
    val count: Int? = null,
    val sd: Double? = null,
    val startingSd: Int? = null,
    val mean: Double? = null,
    val startingMean: Int? = null
)