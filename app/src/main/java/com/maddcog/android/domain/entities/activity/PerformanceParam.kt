package com.maddcog.android.domain.entities.activity

data class PerformanceParam(
    val name: String,
    val value: Double,
    val order: Int,
)