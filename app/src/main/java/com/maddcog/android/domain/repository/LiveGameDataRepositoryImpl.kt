package com.maddcog.android.domain.repository

import com.maddcog.android.data.network.MaddcogApi
import com.maddcog.android.data.sensordatarecorder.IStorage
import com.maddcog.android.domain.api.ILiveGameDataRepository
import com.maddcog.android.domain.api.ISensorDataApiService
import com.maddcog.android.domain.api.ISharedPreferencesStorage
import com.maddcog.android.domain.entities.activity.UserActivity
import com.maddcog.android.gameStats.GameStats
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
open class LiveGameDataRepositoryImpl @Inject constructor(
    private val api: MaddcogApi,
    private val storage: ISharedPreferencesStorage,
    private val sensorApiService: ISensorDataApiService,
    private val rawDataStorage: IStorage,
) : ILiveGameDataRepository {

    override fun addActivity(activity: UserActivity): Boolean {
        val activitiesList = mutableListOf<UserActivity>()
        activitiesList.add(activity)
        return activitiesList.contains(activity)
    }

    override fun addTags(tags: List<String>, activity: UserActivity): List<String> {
        activity.tags = tags
        return activity.tags
    }

    override fun addGame(activityTypeName: String, activity: UserActivity): String {
        activity.activityType.name = activityTypeName
        return activity.activityType.name
    }

    override fun addTagsToStorage(tags: Set<String>) {
        storage.saveTags(tags)
    }

    override suspend fun sendFakeSensorData() {
        sensorApiService.sendSensorData()
    }

    override suspend fun sendFakeRawActivity() {
        sensorApiService.sendRawActivity()
    }

    override suspend fun sendRawActivity(gameStats: GameStats) {
        val uuid = storage.getNewSessionUUID() ?: run {
            val newUuid = UUID.randomUUID().toString()
            storage.saveNewSessionUUID(newUuid)
            newUuid
        }
        storage.getNewSessionInfo()?.let { activityItem ->
            sensorApiService.sendNewGameSession(gameStats, rawDataStorage.getMentalPerformanceList(), activityItem, uuid)
        }
    }
}