package com.maddcog.android.domain.api

interface IResourcesProvider {

    fun getString(id: Int): String

    fun getInteger(id: Int): Int

    fun getStringFormatted(id: Int, vararg args: Any): String
}