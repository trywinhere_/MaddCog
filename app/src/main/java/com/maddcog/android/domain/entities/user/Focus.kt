package com.maddcog.android.domain.entities.user

data class Focus(
    val count: Int? = null,
    val sd: Double? = null,
    val mean: Double? = null
)