package com.maddcog.android.domain.usecase

import com.maddcog.android.domain.api.ISharedPreferencesStorage
import com.maddcog.android.domain.api.ITeamsApiService
import com.maddcog.android.domain.entities.team.Team
import javax.inject.Inject

open class TeamUseCase @Inject constructor(
    private val apiService: ITeamsApiService,
    private val storage: ISharedPreferencesStorage
) {

    open suspend fun leaveTeam(teamId: String): Boolean {
        return apiService.leaveTeam(teamId)
    }

    open suspend fun getTeams(): List<Team> {
        return apiService.getTeams()
    }

    fun saveTeamName(name: String) {
        storage.saveTeamName(teamName = name)
    }

    fun getTeamName(): String? {
        return storage.getTeamName()
    }

    open suspend fun removeTeammate(userId: String) {
        apiService.removeTeamMate(userId)
    }

    open suspend fun inviteUser(userEmail: String): Boolean {
        return apiService.inviteUser(userEmail)
    }
}