package com.maddcog.android.domain.entities.activity

data class TeamMember(
    val characterName: String,
    val characterImage: String,
    val fatigue: Double,
    val fatigueText: String,
    val flow: Double,
    val flowText: String,
    val gamerImage: String,
    val gamerName: String,
    val grade: Double,
    val performanceParams: List<TeamMemberParam>,
)