package com.maddcog.android.domain.entities.activity

data class Match(
    val start: Int,
    val end: Int,
    val gameMode: String,
    val mapName: String,
    var myPlayer: MyPlayer,
    val stayedTillGameOver: Boolean,
    val team: List<TeamMember>,
    val teamCtScore: Int,
    val teamTScore: Int,
    val tileImage: String,
    val tileMetricDescription: String,
    val tileMetricText: String,
    val timelineEvents: List<TimelineEvent>,
)