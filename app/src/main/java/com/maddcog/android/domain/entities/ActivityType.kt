package com.maddcog.android.domain.entities

import com.google.gson.annotations.SerializedName

data class ActivityType(
    @SerializedName("activity_type_id") var activityTypeId: Int? = null,
    @SerializedName("activity_type_name") var activityTypeName: String? = null,
    @SerializedName("image_square") var imageSquare: String? = null,
    @SerializedName("image_rectangle") var imageRectangle: String? = null
)