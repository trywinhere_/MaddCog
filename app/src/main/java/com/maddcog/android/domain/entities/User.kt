package com.maddcog.android.domain.entities

data class User(
    val email: String,
    var password: String
)