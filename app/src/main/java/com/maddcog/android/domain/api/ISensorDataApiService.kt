package com.maddcog.android.domain.api

import com.maddcog.android.data.network.dto.rawActivity.RawMentalPerformance
import com.maddcog.android.domain.entities.ActivityItem
import com.maddcog.android.gameStats.GameStats

interface ISensorDataApiService {

    suspend fun sendSensorData()

    suspend fun sendRawActivity()

    suspend fun sendNewGameSession(gameStats: GameStats, mentalList: List<RawMentalPerformance>, activityItem: ActivityItem, uuid: String)
}