package com.maddcog.android.domain.entities.activity

data class ActivitySummary(
    var fatigueZoneHigh: Double,
    val fatigueZoneNormal: Double,
    val fatigueZoneLow: Double,
    val flowZoneNormal: Double,
    var flowZoneHigh: Double,
    val fatigueEndAvg: Double,
    val flowAvg: Double,
    val flowZoneLow: Double,
    val fatigueAvg: Double,
)