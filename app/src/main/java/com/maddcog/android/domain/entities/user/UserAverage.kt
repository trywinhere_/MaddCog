package com.maddcog.android.domain.entities.user

data class UserAverage(
    val name: String,
    val avg: Double,
    val median: Double,
)