package com.maddcog.android.domain.entities.user

data class FlowSd(
    val count: Int? = null,
    val sd: Double? = null,
    val mean: Double? = null
)