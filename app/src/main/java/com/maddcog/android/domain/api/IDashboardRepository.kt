package com.maddcog.android.domain.api

import androidx.lifecycle.LiveData
import com.maddcog.android.domain.entities.ActivityItem
import com.maddcog.android.domain.entities.activity.UserActivity
import com.maddcog.android.domain.entities.team.Team

interface IDashboardRepository {

    val teamInfo: LiveData<List<Team>>
    val activityTypeInfo: LiveData<List<ActivityItem>>
    val allActivitiesInfo: LiveData<List<UserActivity>>

    suspend fun getTeams()

    suspend fun getActivityTypes()

    suspend fun getAllActivitiesInfo()

    suspend fun deleteActivity(userActivity: UserActivity)
}