package com.maddcog.android.domain.entities.team

data class Team(
    var teamMembers: ArrayList<TeamMembers> = arrayListOf(),
    var teamName: String? = null,
    var teamId: String? = null,
    var exchange: String? = null
)