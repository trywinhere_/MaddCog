package com.maddcog.android.domain.repository

import androidx.lifecycle.MutableLiveData
import com.maddcog.android.data.network.MaddcogApi
import com.maddcog.android.data.network.errors.NetWorkException
import com.maddcog.android.data.network.errors.UnknownException
import com.maddcog.android.domain.api.IDashboardRepository
import com.maddcog.android.domain.entities.ActivityItem
import com.maddcog.android.domain.entities.activity.UserActivity
import com.maddcog.android.domain.entities.team.Team
import com.maddcog.android.domain.usecase.DashboardUseCase
import java.io.IOException
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
open class DashboardRepositoryImpl @Inject constructor(
    private val apiService: MaddcogApi,
    private val useCase: DashboardUseCase
) : IDashboardRepository {

    override val teamInfo = MutableLiveData<List<Team>>()

    override suspend fun getTeams() {
        val teams = useCase.getTeams()
        teamInfo.value = teams
    }

    override val activityTypeInfo = MutableLiveData<List<ActivityItem>>()

    override suspend fun getActivityTypes() {
        val result = useCase.getActivitiesTypes()
        activityTypeInfo.value = result
    }

    override val allActivitiesInfo = MutableLiveData<List<UserActivity>>()

    override suspend fun getAllActivitiesInfo() {
        val result = useCase.getActivities()
        allActivitiesInfo.value = result
    }

    override suspend fun deleteActivity(userActivity: UserActivity) {
        try {
            apiService.deleteActivity(userActivity.activityId)
            getAllActivitiesInfo()
        } catch (e: IOException) {
            throw NetWorkException
        } catch (e: Exception) {
            throw UnknownException
        }
    }
}