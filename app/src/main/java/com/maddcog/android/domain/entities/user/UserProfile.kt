package com.maddcog.android.domain.entities.user

import com.maddcog.android.domain.entities.UserGame

data class UserProfile(
    val id: String,
    val email: String,
    val name: String,
    val userGames: List<UserGame>,
    val userData: UserData,
    val endpoint: String,
)