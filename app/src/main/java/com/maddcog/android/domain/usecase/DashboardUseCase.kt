package com.maddcog.android.domain.usecase

import com.maddcog.android.domain.api.IActivitiesApiService
import com.maddcog.android.domain.api.ISharedPreferencesStorage
import com.maddcog.android.domain.api.ITeamsApiService
import com.maddcog.android.domain.entities.ActivityItem
import com.maddcog.android.domain.entities.activity.UserActivity
import com.maddcog.android.domain.entities.team.Team
import javax.inject.Inject

open class DashboardUseCase @Inject constructor(
    private val apiService: IActivitiesApiService,
    private val teamsApiService: ITeamsApiService,
    private val storage: ISharedPreferencesStorage
) {

    open suspend fun getActivities(): List<UserActivity> {
        return try {
            val result = apiService.getUserActivities()
            if (result != null) {
                storage.saveUserActivityList(result)
            }
            result
        } catch (e: Exception) {
            storage.getUserActivityList() ?: emptyList()
        }
    }

    open suspend fun getActivitiesTypes(): List<ActivityItem> {
        return try {
            val result = apiService.loadAllActivitiesList()
            if (result != null) {
                storage.saveActivityItemList(result)
            }
            result
        } catch (e: Exception) {
            storage.getActivityItemList() ?: emptyList()
        }
    }

    open suspend fun getTeams(): List<Team> {
        return try {
            val teams = teamsApiService.getTeams()
            if (teams != null) {
                storage.saveTeamsList(teams)
            }
            teams
        } catch (e: Exception) {
            storage.getTeamsList() ?: emptyList()
        }
    }

    fun saveGameName(title: String) {
        storage.saveGameName(title)
    }

    fun saveGameUrl(url: String) {
        storage.saveGameUrl(url)
    }

    fun getSavedStatus(): Boolean {
        return storage.getDeviceStatus()
    }
}