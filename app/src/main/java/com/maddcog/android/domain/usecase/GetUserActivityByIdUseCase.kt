package com.maddcog.android.domain.usecase

import com.maddcog.android.baseui.model.Resource
import com.maddcog.android.baseui.ui.theme.INTERNET_CONNECTION_ERROR
import com.maddcog.android.baseui.ui.theme.UNEXPECTED_ERROR
import com.maddcog.android.domain.api.IActivitiesApiService
import com.maddcog.android.domain.api.ISharedPreferencesStorage
import com.maddcog.android.domain.entities.activity.UserActivityData
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

class GetUserActivityByIdUseCase @Inject constructor(
    private val api: IActivitiesApiService,
    private val storage: ISharedPreferencesStorage,
) {

    operator fun invoke(userActivityId: String): Flow<Resource<UserActivityData?>> = flow {
        try {
            emit(Resource.Loading())
            val userActivity = api.getUserActivityById(userActivityId)
            val userData = storage.getUserProfile()
            emit(
                Resource.Success(
                    UserActivityData(
                        userActivity, userData
                    )
                )
            )
        } catch (e: HttpException) {
            emit(Resource.Error(e.localizedMessage ?: UNEXPECTED_ERROR))
        } catch (e: IOException) {
            emit(Resource.Error(INTERNET_CONNECTION_ERROR))
        }
    }

    fun getLastSessionUuid(): String? {
        return storage.getNewSessionUUID()
    }
}