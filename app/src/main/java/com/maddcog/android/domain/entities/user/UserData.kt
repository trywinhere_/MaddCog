package com.maddcog.android.domain.entities.user

data class UserData(
    val fatiguePercentiles: FatiguePercentiles? = FatiguePercentiles(),
    val fatiguePred: FatiguePred? = FatiguePred(),
    val fatigueStart: FatigueStart? = FatigueStart(),
    val flow: Flow? = Flow(),
    val flowSd: FlowSd? = FlowSd(),
    val focus: Focus? = Focus(),
    val hb5min: Hb5min? = Hb5min(),
    val lfhf5min: Lfhf5min? = Lfhf5min(),
    val mf5min: Mf5min? = Mf5min(),
    val tablog5min: Tablog5min? = Tablog5min(),
    val tplog5min: Tplog5min? = Tplog5min(),
    val trendsInsight: String,
    val trendsInsightValue: Int,
    val userInsight: String,
    val wtalphaavgcurt: Wtalphaavgcurt? = Wtalphaavgcurt(),
    val wtbetaavgcurt: Wtbetaavgcurt? = Wtbetaavgcurt()
)