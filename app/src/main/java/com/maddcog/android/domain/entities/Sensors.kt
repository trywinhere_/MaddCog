package com.maddcog.android.domain.entities

import com.google.gson.annotations.SerializedName

data class Sensors(
    @SerializedName("sensor_name")
    val sensorName: String,
    @SerializedName("sensor_type")
    val sensorType: String
)