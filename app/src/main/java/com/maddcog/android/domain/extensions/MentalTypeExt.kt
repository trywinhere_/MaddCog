package com.maddcog.android.domain.extensions

import com.maddcog.android.domain.entities.MentalType

fun MentalType.toDoubleValue(): Double {
    return when (this) {
        MentalType.MID -> 5.0
        MentalType.HI -> 10.0
        MentalType.LOW -> 0.0
        MentalType.NONE -> 0.0
    }
}