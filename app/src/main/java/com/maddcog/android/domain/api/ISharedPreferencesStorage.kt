package com.maddcog.android.domain.api

import android.content.SharedPreferences
import com.maddcog.android.domain.entities.ActivityItem
import com.maddcog.android.domain.entities.RegionType
import com.maddcog.android.domain.entities.activity.UserActivity
import com.maddcog.android.domain.entities.team.Team
import com.maddcog.android.domain.entities.user.UserProfile
import com.maddcog.android.settings.presentation.selectLanguage.model.Language

interface ISharedPreferencesStorage {

    fun getLanguageList(): List<Language>

    fun getRegionList(): List<RegionType>

    fun saveUserName(name: String)
    fun getUserName(): String?

    fun saveCognitoToken(token: String)
    fun getCognitoToken(): String?

    fun saveToken(token: String)
    fun getToken(): String?

    fun saveEmail(email: String)
    fun getEmail(): String?

    fun savePassword(password: String)
    fun getPassword(): String?

    fun saveStateSignInScreen(showing: Boolean)
    fun getStateSignInScreen(): Boolean

    fun registerSharedPrefsListener(changeListener: SharedPreferences.OnSharedPreferenceChangeListener)
    fun unregisterSharedPrefsListener(changeListener: SharedPreferences.OnSharedPreferenceChangeListener)

    suspend fun saveUserProfile(profile: UserProfile)
    suspend fun getUserProfile(): UserProfile?

    fun clearOnLogout(): Boolean

    fun saveStateSignInInLogginingIn(showing: Boolean)
    fun getStateSignInInLogginingIn(): Boolean

    fun saveStateNotifications(showing: Boolean)
    fun getStateNotifications(): Boolean

    fun saveStateGoogleSignIn(showing: Boolean)
    fun getStateGoogleSignIn(): Boolean

    fun saveGoogleSignIn(act: Boolean)
    fun getGoogleSignIn(): Boolean

    fun saveGameName(gameName: String)
    fun getGameName(): String?

    fun saveTags(tags: Set<String>)
    fun getTags(): MutableSet<String>?

    fun saveTeamName(teamName: String)
    fun getTeamName(): String?

    fun saveGameUrl(gameUrl: String)
    fun getGameUrl(): String?

    fun saveNewSessionInfo(activityItem: ActivityItem)
    fun getNewSessionInfo(): ActivityItem?

    fun saveNewSessionUUID(uuid: String)
    fun getNewSessionUUID(): String?

    fun saveTeamsList(teams: List<Team>)
    fun getTeamsList(): List<Team>?

    fun saveActivityItemList(activityItems: List<ActivityItem>)
    fun getActivityItemList(): List<ActivityItem>?

    fun saveUserActivityList(userActivityList: List<UserActivity>)
    fun getUserActivityList(): List<UserActivity>?

    fun saveDeviceStatus(isConnected: Boolean)

    fun getDeviceStatus(): Boolean
}