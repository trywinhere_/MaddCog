package com.maddcog.android.domain.entities.user

data class FatiguePercentiles(
    val high: Int? = null,
    val low: Int? = null,
    val max: Int? = null,
    val base: Int? = null
)