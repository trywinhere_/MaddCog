package com.maddcog.android.domain.api

import androidx.lifecycle.LiveData
import com.maddcog.android.domain.entities.AimGame

interface IAimGameRepository {

    val aimGameScore: LiveData<AimGame>

    suspend fun getAimGameScore()
}