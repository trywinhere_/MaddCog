package com.maddcog.android.domain.entities.activity

import com.maddcog.android.domain.entities.ActivityItem
import org.joda.time.DateTime

data class UserActivity(
    val activityId: String,
    val duration: Int,
    val mentalPerformanceTimeline: List<MentalPerformanceTimeline>,
    val activityType: ActivityItem,
    val timestamp: DateTime,
    val activitySummary: ActivitySummary,
    var match: Match,
    var tags: List<String>,
    val isForTest: Boolean,
)