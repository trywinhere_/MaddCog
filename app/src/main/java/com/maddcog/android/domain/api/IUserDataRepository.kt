package com.maddcog.android.domain.api

import com.maddcog.android.domain.entities.user.UserProfile

interface IUserDataRepository {

    suspend fun loadAndSaveProfile()

    suspend fun getProfile(): UserProfile?
}