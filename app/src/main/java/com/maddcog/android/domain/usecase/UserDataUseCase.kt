package com.maddcog.android.domain.usecase

import com.maddcog.android.baseui.model.Resource
import com.maddcog.android.baseui.ui.theme.INTERNET_CONNECTION_ERROR
import com.maddcog.android.baseui.ui.theme.UNEXPECTED_ERROR
import com.maddcog.android.domain.api.ISharedPreferencesStorage
import com.maddcog.android.domain.entities.user.UserProfileData
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

open class UserDataUseCase @Inject constructor(
    private val localStorage: ISharedPreferencesStorage,
) {

    open operator fun invoke(): Flow<Resource<UserProfileData?>> = flow {
        try {
            emit(Resource.Loading())
            val profile = localStorage.getUserProfile()
            val languageList = localStorage.getLanguageList()
            val userData = UserProfileData(
                profile = profile,
                languageList = languageList
            )

            emit(Resource.Success(userData))
        } catch (e: HttpException) {
            emit(Resource.Error(e.localizedMessage ?: UNEXPECTED_ERROR))
        } catch (e: IOException) {
            emit(Resource.Error(INTERNET_CONNECTION_ERROR))
        }
    }
}