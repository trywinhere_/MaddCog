package com.maddcog.android.domain.repository

import android.app.Activity
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.amazonaws.mobileconnectors.cognitoidentityprovider.*
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.AuthenticationContinuation
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.ChallengeContinuation
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.MultiFactorAuthenticationContinuation
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.AuthenticationHandler
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.GenericHandler
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.GetDetailsHandler
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.SignUpHandler
import com.amazonaws.services.cognitoidentityprovider.model.SignUpResult
import com.amplifyframework.auth.AuthException
import com.amplifyframework.auth.AuthProvider
import com.amplifyframework.auth.cognito.AWSCognitoAuthSession
import com.amplifyframework.auth.cognito.exceptions.service.UserCancelledException
import com.amplifyframework.auth.options.AuthSignOutOptions
import com.amplifyframework.auth.result.AuthSignInResult
import com.amplifyframework.core.Amplify.Auth
import com.maddcog.android.domain.api.IAuthRepository
import com.maddcog.android.domain.api.ISharedPreferencesStorage
import com.maddcog.android.domain.entities.User
import com.maddcog.android.domain.entities.UserSignUp
import javax.inject.Inject
import javax.inject.Singleton

private const val NAME = "name"
private const val EMAIL = "email"

@Singleton
open class AuthRepositoryImpl @Inject constructor(
    private val sharedPref: ISharedPreferencesStorage,
    private val cognitoUserPool: CognitoUserPool
) : IAuthRepository {

    private var cognitoUser: CognitoUser? = null

    override val errorResponseSignIn = MutableLiveData<String?>()

    override val tokenResponseSignIn = MutableLiveData<String?>()

    override val errorResponseSignUp = MutableLiveData<String?>()

    override val tokenResponse = MutableLiveData<String?>()

    override val errorResponse = MutableLiveData<String?>()

    override val googleSignInError = MutableLiveData<Boolean>()

    override val successChangePassword = MutableLiveData<Boolean>()

    override val errorChangePassword = MutableLiveData<String?>()

    override suspend fun changePassword(oldPassword: String, newPassword: String) {
        val genericHandler = object : GenericHandler {
            override fun onSuccess() {
                sharedPref.savePassword(newPassword)
                successChangePassword.value = true
            }

            override fun onFailure(exception: java.lang.Exception?) {
                errorChangePassword.value = exception?.message
            }
        }

        val user = cognitoUserPool.currentUser

        user.changePasswordInBackground(
            oldPassword,
            newPassword,
            genericHandler
        )
    }

    override fun signOut() {
        val user = cognitoUserPool.currentUser
        user.signOut()

        val options = AuthSignOutOptions.builder()
            .globalSignOut(true)
            .build()

        Auth.signOut(options, { })
    }

    override fun signIn(user: User) {
        errorResponseSignUp.value = null
        val cognitoUser = cognitoUserPool.getUser(user.email.trim())
        val getDetailHandler = object : GetDetailsHandler {
            override fun onSuccess(cognitoUserDetails: CognitoUserDetails?) {
            }

            override fun onFailure(exception: Exception?) {
                errorResponseSignIn.value = exception?.message
            }
        }

        val authHandler = object : AuthenticationHandler {
            override fun onSuccess(userSession: CognitoUserSession?, newDevice: CognitoDevice?) {
                cognitoUser.getDetailsInBackground(getDetailHandler)
                sharedPref.saveToken(userSession?.idToken?.jwtToken.toString())
                tokenResponse.value = sharedPref.getToken()
                saveEmail(user.email)
                savePassword(user.password)
                saveStateSignIn(false)
                sharedPref.saveCognitoToken(userSession?.accessToken?.jwtToken.toString())
                sharedPref.saveUserName(userSession?.username.toString())
                println("TOKEN " + userSession?.accessToken?.jwtToken.toString())
            }

            override fun getAuthenticationDetails(
                authenticationContinuation: AuthenticationContinuation?,
                userId: String?
            ) {
                val authDetail =
                    com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.AuthenticationDetails(
                        userId,
                        user.password.trim(),
                        null
                    )
                authenticationContinuation?.setAuthenticationDetails(authDetail)
                authenticationContinuation?.continueTask()
            }

            override fun getMFACode(continuation: MultiFactorAuthenticationContinuation?) {
                continuation?.continueTask()
            }

            override fun authenticationChallenge(continuation: ChallengeContinuation?) {
                continuation?.continueTask()
            }

            override fun onFailure(exception: Exception?) {
                val error = exception?.message
                errorResponseSignIn.value = error
            }
        }

        cognitoUser.getSessionInBackground(authHandler)
    }

    override suspend fun signUp(userInput: UserSignUp) {
        errorResponseSignIn.value = null
        val userAttributes = CognitoUserAttributes()
        userAttributes.addAttribute(NAME, userInput.name)
        userAttributes.addAttribute(EMAIL, userInput.email.trim())

        val signHandler = object : SignUpHandler {
            override fun onSuccess(user: CognitoUser?, signUpResult: SignUpResult?) {
                cognitoUser = user

                val signInUser = User(
                    email = userInput.email,
                    password = userInput.password
                )
                signIn(signInUser)
            }

            override fun onFailure(exception: Exception?) {
                errorResponseSignUp.value = exception?.message
            }
        }

        cognitoUserPool.signUpInBackground(
            userInput.email.trim(),
            userInput.password.trim(),
            userAttributes,
            null,
            signHandler
        )
    }

    override fun clearErrorsSignIn() {
        errorResponseSignIn.value = null
    }

    override fun clearErrorsSignUp() {
        errorResponseSignUp.value = null
    }

    override fun clearChangePassword() {
        successChangePassword.value = false
        errorChangePassword.value = null
    }

    override fun saveEmail(email: String) {
        sharedPref.saveEmail(email)
    }

    override fun getEmail(): String? {
        return sharedPref.getEmail()
    }

    override fun savePassword(password: String) {
        sharedPref.savePassword(password)
    }

    override fun getPassword(): String? {
        return sharedPref.getPassword()
    }

    override fun getStateSignIn(): Boolean {
        return sharedPref.getStateSignInScreen()
    }

    override fun saveStateSignIn(showing: Boolean) {
        sharedPref.saveStateSignInScreen(showing)
    }

    override fun getStateSignInLoggingingIn(): Boolean {
        return sharedPref.getStateSignInInLogginingIn()
    }

    override fun saveStateSignInLoggingingIn(showing: Boolean) {
        sharedPref.saveStateSignInInLogginingIn(showing)
    }

    override val googleToken = MutableLiveData<String?>()

    override fun googleSignIn(activity: Activity) {
        try {
            Auth.signInWithSocialWebUI(
                AuthProvider.google(),
                activity, { result: AuthSignInResult ->
                    if (result.isSignedIn) {
                        googleSignInError.postValue(false)
                        Auth.fetchAuthSession(
                            {
                                val session = it as AWSCognitoAuthSession
                                val accessToken = session.userPoolTokensResult.value?.idToken
                                googleToken.postValue(accessToken.toString())
                                sharedPref.saveToken(accessToken.toString())
                                sharedPref.saveCognitoToken(accessToken.toString())
                            },
                            { Log.e("GOOGLE SIGNIN", "Failed to fetch session", it) }
                        )
                    }
                    Log.i("GOOGLE SIGNIN AuthSignInResult ", result.toString())
                }
            ) { error: AuthException ->
                googleSignInError.postValue(true)
                Log.e("GOOGLE SIGNIN AuthProvider error", error.message.toString())
            }
        } catch (E: UserCancelledException) {
            googleSignInError.value = true
            return
        } catch (e: Exception) {
            googleSignInError.value = true
        }
    }

    override fun clearTokenResponse() {
        tokenResponse.value = null
        tokenResponseSignIn.value = null
        googleToken.value = null
    }

    override fun clearGoogleError() {
        googleSignInError.value = false
    }
}