package com.maddcog.android.domain.entities

data class UserSignUp(
    val name: String,
    val email: String,
    val password: String
)