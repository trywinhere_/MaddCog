package com.maddcog.android.domain.repository

import com.maddcog.android.domain.api.IProfileApiService
import com.maddcog.android.domain.api.ISharedPreferencesStorage
import com.maddcog.android.domain.api.IUserDataRepository
import com.maddcog.android.domain.entities.user.UserProfile
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
open class UserDataRepositoryImpl @Inject constructor(
    private val profileApi: IProfileApiService,
    private val storage: ISharedPreferencesStorage,
) : IUserDataRepository {

    override suspend fun loadAndSaveProfile() {
        val profile = profileApi.loadUserProfile()
        if (profile != null) {
            storage.saveUserProfile(profile)
        } else {
            getProfile()
        }
    }

    override suspend fun getProfile(): UserProfile? {
        return storage.getUserProfile()
    }
}