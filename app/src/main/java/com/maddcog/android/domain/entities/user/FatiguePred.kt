package com.maddcog.android.domain.entities.user

data class FatiguePred(
    val count: Int? = null,
    val min: Double? = null,
    val startingP2: Double? = null,
    val max: Double? = null,
    val startingP1: Double? = null,
    val countVal: ArrayList<Int> = arrayListOf()
)