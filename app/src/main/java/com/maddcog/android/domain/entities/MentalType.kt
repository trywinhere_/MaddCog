package com.maddcog.android.domain.entities

enum class MentalType {
    HI, MID, LOW, NONE
}