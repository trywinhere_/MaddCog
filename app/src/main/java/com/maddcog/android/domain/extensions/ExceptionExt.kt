package com.maddcog.android.domain.extensions

import com.maddcog.android.R
import java.net.SocketTimeoutException
import java.net.UnknownHostException

fun Throwable.showErrorType(): Int {
    return when (this) {
        is UnknownHostException -> R.string.no_network
        is SocketTimeoutException -> R.string.no_network
        else -> R.string.unknown_exception
    }
}