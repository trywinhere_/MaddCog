package com.maddcog.android.domain.entities.user

import com.maddcog.android.settings.presentation.selectLanguage.model.Language

data class UserProfileData(
    val profile: UserProfile?,
    val languageList: List<Language>,
)