package com.maddcog.android.domain.entities

import com.maddcog.android.domain.entities.user.UserAverage

data class UserGame(
    val generatedId: String,
    val gameName: String,
    val gameNumber: Int,
    val userName: String?,
    val region: String,
    val userAverages: List<UserAverage>,
)