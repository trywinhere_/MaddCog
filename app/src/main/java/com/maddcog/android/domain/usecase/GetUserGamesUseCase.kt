package com.maddcog.android.domain.usecase

import com.maddcog.android.baseui.model.Resource
import com.maddcog.android.baseui.ui.theme.INTERNET_CONNECTION_ERROR
import com.maddcog.android.baseui.ui.theme.UNEXPECTED_ERROR
import com.maddcog.android.domain.api.IActivitiesApiService
import com.maddcog.android.domain.api.IProfileApiService
import com.maddcog.android.domain.api.IUserDataRepository
import com.maddcog.android.domain.entities.UserGame
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

open class GetUserGamesUseCase @Inject constructor(
    private val repository: IUserDataRepository,
    private val api: IActivitiesApiService,
    private val profileApi: IProfileApiService,
) {

    open operator fun invoke(): Flow<Resource<List<UserGame>?>> = flow {
        try {
            emit(Resource.Loading())
            val games = profileApi.loadUserProfile()?.userGames?.map {
                UserGame(
                    generatedId = it.generatedId,
                    gameName = it.gameName,
                    userName = it.userName,
                    gameNumber = it.gameNumber,
                    region = it.region,
                    userAverages = it.userAverages,
                )
            }

            emit(Resource.Success(games))
        } catch (e: HttpException) {
            emit(Resource.Error(e.localizedMessage ?: UNEXPECTED_ERROR))
        } catch (e: IOException) {
            emit(Resource.Error(INTERNET_CONNECTION_ERROR))
        }
    }
}