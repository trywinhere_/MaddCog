package com.maddcog.android.domain.usecase

import com.maddcog.android.baseui.model.Resource
import com.maddcog.android.baseui.ui.theme.INTERNET_CONNECTION_ERROR
import com.maddcog.android.baseui.ui.theme.UNEXPECTED_ERROR
import com.maddcog.android.domain.api.IActivitiesApiService
import com.maddcog.android.domain.api.ISharedPreferencesStorage
import com.maddcog.android.domain.entities.trends.TrendsDetailsData
import com.maddcog.android.trends.utils.FiltersListHandler
import com.maddcog.android.trends.utils.FiltersListHandler.Companion.ALL
import com.maddcog.android.trends.utils.FiltersListHandler.Companion.ALL_ACTIVITIES
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

open class GetTrendsDetailsUseCase @Inject constructor(
    private val apiService: IActivitiesApiService,
    private val storage: ISharedPreferencesStorage,
    private val filtersHandler: FiltersListHandler,
) {

    open operator fun invoke(): Flow<Resource<TrendsDetailsData?>> = flow {
        try {
            emit(Resource.Loading())

            val trendsDetailsData: TrendsDetailsData = coroutineScope {

                val activitiesList = async {
                    apiService.getUserActivities()
                }.await()
                val profileAsync = async {
                    storage.getUserProfile()
                }

                val gameList = filtersHandler.getFilterList(
                    ALL_ACTIVITIES,
                    activitiesList.map { it.activityType.name }
                )
                val championsList = filtersHandler.getFilterList(
                    ALL,
                    activitiesList.map { it.match.myPlayer.characterName }
                )
                val userNameList = filtersHandler.getFilterList(
                    ALL,
                    activitiesList.map { it.match.myPlayer.gamerName }
                )
                val metricList = filtersHandler.getMetricList(activitiesList)
                val tagList = filtersHandler.getTagsList(activitiesList)
                val durationList = filtersHandler.getDurationList()

                TrendsDetailsData(
                    trendsInsight = profileAsync.await()?.userData?.trendsInsight ?: "",
                    trendsInsightValue = profileAsync.await()?.userData?.trendsInsightValue ?: 0,
                    durationFilter = durationList.first(),
                    durationList = durationList,
                    metricFilter = metricList.first(),
                    metricList = metricList,
                    gameList = gameList,
                    gameFilter = gameList.first(),
                    tagFilter = tagList.first(),
                    tagList = tagList,
                    championFilter = championsList.first(),
                    championsList = championsList,
                    userNameFilter = userNameList.first(),
                    userNameList = userNameList,
                    initialData = activitiesList,
                )
            }

            emit(Resource.Success(trendsDetailsData))
        } catch (e: HttpException) {
            emit(Resource.Error(e.localizedMessage ?: UNEXPECTED_ERROR))
        } catch (e: IOException) {
            emit(Resource.Error(INTERNET_CONNECTION_ERROR))
        }
    }
}