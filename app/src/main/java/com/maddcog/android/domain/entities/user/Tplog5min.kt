package com.maddcog.android.domain.entities.user

data class Tplog5min(
    val count: Int? = null,
    val sd: Double? = null,
    val startingSd: Double? = null,
    val mean: Double? = null,
    val startingMean: Double? = null
)