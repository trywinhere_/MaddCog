package com.maddcog.android.domain.entities

data class ActivityItem(
    val id: Int,
    var name: String,
    val gameType: String,
    val hasGrade: Boolean,
    val hasTeamScore: Boolean,
    val imageBig: String,
    val imageSmall: String,
    val maddcogIntegrated: String,
    val maddcogOwned: Boolean,
)