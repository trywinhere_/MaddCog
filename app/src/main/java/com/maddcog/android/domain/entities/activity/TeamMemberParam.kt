package com.maddcog.android.domain.entities.activity

data class TeamMemberParam(
    val name: String,
    val value: Double,
)