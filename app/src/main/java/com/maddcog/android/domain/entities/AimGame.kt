package com.maddcog.android.domain.entities

data class AimGame(
    val accuracy: Double = 0.0,
    val duration: Long = 0,
    val start: Long = 0,
    val targets: Double = 0.0
)