package com.maddcog.android.domain.api

import com.maddcog.android.domain.entities.team.Team

interface ITeamsApiService {

    suspend fun getTeams(): List<Team>

    suspend fun leaveTeam(teamId: String): Boolean

    suspend fun removeTeamMate(teamId: String): Boolean

    suspend fun inviteUser(userEmail: String): Boolean
}