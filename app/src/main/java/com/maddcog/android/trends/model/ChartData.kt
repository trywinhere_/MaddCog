package com.maddcog.android.trends.model

data class ChartData(
    val metric: Double,
    val flow: Double,
    val date: String,
)