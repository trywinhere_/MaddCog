package com.maddcog.android.trends.filters

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.selection.selectable
import androidx.compose.foundation.selection.selectableGroup
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import com.maddcog.android.baseui.components.BasicTopAppBar
import com.maddcog.android.baseui.components.SelectableButton
import com.maddcog.android.baseui.ui.theme.Black
import com.maddcog.android.baseui.ui.theme.Spacing_10
import com.maddcog.android.baseui.ui.theme.Spacing_12
import com.maddcog.android.data.extensions.toLocalName

@Composable
fun FilterListScreen(
    onItemSelected: () -> Unit,
    onBackPressed: () -> Unit,
    initialSelect: String,
    filtersList: List<String>,
    setFilter: (String) -> Unit,
    titleResource: Int,
) {
    val context = LocalContext.current
    var selectedItem by remember { mutableStateOf(initialSelect.toLocalName(context)) }

    Column(
        Modifier
            .background(Black)
    ) {
        BasicTopAppBar(title = titleResource) {
            onBackPressed()
        }
        LazyColumn(
            modifier = Modifier
                .background(Color.Black)
                .fillMaxSize()
                .padding(Spacing_12)
                .selectableGroup()
        ) {
            item {
                filtersList.forEach { filter ->
                    SelectableButton(
                        modifier = Modifier.selectable(
                            selected = (selectedItem == filter.toLocalName(LocalContext.current)),
                            onClick = {
                                selectedItem = filter
                                setFilter(filter)
                                onItemSelected()
                            }
                        ),
                        name = filter.toLocalName(LocalContext.current),
                        selectionTerm = (selectedItem == filter.toLocalName(LocalContext.current))
                    )
                    Spacer(modifier = Modifier.height(Spacing_10))
                }
            }
        }
    }
}