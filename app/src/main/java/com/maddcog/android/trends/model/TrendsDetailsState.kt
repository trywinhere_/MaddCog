package com.maddcog.android.trends.model

data class TrendsDetailsState(
    val isLoading: Boolean = false,
    val trendsDetailsData: TrendsDetailsScreenData? = null,
    val error: String = "",
)