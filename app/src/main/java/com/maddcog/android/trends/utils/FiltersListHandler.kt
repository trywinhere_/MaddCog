package com.maddcog.android.trends.utils

import com.maddcog.android.domain.entities.activity.UserActivity
import com.maddcog.android.trends.extensions.GAME_GRADE
import com.maddcog.android.trends.extensions.MENTAL_PERFORMANCE
import com.maddcog.android.trends.extensions.WIN_RATE
import javax.inject.Inject

class FiltersListHandler @Inject constructor() {

    companion object {
        const val LAST_MONTH = "Last month"
        const val LAST_WEEK = "Last week"
        const val HOURLY = "Hourly"
        const val PER_GAME = "Per game"
        const val DURING_GAME = "During game"

        const val ALL = "All"
        const val ALL_ACTIVITIES = "All activities"
    }

    fun getFilterList(addFirst: String, list: List<String>): List<String> {
        val gameList = mutableListOf<String>()
        gameList.add(addFirst)
        list.forEach {
            if (!gameList.contains(it)) {
                gameList.add(it)
            }
        }
        return gameList
    }

    fun getDurationList(): List<String> {
        return listOf(ALL, LAST_MONTH, LAST_WEEK, HOURLY, PER_GAME, DURING_GAME)
    }

    fun getMetricList(list: List<UserActivity>): List<String> {
        val metricList = mutableListOf<String>()
        val performanceParamsList = mutableListOf<String>()
        metricList.addAll(
            listOf(
                MENTAL_PERFORMANCE,
                WIN_RATE,
                GAME_GRADE,
            )
        )

        list.forEach {
            it.match.myPlayer.performanceParams.forEach { param ->
                performanceParamsList.add(param.name)
            }
        }

        performanceParamsList.forEach {
            if (!metricList.contains(it)) {
                metricList.add(it)
            }
        }

        return metricList
    }

    fun getTagsList(list: List<UserActivity>): List<String> {
        val tagList = mutableListOf<String>()

        tagList.add(ALL)

        list.forEach {
            it.tags.forEach { tag ->
                if (!tagList.contains(tag)) {
                    tagList.add(tag)
                }
            }
        }

        return tagList
    }
}