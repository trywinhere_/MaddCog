package com.maddcog.android.trends.filters.duration

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.ui.platform.ComposeView
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.maddcog.android.R
import com.maddcog.android.baseui.presentation.BaseFragment
import com.maddcog.android.baseui.presentation.BaseViewModel
import com.maddcog.android.baseui.ui.theme.MaddcogTheme
import com.maddcog.android.trends.TrendsDetailsFragment.Companion.TRENDS_DURATION_DATA_KEY
import com.maddcog.android.trends.TrendsDetailsFragment.Companion.TRENDS_DURATION_KEY
import com.maddcog.android.trends.filters.FilterListScreen
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DurationFilterFragment : BaseFragment() {

    private val viewModel: DurationFilterViewModel by viewModels()
    private val args by navArgs<DurationFilterFragmentArgs>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.setDuration(args.duration)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = ComposeView(requireContext())
        view.setContent {
            MaddcogTheme {
                FilterListScreen(
                    onItemSelected = {
                        setFragmentResult(
                            TRENDS_DURATION_KEY,
                            Bundle().apply {
                                putString(TRENDS_DURATION_DATA_KEY, viewModel.duration)
                            }
                        )
                        viewModel.navigateBack()
                    },
                    onBackPressed = {
                        viewModel.navigateBack()
                    },
                    args.duration,
                    args.durationList.toList(),
                    setFilter = {
                        viewModel.setDuration(it)
                    },
                    R.string.duration_filter,
                )
            }
        }

        return view
    }

    override fun withViewModel(): BaseViewModel = viewModel.apply {
    }
}