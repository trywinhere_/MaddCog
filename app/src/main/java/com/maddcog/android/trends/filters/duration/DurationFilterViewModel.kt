package com.maddcog.android.trends.filters.duration

import com.maddcog.android.baseui.presentation.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class DurationFilterViewModel @Inject constructor() : BaseViewModel() {

    private var _duration: String? = null
    val duration: String
        get() = _duration!!

    fun setDuration(duration: String) {
        _duration = duration
    }
}