package com.maddcog.android.trends.extensions

import android.content.Context
import androidx.compose.ui.graphics.Color
import com.maddcog.android.R
import com.maddcog.android.baseui.ui.theme.FatigueChart
import com.maddcog.android.baseui.ui.theme.GameGrade
import com.maddcog.android.baseui.ui.theme.GameStatChart
import com.maddcog.android.baseui.ui.theme.WIN
import com.maddcog.android.baseui.ui.theme.WinRate
import com.maddcog.android.data.extensions.roundNumbers
import com.maddcog.android.data.utils.DateFormatter
import com.maddcog.android.domain.entities.activity.UserActivity
import com.maddcog.android.domain.entities.trends.TrendsDetailsData
import com.maddcog.android.gameSummary.extensions.FLOW
import com.maddcog.android.trends.model.ChartData
import com.maddcog.android.trends.model.TrendsDetailsScreenData
import org.joda.time.DateTime
import kotlin.math.roundToInt

const val FLOW_LEGEND = "Flow"
const val FATIGUE_LEGEND = "Fatigue"
const val WIN_RATE_LEGEND = "Win rate"
const val GAME_GRADE_LEGEND = "Grade"
const val MENTAL_PERFORMANCE = "Mental performance"
const val WIN_RATE = "Win rate"
const val GAME_GRADE = "Game grade"
const val MIN = " min"

fun TrendsDetailsData.toScreenData(): TrendsDetailsScreenData {
    return TrendsDetailsScreenData(
        gradeAvg = getGradeAvg(initialData),
        flowAvg = getFlowAvg(initialData),
        fatigueAvg = getFatigueAvg(initialData),
        winNumber = getWinNumber(initialData),
        trendsInsight = this.trendsInsight,
        trendsInsightValue = this.trendsInsightValue,
        durationFilter = this.durationFilter,
        durationList = this.durationList,
        metricFilter = this.metricFilter,
        metricList = this.metricList,
        gameFilter = this.gameFilter,
        gameList = this.gameList,
        tagFilter = this.tagFilter,
        tagList = this.tagList,
        championFilter = this.championFilter,
        championsList = this.championsList,
        userNameFilter = this.userNameFilter,
        userNameList = this.userNameList,
        filteredData = this.initialData,
        initialData = this.initialData
    )
}

fun getGradeAvg(list: List<UserActivity>): String {
    return getDoubleAverage(list.map { it.match.myPlayer.grade }).toString()
}

fun getFlowAvg(list: List<UserActivity>): String {
    return getDoubleAverage(list.map { it.activitySummary.flowZoneHigh }).roundToInt().toString()
}

fun getFatigueAvg(list: List<UserActivity>): String {
    return getDoubleAverage(list.map { it.activitySummary.fatigueZoneHigh }).roundToInt().toString()
}

fun getDoubleAverage(list: List<Double>?): Double {
    val doubleList = mutableListOf<Double>()
    list?.forEach {
        doubleList.add(it)
    }
    val average = doubleList.average()
    return if (!average.isNaN()) average.roundNumbers() else 0.0
}

fun getWinNumber(list: List<UserActivity>?): String {
    val outcomeList = list?.map { it.match.myPlayer.outcome }
    val matchList = mutableListOf<String>()
    outcomeList?.forEach {
        matchList.add(it)
    }

    val winList = matchList.filter { it == WIN }
    val result = if (matchList.size != 0) winList.size / matchList.size * 100 else 0

    return result.toString()
}

fun getChartDataList(
    dataList: List<UserActivity>,
    dateFormatter: DateFormatter,
    metricFilter: String,
): List<ChartData> {
    val chartDataList = dataList.map {
        ChartData(
            metric = when (metricFilter) {
                MENTAL_PERFORMANCE -> it.match.myPlayer.fatigue
                WIN_RATE -> it.match.myPlayer.fatigue
                GAME_GRADE -> it.match.myPlayer.grade
                else -> it.match.myPlayer.performanceParams.first { param -> param.name == metricFilter }.value
            },
            flow = it.match.myPlayer.flow,
            date = dateFormatter.getOnlyTextDateStr(it.timestamp)
        )
    }
    val chartMap = chartDataList.groupBy { data -> data.date }
    return chartMap.map {
        ChartData(
            date = it.key,
            metric = it.value.map { data -> data.metric }.average(),
            flow = it.value.map { data -> data.flow }.average(),
        )
    }
}

fun getChartColor(
    metricFilter: String,
): Color {
    return when (metricFilter) {
        MENTAL_PERFORMANCE -> FatigueChart
        WIN_RATE -> WinRate
        GAME_GRADE -> GameGrade
        else -> GameStatChart
    }
}

fun getMetricLegendText(
    metricFilter: String,
    context: Context,
): String {
    return when (metricFilter) {
        MENTAL_PERFORMANCE -> context.resources.getString(R.string.fatigue)
        WIN_RATE -> context.resources.getString(R.string.win_rate)
        GAME_GRADE -> context.resources.getString(R.string.game_grade)
        FLOW -> context.resources.getString(R.string.flow_filter)
        else -> metricFilter
    }
}

fun getDuringGameMap(
    dataList: List<UserActivity>,
): MutableMap<Int, GameData> {
    val dataMap = mutableMapOf<Int, GameData>()
    val maxMinutes = dataList.maxOf { (it.match.end - it.match.start) / 60 }
    for (i in 1..maxMinutes) {
        val flowList = mutableListOf<Double>()
        val fatigueList = mutableListOf<Double>()
        dataList.forEach { userActivity ->
            userActivity.mentalPerformanceTimeline.forEach {
                if (it.timestamp <= userActivity.match.start + i * 60 && it.timestamp > userActivity.match.start + (i - 1) * 60) {
                    flowList.add(it.flow)
                    fatigueList.add(it.fatigue)
                }
            }
        }
        dataMap[i] = GameData(
            flow = if(flowList.isNotEmpty()) flowList.average() else 0.0,
            fatigue = if(fatigueList.isNotEmpty()) fatigueList.average() else 0.0
        )
    }
    return dataMap
}

fun getHourlyMap(
    dataList: List<UserActivity>,
): MutableMap<Int, GameData> {
    val hoursList = listOf(7, 9, 11, 13, 15, 17, 19, 21)
    val dataMap = mutableMapOf<Int, GameData>()
    hoursList.forEach { hour ->
        val flowList = mutableListOf<Double>()
        val fatigueList = mutableListOf<Double>()
        dataList.forEach { userActivity ->
            userActivity.mentalPerformanceTimeline.forEach {
                if (getHourOfTheDay(it.timestamp) == hour.toString()) {
                    flowList.add(it.flow)
                    fatigueList.add(it.fatigue)
                }
            }
        }
        dataMap[hour] = GameData(
            flow = if (flowList.isNotEmpty()) flowList.average() else 0.0,
            fatigue = if (fatigueList.isNotEmpty()) fatigueList.average() else 0.0,
        )
    }
    return dataMap
}

fun getPerGameMap(
    dataList: List<UserActivity>,
): MutableMap<Int, GameData> {
    var counter = 1
    val dataMap = mutableMapOf<Int, GameData>()
    val tempList = mutableListOf<UserActivity>()
    for (i in dataList.indices) {
        if (i == 0) {
            tempList.add(dataList[i])
        } else {
            val current = dataList[i]
            val previous = dataList[i - 1]
            if (current.match.start - previous.match.end <= 1200) {
                tempList.add(dataList[i])
                if (i == dataList.lastIndex) {
                    dataMap[counter] = getGameData(tempList)
                }
            } else {
                dataMap[counter] = getGameData(tempList)
                counter += 1
                tempList.clear()
                tempList.add(dataList[i])
            }
        }
    }

    return dataMap
}

fun getGameData(list: List<UserActivity>): GameData {
    val tempFlowList = mutableListOf<Double>()
    val tempFatigueList = mutableListOf<Double>()
    list.forEach {
        it.mentalPerformanceTimeline.forEach { mental ->
            tempFlowList.add(mental.flow)
            tempFatigueList.add(mental.fatigue)
        }
    }

    return GameData(flow = tempFlowList.average(), fatigue = tempFatigueList.average())
}

fun getHourOfTheDay(timestamp: Int): String {
    return DateTime((timestamp * 1000).toLong()).hourOfDay().asText
}

data class GameData(
    val flow: Double,
    val fatigue: Double,
)