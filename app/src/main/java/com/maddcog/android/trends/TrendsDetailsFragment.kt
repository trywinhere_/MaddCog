package com.maddcog.android.trends

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.ui.platform.ComposeView
import androidx.fragment.app.setFragmentResultListener
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import com.maddcog.android.baseui.presentation.BaseFragment
import com.maddcog.android.baseui.presentation.BaseViewModel
import com.maddcog.android.baseui.ui.theme.MaddcogTheme
import com.maddcog.android.baseui.ui.theme.TRENDS
import com.maddcog.android.data.utils.DateFormatter
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class TrendsDetailsFragment : BaseFragment() {

    companion object {
        const val TRENDS_DURATION_KEY = "trends:details:duration"
        const val TRENDS_DURATION_DATA_KEY = "trends:details:duration:data"

        const val TRENDS_METRIC_KEY = "trends:details:metric"
        const val TRENDS_METRIC_DATA_KEY = "trends:details:metric:data"

        const val TRENDS_GAME_KEY = "trends:details:game"
        const val TRENDS_GAME_DATA_KEY = "trends:details:game:data"

        const val TRENDS_TAG_KEY = "trends:details:tag"
        const val TRENDS_TAG_DATA_KEY = "trends:details:tag:data"

        const val TRENDS_CHAMPION_KEY = "trends:details:champion"
        const val TRENDS_CHAMPION_DATA_KEY = "trends:details:champion:data"

        const val TRENDS_USERNAME_KEY = "trends:details:username"
        const val TRENDS_USERNAME_DATA_KEY = "trends:details:username:data"
    }

    private val viewModel: TrendsDetailsViewModel by viewModels()
    private val args by navArgs<TrendsDetailsFragmentArgs>()
    private val dateFormatter = DateFormatter()
    private lateinit var firebaseAnalytics: FirebaseAnalytics

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.getTrendsDetails(args.filter)
        firebaseAnalytics = Firebase.analytics
        firebaseAnalytics.logEvent(
            "Trends",
            Bundle().apply {
                this.putString(
                    TRENDS, "Trends"
                )
            }
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = ComposeView(requireContext())
        view.setContent {
            MaddcogTheme {
                TrendsDetailsScreen(
                    viewModel = viewModel,
                    dateFormatter
                )
            }
        }

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setFragmentResultListener(TRENDS_DURATION_KEY) { _, bundle ->
            val content = bundle.getString(TRENDS_DURATION_DATA_KEY)
            viewModel.refreshDuration(content)
        }

        setFragmentResultListener(TRENDS_METRIC_KEY) { _, bundle ->
            val content = bundle.getString(TRENDS_METRIC_DATA_KEY)
            viewModel.refreshMetric(content)
        }

        setFragmentResultListener(TRENDS_GAME_KEY) { _, bundle ->
            val content = bundle.getString(TRENDS_GAME_DATA_KEY)
            viewModel.refreshGame(content)
        }

        setFragmentResultListener(TRENDS_TAG_KEY) { _, bundle ->
            val content = bundle.getString(TRENDS_TAG_DATA_KEY)
            viewModel.refreshTag(content)
        }

        setFragmentResultListener(TRENDS_CHAMPION_KEY) { _, bundle ->
            val content = bundle.getString(TRENDS_CHAMPION_DATA_KEY)
            viewModel.refreshChampion(content)
        }

        setFragmentResultListener(TRENDS_USERNAME_KEY) { _, bundle ->
            val content = bundle.getString(TRENDS_USERNAME_DATA_KEY)
            viewModel.refreshUserName(content)
        }
    }

    override fun withViewModel(): BaseViewModel = viewModel.apply {
        errorState.observe(viewLifecycleOwner) { isError ->
            if (isError) {
                showToastMessage(state.value.error)
                viewModel.navigateBack()
            }
        }
    }
}