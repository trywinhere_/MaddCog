package com.maddcog.android.trends.filters.tag

import com.maddcog.android.baseui.presentation.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class TagFilterViewModel @Inject constructor() : BaseViewModel() {

    private var _tag: String? = null
    val tag: String
        get() = _tag!!

    fun setTag(tag: String) {
        _tag = tag
    }
}