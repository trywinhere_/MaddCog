package com.maddcog.android.trends.components

import android.graphics.Paint
import androidx.compose.foundation.Canvas
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.CornerRadius
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Rect
import androidx.compose.ui.geometry.RoundRect
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Path
import androidx.compose.ui.graphics.PathEffect
import androidx.compose.ui.graphics.StrokeCap
import androidx.compose.ui.graphics.asAndroidPath
import androidx.compose.ui.graphics.asComposePath
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.graphics.nativeCanvas
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.text.ExperimentalTextApi
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.drawText
import androidx.compose.ui.text.rememberTextMeasurer
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.maddcog.android.R
import com.maddcog.android.baseui.ui.theme.ChartLineGrey
import com.maddcog.android.baseui.ui.theme.FatigueChart
import com.maddcog.android.baseui.ui.theme.FlowChart
import com.maddcog.android.gameSummary.extensions.FLOW
import com.maddcog.android.trends.extensions.GameData
import com.maddcog.android.trends.extensions.MENTAL_PERFORMANCE
import com.maddcog.android.trends.extensions.getMetricLegendText
import com.maddcog.android.trends.model.ChartData
import com.maddcog.android.trends.utils.FiltersListHandler.Companion.DURING_GAME
import com.maddcog.android.trends.utils.FiltersListHandler.Companion.HOURLY
import com.maddcog.android.trends.utils.FiltersListHandler.Companion.PER_GAME
import kotlin.math.roundToInt

@OptIn(ExperimentalTextApi::class)
@Composable
fun TrendsChart(
    chartData: List<ChartData>,
    modifier: Modifier = Modifier,
    graphColor: Color = FatigueChart,
    metricFilter: String,
    duringGameData: Map<Int, GameData>,
    hourlyGameData: Map<Int, GameData>,
    perGameData: Map<Int, GameData>,
    durationFilter: String,
) {
    val spacing = 120f
    val textMeasure = rememberTextMeasurer()

    val metricList = chartData.map { it.metric }
    val flowList = chartData.map { it.flow }
    val dateList = chartData.map { it.date }

    val transparentGraphColor = remember {
        graphColor.copy(alpha = 0.65f)
    }
    val upperValue = remember(metricList) {
        (metricList.maxOrNull()?.plus(10))?.roundToInt() ?: 0
    }
    val lowerValue = remember(metricList) {
        metricList.minOrNull()?.toInt() ?: 0
    }
    val upperFlowValue = remember(flowList) {
        (flowList.maxOrNull()?.plus(1))?.roundToInt() ?: 0
    }
    val lowerFlowValue = remember(flowList) {
        flowList.minOrNull()?.toInt() ?: 0
    }

    val flowAverage = (flowList.average() - lowerFlowValue) / (upperFlowValue - lowerFlowValue)
    val metricAverage = (metricList.average() - lowerValue) / (upperValue - lowerValue)
    val context = LocalContext.current

    val density = LocalDensity.current
    val textPaint = remember(density) {
        Paint().apply {
            color = android.graphics.Color.WHITE
            textAlign = Paint.Align.CENTER
            textSize = density.run { 12.sp.toPx() }
        }
    }

    Canvas(modifier = modifier) {
        val canvasHeight = size.height
        val spacePerDate = (size.width - spacing) / dateList.size
        val valueStep = (upperValue - lowerValue) / 4f

        // for during game case only
        val duringGameFlowList = duringGameData.values.toList().map { it.flow }
        val duringGameFatigueList = duringGameData.values.toList().map { it.fatigue }

        val upperDuringGameFlowValue = (duringGameFlowList.maxOrNull()?.plus(10))?.roundToInt() ?: 0
        val lowerDuringGameFlowValue = duringGameFlowList.minOrNull()?.toInt() ?: 0

        val upperDuringGameFatigueValue =
            (duringGameFatigueList.maxOrNull()?.plus(10))?.roundToInt() ?: 0
        val lowerDuringGameFatigueValue = duringGameFatigueList.minOrNull()?.toInt() ?: 0

        val duringGameXStep = (size.width - spacing) / duringGameData.size
        val duringGameYStep = (upperDuringGameFatigueValue - lowerDuringGameFatigueValue) / 4f

        // for hourly case only
        val hourlyFlowList = hourlyGameData.values.toList().map { it.flow }
        val hourlyFatigueList = hourlyGameData.values.toList().map { it.fatigue }

        val upperHourlyFlowValue = (hourlyFlowList.maxOrNull()?.plus(10))?.roundToInt() ?: 0
        val lowerHourlyFlowValue = hourlyFlowList.minOrNull()?.toInt() ?: 0

        val upperHourlyFatigueValue =
            (hourlyFatigueList.maxOrNull()?.plus(10))?.roundToInt() ?: 0
        val lowerHourlyFatigueValue = hourlyFatigueList.minOrNull()?.toInt() ?: 0

        val hourlyXStep = (size.width - spacing) / hourlyGameData.size
        val hourlyYStep = (upperHourlyFatigueValue - lowerHourlyFatigueValue) / 4f

        // for per game case only
        val perGameFlowList = perGameData.values.toList().map { it.flow }
        val perGameFatigueList = perGameData.values.toList().map { it.fatigue }

        val upperPerGameFlowValue = (perGameFlowList.maxOrNull()?.plus(10))?.roundToInt() ?: 0
        val lowerPerGameFlowValue = perGameFlowList.minOrNull()?.toInt() ?: 0

        val upperPerGameFatigueValue =
            (perGameFatigueList.maxOrNull()?.plus(10))?.roundToInt() ?: 0
        val lowerPerGameFatigueValue = perGameFatigueList.minOrNull()?.toInt() ?: 0

        val perGameXStep = (size.width - spacing) / perGameData.size
        val perGameYStep = (upperPerGameFatigueValue - lowerPerGameFatigueValue) / 4f

        // metric path
        var lastX = 0f
        val metricPath = Path().apply {
            val height = size.height
            for (i in metricList.indices) {
                val metric = metricList[i]
                val nextMetric = metricList.getOrNull(i + 1) ?: metricList.last()
                val leftRatio = (metric - lowerValue) / (upperValue - lowerValue)
                val rightRatio = (nextMetric - lowerValue) / (upperValue - lowerValue)

                val x1 = spacing + i * spacePerDate
                val y1 = height - spacing - (leftRatio * height).toFloat()
                val x2 = spacing + (i + 1) * spacePerDate
                val y2 = height - spacing - (rightRatio * height).toFloat()
                if (i == 0) {
                    moveTo(x1, y1)
                }
                lastX = (x1 + x2) / 2f
                quadraticBezierTo(
                    x1, y1, lastX, (y1 + y2) / 2f
                )
            }
        }

        // flow path
        val flowPath = Path().apply {
            val height = size.height
            for (i in flowList.indices) {
                val flow = flowList[i]
                val nextFlow = flowList.getOrNull(i + 1) ?: flowList.last()
                val leftRatio = (flow - lowerFlowValue) / (upperFlowValue - lowerFlowValue)
                val rightRatio = (nextFlow - lowerFlowValue) / (upperFlowValue - lowerFlowValue)

                val x1 = spacing + i * spacePerDate
                val y1 = height - spacing - (leftRatio * height).toFloat()
                val x2 = spacing + (i + 1) * spacePerDate
                val y2 = height - spacing - (rightRatio * height).toFloat()
                if (i == 0) {
                    moveTo(x1, y1)
                }
                lastX = (x1 + x2) / 2f
                quadraticBezierTo(
                    x1, y1, lastX, (y1 + y2) / 2f
                )
            }
        }

        // fill path
        val fillPath = android.graphics.Path(metricPath.asAndroidPath())
            .asComposePath()
            .apply {
                lineTo(lastX, size.height - spacing)
                lineTo(spacing, size.height - spacing)
                close()
            }

        fun drawDuringGameXandYaxis() {
            val slicedList = mutableListOf<Int>()
            val duringGameKeys = duringGameData.keys.toList()
            val keysLastIndex = duringGameKeys.lastIndex
            val medianIndex =
                if (keysLastIndex % 2 == 0) keysLastIndex / 2 else (keysLastIndex - 1) / 2
            slicedList.add(duringGameKeys.first())
            slicedList.add(duringGameKeys.elementAt(medianIndex))
            slicedList.add(duringGameKeys.last())
            val slicedListStep = (size.width - spacing) / 2
            (slicedList.indices step 1).forEach { i ->
                val minute = slicedList[i]
                drawContext.canvas.nativeCanvas.apply {
                    drawText(
                        context.resources.getString(R.string.minutes_value, minute.toString()),
                        (spacing * 0.6f) + i * slicedListStep,
                        size.height - 50,
                        textPaint
                    )
                }
            }

            (0..3).forEach { i ->
                drawContext.canvas.nativeCanvas.apply {
                    drawText(
                        (lowerDuringGameFatigueValue + duringGameYStep * i).roundToInt().toString(),
                        30f,
                        size.height - spacing - i * size.height / 4f,
                        textPaint
                    )
                }
                drawLine(
                    color = ChartLineGrey,
                    start = Offset(spacing, size.height - spacing - i * size.height / 4),
                    end = Offset(size.width - spacing, size.height - spacing - i * size.height / 4f)
                )
            }
        }

        fun drawHourlyXandYaxis() {
            val hourlyKeys = hourlyGameData.keys.toList()
            (hourlyKeys.indices step 1).forEach { i ->
                val hour = hourlyKeys[i]
                drawContext.canvas.nativeCanvas.apply {
                    drawText(
                        hour.toString(),
                        (spacing * 0.6f) + i * hourlyXStep,
                        size.height - 50,
                        textPaint
                    )
                }
            }

            (0..3).forEach { i ->
                drawContext.canvas.nativeCanvas.apply {
                    drawText(
                        (lowerHourlyFatigueValue + hourlyYStep * i).roundToInt().toString(),
                        30f,
                        size.height - spacing - i * size.height / 4f,
                        textPaint
                    )
                }
                drawLine(
                    color = ChartLineGrey,
                    start = Offset(spacing, size.height - spacing - i * size.height / 4),
                    end = Offset(size.width - spacing, size.height - spacing - i * size.height / 4f)
                )
            }
        }

        fun drawPerGameXandYaxis() {
            val perGameKeys = perGameData.keys.toList()
            val slicedStep = (size.width - spacing) / (perGameKeys.size - 1)
            (perGameKeys.indices step 1).forEach { i ->
                val game = perGameKeys[i]
                drawContext.canvas.nativeCanvas.apply {
                    drawText(
                        game.toString(),
                        (spacing * 0.6f) + i * slicedStep,
                        size.height - 50,
                        textPaint
                    )
                }
            }

            (0..3).forEach { i ->
                drawContext.canvas.nativeCanvas.apply {
                    drawText(
                        (lowerPerGameFatigueValue + perGameYStep * i).roundToInt().toString(),
                        30f,
                        size.height - spacing - i * size.height / 4f,
                        textPaint
                    )
                }
                drawLine(
                    color = ChartLineGrey,
                    start = Offset(spacing, size.height - spacing - i * size.height / 4),
                    end = Offset(size.width - spacing, size.height - spacing - i * size.height / 4f)
                )
            }
        }

        fun drawDuringGameChart() {
            // metric path
            var duringGameLastX = 0f
            val duringGameMetricPath = Path().apply {
                val height = size.height
                for (i in duringGameFatigueList.indices) {
                    val duringGameFatigue = duringGameFatigueList[i]
                    val nextDuringGameFatigue =
                        duringGameFatigueList.getOrNull(i + 1) ?: duringGameFatigueList.last()
                    val leftRatio =
                        (duringGameFatigue - lowerDuringGameFatigueValue) / (upperDuringGameFatigueValue - lowerDuringGameFatigueValue)
                    val rightRatio =
                        (nextDuringGameFatigue - lowerDuringGameFatigueValue) / (upperDuringGameFatigueValue - lowerDuringGameFatigueValue)

                    val x1 = spacing + i * duringGameXStep
                    val y1 = height - spacing - (leftRatio * height).toFloat()
                    val x2 = spacing + (i + 1) * duringGameXStep
                    val y2 = height - spacing - (rightRatio * height).toFloat()
                    if (i == 0) {
                        moveTo(x1, y1)
                    }
                    duringGameLastX = (x1 + x2) / 2f
                    quadraticBezierTo(
                        x1, y1, duringGameLastX, (y1 + y2) / 2f
                    )
                }
            }

            // flow path
            val duringGameFlowPath = Path().apply {
                val height = size.height
                for (i in duringGameFlowList.indices) {
                    val flow = duringGameFlowList[i]
                    val nextFlow = duringGameFlowList.getOrNull(i + 1) ?: duringGameFlowList.last()
                    val leftRatio =
                        (flow - lowerDuringGameFlowValue) / (upperDuringGameFlowValue - lowerDuringGameFlowValue)
                    val rightRatio =
                        (nextFlow - lowerDuringGameFlowValue) / (upperDuringGameFlowValue - lowerDuringGameFlowValue)

                    val x1 = spacing + i * duringGameXStep
                    val y1 = height - spacing - (leftRatio * height).toFloat()
                    val x2 = spacing + (i + 1) * duringGameXStep
                    val y2 = height - spacing - (rightRatio * height).toFloat()
                    if (i == 0) {
                        moveTo(x1, y1)
                    }
                    duringGameLastX = (x1 + x2) / 2f
                    quadraticBezierTo(
                        x1, y1, duringGameLastX, (y1 + y2) / 2f
                    )
                }
            }

            // fill path
            val duringGameFillPath = android.graphics.Path(duringGameMetricPath.asAndroidPath())
                .asComposePath()
                .apply {
                    lineTo(duringGameLastX, size.height - spacing)
                    lineTo(spacing, size.height - spacing)
                    close()
                }

            // draw fatigue fill
            drawPath(
                path = duringGameFillPath,
                brush = Brush.verticalGradient(
                    colors = listOf(
                        transparentGraphColor,
                        transparentGraphColor
                    ),
                    endY = size.height - spacing
                )
            )
            // draw fatigue line
            drawPath(
                path = duringGameMetricPath,
                color = graphColor,
                style = Stroke(
                    width = 0.dp.toPx(),
                    cap = StrokeCap.Round
                )
            )
            // draw flow
            drawPath(
                path = duringGameFlowPath,
                color = FlowChart,
                style = Stroke(
                    width = 3.dp.toPx(),
                    cap = StrokeCap.Round
                )
            )
        }

        fun drawHourlyChart() {
            // metric path
            var hourlyLastX = 0f
            val hourlyMetricPath = Path().apply {
                val height = size.height
                for (i in hourlyFatigueList.indices) {
                    val hourlyFatigue = hourlyFatigueList[i]
                    val nextHourlyFatigue =
                        hourlyFatigueList.getOrNull(i + 1) ?: hourlyFatigueList.last()
                    val leftRatio =
                        (hourlyFatigue - lowerHourlyFatigueValue) / (upperHourlyFatigueValue - lowerHourlyFatigueValue)
                    val rightRatio =
                        (nextHourlyFatigue - lowerHourlyFatigueValue) / (upperHourlyFatigueValue - lowerHourlyFatigueValue)

                    val x1 = spacing + i * hourlyXStep
                    val y1 = height - spacing - (leftRatio * height).toFloat()
                    val x2 = spacing + (i + 1) * hourlyXStep
                    val y2 = height - spacing - (rightRatio * height).toFloat()
                    if (i == 0) {
                        moveTo(x1, y1)
                    }
                    hourlyLastX = (x1 + x2) / 2f
                    quadraticBezierTo(
                        x1, y1, hourlyLastX, (y1 + y2) / 2f
                    )
                }
            }

            // flow path
            val hourlyFlowPath = Path().apply {
                val height = size.height
                for (i in hourlyFlowList.indices) {
                    val flow = hourlyFlowList[i]
                    val nextFlow = hourlyFlowList.getOrNull(i + 1) ?: hourlyFlowList.last()
                    val leftRatio =
                        (flow - lowerHourlyFlowValue) / (upperHourlyFlowValue - lowerHourlyFlowValue)
                    val rightRatio =
                        (nextFlow - lowerHourlyFlowValue) / (upperHourlyFlowValue - lowerHourlyFlowValue)

                    val x1 = spacing + i * hourlyXStep
                    val y1 = height - spacing - (leftRatio * height).toFloat()
                    val x2 = spacing + (i + 1) * hourlyXStep
                    val y2 = height - spacing - (rightRatio * height).toFloat()
                    if (i == 0) {
                        moveTo(x1, y1)
                    }
                    hourlyLastX = (x1 + x2) / 2f
                    quadraticBezierTo(
                        x1, y1, hourlyLastX, (y1 + y2) / 2f
                    )
                }
            }

            // fill path
            val hourlyFillPath = android.graphics.Path(hourlyMetricPath.asAndroidPath())
                .asComposePath()
                .apply {
                    lineTo(hourlyLastX, size.height - spacing)
                    lineTo(spacing, size.height - spacing)
                    close()
                }

            // draw fatigue fill
            drawPath(
                path = hourlyFillPath,
                brush = Brush.verticalGradient(
                    colors = listOf(
                        transparentGraphColor,
                        transparentGraphColor
                    ),
                    endY = size.height - spacing
                )
            )
            // draw fatigue line
            drawPath(
                path = hourlyMetricPath,
                color = graphColor,
                style = Stroke(
                    width = 0.dp.toPx(),
                    cap = StrokeCap.Round
                )
            )
            // draw flow
            drawPath(
                path = hourlyFlowPath,
                color = FlowChart,
                style = Stroke(
                    width = 3.dp.toPx(),
                    cap = StrokeCap.Round
                )
            )
        }

        fun drawPerGameChart() {
            // metric path
            var perGameLastX = 0f
            val perGameMetricPath = Path().apply {
                val height = size.height
                for (i in perGameFatigueList.indices) {
                    val perGameFatigue = perGameFatigueList[i]
                    val nextPerGameFatigue =
                        perGameFatigueList.getOrNull(i + 1) ?: perGameFatigueList.last()
                    val leftRatio =
                        (perGameFatigue - lowerPerGameFatigueValue) / (upperPerGameFatigueValue - lowerPerGameFatigueValue)
                    val rightRatio =
                        (nextPerGameFatigue - lowerPerGameFatigueValue) / (upperPerGameFatigueValue - lowerPerGameFatigueValue)

                    val x1 = spacing + i * perGameXStep
                    val y1 = height - spacing - (leftRatio * height).toFloat()
                    val x2 = spacing + (i + 1) * perGameXStep
                    val y2 = height - spacing - (rightRatio * height).toFloat()
                    if (i == 0) {
                        moveTo(x1, y1)
                    }
                    perGameLastX = (x1 + x2) / 2f
                    quadraticBezierTo(
                        x1, y1, perGameLastX, (y1 + y2) / 2f
                    )
                }
            }

            // flow path
            val perGameFlowPath = Path().apply {
                val height = size.height
                for (i in perGameFlowList.indices) {
                    val flow = perGameFlowList[i]
                    val nextFlow = perGameFlowList.getOrNull(i + 1) ?: perGameFlowList.last()
                    val leftRatio =
                        (flow - lowerPerGameFlowValue) / (upperPerGameFlowValue - lowerPerGameFlowValue)
                    val rightRatio =
                        (nextFlow - lowerPerGameFlowValue) / (upperPerGameFlowValue - lowerPerGameFlowValue)

                    val x1 = spacing + i * perGameXStep
                    val y1 = height - spacing - (leftRatio * height).toFloat()
                    val x2 = spacing + (i + 1) * perGameXStep
                    val y2 = height - spacing - (rightRatio * height).toFloat()
                    if (i == 0) {
                        moveTo(x1, y1)
                    }
                    perGameLastX = (x1 + x2) / 2f
                    quadraticBezierTo(
                        x1, y1, perGameLastX, (y1 + y2) / 2f
                    )
                }
            }

            // fill path
            val perGameFillPath = android.graphics.Path(perGameMetricPath.asAndroidPath())
                .asComposePath()
                .apply {
                    lineTo(perGameLastX, size.height - spacing)
                    lineTo(spacing, size.height - spacing)
                    close()
                }

            // draw fatigue fill
            drawPath(
                path = perGameFillPath,
                brush = Brush.verticalGradient(
                    colors = listOf(
                        transparentGraphColor,
                        transparentGraphColor
                    ),
                    endY = size.height - spacing
                )
            )
            // draw fatigue line
            drawPath(
                path = perGameMetricPath,
                color = graphColor,
                style = Stroke(
                    width = 0.dp.toPx(),
                    cap = StrokeCap.Round
                )
            )
            // draw flow
            drawPath(
                path = perGameFlowPath,
                color = FlowChart,
                style = Stroke(
                    width = 3.dp.toPx(),
                    cap = StrokeCap.Round
                )
            )
        }

        fun drawDefaultXandYaxis() {
            // date x-axis
            (dateList.indices step 1).forEach { i ->
                val date = dateList[i]
                drawContext.canvas.nativeCanvas.apply {
                    drawText(
                        date,
                        spacing + i * spacePerDate,
                        size.height - 50,
                        textPaint
                    )
                }
            }

            // value y-axis
            (0..3).forEach { i ->
                drawContext.canvas.nativeCanvas.apply {
                    drawText(
                        (lowerValue + valueStep * i).roundToInt().toString(),
                        30f,
                        size.height - spacing - i * size.height / 4f,
                        textPaint
                    )
                }
                drawLine(
                    color = ChartLineGrey,
                    start = Offset(spacing, size.height - spacing - i * size.height / 4),
                    end = Offset(size.width - spacing, size.height - spacing - i * size.height / 4f)
                )
            }
        }

        fun drawFlowLine() {
            // draw flow
            drawPath(
                path = flowPath,
                color = FlowChart,
                style = Stroke(
                    width = 3.dp.toPx(),
                    cap = StrokeCap.Round
                )
            )
        }

        fun drawBarGraph() {
            // bar graph
            (metricList.indices).forEach { i ->
                val metric = metricList[i]
                val ratio = (metric - lowerValue) / (upperValue - lowerValue)
                val cornerRadius = CornerRadius(10f, 10f)
                val ySize = size.height - spacing - (ratio * size.height).toFloat()
                val startPoint = size.height - spacing
                val path = Path().apply {
                    addRoundRect(
                        RoundRect(
                            rect = Rect(
                                offset = Offset(spacing + i * spacePerDate - 8f, ySize),
                                size = Size(
                                    8.dp.toPx(),
                                    startPoint - ySize
                                ),
                            ),
                            topLeft = cornerRadius,
                            topRight = cornerRadius,
                        )
                    )
                }
                drawPath(path, color = graphColor)
            }
        }

        fun drawCurveGraph() {
            // draw fatigue fill
            drawPath(
                path = fillPath,
                brush = Brush.verticalGradient(
                    colors = listOf(
                        transparentGraphColor,
                        transparentGraphColor
                    ),
                    endY = size.height - spacing
                )
            )
            // draw fatigue line
            drawPath(
                path = metricPath,
                color = graphColor,
                style = Stroke(
                    width = 0.dp.toPx(),
                    cap = StrokeCap.Round
                )
            )
        }

        fun drawLegend() {
            drawText(
                textMeasurer = textMeasure,
                text = buildAnnotatedString {
                    withStyle(
                        style = SpanStyle(
                            color = Color.White,
                        ),
                    ) {
                        append(getMetricLegendText(metricFilter, context))
                    }
                },
                topLeft = Offset(x = (size.width / 4f) + 30f, y = size.height - 45f)
            )
            drawCircle(
                color = graphColor,
                radius = 15f,
                center = Offset(x = size.width / 4f, y = size.height - 18f)
            )

            drawText(
                textMeasurer = textMeasure,
                text = buildAnnotatedString {
                    withStyle(
                        style = SpanStyle(
                            color = Color.White,
                        ),
                    ) {
                        append(getMetricLegendText(FLOW, context))
                    }
                },
                topLeft = Offset(x = (size.width * 2f / 3f) + 30f, y = size.height - 45f)
            )
            drawCircle(
                color = FlowChart,
                radius = 15f,
                center = Offset(x = (size.width * 2f / 3f), y = size.height - 18f)
            )
        }

        fun drawAverageLine() {
            if (metricFilter != MENTAL_PERFORMANCE) {

                // average line (metric)
                drawLine(
                    color = Color.LightGray,
                    start = Offset(
                        spacing,
                        canvasHeight - spacing - (metricAverage * canvasHeight).toFloat()
                    ),
                    end = Offset(
                        size.width - spacing,
                        canvasHeight - spacing - (metricAverage * canvasHeight).toFloat()
                    ),
                    pathEffect = PathEffect.dashPathEffect(
                        floatArrayOf(
                            10f,
                            10f
                        ),
                        0f
                    )
                )
            } else {
                // average line (flow)
                drawLine(
                    color = Color.LightGray,
                    start = Offset(
                        spacing,
                        canvasHeight - spacing - (flowAverage * canvasHeight).toFloat()
                    ),
                    end = Offset(
                        size.width - spacing,
                        canvasHeight - spacing - (flowAverage * canvasHeight).toFloat()
                    ),
                    pathEffect = PathEffect.dashPathEffect(
                        floatArrayOf(
                            10f,
                            10f
                        ),
                        0f
                    )
                )
            }
        }

        when {
            metricFilter != MENTAL_PERFORMANCE && (durationFilter != DURING_GAME && durationFilter != HOURLY && durationFilter != PER_GAME) -> {
                drawBarGraph()
                drawFlowLine()
                drawAverageLine()
                drawLegend()
                drawDefaultXandYaxis()
            }
            metricFilter == MENTAL_PERFORMANCE && (durationFilter != DURING_GAME && durationFilter != HOURLY && durationFilter != PER_GAME) -> {
                drawCurveGraph()
                drawFlowLine()
                drawAverageLine()
                drawLegend()
                drawDefaultXandYaxis()
            }
            metricFilter == MENTAL_PERFORMANCE && durationFilter == DURING_GAME -> {
                drawDuringGameXandYaxis()
                drawDuringGameChart()
                drawLegend()
            }
            metricFilter == MENTAL_PERFORMANCE && durationFilter == HOURLY -> {
                drawHourlyXandYaxis()
                drawHourlyChart()
                drawLegend()
            }
            metricFilter == MENTAL_PERFORMANCE && durationFilter == PER_GAME -> {
                drawPerGameXandYaxis()
                drawPerGameChart()
                drawLegend()
            }
        }
    }
}