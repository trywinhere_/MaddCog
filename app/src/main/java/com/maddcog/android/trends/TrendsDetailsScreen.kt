package com.maddcog.android.trends

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import com.maddcog.android.R
import com.maddcog.android.baseui.components.BasicTopAppBar
import com.maddcog.android.baseui.extensions.toPercentageString
import com.maddcog.android.baseui.ui.theme.BoxDarkGrey
import com.maddcog.android.baseui.ui.theme.FatigueText
import com.maddcog.android.baseui.ui.theme.FlowText
import com.maddcog.android.baseui.ui.theme.GameGrade
import com.maddcog.android.baseui.ui.theme.Spacing_10
import com.maddcog.android.baseui.ui.theme.Spacing_12
import com.maddcog.android.baseui.ui.theme.Spacing_15
import com.maddcog.android.baseui.ui.theme.Spacing_30
import com.maddcog.android.baseui.ui.theme.Spacing_40
import com.maddcog.android.baseui.ui.theme.Spacing_6
import com.maddcog.android.baseui.ui.theme.Text_16
import com.maddcog.android.baseui.ui.theme.Text_18
import com.maddcog.android.baseui.ui.theme.White
import com.maddcog.android.baseui.ui.theme.WinRate
import com.maddcog.android.baseui.ui.theme.subHeadGrayText
import com.maddcog.android.dashboard.components.AdviceText
import com.maddcog.android.dashboard.components.TrendCard
import com.maddcog.android.data.extensions.getTrendsInsightText
import com.maddcog.android.data.extensions.toLocalName
import com.maddcog.android.data.utils.DateFormatter
import com.maddcog.android.settings.components.MenuButtonWithValue
import com.maddcog.android.trends.components.TrendsChart
import com.maddcog.android.trends.extensions.getChartColor
import com.maddcog.android.trends.extensions.getChartDataList
import com.maddcog.android.trends.extensions.getDuringGameMap
import com.maddcog.android.trends.extensions.getHourlyMap
import com.maddcog.android.trends.extensions.getPerGameMap

@Composable
fun TrendsDetailsScreen(
    viewModel: TrendsDetailsViewModel,
    dateFormatter: DateFormatter,
) {
    val state = viewModel.state.value.trendsDetailsData
    val context = LocalContext.current

    // FIXME: hardcoded images, fix later
    val flowPicture =
        if (state?.flowAvg == "0.0") painterResource(id = R.drawable.ic_minus_flow)
        else painterResource(id = R.drawable.ic_flow)
    val fatiguePicture =
        if (state?.fatigueAvg == "0") painterResource(id = R.drawable.ic_minus_fatigue)
        else painterResource(id = R.drawable.ic_fatique)
    val winRatePicture =
        if (state?.winNumber == "0.0") painterResource(id = R.drawable.ic_minus_win_rate)
        else painterResource(id = R.drawable.ic_minus_win_rate)
    val gradePicture =
        if (state?.gradeAvg == "0.0") painterResource(id = R.drawable.ic_minus_grade)
        else painterResource(id = R.drawable.ic_minus_grade)

    Column() {
        BasicTopAppBar(title = R.string.trends) {
            viewModel.navigateBack()
        }
        Box(Modifier.fillMaxWidth()) {
            LazyColumn(
                modifier = Modifier
                    .background(Color.Black)
                    .fillMaxSize()
                    .padding(Spacing_12)
            ) {
                if (!viewModel.state.value.isLoading) {
                    item {
                        Spacer(modifier = Modifier.height(Spacing_10))
                        Card(backgroundColor = BoxDarkGrey, shape = RoundedCornerShape(5)) {
                            state?.filteredData?.let {
                                if (it.size > 1) {
                                    TrendsChart(
                                        chartData = getChartDataList(
                                            state.filteredData,
                                            dateFormatter,
                                            state.metricFilter,
                                        ),
                                        modifier = Modifier
                                            .fillMaxWidth()
                                            .height(290.dp)
                                            .padding(Spacing_6),
                                        graphColor = getChartColor(state.metricFilter),
                                        metricFilter = state.metricFilter,
                                        duringGameData = getDuringGameMap(state.filteredData),
                                        durationFilter = state.durationFilter,
                                        hourlyGameData = getHourlyMap(state.filteredData),
                                        perGameData = getPerGameMap(state.filteredData)
                                    )
                                } else {
                                    Box(
                                        modifier = Modifier
                                            .fillMaxWidth()
                                            .height(300.dp),
                                        contentAlignment = Alignment.Center
                                    ) {
                                        Text(
                                            text = stringResource(id = R.string.not_enough_data_graph),
                                            color = White,
                                        )
                                    }
                                }
                            }
                        }
                        Spacer(modifier = Modifier.height(Spacing_10))
                        Row(
                            modifier = Modifier
                                .fillMaxWidth(),
                            horizontalArrangement = Arrangement.spacedBy(Spacing_10)
                        ) {
                            Column(
                                modifier = Modifier
                                    .weight(1f)
                            ) {
                                TrendCard(
                                    trendName = stringResource(id = R.string.grade),
                                    trendScore = state?.gradeAvg.toString(),
                                    trendIcon = gradePicture,
                                    trendColor = GameGrade,
                                    onClicked = { }
                                )
                                Spacer(modifier = Modifier.height(Spacing_10))
                                TrendCard(
                                    trendName = stringResource(id = R.string.high_fatigue),
                                    trendScore = state?.fatigueAvg.toPercentageString(),
                                    trendIcon = fatiguePicture,
                                    trendColor = FatigueText,
                                    onClicked = { }
                                )
                            }
                            Column(
                                modifier = Modifier
                                    .weight(1f)
                            ) {
                                TrendCard(
                                    trendName = stringResource(id = R.string.win_rate),
                                    trendScore = state?.winNumber.toPercentageString(),
                                    trendIcon = winRatePicture,
                                    trendColor = WinRate,
                                    onClicked = { }
                                )
                                Spacer(modifier = Modifier.height(Spacing_10))
                                TrendCard(
                                    trendName = stringResource(id = R.string.high_flow),
                                    trendScore = state?.flowAvg.toPercentageString(),
                                    trendIcon = flowPicture,
                                    trendColor = FlowText,
                                    onClicked = { }
                                )
                            }
                        }
                        Spacer(modifier = Modifier.height(Spacing_15))
                        Card(
                            shape = RoundedCornerShape(Spacing_10), backgroundColor = BoxDarkGrey,
                        ) {
                            val value = state?.trendsInsightValue
                            AdviceText(
                                text = state?.trendsInsight.getTrendsInsightText(value, LocalContext.current)
                            )
                        }
                        Spacer(modifier = Modifier.height(Spacing_30))
                        Row(
                            modifier = Modifier
                                .fillMaxWidth(),
                            horizontalArrangement = Arrangement.SpaceBetween,
                            verticalAlignment = Alignment.CenterVertically,
                        ) {
                            Text(
                                text = stringResource(id = R.string.filters_header),
                                fontSize = Text_18,
                                fontWeight = FontWeight.Bold,
                                color = White
                            )
                            val matches = state?.filteredData?.size
                            Text(
                                text = context.resources.getString(R.string.matches_number, matches),
                                fontSize = Text_16,
                                color = subHeadGrayText,
                            )
                        }
                        Spacer(modifier = Modifier.height(Spacing_15))
                        MenuButtonWithValue(
                            name = R.string.game_filter,
                            value = state?.gameFilter?.toLocalName(context).toString()
                        ) {
                            viewModel.navigateToGameFilters(
                                viewModel.trendsData.gameFilter.toLocalName(context),
                                viewModel.trendsData.gameList,
                            )
                        }
                        Spacer(modifier = Modifier.height(Spacing_10))
                        MenuButtonWithValue(
                            name = R.string.metric_filter,
                            value = state?.metricFilter?.toLocalName(context).toString()
                        ) {
                            viewModel.navigateToMetricFilters(
                                viewModel.trendsData.metricFilter.toLocalName(context),
                                viewModel.trendsData.metricList,
                            )
                        }
                        Spacer(modifier = Modifier.height(Spacing_10))
                        MenuButtonWithValue(
                            name = R.string.duration_filter,
                            value = state?.durationFilter?.toLocalName(context).toString()
                        ) {
                            viewModel.navigateToDurationFilters(
                                viewModel.trendsData.durationFilter.toLocalName(context),
                                viewModel.trendsData.durationList,
                            )
                        }
                        Spacer(modifier = Modifier.height(Spacing_10))
                        MenuButtonWithValue(
                            name = R.string.tag_filter,
                            value = state?.tagFilter?.toLocalName(context).toString()
                        ) {
                            viewModel.navigateToTagFilters(
                                viewModel.trendsData.tagFilter.toLocalName(context),
                                viewModel.trendsData.tagList,
                            )
                        }
                        Spacer(modifier = Modifier.height(Spacing_10))
                        MenuButtonWithValue(
                            name = R.string.champion_filter,
                            value = state?.championFilter?.toLocalName(context).toString()
                        ) {
                            viewModel.navigateToChampionFilters(
                                viewModel.trendsData.championFilter.toLocalName(context),
                                viewModel.trendsData.championsList
                            )
                        }
                        Spacer(modifier = Modifier.height(Spacing_10))
                        MenuButtonWithValue(
                            name = R.string.username_filter,
                            value = state?.userNameFilter?.toLocalName(context).toString()
                        ) {
                            viewModel.navigateToUsernameFilters(
                                viewModel.trendsData.userNameFilter.toLocalName(context),
                                viewModel.trendsData.userNameList
                            )
                        }
                        Spacer(modifier = Modifier.height(Spacing_10))
                    }
                }
            }
            if (viewModel.state.value.isLoading) {
                CircularProgressIndicator(
                    modifier = Modifier
                        .height(Spacing_40)
                        .width(Spacing_40)
                        .align(Alignment.Center),
                    color = Color.Red
                )
            }
        }
    }
}