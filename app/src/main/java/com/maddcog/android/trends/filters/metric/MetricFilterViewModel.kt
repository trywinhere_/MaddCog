package com.maddcog.android.trends.filters.metric

import com.maddcog.android.baseui.presentation.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MetricFilterViewModel @Inject constructor() : BaseViewModel() {

    private var _metric: String? = null
    val metric: String
        get() = _metric!!

    fun setMetric(metric: String) {
        _metric = metric
    }
}