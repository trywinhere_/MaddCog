package com.maddcog.android.trends

import com.maddcog.android.baseui.model.NavCommand

interface ITrendsDetailsRouter {

    fun navigateToDurationFilters(duration: String, list: List<String>): NavCommand

    fun navigateToMetricFilters(metric: String, list: List<String>): NavCommand

    fun navigateToGameFilters(game: String, list: List<String>): NavCommand

    fun navigateToTagFilters(tag: String, list: List<String>): NavCommand

    fun navigateToChampionFilters(champion: String, list: List<String>): NavCommand

    fun navigateToUsernameFilters(username: String, list: List<String>): NavCommand
}