package com.maddcog.android.trends

import android.os.Bundle
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import com.maddcog.android.baseui.model.Resource
import com.maddcog.android.baseui.presentation.BaseViewModel
import com.maddcog.android.baseui.ui.theme.*
import com.maddcog.android.domain.entities.activity.UserActivity
import com.maddcog.android.domain.usecase.GetTrendsDetailsUseCase
import com.maddcog.android.trends.extensions.GAME_GRADE
import com.maddcog.android.trends.extensions.MENTAL_PERFORMANCE
import com.maddcog.android.trends.extensions.WIN_RATE
import com.maddcog.android.trends.extensions.getFatigueAvg
import com.maddcog.android.trends.extensions.getFlowAvg
import com.maddcog.android.trends.extensions.getGradeAvg
import com.maddcog.android.trends.extensions.getWinNumber
import com.maddcog.android.trends.extensions.toScreenData
import com.maddcog.android.trends.model.TrendsDetailsScreenData
import com.maddcog.android.trends.model.TrendsDetailsState
import com.maddcog.android.trends.utils.FiltersListHandler
import com.maddcog.android.trends.utils.FiltersListHandler.Companion.ALL
import com.maddcog.android.trends.utils.FiltersListHandler.Companion.ALL_ACTIVITIES
import com.maddcog.android.trends.utils.FiltersListHandler.Companion.DURING_GAME
import com.maddcog.android.trends.utils.FiltersListHandler.Companion.HOURLY
import com.maddcog.android.trends.utils.FiltersListHandler.Companion.LAST_MONTH
import com.maddcog.android.trends.utils.FiltersListHandler.Companion.LAST_WEEK
import com.maddcog.android.trends.utils.FiltersListHandler.Companion.PER_GAME
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@HiltViewModel
class TrendsDetailsViewModel @Inject constructor(
    private val useCase: GetTrendsDetailsUseCase,
    private val router: ITrendsDetailsRouter,
    private val filtersHandler: FiltersListHandler,
) : BaseViewModel() {

    private val _errorState = MutableLiveData<Boolean>()
    val errorState: LiveData<Boolean>
        get() = _errorState

    private val _state = mutableStateOf(TrendsDetailsState())
    val state: State<TrendsDetailsState> = _state

    private var _trendsData: TrendsDetailsScreenData? = null
    val trendsData: TrendsDetailsScreenData
        get() = _trendsData!!

    val firebaseAnalytics: FirebaseAnalytics = Firebase.analytics

    fun getTrendsDetails(filter: String?) {
        useCase().onEach { result ->
            when (result) {
                is Resource.Success -> {
                    _trendsData = result.data?.toScreenData()
                    filter?.let {
                        refreshMetric(filter)
                    }
                    _state.value = TrendsDetailsState(trendsDetailsData = _trendsData)
                    _errorState.value = false
                }
                is Resource.Error -> {
                    _state.value = TrendsDetailsState(error = result.message ?: UNEXPECTED_ERROR)
                    _errorState.value = true
                }
                is Resource.Loading -> {
                    _state.value = TrendsDetailsState(isLoading = true)
                    _errorState.value = false
                }
            }
        }.launchIn(viewModelScope)
    }

    fun refreshDuration(text: String?) {
        text?.let { filter ->
            trendsData.durationFilter = filter
            generateFilteredData()
            if (filter == DURING_GAME || filter == HOURLY || filter == PER_GAME) {
                trendsData.metricList = listOf(MENTAL_PERFORMANCE)
                trendsData.metricFilter = MENTAL_PERFORMANCE
            } else {
                trendsData.metricList = filtersHandler.getMetricList(trendsData.filteredData)
            }
        }
        refreshData()
    }

    fun refreshMetric(metric: String?) {
        metric?.let {
            trendsData.metricFilter = it
        }
        generateFilteredData()
        refreshData()
    }

    fun refreshGame(text: String?) {
        text?.let {
            trendsData.gameFilter = it
            trendsData.metricFilter = MENTAL_PERFORMANCE
        }
        generateFilteredData()
        trendsData.metricList = filtersHandler.getMetricList(trendsData.filteredData)
        trendsData.tagList = filtersHandler.getTagsList(trendsData.filteredData)
        trendsData.championsList = filtersHandler.getFilterList(
            ALL,
            trendsData.filteredData.map { it.match.myPlayer.characterName }
        )
        trendsData.userNameList = filtersHandler.getFilterList(
            ALL,
            trendsData.filteredData.map { it.match.myPlayer.gamerName }
        )
        refreshData()
    }

    fun refreshTag(text: String?) {
        text?.let {
            trendsData.tagFilter = it
        }
        generateFilteredData()
        refreshData()
    }

    fun refreshChampion(champion: String?) {
        champion?.let {
            trendsData.championFilter = it
        }
        generateFilteredData()
        refreshData()
    }

    fun refreshUserName(username: String?) {
        username?.let {
            trendsData.userNameFilter = it
        }
        generateFilteredData()
        refreshData()
    }

    private fun generateFilteredData() {
        var finalList = trendsData.initialData

        val gameFilter = trendsData.gameFilter
        val timeFilter = trendsData.durationFilter
        val championFilter = trendsData.championFilter
        val usernameFilter = trendsData.userNameFilter
        val tagFilter = trendsData.tagFilter
        val metricFilter = trendsData.metricFilter

        finalList = finalList.useGameFilter(gameFilter)
        finalList = finalList.useDurationFilter(timeFilter)
        finalList = finalList.useChampionFilter(championFilter)
        finalList = finalList.useUsernameFilter(usernameFilter)
        finalList = finalList.useTagFilter(tagFilter)
        finalList = finalList.useMetricFilter(metricFilter)

        trendsData.filteredData = finalList
        trendsData.gradeAvg = getGradeAvg(trendsData.filteredData)
        trendsData.flowAvg = getFlowAvg(trendsData.filteredData)
        trendsData.fatigueAvg = getFatigueAvg(trendsData.filteredData)
        trendsData.winNumber = getWinNumber(trendsData.filteredData)
    }

    private fun List<UserActivity>.useGameFilter(gameFilter: String): List<UserActivity> {
        val list = this
        return when (gameFilter) {
            ALL_ACTIVITIES -> list
            else -> list.filter { it.activityType.name == gameFilter }
        }
    }

    private fun List<UserActivity>.useDurationFilter(timeFilter: String): List<UserActivity> {
        val list = this
        return when (timeFilter) {
            LAST_MONTH -> list.filter { it.timestamp > list.last().timestamp.minusDays(30) }
            LAST_WEEK -> list.filter { it.timestamp > list.last().timestamp.minusDays(7) }
            HOURLY -> list
            PER_GAME -> list
            DURING_GAME -> list
            else -> list
        }
    }

    private fun List<UserActivity>.useChampionFilter(championFilter: String): List<UserActivity> {
        val list = this
        return when (championFilter) {
            ALL -> list
            else -> list.filter { it.match.myPlayer.characterName == championFilter }
        }
    }

    private fun List<UserActivity>.useUsernameFilter(usernameFilter: String): List<UserActivity> {
        val list = this
        return when (usernameFilter) {
            ALL -> list
            else -> list.filter { it.match.myPlayer.gamerName == usernameFilter }
        }
    }

    private fun List<UserActivity>.useTagFilter(tagFilter: String): List<UserActivity> {
        val list = this
        return when (tagFilter) {
            ALL -> list
            else -> list.filter { it.tags.any { tag -> tag == tagFilter } }
        }
    }

    private fun List<UserActivity>.useMetricFilter(metricFilter: String): List<UserActivity> {
        val list = this
        return when (metricFilter) {
            MENTAL_PERFORMANCE -> list
            WIN_RATE -> list
            GAME_GRADE -> list
            else -> list.filter { it.match.myPlayer.performanceParams.any { param -> param.name == metricFilter } }
        }
    }

    fun refreshData() {
        _state.value = TrendsDetailsState(trendsDetailsData = _trendsData)
    }

    fun navigateToDurationFilters(duration: String, list: List<String>) {
        firebaseAnalytics.logEvent(
            "trends_filter_duration",
            Bundle().apply {
                this.putString(
                    TRENDS_FILTER_DURATION, "trends_filter_duration $duration"
                )
            }
        )
        navigateTo(router.navigateToDurationFilters(duration, list))
    }

    fun navigateToMetricFilters(metric: String, list: List<String>) {
        firebaseAnalytics.logEvent(
            "trends_filter_game",
            Bundle().apply {
                this.putString(
                    TRENDS_FILTER_METRIC, "trends_filter_metric $metric"
                )
            }
        )
        navigateTo(router.navigateToMetricFilters(metric, list))
    }

    fun navigateToGameFilters(game: String, list: List<String>) {
        firebaseAnalytics.logEvent(
            "trends_filter_game",
            Bundle().apply {
                this.putString(
                    TRENDS_FILTER_GAME, "trends_filter_game $game"
                )
            }
        )
        navigateTo(router.navigateToGameFilters(game, list))
    }

    fun navigateToTagFilters(tag: String, list: List<String>) {
        firebaseAnalytics.logEvent(
            "trends_filter_tag",
            Bundle().apply {
                this.putString(
                    TRENDS_FILTER_TAG, "trends_filter_tag $tag"
                )
            }
        )
        navigateTo(router.navigateToTagFilters(tag, list))
    }

    fun navigateToChampionFilters(champion: String, list: List<String>) {
        firebaseAnalytics.logEvent(
            "trends_filter_champion",
            Bundle().apply {
                this.putString(
                    TRENDS_FILTER_CHAMPION, "trends_filter_champion $champion"
                )
            }
        )
        navigateTo(router.navigateToChampionFilters(champion, list))
    }

    fun navigateToUsernameFilters(username: String, list: List<String>) {
        firebaseAnalytics.logEvent(
            "trends_filter_username",
            Bundle().apply {
                this.putString(
                    TRENDS_FILTER_USERNAME, "trends_filter_username $username"
                )
            }
        )
        navigateTo(router.navigateToUsernameFilters(username, list))
    }
}