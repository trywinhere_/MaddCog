package com.maddcog.android.trends.filters.username

import com.maddcog.android.baseui.presentation.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class UsernameFilterViewModel @Inject constructor() : BaseViewModel() {

    private var _username: String? = null
    val username: String
        get() = _username!!

    fun setUsername(username: String) {
        _username = username
    }
}