package com.maddcog.android.trends.filters.game

import com.maddcog.android.baseui.presentation.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class GameFilterViewModel@Inject constructor() : BaseViewModel() {

    private var _game: String? = null
    val game: String
        get() = _game!!

    fun setGame(game: String) {
        _game = game
    }
}