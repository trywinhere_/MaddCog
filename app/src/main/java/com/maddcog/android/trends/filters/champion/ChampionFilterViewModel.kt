package com.maddcog.android.trends.filters.champion

import com.maddcog.android.baseui.presentation.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class ChampionFilterViewModel @Inject constructor() : BaseViewModel() {

    private var _champion: String? = null
    val champion: String
        get() = _champion!!

    fun setChampion(champion: String) {
        _champion = champion
    }
}