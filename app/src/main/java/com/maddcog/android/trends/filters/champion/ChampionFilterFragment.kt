package com.maddcog.android.trends.filters.champion

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.ui.platform.ComposeView
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.maddcog.android.R
import com.maddcog.android.baseui.presentation.BaseFragment
import com.maddcog.android.baseui.presentation.BaseViewModel
import com.maddcog.android.baseui.ui.theme.MaddcogTheme
import com.maddcog.android.trends.TrendsDetailsFragment
import com.maddcog.android.trends.filters.FilterListScreen
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ChampionFilterFragment : BaseFragment() {

    private val viewModel: ChampionFilterViewModel by viewModels()
    private val args by navArgs<ChampionFilterFragmentArgs>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.setChampion(args.champion)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = ComposeView(requireContext())
        view.setContent {
            MaddcogTheme {
                FilterListScreen(
                    onItemSelected = {
                        setFragmentResult(
                            TrendsDetailsFragment.TRENDS_CHAMPION_KEY,
                            Bundle().apply {
                                putString(TrendsDetailsFragment.TRENDS_CHAMPION_DATA_KEY, viewModel.champion)
                            }
                        )
                        viewModel.navigateBack()
                    },
                    onBackPressed = {
                        viewModel.navigateBack()
                    },
                    args.champion,
                    args.championsList.toList(),
                    setFilter = {
                        viewModel.setChampion(it)
                    },
                    R.string.champion_filter,
                )
            }
        }

        return view
    }

    override fun withViewModel(): BaseViewModel = viewModel.apply {
    }
}