package com.maddcog.android.baseui.ui.theme

import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight

val WhiteText24Bold = TextStyle(
    fontFamily = FontFamily.Default,
    fontWeight = FontWeight.Bold,
    fontSize = Text_24,
    color = Color.White
)

val WhiteText18Bold = TextStyle(
    fontFamily = FontFamily.Default,
    fontWeight = FontWeight.Bold,
    fontSize = Text_18,
    color = Color.White
)

val WhiteText16Bold = TextStyle(
    fontFamily = FontFamily.Default,
    fontWeight = FontWeight.Bold,
    fontSize = Text_16,
    color = Color.White
)

val WhiteText12Bold = TextStyle(
    fontFamily = FontFamily.Default,
    fontWeight = FontWeight.Bold,
    fontSize = Text_12,
    color = Color.White
)

val WhiteText14Bold = TextStyle(
    fontFamily = FontFamily.Default,
    fontWeight = FontWeight.Bold,
    fontSize = Text_14,
    color = Color.White
)

val WhiteText16 = TextStyle(
    fontFamily = FontFamily.Default,
    fontWeight = FontWeight.Normal,
    fontSize = Text_16,
    color = Color.White
)

val BlueText16 = TextStyle(
    fontFamily = FontFamily.Default,
    fontWeight = FontWeight.Normal,
    fontSize = Text_16,
    color = MarineBlue
)

val WhiteText14 = TextStyle(
    fontFamily = FontFamily.Default,
    fontWeight = FontWeight.Normal,
    fontSize = Text_14,
    color = Color.White
)

val WhiteText12 = TextStyle(
    fontFamily = FontFamily.Default,
    fontWeight = FontWeight.Normal,
    fontSize = Text_12,
    color = Color.White
)

val HintGreyText16 = TextStyle(
    fontFamily = FontFamily.Default,
    fontWeight = FontWeight.Normal,
    fontSize = Text_16,
    color = HintLightGrey
)

val HintGreyText12 = TextStyle(
    fontFamily = FontFamily.Default,
    fontWeight = FontWeight.Normal,
    fontSize = Text_12,
    color = HintLightGrey
)

val RedText16 = TextStyle(
    fontFamily = FontFamily.Default,
    fontWeight = FontWeight.Normal,
    fontSize = Text_16,
    color = Red
)

val RedText16Bold = TextStyle(
    fontFamily = FontFamily.Default,
    fontWeight = FontWeight.Bold,
    fontSize = Text_16,
    color = Red
)

val RedText14Bold = TextStyle(
    fontFamily = FontFamily.Default,
    fontWeight = FontWeight.Bold,
    fontSize = Text_14,
    color = Red
)

val RedText12 = TextStyle(
    fontFamily = FontFamily.Default,
    fontWeight = FontWeight.Normal,
    fontSize = Text_12,
    color = Red
)

val TickGreyText14 = TextStyle(
    fontFamily = FontFamily.Default,
    fontWeight = FontWeight.Normal,
    fontSize = Text_14,
    color = LineChartTickGrey
)

val BlackText12Bold = TextStyle(
    fontFamily = FontFamily.Default,
    fontWeight = FontWeight.Bold,
    fontSize = Text_12,
    color = Color.Black
)
