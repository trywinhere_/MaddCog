package com.maddcog.android.baseui.extensions

import android.content.Intent
import android.net.Uri
import com.maddcog.android.baseui.model.NavCommand
import java.util.*

const val HTTP_404 = "HTTP 404 "

fun String.openWebLink(): NavCommand {
    return NavCommand
        .Intent(
            Intent(Intent.ACTION_VIEW).let {
                it.data = Uri.parse(this)
                it
            }
        )
}

fun String?.toPercentageString(): String {
    return "$this %"
}

fun String.toCapitalize(): String {
    return this.replaceFirstChar { if (it.isLowerCase()) it.titlecase(Locale.getDefault()) else it.toString() }
}