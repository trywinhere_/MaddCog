package com.maddcog.android.baseui.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.layoutId
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.ConstraintSet
import com.maddcog.android.R
import com.maddcog.android.baseui.ui.theme.Spacing_10
import com.maddcog.android.baseui.ui.theme.WhiteText18Bold
import com.maddcog.android.baseui.ui.theme.arrow_back

@Composable
fun BasicTopAppBar(
    modifier: Modifier = Modifier,
    title: Int,
    color: Color = Color.Black,
    navigateBack: () -> Unit,
) {
    TopAppBar(
        backgroundColor = color,
        elevation = 0.dp,
        modifier = modifier,
    ) {
        val constraints = ConstraintSet {
            val navIcon = createRefFor("nav_icon")
            val text = createRefFor("title")

            constrain(navIcon) {
                top.linkTo(parent.top)
                start.linkTo(parent.start)
                bottom.linkTo(parent.bottom)
            }
            constrain(text) {
                top.linkTo(parent.top)
                bottom.linkTo(parent.bottom)
                end.linkTo(parent.end)
                start.linkTo(parent.start)
            }
        }
        ConstraintLayout(
            constraints,
            modifier = Modifier.fillMaxWidth()
        ) {
            Text(
                text = stringResource(id = title),
                style = WhiteText18Bold,
                modifier = Modifier.layoutId("title")
            )
            Image(
                painterResource(id = R.drawable.ic_arrow_back),
                contentDescription = arrow_back,
                modifier = Modifier
                    .layoutId("nav_icon")
                    .clickable {
                        navigateBack()
                    }
                    .padding(start = Spacing_10)
            )
        }
    }
}