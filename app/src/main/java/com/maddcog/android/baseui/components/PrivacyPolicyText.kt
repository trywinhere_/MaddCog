package com.maddcog.android.baseui.components

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.width
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import com.google.accompanist.flowlayout.FlowMainAxisAlignment
import com.google.accompanist.flowlayout.FlowRow
import com.maddcog.android.R
import com.maddcog.android.baseui.ui.theme.RedText12
import com.maddcog.android.baseui.ui.theme.Spacing_5
import com.maddcog.android.baseui.ui.theme.WhiteText12

@Composable
fun PrivacyPolicyText(
    navigateToPrivacyPolicy: () -> Unit,
) {
    FlowRow(
        modifier = Modifier.fillMaxWidth(),
        mainAxisAlignment = FlowMainAxisAlignment.Center
    ) {
        Text(
            text = stringResource(id = R.string.police_agreement),
            style = WhiteText12,
        )
        Spacer(modifier = Modifier.width(Spacing_5))
        Text(
            text = stringResource(id = R.string.privacy_policy),
            style = RedText12,
            modifier = Modifier.clickable {
                navigateToPrivacyPolicy()
            }
        )
    }
}