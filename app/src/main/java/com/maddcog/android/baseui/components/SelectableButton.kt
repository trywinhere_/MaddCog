package com.maddcog.android.baseui.components

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.CheckCircle
import androidx.compose.material.icons.outlined.RadioButtonUnchecked
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import com.maddcog.android.baseui.ui.theme.BoxDarkGrey
import com.maddcog.android.baseui.ui.theme.Spacing_0
import com.maddcog.android.baseui.ui.theme.Spacing_12
import com.maddcog.android.baseui.ui.theme.Spacing_16
import com.maddcog.android.baseui.ui.theme.WhiteText16

@Composable
fun SelectableButton(
    modifier: Modifier,
    name: String,
    selectionTerm: Boolean,
) {
    Card(
        modifier = modifier
            .fillMaxWidth(),
        shape = RoundedCornerShape(20),
        elevation = Spacing_0,
        backgroundColor = BoxDarkGrey
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .background(color = BoxDarkGrey)
                .padding(horizontal = Spacing_12, vertical = Spacing_16),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Text(
                text = name,
                style = WhiteText16
            )
            Icon(
                modifier = Modifier.padding(end = Spacing_16),
                imageVector = if (selectionTerm)
                    Icons.Outlined.CheckCircle else
                    Icons.Outlined.RadioButtonUnchecked,
                contentDescription = null,
                tint = Color.White
            )
        }
    }
}