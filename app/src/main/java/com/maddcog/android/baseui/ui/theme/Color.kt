package com.maddcog.android.baseui.ui.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF000000)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)

val Black = Color(0xFF000000)
val BoxDarkGrey = Color(0xFF232323)
val HintLightGrey = Color(0xFF808080)
val White = Color(0xFFFFFFFF)
val Red = Color(0xFFEA3323)
val MarineBlue = Color(0xFF427fCF)
val GameGrade = Color(0xFF7EF7C5)
val WinRate = Color(0xFFF4BA40)
val fatigue = Color(0xFFEC6253)
val FatigueText = Color(0xFF8A3E37)
val FlowText = Color(0xFF9D60DD)
val subHeadGrayText = Color(0xFF919191)
val DialogBackgroundColor = Color(0x4D000000)
val BlackSemiTransparent = Color(0, 0, 0, 0x66)

val FatigueChart = Color(0xFF372827)
val ChartLineGrey = Color(0xFF555454)
val FlowChart = Color(0xFF8454B7)
val GameStatChart = Color(0xFF7BA5CC)
val LineChartEmptyGrey = Color(0xFF313131)
val LineChartTickGrey = Color(0xFF7F7F7F)

val ImageTileTint = Color(0xFF2c2c2c)

val KdaText = Color(0xFFCE8883)
val KillParticipationText = Color(0xFF9FB7D0)
val CreepScoreText = Color(0xFFC5C378)

val KillsChartColor = Color(0xFF4D7C2E)
val DeathsChartColor = Color(0xFF7F7F7F)
val FlowBreakdownColor = Color(0xFF1F132C)
val GameSummaryBannerTint = Color(0, 0, 0, 0xBF)

val LiveGameDataColor = Color(0xFF100D16)
