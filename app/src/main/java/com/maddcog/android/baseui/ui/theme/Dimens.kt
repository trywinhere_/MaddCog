package com.maddcog.android.baseui.ui.theme

import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

val MATERIAL_DEFAULT_HEIGHT = 56.dp

val Spacing_0 = 0.dp
val Spacing_2 = 2.dp
val Spacing_3 = 3.dp
val Spacing_5 = 5.dp
val Spacing_7 = 7.dp
val Spacing_6 = 6.dp
val Spacing_8 = 8.dp
val Spacing_10 = 10.dp
val Spacing_12 = 12.dp
val Spacing_15 = 15.dp
val Spacing_16 = 16.dp
val Spacing_18 = 18.dp
val Spacing_20 = 20.dp
val Spacing_21 = 21.dp
val Spacing_26 = 26.dp
val Spacing_30 = 30.dp
val Spacing_40 = 40.dp
val Spacing_50 = 50.dp
val Spacing_60 = 60.dp
val Spacing_70 = 70.dp
val Spacing_80 = 80.dp
val Spacing_90 = 90.dp
val Spacing_120 = 120.dp
val Spacing_150 = 150.dp
val Spacing_170 = 170.dp
val Spacing_180 = 180.dp
val Spacing_220 = 220.dp
val Spacing_250 = 250.dp
val Spacing_360 = 360.dp

val Text_8 = 8.sp
val Text_10 = 10.sp
val Text_12 = 12.sp
val Text_14 = 14.sp
val Text_16 = 16.sp
val Text_18 = 18.sp
val Text_20 = 20.sp
val Text_22 = 22.sp
val Text_24 = 24.sp
val Text_40 = 40.sp

const val oneLine = 1
