package com.maddcog.android.baseui.components

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import com.maddcog.android.baseui.ui.theme.Black
import com.maddcog.android.baseui.ui.theme.Spacing_50
import com.maddcog.android.baseui.ui.theme.Text_18
import com.maddcog.android.baseui.ui.theme.White

@Composable
fun SimpleButton(
    name: Int,
    onClick: () -> Unit,
) {
    Button(
        onClick = {
            onClick()
        }, shape = RoundedCornerShape(20),
        colors = ButtonDefaults.buttonColors(
            backgroundColor = White,
            contentColor = Black
        ),
        modifier = Modifier
            .fillMaxWidth()
            .height(Spacing_50)
    ) {
        Text(
            text = stringResource(id = name),
            color = Black,
            textAlign = TextAlign.Center,
            modifier = Modifier.fillMaxWidth(),
            fontSize = Text_18
        )
    }
}