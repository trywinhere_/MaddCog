package com.maddcog.android.baseui.presentation

import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.AlertDialog
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import com.maddcog.android.R
import com.maddcog.android.baseui.ui.theme.BoxDarkGrey
import com.maddcog.android.baseui.ui.theme.Spacing_10

@Composable
fun NativeAlertDialog(
    onExit: () -> Unit,
    onSuccess: () -> Unit,
    successName: Int,
    disclaimerFirst: Int,
    disclaimerSecond: Int,
) {
    AlertDialog(
        backgroundColor = BoxDarkGrey,
        shape = RoundedCornerShape(Spacing_10),
        onDismissRequest = {
            onExit()
        },
        title = {
            Text(text = stringResource(id = disclaimerFirst), color = Color.White)
        },
        text = {
            Text(text = stringResource(id = disclaimerSecond), color = Color.White)
        },
        confirmButton = {
            TextButton(
                onClick = { onSuccess() }
            ) {
                Text(
                    text = stringResource(id = successName),
                    color = Color.White,
                )
            }
        },
        dismissButton = {
            TextButton(
                onClick = { onExit() }
            ) {
                Text(
                    text = stringResource(id = R.string.cancel_btn),
                    color = Color.White,
                )
            }
        }
    )
}