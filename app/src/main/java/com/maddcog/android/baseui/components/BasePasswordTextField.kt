package com.maddcog.android.baseui.components

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.material.TextFieldDefaults
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Visibility
import androidx.compose.material.icons.filled.VisibilityOff
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.onFocusChanged
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.input.VisualTransformation
import com.maddcog.android.baseui.ui.theme.BoxDarkGrey
import com.maddcog.android.baseui.ui.theme.HintLightGrey
import com.maddcog.android.baseui.ui.theme.Text_18
import com.maddcog.android.baseui.ui.theme.White

@Composable
fun BasePasswordTextField(
    saveText: (String) -> Unit,
    placeholder: Int,
) {
    var inputText by remember { mutableStateOf(TextFieldValue("")) }
    var passwordVisible by rememberSaveable { mutableStateOf(false) }

    TextField(
        value = inputText,
        onValueChange = { text ->
            inputText = text
            saveText(text.text)
        },
        placeholder = {
            Text(
                text = stringResource(id = placeholder),
                color = HintLightGrey,
                fontSize = Text_18
            )
        },
        modifier = Modifier
            .fillMaxWidth()
            .onFocusChanged {
                if (it.isFocused) {
                    inputText = TextFieldValue("")
                }
            },
        shape = RoundedCornerShape(15),
        colors = TextFieldDefaults.textFieldColors(
            backgroundColor = BoxDarkGrey,
            textColor = White, placeholderColor = HintLightGrey,
            cursorColor = White
        ),
        singleLine = true,
        visualTransformation = if (passwordVisible) VisualTransformation.None else PasswordVisualTransformation(),
        keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Password),
        trailingIcon = {
            val image = if (passwordVisible)
                Icons.Filled.Visibility
            else Icons.Filled.VisibilityOff

            IconButton(onClick = { passwordVisible = !passwordVisible }) {
                Icon(imageVector = image, contentDescription = "")
            }
        }
    )
}