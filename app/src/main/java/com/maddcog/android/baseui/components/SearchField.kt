package com.maddcog.android.baseui.components

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.TextField
import androidx.compose.material.TextFieldDefaults
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.Search
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.unit.dp
import com.maddcog.android.baseui.ui.theme.BoxDarkGrey
import com.maddcog.android.baseui.ui.theme.HintLightGrey
import com.maddcog.android.baseui.ui.theme.Spacing_15
import com.maddcog.android.baseui.ui.theme.Spacing_21
import com.maddcog.android.baseui.ui.theme.Spacing_26
import com.maddcog.android.baseui.ui.theme.Spacing_8
import com.maddcog.android.baseui.ui.theme.White
import com.maddcog.android.baseui.ui.theme.WhiteText16

@Composable
fun SearchBox(state: MutableState<TextFieldValue>) {

    TextField(
        value = state.value,
        onValueChange = { text ->
            state.value = text
        },
        modifier = Modifier
            .padding(horizontal = Spacing_15)
            .fillMaxWidth()
            .height(52.dp),
        shape = RoundedCornerShape(40),
        colors = TextFieldDefaults.textFieldColors(
            backgroundColor = BoxDarkGrey,
            textColor = White, placeholderColor = HintLightGrey,
            cursorColor = Color.White
        ),
        singleLine = true,
        textStyle = WhiteText16,
        leadingIcon = {
            Icon(
                Icons.Default.Search,
                contentDescription = "",
                modifier = Modifier
                    .padding(Spacing_8)
                    .size(Spacing_26),
                tint = Color.White
            )
        },
        trailingIcon = {
            if (state.value != TextFieldValue("")) {
                IconButton(
                    onClick = {
                        state.value =
                            TextFieldValue("") // Remove text from TextField when you press the 'X' icon
                    }
                ) {
                    Icon(
                        Icons.Default.Close,
                        contentDescription = "",
                        modifier = Modifier
                            .padding(Spacing_15)
                            .size(Spacing_21),
                        tint = Color.White
                    )
                }
            }
        }
    )
}