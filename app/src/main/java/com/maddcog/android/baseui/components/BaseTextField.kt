package com.maddcog.android.baseui.components

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.material.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.TextFieldValue
import com.maddcog.android.baseui.ui.theme.BoxDarkGrey
import com.maddcog.android.baseui.ui.theme.HintLightGrey
import com.maddcog.android.baseui.ui.theme.Text_18
import com.maddcog.android.baseui.ui.theme.White

@Composable
fun BaseTextField(
    saveText: (String) -> Unit,
    placeholder: Int,
) {
    var inputText by remember { mutableStateOf(TextFieldValue("")) }

    TextField(
        value = inputText,
        onValueChange = { text ->
            inputText = text
            saveText(text.text)
        },
        placeholder = {
            Text(
                text = stringResource(id = placeholder),
                color = HintLightGrey,
                fontSize = Text_18
            )
        },
        modifier = Modifier
            .fillMaxWidth(),
        shape = RoundedCornerShape(15),
        colors = TextFieldDefaults.textFieldColors(
            backgroundColor = BoxDarkGrey,
            textColor = White, placeholderColor = HintLightGrey,
            cursorColor = White
        ),
        singleLine = true
    )
}