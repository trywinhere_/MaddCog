package com.maddcog.android.baseui.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import com.maddcog.android.R
import com.maddcog.android.baseui.ui.theme.BoxDarkGrey
import com.maddcog.android.baseui.ui.theme.Spacing_0
import com.maddcog.android.baseui.ui.theme.Spacing_12
import com.maddcog.android.baseui.ui.theme.Spacing_16
import com.maddcog.android.baseui.ui.theme.WhiteText16
import com.maddcog.android.baseui.ui.theme.arrow_right

@Composable
fun ChangeableMenuButton(
    name: String,
    navigateTo: () -> Unit
) {
    var textField by remember { mutableStateOf(name) }

    Card(
        modifier = Modifier
            .fillMaxWidth()
            .clickable {
                navigateTo()
            },
        shape = RoundedCornerShape(20),
        elevation = Spacing_0,
        backgroundColor = BoxDarkGrey,
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .background(color = BoxDarkGrey)
                .padding(horizontal = Spacing_12, vertical = Spacing_16),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Text(
                text = textField,
                style = WhiteText16
            )
            Image(
                painterResource(id = R.drawable.ic_arrow_forward),
                contentDescription = arrow_right
            )
        }
    }
}