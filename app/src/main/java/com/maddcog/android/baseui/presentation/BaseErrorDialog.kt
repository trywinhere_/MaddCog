package com.maddcog.android.baseui.presentation

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.window.Dialog
import com.maddcog.android.R
import com.maddcog.android.baseui.ui.theme.BlueText16
import com.maddcog.android.baseui.ui.theme.BoxDarkGrey
import com.maddcog.android.baseui.ui.theme.HintLightGrey
import com.maddcog.android.baseui.ui.theme.Spacing_10
import com.maddcog.android.baseui.ui.theme.Spacing_16
import com.maddcog.android.baseui.ui.theme.Spacing_2
import com.maddcog.android.baseui.ui.theme.Spacing_6
import com.maddcog.android.baseui.ui.theme.WhiteText14
import com.maddcog.android.baseui.ui.theme.WhiteText16Bold

@Composable
fun BaseErrorDialog(
    onExit: () -> Unit,
    disclaimerFirst: Int,
    disclaimerSecond: Int,
) {
    Dialog(
        onDismissRequest = {
            onExit()
        }
    ) {
        Surface(
            modifier = Modifier
                .fillMaxWidth()
                .wrapContentHeight(),
            shape = RoundedCornerShape(Spacing_16),
            color = BoxDarkGrey
        ) {
            Column(
                modifier = Modifier.padding(top = Spacing_16)
            ) {

                Text(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(top = Spacing_10),
                    text = stringResource(id = disclaimerFirst),
                    style = WhiteText16Bold,
                    textAlign = TextAlign.Center
                )
                Text(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(
                            top = Spacing_6,
                            bottom = Spacing_10,
                            start = Spacing_16,
                            end = Spacing_16
                        ),
                    text = stringResource(id = disclaimerSecond),
                    style = WhiteText14,
                    textAlign = TextAlign.Center
                )

                Row(
                    modifier = Modifier.fillMaxWidth(),
                ) {
                    Box(
                        modifier = Modifier
                            .fillMaxWidth()
                            .clickable {
                                onExit()
                            }
                            .drawBehind {
                                drawLine(
                                    color = HintLightGrey,
                                    start = Offset(0f, 0f),
                                    end = Offset(size.width, 0f),
                                    strokeWidth = Spacing_2.toPx()
                                )
                            },
                        contentAlignment = Alignment.Center
                    ) {
                        Text(
                            text = stringResource(id = R.string.dismiss),
                            style = BlueText16,
                            modifier = Modifier.padding(vertical = Spacing_10)
                        )
                    }
                }
            }
        }
    }
}