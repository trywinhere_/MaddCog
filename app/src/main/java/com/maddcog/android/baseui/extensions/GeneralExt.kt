package com.maddcog.android.baseui.extensions

inline fun <T> orNull(block: () -> T): T? = try {
    block.invoke()
} catch (exc: Throwable) {
    null
}