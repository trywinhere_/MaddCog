package com.maddcog.android.livegamedata.liveGameStats

import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.ui.platform.ComposeView
import androidx.fragment.app.viewModels
import com.maddcog.android.baseui.presentation.BaseFragment
import com.maddcog.android.baseui.presentation.BaseViewModel
import com.maddcog.android.baseui.ui.theme.MaddcogTheme
import com.maddcog.android.data.bluetoothle.BluetoothStateReceiver
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class LiveGameStatsFragment : BaseFragment() {

    private val viewModel: LiveGameStatsViewModel by viewModels()

    @Inject
    lateinit var bluetoothAdapter: BluetoothAdapter
    private lateinit var bluetoothStateReceiver: BluetoothStateReceiver
    private var isBluetoothDialogAlreadyShown = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bluetoothStateReceiver = BluetoothStateReceiver(viewModel.bluetoothState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = ComposeView(requireContext())

        view.setContent {
            MaddcogTheme {
                LiveGameStatsScreen(
                    onClickEndMatch = { viewModel.navigateToGameStats() },
                    viewModel = viewModel,
                    doReconnect = { showBluetoothDialog() }
                )
            }
        }

        return view
    }

    override fun onResume() {
        super.onResume()
        requireContext().registerReceiver(bluetoothStateReceiver, IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED))
        activity?.window?.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
    }

    override fun withViewModel(): BaseViewModel = viewModel.apply {
    }

    override fun onPause() {
        super.onPause()
        requireContext().unregisterReceiver(bluetoothStateReceiver)
        activity?.window?.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
    }

    private fun showBluetoothDialog() {
        if (!bluetoothAdapter.isEnabled) {
            val enabledBluetoothIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            startBluetoothIntentForResult.launch(enabledBluetoothIntent)
            isBluetoothDialogAlreadyShown = true
        } else {
            viewModel.reconnect()
        }
    }

    private val startBluetoothIntentForResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            isBluetoothDialogAlreadyShown = false
            if (result.resultCode != Activity.RESULT_OK) {
                showBluetoothDialog()
            } else {
                viewModel.reconnect()
            }
        }
}