package com.maddcog.android.livegamedata.components

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.material.*
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.Search
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.TextFieldValue
import com.maddcog.android.R
import com.maddcog.android.baseui.ui.theme.*

private const val SEARCH = "Search or add a new tag"

@Composable
fun SearchBoxWithButton(
    state: MutableState<TextFieldValue>,
    onAddTag: () -> Unit
) {
    val focusManager = LocalFocusManager.current

    Row(modifier = Modifier.fillMaxWidth()) {

        TextField(
            value = state.value,
            placeholder = {
                Text(
                    text = SEARCH,
                    color = White,
                    fontSize = Text_14
                )
            },
            onValueChange = { text ->
                state.value = text
            },
            modifier = Modifier
                .padding(start = Spacing_15, end = Spacing_3)
                .height(Spacing_50),
            shape = RoundedCornerShape(15),
            colors = TextFieldDefaults.textFieldColors(
                backgroundColor = BoxDarkGrey,
                textColor = White, placeholderColor = HintLightGrey,
                cursorColor = Color.White
            ),
            singleLine = true,
            textStyle = WhiteText14,
            leadingIcon = {
                Icon(
                    Icons.Default.Search,
                    contentDescription = "",
                    modifier = Modifier
                        .padding(Spacing_8)
                        .size(Spacing_26),
                    tint = Color.White
                )
            },
            keyboardActions = KeyboardActions(onDone = { focusManager.clearFocus() }),
            trailingIcon = {
                if (state.value != TextFieldValue("")) {
                    IconButton(
                        onClick = {
                            state.value =
                                TextFieldValue("") // Remove text from TextField when you press the 'X' icon
                        }
                    ) {
                        Icon(
                            Icons.Default.Close,
                            contentDescription = "",
                            modifier = Modifier
                                .padding(end = Spacing_5)
                                .size(Spacing_21),
                            tint = Color.White
                        )
                    }
                }
            }
        )

        Spacer(modifier = Modifier.weight(1f))

        TextButton(
            onClick = {
                onAddTag()
                focusManager.clearFocus()
                state.value = TextFieldValue("")
            },
            colors = ButtonDefaults.buttonColors(
                backgroundColor = White,
                contentColor = Black
            ),
            shape = RoundedCornerShape(15f),
            modifier = Modifier
                .height(Spacing_50)
                .padding(end = Spacing_5)
        ) {
            Text(
                text = stringResource(id = R.string.add), fontSize = Text_12,
                color = Black, fontWeight = FontWeight.Bold
            )
        }
    }
}