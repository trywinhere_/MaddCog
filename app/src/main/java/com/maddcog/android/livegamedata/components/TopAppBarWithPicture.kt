package com.maddcog.android.livegamedata.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.layoutId
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.ConstraintSet
import com.maddcog.android.R
import com.maddcog.android.baseui.ui.theme.*

@Composable
fun TopAppBarWithPicture(
    modifier: Modifier = Modifier,
    textTitle: String,
    onClickedEndMatch: () -> Unit
) {
    TopAppBar(
        backgroundColor = LiveGameDataColor,
        elevation = 0.dp,
        modifier = modifier,
    ) {
        val constraints = ConstraintSet {
            val navIcon = createRefFor("nav_icon")
            val text = createRefFor("title")
            val button = createRefFor("button")

            constrain(navIcon) {
                top.linkTo(parent.top)
                start.linkTo(parent.start)
                bottom.linkTo(parent.bottom)
            }
            constrain(text) {
                top.linkTo(parent.top)
                bottom.linkTo(parent.bottom)
                end.linkTo(parent.end)
                start.linkTo(parent.start)
            }
            constrain(button) {
                top.linkTo(parent.top)
                bottom.linkTo(parent.bottom)
                end.linkTo(parent.end)
            }
        }
        ConstraintLayout(
            constraints,
            modifier = Modifier.fillMaxWidth()
        ) {
            Text(
                text = textTitle,
                style = WhiteText18Bold,
                modifier = Modifier.layoutId("title")
            )
            Image(
                painterResource(id = R.drawable.ic_people),
                contentDescription = persons,
                modifier = Modifier
                    .layoutId("nav_icon")
                    .padding(start = Spacing_10)
            )

            Button(
                onClick = onClickedEndMatch,
                colors = ButtonDefaults.buttonColors(
                    backgroundColor = Red,
                    contentColor = White
                ),
                modifier = Modifier
                    .layoutId("button")
                    .padding(end = Spacing_10)
            ) {
                Text(
                    text = stringResource(id = R.string.end_match),
                    style = WhiteText12,
                )
            }
        }
    }
}