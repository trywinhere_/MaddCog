package com.maddcog.android.livegamedata.components

import androidx.compose.animation.core.LinearEasing
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.animation.core.tween
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.layout.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.StrokeCap
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.unit.Dp
import com.maddcog.android.baseui.ui.theme.*

@Composable
fun CircleForPreparedWithName(
    value: Double,
    size: Dp,
    backgroundCircleColor: Color
) {

    val DATA_MAX = 99.0
    val thickness: Dp = Spacing_10

    val sweepAngles by animateFloatAsState(
        targetValue = (value / DATA_MAX).toFloat(),
        animationSpec = tween(durationMillis = 1000, easing = LinearEasing)
    )

    Canvas(
        modifier = Modifier.size(size)
    ) {

        val arcRadius = size.toPx()

        drawCircle(
            color = backgroundCircleColor,
            alpha = 0.2F,
            radius = arcRadius / 2,
            style = Stroke(width = thickness.toPx(), cap = StrokeCap.Butt)
        )

        drawArc(
            color = backgroundCircleColor,
            startAngle = -90f,
            sweepAngle = 360f * sweepAngles,
            useCenter = false,
            style = Stroke(width = thickness.toPx(), cap = StrokeCap.Round),
            size = Size(arcRadius, arcRadius),
            topLeft = Offset(
                x = (size.toPx() - arcRadius) / 2,
                y = (size.toPx() - arcRadius) / 2
            )
        )
    }
}