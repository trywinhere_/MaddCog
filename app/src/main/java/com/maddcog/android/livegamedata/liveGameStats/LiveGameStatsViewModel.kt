package com.maddcog.android.livegamedata.liveGameStats

import androidx.lifecycle.viewModelScope
import com.maddcog.android.baseui.presentation.BaseViewModel
import com.maddcog.android.data.bluetoothle.ConnectionState
import com.maddcog.android.data.bluetoothle.IBleReceiveManager
import com.maddcog.android.data.network.dto.rawActivity.RawMentalPerformance
import com.maddcog.android.data.sensordatarecorder.ISensorDataRecorder
import com.maddcog.android.data.sensordatarecorder.IStorage
import com.maddcog.android.data.sensordatarecorder.SensorDataQuality
import com.maddcog.android.domain.api.ISharedPreferencesStorage
import com.maddcog.android.domain.entities.MentalType
import com.maddcog.android.domain.entities.activity.UserActivity
import com.maddcog.android.livegamedata.ILiveGameDataRouter
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

private const val LO = "LO"

@HiltViewModel
class LiveGameStatsViewModel @Inject constructor(
    private val router: ILiveGameDataRouter,
    private val bleReceiveManager: IBleReceiveManager,
    private val rawDataStorage: IStorage,
    private val storage: ISharedPreferencesStorage,
    private val sensorDataRecorder: ISensorDataRecorder,
) : BaseViewModel() {

    val dataAlerts: StateFlow<ConnectionState> = bleReceiveManager.dataAlerts

    val batteryLevel: StateFlow<Int> = rawDataStorage.batteryLevel
    val mentalPerformanceList: StateFlow<Int> = rawDataStorage.mentalPerformanceQuantity
    var lastMentalPerformance: StateFlow<RawMentalPerformance> = rawDataStorage.lastMentalPerformance

    val dataQuality: StateFlow<SensorDataQuality> = sensorDataRecorder.dataQuality
    val signalLevel: StateFlow<Int> = sensorDataRecorder.signalLevel
    val countGoodEEG: StateFlow<Int> = sensorDataRecorder.goodEEGcount
    val countBadEEG: StateFlow<Int> = sensorDataRecorder.badEEGcount

    val bluetoothState = MutableStateFlow(false)

    fun getGameName(): String? {
        return storage.getNewSessionInfo()?.name
    }

    fun getDisplayedFlowBle(flow: Double): String {
        if (flow < 40.3) return LO
        else if (flow in 40.3..50.9) return MentalType.MID.toString()
        else return MentalType.HI.toString()
    }

    fun getDisplayedFatigueBle(fatigue: Double): String {
        if (fatigue < 33) return LO
        else if (fatigue in 33.0..66.0) return MentalType.MID.toString()
        else return MentalType.HI.toString()
    }

    fun getDisplayedFlow(activity: UserActivity): String {
        val flow = activity.activitySummary.flowAvg
        if (flow < 40.3) return LO
        else if (flow in 40.3..50.9) return MentalType.MID.toString()
        else return MentalType.HI.toString()
    }

    fun getDisplayedFatigue(activity: UserActivity): String {
        val fatigue = activity.activitySummary.fatigueAvg
        if (fatigue < 33) return LO
        else if (fatigue in 33.0..66.0) return MentalType.MID.toString()
        else return MentalType.HI.toString()
    }

    fun reconnect() {
        bleReceiveManager.reconnect()
    }

    fun navigateToGameStats() {
        storage.getNewSessionInfo()?.name?.let { navigateTo(router.navigateToGameStatsAdding(it)) }
    }

    fun navigateToDashboard() {
        viewModelScope.launch(Dispatchers.IO) {
            bleReceiveManager.disconnect()
            rawDataStorage.clear()
            withContext(Dispatchers.Main) {
                navigateTo(router.navigateToDashboards())
            }
        }
    }
}