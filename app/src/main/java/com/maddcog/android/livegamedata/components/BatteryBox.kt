package com.maddcog.android.livegamedata.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import com.maddcog.android.R
import com.maddcog.android.baseui.ui.theme.*
import com.maddcog.android.data.bluetoothle.ConnectionState
import com.maddcog.android.data.network.dto.rawActivity.RawMentalPerformance
import com.maddcog.android.data.sensordatarecorder.SensorDataQuality

@Composable
fun BatteryBox(
    lastMentalStats: RawMentalPerformance,
    bluetoothState: Boolean,
    mentalSize: Int,
    goodEEG: Int,
    badEEG: Int,
    sensorAlerts: ConnectionState,
    signalQuality: SensorDataQuality,
    isHrError: Boolean,
    signal: Int,
    batteryLevel: Int,
    isEegError: Boolean,
    doReconnect: () -> Unit,
) {

        val picture = when (batteryLevel) {
            in 0..10 -> painterResource(id = R.drawable.ic_battery_0_bar)
            in 10..15 -> painterResource(id = R.drawable.ic_battery_1_bar)
            in 15..40 -> painterResource(id = R.drawable.ic_battery_2_bar)
            in 40..50 -> painterResource(id = R.drawable.ic_battery_3_bar)
            in 50..60 -> painterResource(id = R.drawable.ic_battery_4_bar)
            in 60..75 -> painterResource(id = R.drawable.ic_battery_5_bar)
            in 75..90 -> painterResource(id = R.drawable.ic_battery_6_bar)
            else -> painterResource(id = R.drawable.ic_battery_full)
    }

    Column(modifier = Modifier.fillMaxWidth(), verticalArrangement = Arrangement.Center, horizontalAlignment = Alignment.CenterHorizontally) {
        if (sensorAlerts == ConnectionState.Disconnected || sensorAlerts == ConnectionState.CannotFoundDevice || bluetoothState) {
            Text(
                text = stringResource(id = R.string.device_error_message),
                fontWeight = FontWeight.Bold,
                color = White,
                fontSize = Text_16,
                modifier = Modifier.fillMaxWidth(),
                textAlign = TextAlign.Center
            )
            Text(
                text = stringResource(id = R.string.device_error_title),
                style = if (isHrError || isEegError) RedText16Bold else WhiteText16Bold,
                modifier = Modifier.fillMaxWidth(),
                textAlign = TextAlign.Center
            )
            Button(
                colors = ButtonDefaults.buttonColors(backgroundColor = Color.White),
                onClick = { doReconnect() }
            ) {
                Text(text = "Reconnect", style = BlackText12Bold) // FIXME: fix hardcode
            }
        } else {
            Text(
                text = stringResource(id = signalQuality.toStatusMessage()),
                fontWeight = FontWeight.Bold,
                color = White,
                fontSize = Text_16,
                modifier = Modifier.fillMaxWidth(),
                textAlign = TextAlign.Center
            )
            Text(
                text = stringResource(id = signalQuality.toStatusTitle()),
                style = if (isHrError || isEegError) RedText16Bold else WhiteText16Bold,
                modifier = Modifier.fillMaxWidth(),
                textAlign = TextAlign.Center
            )
        }
//        if (true) {
//            Text(
//                text = "Signal: $signal",
//                style = WhiteText16Bold,
//                modifier = Modifier.fillMaxWidth(),
//                textAlign = TextAlign.Center
//            )
//            Text(
//                text = "EEG count good/bad: $goodEEG/$badEEG",
//                style = WhiteText16Bold,
//                modifier = Modifier.fillMaxWidth(),
//                textAlign = TextAlign.Center
//            )
//            Text(
//                text = "Mental list size: $mentalSize",
//                style = WhiteText16Bold,
//                modifier = Modifier.fillMaxWidth(),
//                textAlign = TextAlign.Center
//            )
//            Text(
//                text = "FG:${lastMentalStats.fatigue} - FL: ${lastMentalStats.flow} - FC: ${lastMentalStats.focus} ",
//                style = WhiteText16Bold,
//                modifier = Modifier.fillMaxWidth(),
//                textAlign = TextAlign.Center
//            )
//        }

        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.Center,
            verticalAlignment = Alignment.CenterVertically,
        ) {
            Image(painter = picture, contentDescription = "", modifier = Modifier.rotate(90f))
            Text(text = "$batteryLevel%", color = White, fontSize = Text_12)
        }
    }
}

fun SensorDataQuality.toStatusTitle(): Int {
    return when (this) {
        SensorDataQuality.HrError -> R.string.hr_data_issue_title
        SensorDataQuality.EegError -> R.string.eeg_data_issue_title
        SensorDataQuality.DeviceError -> R.string.device_error_title
        SensorDataQuality.Good -> R.string.game_band_connected
        else -> R.string.empty_string
    }
}

fun SensorDataQuality.toStatusMessage(): Int {
    return when (this) {
        SensorDataQuality.HrError -> R.string.hr_data_issue_message
        SensorDataQuality.EegError -> R.string.eeg_data_issue_message
        SensorDataQuality.DeviceError -> R.string.device_error_message
        else -> R.string.empty_string
    }
}