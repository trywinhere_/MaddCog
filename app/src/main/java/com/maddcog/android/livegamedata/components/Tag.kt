package com.maddcog.android.livegamedata.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import com.maddcog.android.R
import com.maddcog.android.baseui.ui.theme.*

@Composable
fun Tag(name: String, onAddTag: () -> Unit ) {

    val stateTagChanged = remember { mutableStateOf(false) }
    val backgroundColor = if (stateTagChanged.value) White else BoxDarkGrey
    val txtColor = if (stateTagChanged.value) Black else HintLightGrey
    val imageVisibility = !stateTagChanged.value

    Card(
        shape = RoundedCornerShape(15),
        backgroundColor = backgroundColor,
        modifier = Modifier
            .padding(start = Spacing_5)
            .height(Spacing_40)
            .width(Spacing_70)
            .clickable {
                stateTagChanged.value = !stateTagChanged.value
            }
    ) {
        Row(horizontalArrangement = Arrangement.Center) {

            if (imageVisibility) {
                Image(
                    painter = painterResource(id = R.drawable.ic_baseline_add_24),
                    contentDescription = stringResource(
                        id = R.string.add
                    ),
                    modifier = Modifier.align(Alignment.CenterVertically)
                )
            }

            Spacer(modifier = Modifier.width(Spacing_5))

            Text(
                text = name,
                fontSize = Text_12,
                fontWeight = FontWeight.Bold,
                color = txtColor,
                modifier = Modifier.align(Alignment.CenterVertically)
            )
        }
    }
}