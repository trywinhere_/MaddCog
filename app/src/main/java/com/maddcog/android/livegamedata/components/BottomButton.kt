package com.maddcog.android.livegamedata.components

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import com.maddcog.android.baseui.ui.theme.*

@Composable
fun BottomButton(onButtonClicked: () -> Unit, titleText: String, getEvent: () -> Unit, saveTags: () -> Unit) {

    Column() {
        Button(
            onClick = { onButtonClicked() }, shape = RoundedCornerShape(20),
            colors = ButtonDefaults.buttonColors(
                backgroundColor = White,
                contentColor = Black
            ),
            modifier = Modifier
                .fillMaxWidth()
                .height(Spacing_50)
                .padding(horizontal = Spacing_15)
                .clickable { }
        ) {

            Text(
                text = titleText,
                color = Black,
                textAlign = TextAlign.Center,
                modifier = Modifier.fillMaxWidth(),
                fontSize = Text_16,
                fontWeight = FontWeight.Bold
            )
        }
    }
}