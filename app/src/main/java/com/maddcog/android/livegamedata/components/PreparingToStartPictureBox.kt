package com.maddcog.android.livegamedata.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import com.maddcog.android.R
import com.maddcog.android.baseui.ui.theme.*

@Composable
fun PreparingToStartPictureBox(
    icon: Painter,
    textToDo: String,
    onButtonClicked: () -> Unit,
    textForButton: String
) {

    Column(modifier = Modifier.fillMaxSize(), horizontalAlignment = Alignment.CenterHorizontally) {
        Row(modifier = Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.Center) {
            Image(
                painter = icon, contentDescription = stringResource(id = R.string.preparing_to_start),
                modifier = Modifier.size(Spacing_120)
            )
        }
        Spacer(modifier = Modifier.height(Spacing_21))
        Text(
            text = textToDo,
            fontSize = Text_14,
            color = HintLightGrey,
            modifier = Modifier.fillMaxWidth(),
            textAlign = TextAlign.Center
        )
        Spacer(modifier = Modifier.height(Spacing_10))
        Button(
            onClick = onButtonClicked,
            modifier = Modifier
                .width(Spacing_180)
                .height(Spacing_50),
            colors = ButtonDefaults.buttonColors(
                backgroundColor = White,
                contentColor = Black
            ),
            shape = RoundedCornerShape(20)
        ) {
            Text(
                text = textForButton,
                fontSize = Text_14,
                color = Black,
                modifier = Modifier.fillMaxWidth(),
                textAlign = TextAlign.Center
            )
        }
    }
}