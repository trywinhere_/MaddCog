package com.maddcog.android.livegamedata.startnewsession

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import com.maddcog.android.baseui.model.StatefulData
import com.maddcog.android.baseui.presentation.BaseViewModel
import com.maddcog.android.domain.api.ILiveGameDataRepository
import com.maddcog.android.domain.api.ISharedPreferencesStorage
import com.maddcog.android.domain.entities.ActivityItem
import com.maddcog.android.domain.entities.UserGame
import com.maddcog.android.domain.entities.activity.UserActivity
import com.maddcog.android.domain.usecase.GetGamesListUseCase
import com.maddcog.android.domain.usecase.GetUserGamesUseCase
import com.maddcog.android.livegamedata.ILiveGameDataRouter
import com.maddcog.android.settings.presentation.gameIntegration.presentation.pickGame.PickGameKey
import com.maddcog.android.settings.presentation.gameIntegration.states.ActivityItemsState
import com.maddcog.android.settings.presentation.gameIntegration.states.ConnectedGamesState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

@HiltViewModel
class StartNewSessionViewModel @Inject constructor(
    private val router: ILiveGameDataRouter,
    private val repository: ILiveGameDataRepository,
    private val useCase: GetUserGamesUseCase,
    private val useCaseItem: GetGamesListUseCase,
    private val storage: ISharedPreferencesStorage
) : BaseViewModel() {

    private val _state = mutableStateOf(ConnectedGamesState())
    val state: State<ConnectedGamesState> = _state

    private val _stateActivityItem = mutableStateOf(ActivityItemsState())
    val stateActivityItem: State<ActivityItemsState> = _stateActivityItem

    private val _sendFake = statefulSharedFlow<Unit>()
    val sendFake: Flow<StatefulData<Unit>>
        get() = _sendFake

    var tags = mutableListOf<String>()

    fun navigateToPreparingToStart() {
        navigateTo(router.navigateToPreparingToStart(getNewGameSession()?.name))
    }

    fun fakeSensor() {
        storage.saveDeviceStatus(true)
    }

    fun addActivity(activity: UserActivity): Boolean {
        return repository.addActivity(activity)
    }

    fun addTags(tags: List<String>, activity: UserActivity): List<String> {
        return repository.addTags(tags, activity)
    }

    fun addGameName(activityTypeName: String, activity: UserActivity): String {
        return repository.addGame(activityTypeName, activity)
    }

    fun navigateToPickGame() {
        navigateTo(router.navigateToPickGameDeepLink(PickGameKey.StartNewGame))
    }

    fun getNewGameSession(): ActivityItem? {
        return storage.getNewSessionInfo()
    }

    fun saveTag(tag: String) {
        tags.add(tag)
    }

    fun getTagList(): MutableList<String> {
        return tags
    }

    fun saveTagsToStorage() {
        repository.addTagsToStorage(tags.toSet())
    }

    fun getGameName(): String? {
        return storage.getGameName()
    }

    fun sendFakeData() {
        _sendFake.fetch {
            repository.sendFakeRawActivity()
        }
    }

    /**
     * Not used, but alive in tests
     */
    fun getImageUrl(activityItemsList: List<ActivityItem>, gameName: String): String {
        if (activityItemsList.isEmpty() || gameName == "") {
            return ""
        } else {
            val itemNeeded =
                activityItemsList.find { activityItem -> activityItem.name == gameName }
            if (itemNeeded != null) return itemNeeded.imageSmall
            else return ""
        }
    }

    fun getGameName(gamesList: MutableList<UserGame>): String {
        val result = storage.getGameName()
        return when {
            result == null && gamesList.isEmpty() -> ""
            result == null && gamesList.isNotEmpty() -> gamesList.first().gameName
            else -> result.toString()
        }
    }
}