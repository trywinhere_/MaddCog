package com.maddcog.android.livegamedata.aimgame

import android.net.Uri
import androidx.compose.foundation.layout.*
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import com.maddcog.android.R
import com.maddcog.android.baseui.components.BasicTopAppBar
import com.maddcog.android.baseui.ui.theme.*
import com.maddcog.android.livegamedata.components.*

@Composable
fun WarmUpScreen(navigateBack: () -> Unit) {

    Surface(color = Black, modifier = Modifier.fillMaxSize()) {

        val url = Uri.parse(MADDCOG_WARM_UP_VIDEO_URL)

        Column(modifier = Modifier.fillMaxSize()) {

            BasicTopAppBar(title = R.string.warm_up_stretches) {
                navigateBack()
            }

            Column(Modifier.fillMaxSize(), verticalArrangement = Arrangement.SpaceAround) {
                Spacer(modifier = Modifier.height(Spacing_30))
                VideoWarmUp(url)
                Spacer(modifier = Modifier.height(Spacing_30))
                BottomButton(
                    onButtonClicked = { navigateBack() },
                    titleText = stringResource(id = R.string.end_warm_up),
                    {},
                    {}
                )
            }
        }
    }
}