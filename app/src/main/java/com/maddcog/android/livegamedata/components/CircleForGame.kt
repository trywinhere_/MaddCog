package com.maddcog.android.livegamedata.components

import androidx.compose.foundation.Canvas
import androidx.compose.foundation.layout.size
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.StrokeCap
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.unit.Dp
import com.maddcog.android.baseui.ui.theme.*

@Composable
fun CircleForGame(value: Double, circleSize: Dp, circleColor: Color) {

    val thickness: Dp = Spacing_7

    val sweepAngles = value * Number_for_angle / Number_for_div_for_angle

    Canvas(
        modifier = Modifier
            .size(circleSize)
    ) {

        val arcRadius = circleSize.toPx()

        drawCircle(
            color = circleColor,
            alpha = 0.2F,
            radius = arcRadius / 2,
            style = Stroke(width = thickness.toPx(), cap = StrokeCap.Butt)
        )

        drawArc(
            color = GameGrade,
            startAngle = -90f,
            sweepAngle = sweepAngles.toFloat(),
            useCenter = false,
            style = Stroke(width = thickness.toPx(), cap = StrokeCap.Round),
            size = Size(arcRadius, arcRadius),
            topLeft = Offset(
                x = (circleSize.toPx() - arcRadius) / 2,
                y = (circleSize.toPx() - arcRadius) / 2
            )
        )
    }
}