package com.maddcog.android.livegamedata.aimgame

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import com.maddcog.android.R
import com.maddcog.android.baseui.components.BasicTopAppBar
import com.maddcog.android.baseui.ui.theme.*
import com.maddcog.android.domain.entities.AimGame
import com.maddcog.android.livegamedata.components.BottomButton
import com.maddcog.android.livegamedata.components.PersonalBestText

@Composable
fun AimGameScreen(
    openSite: () -> Unit,
    navigateBack: () -> Unit,
    aimGameScore: AimGame
) {
    Surface(color = Black, modifier = Modifier.fillMaxSize()) {

        Column(modifier = Modifier.fillMaxSize()) {

            LazyColumn(
                modifier = Modifier
                    .fillMaxSize()
                    .weight(1f)
            ) {

                item {

                    BasicTopAppBar(title = R.string.aim_game) {
                        navigateBack()
                    }

                    Spacer(modifier = Modifier.height(Spacing_20))

                    Row(
                        horizontalArrangement = Arrangement.Center,
                        modifier = Modifier.fillMaxWidth()
                    ) {
                        Image(
                            painter = painterResource(id = R.drawable.ic_monitor),
                            contentDescription = stringResource(
                                id = R.string.aim_game
                            ),
                            modifier = Modifier.size(Spacing_150)
                        )
                    }

                    Spacer(modifier = Modifier.height(Spacing_20))

                    Text(
                        text = stringResource(id = R.string.aim_game_text),
                        color = White,
                        fontWeight = FontWeight.Bold,
                        fontSize = Text_16,
                        textAlign = TextAlign.Center,
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(horizontal = Spacing_20)
                    )

                    Spacer(modifier = Modifier.height(Spacing_50))

                    Text(
                        text = stringResource(id = R.string.log_in_on),
                        color = White,
                        fontWeight = FontWeight.Bold,
                        fontSize = Text_16,
                        textAlign = TextAlign.Center,
                        modifier = Modifier.fillMaxWidth()
                    )

                    Spacer(modifier = Modifier.height(Spacing_30))

                    Text(
                        text = stringResource(id = R.string.maddcog_site),
                        color = White,
                        fontWeight = FontWeight.Bold,
                        fontSize = Text_18,
                        textAlign = TextAlign.Center,
                        modifier = Modifier
                            .fillMaxWidth()
                            .clickable {
                                openSite()
                            }
                    )
                }
            }

            Text(
                text = stringResource(id = R.string.your_result_show_info),
                color = subHeadGrayText,
                fontSize = Text_14,
                textAlign = TextAlign.Center,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = Spacing_20)
            )

            Spacer(modifier = Modifier.height(Spacing_2))

            Text(
                text = stringResource(id = R.string.personal_best),
                color = White,
                fontWeight = FontWeight.Bold,
                fontSize = Text_16,
                modifier = Modifier.padding(start = Spacing_15)
            )

            Spacer(modifier = Modifier.height(Spacing_5))

            Row(
                horizontalArrangement = Arrangement.SpaceAround,
                modifier = Modifier.fillMaxWidth()
            ) {
                PersonalBestText(
                    topText = aimGameScore.accuracy.toString() + " %",
                    bottomText = stringResource(id = R.string.accuracy)
                )
                PersonalBestText(
                    topText = aimGameScore.targets.toString(),
                    bottomText = stringResource(id = R.string.target_sec)
                )
                PersonalBestText(
                    topText = (aimGameScore.duration / 1000).toInt().toString(),
                    bottomText = stringResource(id = R.string.seconds)
                )
            }

            Spacer(modifier = Modifier.height(Spacing_10))

            BottomButton(
                onButtonClicked = { navigateBack() },
                titleText = stringResource(id = R.string.end_warm_up),
                {},
                {}
            )

            Spacer(modifier = Modifier.height(Spacing_5))
        }
    }
}
