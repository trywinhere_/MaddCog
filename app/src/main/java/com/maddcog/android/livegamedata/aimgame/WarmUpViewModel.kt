package com.maddcog.android.livegamedata.aimgame

import com.maddcog.android.baseui.presentation.BaseViewModel
import com.maddcog.android.livegamedata.ILiveGameDataRouter
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class WarmUpViewModel @Inject constructor(
    private val router: ILiveGameDataRouter
) : BaseViewModel() {}