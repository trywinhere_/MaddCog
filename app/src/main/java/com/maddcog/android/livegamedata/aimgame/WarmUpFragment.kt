package com.maddcog.android.livegamedata.aimgame

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.ui.platform.ComposeView
import androidx.fragment.app.viewModels
import com.maddcog.android.baseui.presentation.BaseFragment
import com.maddcog.android.baseui.presentation.BaseViewModel
import com.maddcog.android.baseui.ui.theme.MaddcogTheme
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class WarmUpFragment : BaseFragment() {

    private val viewModel: WarmUpViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        val view = ComposeView(requireContext())

        view.setContent {
            MaddcogTheme {
                WarmUpScreen(navigateBack = { viewModel.navigateBack() })
            }
        }

        return view
    }

    override fun withViewModel(): BaseViewModel {
        return viewModel
    }
}