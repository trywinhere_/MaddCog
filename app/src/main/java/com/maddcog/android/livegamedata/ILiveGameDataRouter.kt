package com.maddcog.android.livegamedata

import com.maddcog.android.baseui.model.NavCommand
import com.maddcog.android.settings.presentation.gameIntegration.presentation.pickGame.PickGameKey

interface ILiveGameDataRouter {

    fun openMaddcogWebsite(): NavCommand

    fun navigateToPreparingToStart(gameName: String?): NavCommand

    fun navigateToLiveGameStats(): NavCommand

    fun navigateToWarmUp(): NavCommand

    fun navigateToPickGameDeepLink(key: PickGameKey): NavCommand

    fun navigateToConnectedGameDeepLink(): NavCommand

    fun navigateToLiveGameSummary(gameName: String): NavCommand

    fun navigateToGameStatsAdding(gameName: String): NavCommand

    fun navigateToDashboards(): NavCommand
}