package com.maddcog.android.livegamedata.preparingtostart

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import com.maddcog.android.R
import com.maddcog.android.baseui.presentation.NativeAlertDialog
import com.maddcog.android.baseui.ui.theme.*
import com.maddcog.android.data.bluetoothle.ConnectionState
import com.maddcog.android.data.network.dto.rawActivity.RawMentalPerformance
import com.maddcog.android.data.sensordatarecorder.SensorDataQuality
import com.maddcog.android.livegamedata.components.BatteryBox
import com.maddcog.android.livegamedata.components.CircleForPreparing
import com.maddcog.android.livegamedata.components.PreparingToStartPictureBox
import com.maddcog.android.livegamedata.components.TopAppBarWithPicture
import com.maddcog.android.settings.components.ScanningProgressGameBand

@Composable
fun PreparingToStartScreen(
    onClickEndMatch: () -> Unit,
    navigateToWarmUp: () -> Unit,
    viewModel: PreparingToStartViewModel,
    doReconnect: () -> Unit,
    notEnoughDataWarning: () -> Unit,
) {
    val FIVE_MIN_DATA = 300.0
    val FIVE_MIN = 300000L

    val mentalSize = viewModel.mentalPerformanceList.collectAsState(-1)
    val battery = viewModel.batteryLevel.collectAsState(0)
    val lastMentalStats = viewModel.lastMentalPerformance.collectAsState(RawMentalPerformance(0.0, 0.0, 0.0, -1))

    val badEEG = viewModel.countBadEEG.collectAsState(-1)
    val goodEEG = viewModel.countGoodEEG.collectAsState(-1)
    val signal = viewModel.signalLevel.collectAsState(-1)

    val connectionState = viewModel.dataAlerts.collectAsState(ConnectionState.NotDefined)
    val dataQuality = viewModel.dataQuality.collectAsState(SensorDataQuality.NotDefined)

    val bluetoothState by viewModel.bluetoothState.collectAsState(initial = false)

    val connectingText = when (connectionState.value) {
        ConnectionState.Connected -> stringResource(id = R.string.establishing_values)
        else -> stringResource(id = R.string.looking_for_sensors)
    }

    val reconnectStateIsShown = viewModel.reconnectLoaderIsShown.collectAsState()

    var openDialog = remember { mutableStateOf(false) }

    Column() {
        TopAppBarWithPicture(textTitle = stringResource(id = R.string.preparing_to_start)) {
            if (mentalSize.value < FIVE_MIN_DATA) {
                openDialog.value = true
            } else {
                onClickEndMatch()
            }
        }
        Box(Modifier.fillMaxWidth()) {
            LazyColumn(
                modifier = Modifier
                    .background(LiveGameDataColor)
                    .fillMaxSize()
            ) {
                item {
                    Spacer(modifier = Modifier.height(Spacing_20))
                    Row(
                        modifier = Modifier.fillMaxWidth(),
                        horizontalArrangement = Arrangement.Center
                    ) {
                        Box() {
                            CircleForPreparing(
                                time = FIVE_MIN,
                                dataSize = mentalSize.value,
                                dataNeeded = FIVE_MIN_DATA,
                                navigateToLiveGameStats = { viewModel.navigateToLiveGameStats() },
                                notEnoughDataWarning = { notEnoughDataWarning() }
                            )
                            Text(
                                text = connectingText,
                                fontSize = Text_16,
                                color = White,
                                modifier = Modifier.align(
                                    Alignment.Center
                                )
                            )
                        }
                    }
                    Spacer(modifier = Modifier.height(Spacing_30))

                    Row(
                        modifier = Modifier
                            .padding(horizontal = Spacing_20)
                            .fillMaxWidth(),
                        horizontalArrangement = Arrangement.Center,
                    ) {
                        Column(
                            modifier = Modifier.weight(1f)
                        ) {
                            PreparingToStartPictureBox(
                                icon = painterResource(id = R.drawable.ic_hand),
                                textToDo = stringResource(id = R.string.get_moving),
                                onButtonClicked = { navigateToWarmUp() },
                                textForButton = stringResource(id = R.string.get_moving_button)
                            )
                        }
                    }
                    Spacer(modifier = Modifier.height(Spacing_80))
                    BatteryBox(
                        lastMentalStats = lastMentalStats.value,
                        bluetoothState = bluetoothState,
                        mentalSize = mentalSize.value,
                        goodEEG = goodEEG.value,
                        badEEG = badEEG.value,
                        sensorAlerts = connectionState.value,
                        signalQuality = dataQuality.value,
                        batteryLevel = battery.value,
                        signal = signal.value,
                        isEegError = dataQuality.value == SensorDataQuality.EegError,
                        isHrError = dataQuality.value == SensorDataQuality.HrError,
                        doReconnect = { doReconnect() }
                    )
                    if (openDialog.value) {
                        NativeAlertDialog(
                            onExit = {
                                openDialog.value = false
                            },
                            onSuccess = {
                                viewModel.navigateToDashboard()
                            },
                            disclaimerFirst = R.string.not_enough_data_dialog_title,
                            disclaimerSecond = R.string.not_enough_data_dialog_message,
                            successName = R.string.not_enough_data_dialog_finish_button
                        )
                    }
                    if (reconnectStateIsShown.value) {
                        ScanningProgressGameBand(
                            stringResource(
                                id =R.string.reconnecting_message
                            )
                        )
                    }
                }
            }
        }
    }
}