package com.maddcog.android.livegamedata.components

import androidx.compose.foundation.layout.*
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import com.maddcog.android.baseui.ui.theme.*

@Composable
fun PersonalBestText(topText: String, bottomText: String) {
    Column(verticalArrangement = Arrangement.Center) {

        Text(
            text = topText,
            fontSize = Text_16,
            color = White,
            fontWeight = FontWeight.Bold,
            modifier = Modifier.align(
                Alignment.CenterHorizontally
            )
        )

        Spacer(modifier = Modifier.height(Spacing_2))

        Text(
            text = bottomText,
            fontSize = Text_14,
            color = subHeadGrayText
        )
    }
}