package com.maddcog.android.livegamedata.aimgame

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.maddcog.android.baseui.presentation.BaseViewModel
import com.maddcog.android.data.model.StateViewModel
import com.maddcog.android.domain.api.IAimGameRepository
import com.maddcog.android.livegamedata.ILiveGameDataRouter
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AimGameViewModel @Inject constructor(
    private val router: ILiveGameDataRouter,
    private val aimGameRepository: IAimGameRepository
) : BaseViewModel() {

    private val _dataState = MutableLiveData<StateViewModel>()
    val dataState: LiveData<StateViewModel>
        get() = _dataState

    val aimGameScore = aimGameRepository.aimGameScore

    fun openMaddcogWebsite() {
        navigateTo(router.openMaddcogWebsite())
    }

    fun getAimGameScore() {
        try {
            viewModelScope.launch {
                aimGameRepository.getAimGameScore()
            }
        } catch (e: Exception) {
            _dataState.value = StateViewModel(error = true)
        }
    }
}