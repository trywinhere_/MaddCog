package com.maddcog.android.livegamedata.aimgame

import androidx.lifecycle.MutableLiveData
import com.maddcog.android.data.network.MaddcogApi
import com.maddcog.android.data.network.errors.ApiException
import com.maddcog.android.data.network.errors.NetWorkException
import com.maddcog.android.data.network.errors.UnknownException
import com.maddcog.android.domain.api.IAimGameRepository
import com.maddcog.android.domain.entities.AimGame
import java.io.IOException
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AimGameRepositoryImpl @Inject constructor(
    private val apiService: MaddcogApi
) : IAimGameRepository {

    override val aimGameScore = MutableLiveData<AimGame>()

    override suspend fun getAimGameScore() {
        try {
            val response = apiService.getAimGameScores()
            aimGameScore.value = response.body() ?: throw ApiException(response.code(), response.message())
            if (!response.isSuccessful) {
                throw ApiException(response.code(), response.message())
            }
        } catch (e: IOException) {
            throw NetWorkException
        } catch (e: Exception) {
            throw UnknownException
        }
    }
}