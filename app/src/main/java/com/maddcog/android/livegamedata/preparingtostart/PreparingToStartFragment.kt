package com.maddcog.android.livegamedata.preparingtostart

import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.ui.platform.ComposeView
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.maddcog.android.baseui.presentation.BaseFragment
import com.maddcog.android.baseui.presentation.BaseViewModel
import com.maddcog.android.baseui.ui.theme.MaddcogTheme
import com.maddcog.android.data.bluetoothle.BluetoothStateReceiver
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class PreparingToStartFragment : BaseFragment() {

    private val viewModel: PreparingToStartViewModel by viewModels()
    private val args by navArgs<PreparingToStartFragmentArgs>()

    @Inject
    lateinit var bluetoothAdapter: BluetoothAdapter
    private lateinit var bluetoothStateReceiver: BluetoothStateReceiver
    private var isBluetoothDialogAlreadyShown = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.initReconnection()
        bluetoothStateReceiver = BluetoothStateReceiver(viewModel.bluetoothState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = ComposeView(requireContext())
        view.setContent {
            MaddcogTheme {
                PreparingToStartScreen(
                    onClickEndMatch = { viewModel.navigateToGameStats() },
                    navigateToWarmUp = { viewModel.navigateToWarmUp() },
                    viewModel = viewModel,
                    doReconnect = { showBluetoothDialog() },
                    notEnoughDataWarning = { showToastMessage("Not enough data gathered to make a summary") } // FIXME: temporary
                )
            }
        }

        return view
    }

    override fun onResume() {
        super.onResume()
        requireContext().registerReceiver(
            bluetoothStateReceiver,
            IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED)
        )
        activity?.window?.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
    }

    override fun withViewModel(): BaseViewModel = viewModel.apply {
    }

    override fun onPause() {
        super.onPause()
        requireContext().unregisterReceiver(bluetoothStateReceiver)
        activity?.window?.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
    }

    private fun showBluetoothDialog() {
        if (!bluetoothAdapter.isEnabled) {
            viewModel.setShowReconnectLoader(true)
            val enabledBluetoothIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            startBluetoothIntentForResult.launch(enabledBluetoothIntent)
            isBluetoothDialogAlreadyShown = true
        } else {
            viewModel.reconnect()
        }
    }

    private val startBluetoothIntentForResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            isBluetoothDialogAlreadyShown = false
            if (result.resultCode != Activity.RESULT_OK) {
                showBluetoothDialog()
            } else {
                CoroutineScope(Dispatchers.Main).launch {
                    delay(1000)
                    viewModel.reconnect()
                }
            }
        }
}