package com.maddcog.android.livegamedata.components

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import coil.compose.AsyncImage
import coil.request.ImageRequest
import com.maddcog.android.R
import com.maddcog.android.baseui.ui.theme.*

@Composable
fun GameCardPicture(onCardClicked: () -> Unit, url: String, name: String) {

    Card(
        shape = RoundedCornerShape(15),
        backgroundColor = BoxDarkGrey,
        modifier = Modifier
            .padding(horizontal = Spacing_15)
            .fillMaxWidth()
            .height(Spacing_90)
            .clickable {
                onCardClicked()
            }
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = Spacing_20)
        ) {

            Spacer(modifier = Modifier.height(Spacing_5))

            Card(
                shape = RoundedCornerShape(Spacing_15),
                modifier = Modifier
                    .align(Alignment.CenterVertically)
                    .height(Spacing_60)
                    .width(Spacing_60),
                border = BorderStroke(width = Spacing_2, FlowText)
            ) {
                AsyncImage(
                    model = ImageRequest.Builder(LocalContext.current)
                        .data(url)
                        .crossfade(true)
                        .placeholder(R.color.white)
                        .build(),
                    contentDescription = "",
                    contentScale = ContentScale.Crop,
                    modifier = Modifier
                        .fillMaxSize()
                )
            }

            Spacer(modifier = Modifier.width(Spacing_12))

            Text(
                text = name,
                fontSize = Text_14,
                color = White,
                modifier = Modifier.align(Alignment.CenterVertically)
            )

            Spacer(modifier = Modifier.weight(1f))

            Image(
                painter = painterResource(id = R.drawable.ic_arrow_forward),
                contentDescription = stringResource(
                    id = R.string.game_band_text
                ),
                modifier = Modifier.align(Alignment.CenterVertically)
            )
        }
    }
}