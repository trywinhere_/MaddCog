package com.maddcog.android.livegamedata.startnewsession

import android.os.Bundle
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Surface
import androidx.compose.material.TextField
import androidx.compose.material.TextFieldDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.style.TextAlign
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import com.maddcog.android.R
import com.maddcog.android.baseui.components.BasicTopAppBar
import com.maddcog.android.baseui.ui.theme.*
import com.maddcog.android.livegamedata.components.BottomButton
import com.maddcog.android.livegamedata.components.GameCardPicture
import com.maddcog.android.livegamedata.components.SearchBoxWithButton
import com.maddcog.android.livegamedata.components.Tag

@Composable
fun StartNewSessionScreen(
    viewModel: StartNewSessionViewModel,
    navigateBack: () -> Unit,
    navigateToPreparingToStart: () -> Unit,
    navigateToConnectedGame: () -> Unit,
    gameImageUrl: String,
    gameName: String
) {
    val firebaseAnalytics: FirebaseAnalytics = Firebase.analytics
    val state = viewModel.state.value
    val textState = remember { mutableStateOf(TextFieldValue("")) }
    var gameText by remember { mutableStateOf(TextFieldValue("")) }
    val focusManager = LocalFocusManager.current
    val tagsNumbers = viewModel.tags.size.toString()
    val tagsList = remember { mutableStateListOf<String>() }

    Surface(color = Black, modifier = Modifier.fillMaxSize()) {

        Column(modifier = Modifier.fillMaxSize()) {

            LazyColumn(
                modifier = Modifier
                    .fillMaxSize()
                    .weight(1f)
            ) {

                item {
                    BasicTopAppBar(title = R.string.start_new_session) { navigateBack() }

                    Spacer(modifier = Modifier.height(Spacing_20))

                    Text(
                        text = stringResource(id = R.string.pick_game_text), fontSize = Text_16,
                        color = White, fontWeight = FontWeight.Bold,
                        modifier = Modifier
                            .padding(horizontal = Spacing_15)
                    )

                    Spacer(modifier = Modifier.height(Spacing_10))

                    GameCardPicture(navigateToConnectedGame, url = gameImageUrl, name = gameName)

                    Spacer(modifier = Modifier.height(Spacing_12))

                    Text(
                        text = stringResource(id = R.string.preparation_tags), fontSize = Text_16,
                        color = White, fontWeight = FontWeight.Bold,
                        modifier = Modifier
                            .padding(horizontal = Spacing_15)
                    )

                    Spacer(modifier = Modifier.height(Spacing_10))

                    SearchBoxWithButton(
                        state = textState,
                        onAddTag = {
                            if (textState.value.text.isNotBlank()) {
                                tagsList.add(textState.value.text)
                            }
                        }
                    )

                    Spacer(modifier = Modifier.height(Spacing_10))

                    LazyRow(modifier = Modifier.padding(horizontal = Spacing_15)) {
                        item {
                            tagsList.forEach { item ->
                                Tag(name = item, onAddTag = {})
                            }
                        }
                    }
                }

                item {
                    Spacer(modifier = Modifier.height(Spacing_20))

                    Text(
                        text = stringResource(id = R.string.tag_info),
                        fontSize = Text_14,
                        color = HintLightGrey,
                        modifier = Modifier.padding(horizontal = Spacing_15),
                        textAlign = TextAlign.Center
                    )

                    Spacer(modifier = Modifier.height(Spacing_30))

                    Text(
                        text = stringResource(id = R.string.pre_game_comments),
                        fontSize = Text_16,
                        color = White,
                        fontWeight = FontWeight.Bold,
                        modifier = Modifier
                            .padding(horizontal = Spacing_15)
                    )

                    Spacer(modifier = Modifier.height(Spacing_10))

                    TextField(
                        value = gameText,
                        onValueChange = { text -> gameText = text },
                        placeholder = {
                            Text(
                                text = stringResource(id = R.string.optional),
                                color = HintLightGrey,
                                fontSize = Text_18
                            )
                        },
                        modifier = Modifier
                            .padding(horizontal = Spacing_15)
                            .fillMaxWidth()
                            .height(Spacing_90),
                        shape = RoundedCornerShape(15),
                        colors = TextFieldDefaults.textFieldColors(
                            backgroundColor = BoxDarkGrey,
                            textColor = White, placeholderColor = HintLightGrey,
                            cursorColor = White
                        ),
                        keyboardActions = KeyboardActions(onDone = { focusManager.clearFocus() })
                    )

                    Spacer(modifier = Modifier.height(Spacing_20))
                }

            }

            BottomButton(
                titleText = stringResource(id = R.string.changed_button_connect_game_band),
                onButtonClicked = navigateToPreparingToStart,
                getEvent = {
                    firebaseAnalytics.logEvent(
                        "start_playing_number_tags",
                        Bundle().apply {
                            this.putString(
                                START_PLAYING, "number_tags $tagsNumbers"
                            )
                        }
                    )
                    firebaseAnalytics.logEvent(
                        "start_playing_activity_type_name",
                        Bundle().apply {
                            this.putString(
                                START_PLAYING, "activity_type_name $gameName"
                            )
                        }
                    )
                },
                saveTags = { viewModel.saveTagsToStorage() }
            )

            Spacer(modifier = Modifier.height(Spacing_5))
        }

        if (state.isLoading) {
            Row(
                modifier = Modifier.fillMaxSize(),
                horizontalArrangement = Arrangement.Center,
                verticalAlignment = Alignment.CenterVertically
            ) {
                CircularProgressIndicator(
                    modifier = Modifier
                        .height(Spacing_40)
                        .width(Spacing_40)
                        .fillMaxSize(),
                    color = Color.Red
                )
            }
        }
    }
}