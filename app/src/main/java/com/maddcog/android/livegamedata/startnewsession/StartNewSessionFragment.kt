package com.maddcog.android.livegamedata.startnewsession

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.compose.ui.platform.ComposeView
import androidx.fragment.app.viewModels
import com.maddcog.android.baseui.presentation.BaseFragment
import com.maddcog.android.baseui.presentation.BaseViewModel
import com.maddcog.android.baseui.ui.theme.MaddcogTheme
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class StartNewSessionFragment : BaseFragment() {

    private val viewModel: StartNewSessionViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        val view = ComposeView(requireContext())

        view.setContent {
            MaddcogTheme {
                StartNewSessionScreen(
                    viewModel = viewModel,
                    navigateToPreparingToStart = {
                        if (viewModel.getNewGameSession()?.name != null) {
                            viewModel.saveTagsToStorage()
                            viewModel.navigateToPreparingToStart()
                        } else {
                            showToastMessage("Please choose the game!") // FIXME: temporary
                        }
                    },
                    navigateToConnectedGame = { viewModel.navigateToPickGame() },
                    gameImageUrl = viewModel.getNewGameSession()?.imageSmall ?: "",
                    gameName = viewModel.getNewGameSession()?.name ?: "",
                    navigateBack = { viewModel.navigateBack() }
                )
            }
        }

        return view
    }

    override fun withViewModel(): BaseViewModel = viewModel.apply {
        sendFake.collectWithState { state ->
            state.isSuccessful {
                Toast.makeText(requireContext(), "Data send!", Toast.LENGTH_SHORT).show()
            }
            state.isError { error ->
                Toast.makeText(requireContext(), error.message, Toast.LENGTH_SHORT).show()
            }
        }
    }
}