package com.maddcog.android.livegamedata.preparingtostart

import androidx.lifecycle.viewModelScope
import com.maddcog.android.baseui.presentation.BaseViewModel
import com.maddcog.android.data.bluetoothle.ConnectionState
import com.maddcog.android.data.bluetoothle.IBleReceiveManager
import com.maddcog.android.data.network.dto.rawActivity.RawMentalPerformance
import com.maddcog.android.data.sensordatarecorder.ISensorDataRecorder
import com.maddcog.android.data.sensordatarecorder.IStorage
import com.maddcog.android.data.sensordatarecorder.SensorDataQuality
import com.maddcog.android.domain.api.IAimGameRepository
import com.maddcog.android.domain.api.ISharedPreferencesStorage
import com.maddcog.android.livegamedata.ILiveGameDataRouter
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class PreparingToStartViewModel @Inject constructor(
    private val router: ILiveGameDataRouter,
    private val aimGameRepository: IAimGameRepository,
    private val bleReceiveManager: IBleReceiveManager,
    private val rawDataStorage: IStorage,
    private val sensorDataRecorder: ISensorDataRecorder,
    private val storage: ISharedPreferencesStorage,
) : BaseViewModel() {

    val dataAlerts: StateFlow<ConnectionState> = bleReceiveManager.dataAlerts

    val batteryLevel: StateFlow<Int> = rawDataStorage.batteryLevel
    val mentalPerformanceList: StateFlow<Int> = rawDataStorage.mentalPerformanceQuantity
    val lastMentalPerformance: StateFlow<RawMentalPerformance> = rawDataStorage.lastMentalPerformance

    val dataQuality: StateFlow<SensorDataQuality> = sensorDataRecorder.dataQuality
    val signalLevel: StateFlow<Int> = sensorDataRecorder.signalLevel
    val countGoodEEG: StateFlow<Int> = sensorDataRecorder.goodEEGcount
    val countBadEEG: StateFlow<Int> = sensorDataRecorder.badEEGcount

    val bluetoothState = MutableStateFlow(false)

    private val _reconnectLoaderIsShown = MutableStateFlow(false)
    val reconnectLoaderIsShown: StateFlow<Boolean> get() = _reconnectLoaderIsShown

    init {
        viewModelScope.launch {
            dataAlerts.collect { state ->
                if (state == ConnectionState.Connected) {
                    setShowReconnectLoader(false)
                }
            }
        }
    }

    fun initReconnection() {
        val status = bleReceiveManager.dataAlerts.value
        val wasSaved = storage.getDeviceStatus()
        status.let {
            if ((it != ConnectionState.Connected || it != ConnectionState.DeviceFound) && wasSaved) {
                searchAndReconnect()
            }
        }
    }

    fun setShowReconnectLoader(value: Boolean) {
        _reconnectLoaderIsShown.value = value
    }

    fun searchAndReconnect() {
        bleReceiveManager.startSearchForReconnect()
    }

    fun reconnect() {
        bleReceiveManager.startSearchForReconnect()
    }

    fun navigateToWarmUp() {
        navigateTo(router.navigateToWarmUp())
    }

    fun navigateToLiveGameStats() {
        navigateTo(router.navigateToLiveGameStats())
    }

    fun navigateToGameStats() {
        storage.getNewSessionInfo()?.name?.let { navigateTo(router.navigateToGameStatsAdding(it)) }
    }

    fun navigateToDashboard() {
        viewModelScope.launch(Dispatchers.IO) {
            bleReceiveManager.disconnect()
            rawDataStorage.clear()
            withContext(Dispatchers.Main) {
                navigateTo(router.navigateToDashboards())
            }
        }
    }
}