package com.maddcog.android.livegamedata.liveGameStats

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import com.maddcog.android.R
import com.maddcog.android.baseui.presentation.NativeAlertDialog
import com.maddcog.android.baseui.ui.theme.*
import com.maddcog.android.data.bluetoothle.ConnectionState
import com.maddcog.android.data.network.dto.rawActivity.RawMentalPerformance
import com.maddcog.android.data.sensordatarecorder.SensorDataQuality
import com.maddcog.android.livegamedata.components.BatteryBox
import com.maddcog.android.livegamedata.components.CircleForPreparedWithName
import com.maddcog.android.livegamedata.components.TopAppBarWithPicture

@Composable
fun LiveGameStatsScreen(
    onClickEndMatch: () -> Unit,
    viewModel: LiveGameStatsViewModel,
    doReconnect: () -> Unit,
) {

    val mentalSize = viewModel.mentalPerformanceList.collectAsState(-1)
    val badEEG = viewModel.countBadEEG.collectAsState(-1)
    val goodEEG = viewModel.countGoodEEG.collectAsState(-1)
    val signal = viewModel.signalLevel.collectAsState(-1)
    val battery = viewModel.batteryLevel.collectAsState(0)
    val connectionState = viewModel.dataAlerts.collectAsState(ConnectionState.NotDefined)
    val lastMentalStats = viewModel.lastMentalPerformance.collectAsState(RawMentalPerformance(0.0, 0.0, 0.0, -1))
    val dataQuality = viewModel.dataQuality.collectAsState(SensorDataQuality.NotDefined)
    val lastMentalPerformance = viewModel.lastMentalPerformance.collectAsState(RawMentalPerformance(0.0, 0.0, 0.0, 0))

    val bluetoothState by viewModel.bluetoothState.collectAsState(initial = false)

    var openDialog = remember { mutableStateOf(false) }

    Column() {
        val gameName = viewModel.getGameName()
        gameName?.let { TopAppBarWithPicture(textTitle = it) { onClickEndMatch() } }

        Box(Modifier.fillMaxWidth()) {
            LazyColumn(
                modifier = Modifier
                    .background(LiveGameDataColor)
                    .fillMaxSize()
            ) {
                item {

                    Spacer(modifier = Modifier.height(Spacing_20))

                    Row(
                        modifier = Modifier.fillMaxWidth(),
                        horizontalArrangement = Arrangement.Center
                    ) {
                        Box() {
                            CircleForPreparedWithName(
                                value = lastMentalPerformance.value.flow ,
                                size = Spacing_180,
                                backgroundCircleColor = FlowText
                            )
                            Text(
                                text = viewModel.getDisplayedFlowBle(lastMentalPerformance.value.flow),
                                fontSize = Text_20,
                                color = FlowText,
                                fontWeight = FontWeight.Bold,
                                modifier = Modifier.align(Alignment.Center)
                            )
                        }
                    }

                    Spacer(modifier = Modifier.height(Spacing_20))

                    Text(
                        text = "Flow",
                        color = HintLightGrey,
                        fontSize = Text_16,
                        modifier = Modifier.fillMaxWidth(),
                        textAlign = TextAlign.Center
                    )

                    Spacer(modifier = Modifier.height(Spacing_50))

                    Row(
                        modifier = Modifier.fillMaxWidth(),
                        horizontalArrangement = Arrangement.Center
                    ) {
                        Box() {
                            CircleForPreparedWithName(
                                value = lastMentalPerformance.value.fatigue,
                                size = Spacing_90,
                                backgroundCircleColor = FatigueText
                            )
                            Text(
                                text = viewModel.getDisplayedFatigueBle(lastMentalPerformance.value.fatigue),
                                fontSize = Text_18,
                                color = FatigueText,
                                fontWeight = FontWeight.Bold,
                                modifier = Modifier.align(Alignment.Center)
                            )
                        }
                    }

                    Spacer(modifier = Modifier.height(Spacing_20))

                    Text(
                        text = "Fatigue",
                        color = HintLightGrey,
                        fontSize = Text_16,
                        modifier = Modifier.fillMaxWidth(),
                        textAlign = TextAlign.Center
                    )

                    Spacer(modifier = Modifier.height(Spacing_80))

                    BatteryBox(
                        lastMentalStats = lastMentalStats.value,
                        bluetoothState = bluetoothState,
                        mentalSize = mentalSize.value,
                        goodEEG = goodEEG.value,
                        badEEG = badEEG.value,
                        sensorAlerts = connectionState.value,
                        signalQuality = dataQuality.value,
                        batteryLevel = battery.value,
                        signal = signal.value,
                        isEegError = dataQuality.value == SensorDataQuality.EegError,
                        isHrError = dataQuality.value == SensorDataQuality.HrError,
                        doReconnect = { doReconnect() }
                    )

                    if (openDialog.value) {
                        NativeAlertDialog(
                            onExit = {
                                openDialog.value = false
                            },
                            onSuccess = {
                                viewModel.navigateToDashboard()
                            },
                            disclaimerFirst = R.string.not_enough_data_dialog_title,
                            disclaimerSecond = R.string.not_enough_data_dialog_message,
                            successName = R.string.not_enough_data_dialog_finish_button
                        )
                    }
                }
            }
        }
    }
}