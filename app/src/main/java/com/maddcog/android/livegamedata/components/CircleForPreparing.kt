package com.maddcog.android.livegamedata.components

import androidx.compose.foundation.Canvas
import androidx.compose.foundation.layout.size
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.StrokeCap
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.unit.Dp
import com.maddcog.android.baseui.ui.theme.*
import kotlinx.coroutines.delay

@Composable
fun CircleForPreparing(
    time: Long,
    dataSize: Int,
    dataNeeded: Double,
    navigateToLiveGameStats: () -> Unit,
    notEnoughDataWarning: () -> Unit,
) {

    val backgroundCircleColor = GameGrade
    val size: Dp = Spacing_180
    val thickness: Dp = Spacing_7

    val initialValue = 0f
    var value by remember { mutableStateOf(initialValue) }
    var currentTime by remember { mutableStateOf(0L) }
    var isTimerRunning by remember { mutableStateOf(false) }
    isTimerRunning = true

    LaunchedEffect(key1 = currentTime, key2 = isTimerRunning) {
        if (currentTime < time && isTimerRunning) {
            delay(100L)
            currentTime += 100L
            value = currentTime.toFloat() / time.toFloat()
        }
        if (currentTime >= 300000L) {
            if (dataSize > 0) {
                navigateToLiveGameStats()
            } else {
                notEnoughDataWarning()
            }
        }
    }

    Canvas(modifier = Modifier.size(size)) {

        val arcRadius = size.toPx()

        drawCircle(
            color = backgroundCircleColor,
            alpha = 0.2F,
            radius = arcRadius / 2,
            style = Stroke(width = thickness.toPx(), cap = StrokeCap.Butt)
        )

        drawArc(
            color = GameGrade,
            startAngle = -90f,
            sweepAngle = 360f * value,
            useCenter = false,
            style = Stroke(width = thickness.toPx(), cap = StrokeCap.Round),
            size = Size(arcRadius, arcRadius),
            topLeft = Offset(
                x = (size.toPx() - arcRadius) / 2,
                y = (size.toPx() - arcRadius) / 2
            )
        )
    }
}