package com.maddcog.android.liveGameSummary.composables

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import com.maddcog.android.R
import com.maddcog.android.baseui.ui.theme.Black
import com.maddcog.android.baseui.ui.theme.HintGreyText12
import com.maddcog.android.baseui.ui.theme.RedText14Bold
import com.maddcog.android.baseui.ui.theme.Spacing_10
import com.maddcog.android.baseui.ui.theme.Spacing_12
import com.maddcog.android.baseui.ui.theme.Spacing_8

@Composable
fun FatigueBlock(
    navigateToBreakTimer: () -> Unit,
) {
    Column(
        Modifier.fillMaxWidth(),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Spacer(modifier = Modifier.height(Spacing_8))
        Image(
            painter = painterResource(id = R.drawable.time_icon_small),
            contentDescription = ""
        )
        Spacer(modifier = Modifier.height(Spacing_8))
        Text(text = stringResource(id = R.string.take_five), style = RedText14Bold)
        Spacer(modifier = Modifier.height(Spacing_8))
        Text(text = stringResource(id = R.string.fatigue_delay), style = HintGreyText12)
        Spacer(modifier = Modifier.height(Spacing_8))
        Button(
            onClick = {
                navigateToBreakTimer()
            },
            colors = ButtonDefaults.buttonColors(backgroundColor = Color.White),
            shape = RoundedCornerShape(Spacing_10),
            modifier = Modifier.padding(horizontal = Spacing_12)
        ) {
            Text(text = stringResource(id = R.string.start_break), color = Black)
        }
    }
}