package com.maddcog.android.liveGameSummary

import com.maddcog.android.baseui.model.NavCommand

interface ILiveGameSummaryRouter {

    fun navigateToBreakTime(): NavCommand

    fun navigateToDashboards(): NavCommand

    fun navigateToGameStats(gameName: String): NavCommand
}