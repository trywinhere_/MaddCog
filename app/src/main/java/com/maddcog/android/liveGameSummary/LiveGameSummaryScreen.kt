package com.maddcog.android.liveGameSummary

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.statusBarsPadding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import com.maddcog.android.R
import com.maddcog.android.baseui.ui.theme.FatigueText
import com.maddcog.android.baseui.ui.theme.FlowText
import com.maddcog.android.baseui.ui.theme.GameGrade
import com.maddcog.android.baseui.ui.theme.Spacing_12
import com.maddcog.android.baseui.ui.theme.Spacing_16
import com.maddcog.android.baseui.ui.theme.Spacing_30
import com.maddcog.android.baseui.ui.theme.Spacing_70
import com.maddcog.android.baseui.ui.theme.Spacing_8
import com.maddcog.android.data.extensions.roundNumbers
import com.maddcog.android.gameSummary.components.CustomCircle
import com.maddcog.android.gameSummary.components.CustomMentalCircle
import com.maddcog.android.gameSummary.components.GettingGameStats
import com.maddcog.android.gameSummary.components.LinearBarBlock
import com.maddcog.android.gameSummary.components.MatchBreakdownBlock
import com.maddcog.android.gameSummary.components.MatchSummaryHeader
import com.maddcog.android.gameSummary.components.MetricSummaryBlock
import com.maddcog.android.gameSummary.components.TeamScoreBarBlock
import com.maddcog.android.gameSummary.extensions.toMentalType
import com.maddcog.android.liveGameSummary.composables.LiveGameSummaryTopAppBar
import com.maddcog.android.settings.components.BoldSectionHeader
import kotlinx.coroutines.delay

@Composable
fun LiveGameSummeryScreen(
    viewModel: LiveGameSummaryViewModel,
) {
    val state = viewModel.state.value.matchData
    val avgParamsList = state?.currentGameData?.userAverages

    LaunchedEffect(key1 = viewModel) {
        delay(30000)
        viewModel.getGameSummary()
    }

    Box(Modifier.fillMaxSize()) {
        Box(
            modifier = Modifier.fillMaxWidth()
        ) {
            LazyColumn(
                modifier = Modifier
                    .background(Color.Black)
                    .fillMaxSize()
            ) {

                if (!viewModel.state.value.isLoading) {
                    item {
                        state?.userActivity?.let {
                            MatchSummaryHeader(userActivity = it)
                        }
                        Spacer(modifier = Modifier.height(Spacing_8))
                        BoldSectionHeader(
                            modifier = Modifier.padding(horizontal = Spacing_12),
                            name = R.string.match_summary_header
                        )
                        Spacer(modifier = Modifier.height(Spacing_30))
                        state?.userActivity?.let {
                            Row(
                                modifier = Modifier.fillMaxWidth(),
                                horizontalArrangement = Arrangement.Start
                            ) {
                                Spacer(modifier = Modifier.width(Spacing_30))
                                CustomCircle(
                                    value = it.match.myPlayer.grade.roundNumbers(),
                                    text = it.match.myPlayer.grade.roundNumbers().toString(),
                                    elementSize = Spacing_70,
                                    backgroundCircleColor = GameGrade
                                )
                                Spacer(modifier = Modifier.width(Spacing_30))
                                CustomMentalCircle(
                                    mentalType = it.match.myPlayer.fatigueText.toMentalType(),
                                    elementSize = Spacing_70,
                                    FatigueText
                                ) {}
                                Spacer(modifier = Modifier.width(Spacing_30))
                                CustomMentalCircle(
                                    mentalType = it.match.myPlayer.flowText.toMentalType(),
                                    elementSize = Spacing_70, FlowText
                                ) {}
                            }
                        }
                        Spacer(modifier = Modifier.height(Spacing_30))
                        state?.userActivity?.match?.myPlayer?.let {
                            TeamScoreBarBlock(
                                modifier = Modifier.padding(horizontal = Spacing_12),
                                metricValue = it.score.toDouble(),
                                scoreText = it.scoreText,
                                outcome = it.outcome
                            )
                        }
                        Spacer(modifier = Modifier.height(Spacing_30))
                        state?.userActivity?.match?.myPlayer?.performanceParams?.forEach {
                            LinearBarBlock(
                                modifier = Modifier.padding(horizontal = Spacing_12),
                                metricName = it.name,
                                metricValue = it.value,
                                metricAverage = avgParamsList?.find { avg ->
                                    avg.name == it.name
                                }?.avg ?: 0.0
                            )
                        }
                        BoldSectionHeader(
                            modifier = Modifier.padding(horizontal = Spacing_12),
                            name = R.string.match_breakdown_header
                        )
                        state?.userActivity?.match?.let {
                            MatchBreakdownBlock(
                                match = it,
                                userActivity = state.userActivity
                            )
                        }
                        Spacer(modifier = Modifier.height(Spacing_16))
                        BoldSectionHeader(
                            modifier = Modifier.padding(horizontal = Spacing_12),
                            name = R.string.team_summary_header
                        )
                        state?.paramsData?.forEach {
                            Spacer(modifier = Modifier.height(Spacing_12))
                            MetricSummaryBlock(metricName = it.key, playersList = it.value)
                        }
                    }
                }
            }

            LiveGameSummaryTopAppBar(
                title = R.string.match,
                color = Color.Transparent.copy(alpha = 0f),
                modifier = Modifier.statusBarsPadding(),
                navigateBack = {
                    viewModel.navigateBack()
                },
                rightButtonClick = {
                    viewModel.navigateToDashboards()
                }
            )

            if (viewModel.state.value.isLoading) {
                GettingGameStats(
                    modifier = Modifier
                        .fillMaxWidth()
                        .align(Alignment.Center)
                        .padding(horizontal = Spacing_12)
                )
            }
        }
    }
}