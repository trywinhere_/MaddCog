package com.maddcog.android.liveGameSummary.breakTimer

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import com.maddcog.android.R
import com.maddcog.android.baseui.ui.theme.Black
import com.maddcog.android.baseui.ui.theme.GameGrade
import com.maddcog.android.baseui.ui.theme.HintGreyText16
import com.maddcog.android.baseui.ui.theme.Spacing_16
import com.maddcog.android.baseui.ui.theme.Spacing_220
import com.maddcog.android.baseui.ui.theme.Spacing_30
import com.maddcog.android.baseui.ui.theme.Spacing_90
import com.maddcog.android.liveGameSummary.composables.BreakTimeTopAppBar
import com.maddcog.android.liveGameSummary.composables.ComposeTimer

@Composable
fun BreakTimerScreen(
    viewModel: BreakTimerViewModel,
    timer: Long,
) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(Black),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        BreakTimeTopAppBar(
            title = R.string.take_five,
            buttonText = R.string.end_break,
            navigateBack = {
                viewModel.navigateBack()
            }
        )
        Spacer(modifier = Modifier.height(Spacing_30))
        ComposeTimer(
            totalTime = timer,
            inactiveBarColor = GameGrade.copy(alpha = 0.2f),
            activeBarColor = GameGrade,
            size = Spacing_220
        )
        Spacer(modifier = Modifier.height(Spacing_30))
        Image(
            modifier = Modifier.size(Spacing_90),
            painter = painterResource(id = R.drawable.break_time_icon),
            contentDescription = ""
        )
        Spacer(modifier = Modifier.height(Spacing_30))
        Text(
            modifier = Modifier.padding(horizontal = Spacing_16),
            text = stringResource(id = R.string.fatigue_delay),
            style = HintGreyText16,
            textAlign = TextAlign.Center
        )
    }
}