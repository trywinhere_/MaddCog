package com.maddcog.android.liveGameSummary

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.ui.platform.ComposeView
import androidx.core.view.WindowCompat
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.maddcog.android.baseui.presentation.BaseFragment
import com.maddcog.android.baseui.presentation.BaseViewModel
import com.maddcog.android.baseui.ui.theme.MaddcogTheme
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LiveGameSummaryFragment : BaseFragment() {

    private val viewModel: LiveGameSummaryViewModel by viewModels()
    private val args by navArgs<LiveGameSummaryFragmentArgs>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        WindowCompat.setDecorFitsSystemWindows(requireActivity().window, false)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = ComposeView(requireContext())
        view.setContent {
            MaddcogTheme(isTransparent = true) {
                LiveGameSummeryScreen(
                    viewModel = viewModel,
                )
            }
        }

        return view
    }

    override fun onStart() {
        super.onStart()
        WindowCompat.setDecorFitsSystemWindows(requireActivity().window, false)
    }

    override fun onStop() {
        super.onStop()
        WindowCompat.setDecorFitsSystemWindows(requireActivity().window, true)
    }

    override fun withViewModel(): BaseViewModel = viewModel.apply {
    }
}