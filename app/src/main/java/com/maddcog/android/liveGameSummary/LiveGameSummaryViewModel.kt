package com.maddcog.android.liveGameSummary

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.maddcog.android.baseui.model.Resource
import com.maddcog.android.baseui.presentation.BaseViewModel
import com.maddcog.android.baseui.ui.theme.UNEXPECTED_ERROR
import com.maddcog.android.domain.usecase.GetUserActivityByIdUseCase
import com.maddcog.android.gameSummary.model.GameSummaryState
import com.maddcog.android.gameSummary.model.MatchData
import com.maddcog.android.gameSummary.model.toMatchData
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@HiltViewModel
class LiveGameSummaryViewModel @Inject constructor(
    private val useCase: GetUserActivityByIdUseCase,
    private val router: ILiveGameSummaryRouter,
) : BaseViewModel() {

    private val _state = mutableStateOf(GameSummaryState(isLoading = true))
    val state: State<GameSummaryState> = _state

    private val _errorState = MutableLiveData<Boolean>()
    val errorState: LiveData<Boolean>
        get() = _errorState

    private var _match: MatchData? = null
    val match: MatchData
        get() = _match!!

    fun getGameSummary() {
        val lastGameUuid = useCase.getLastSessionUuid()
        lastGameUuid?.let { uuid ->
            useCase(uuid).onEach { result ->
                when (result) {
                    is Resource.Success -> {
                        _match = result.data?.toMatchData()
                        _state.value = GameSummaryState(matchData = _match)
                        _errorState.value = false
                    }
                    is Resource.Error -> {
                        _state.value = GameSummaryState(error = result.message ?: UNEXPECTED_ERROR)
                        _errorState.value = true
                    }
                    is Resource.Loading -> {
                        _state.value = GameSummaryState(isLoading = true)
                        _errorState.value = false
                    }
                }
            }.launchIn(viewModelScope)
        }
    }

    fun navigateToDashboards() {
        navigateTo(router.navigateToDashboards())
    }
}