package com.maddcog.android.liveGameSummary.composables

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.layoutId
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.ConstraintSet
import com.maddcog.android.R
import com.maddcog.android.baseui.ui.theme.Spacing_10
import com.maddcog.android.baseui.ui.theme.WhiteText14
import com.maddcog.android.baseui.ui.theme.WhiteText18Bold
import com.maddcog.android.baseui.ui.theme.arrow_back

@Composable
fun LiveGameSummaryTopAppBar(
    modifier: Modifier = Modifier,
    title: Int,
    navigateBack: () -> Unit,
    color: Color = Color.Black,
    rightButtonClick: () -> Unit,
) {
    TopAppBar(
        backgroundColor = color,
        elevation = 0.dp,
        modifier = modifier,
    ) {
        val constraints = ConstraintSet {
            val navIcon = createRefFor("nav_icon")
            val text = createRefFor("title")
            val finish = createRefFor("finish")

            constrain(navIcon) {
                top.linkTo(parent.top)
                start.linkTo(parent.start)
                bottom.linkTo(parent.bottom)
            }
            constrain(text) {
                top.linkTo(parent.top)
                bottom.linkTo(parent.bottom)
                end.linkTo(parent.end)
                start.linkTo(parent.start)
            }
            constrain(finish) {
                top.linkTo(parent.top)
                bottom.linkTo(parent.bottom)
                end.linkTo(parent.end)
            }
        }
        ConstraintLayout(
            constraints,
            modifier = Modifier
                .fillMaxWidth()
        ) {
            Text(
                text = stringResource(id = title),
                style = WhiteText18Bold,
                modifier = Modifier.layoutId("title")
            )
            Image(
                painterResource(id = R.drawable.refresh_icon_mid),
                contentDescription = arrow_back,
                modifier = Modifier
                    .layoutId("nav_icon")
                    .clickable {
                        navigateBack()
                    }
                    .padding(start = Spacing_10)
            )
            Button(
                modifier = Modifier
                    .padding(end = Spacing_10)
                    .layoutId("finish"),
                colors = ButtonDefaults.buttonColors(backgroundColor = Color.Gray),
                onClick = {
                    rightButtonClick()
                }
            ) {
                Text(text = "Finish", style = WhiteText14)
            }
        }
    }
}