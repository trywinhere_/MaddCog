package com.maddcog.android.liveGameSummary.composables

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.layoutId
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.ConstraintSet
import com.maddcog.android.baseui.ui.theme.HintGreyText16
import com.maddcog.android.baseui.ui.theme.WhiteText18Bold

@Composable
fun BreakTimeTopAppBar(
    modifier: Modifier = Modifier,
    title: Int,
    buttonText: Int,
    navigateBack: () -> Unit,
    color: Color = Color.Black,
) {
    TopAppBar(
        backgroundColor = color,
        elevation = 0.dp,
        modifier = modifier,
    ) {
        val constraints = ConstraintSet {
            val navIcon = createRefFor("nav_icon")
            val text = createRefFor("title")

            constrain(navIcon) {
                top.linkTo(parent.top)
                start.linkTo(parent.start)
                bottom.linkTo(parent.bottom)
            }
            constrain(text) {
                top.linkTo(parent.top)
                bottom.linkTo(parent.bottom)
                end.linkTo(parent.end)
                start.linkTo(parent.start)
            }
        }
        ConstraintLayout(
            constraints,
            modifier = Modifier
                .fillMaxWidth()
        ) {
            Text(
                text = stringResource(id = title),
                style = WhiteText18Bold,
                modifier = Modifier.layoutId("title")
            )
            Text(
                text = stringResource(id = buttonText),
                style = HintGreyText16,
                modifier = Modifier.layoutId("nav_icon").clickable {
                    navigateBack()
                }
            )
        }
    }
}