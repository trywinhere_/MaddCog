package com.maddcog.android.liveGameSummary.breakTimer

import com.maddcog.android.baseui.presentation.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class BreakTimerViewModel @Inject constructor() : BaseViewModel()
