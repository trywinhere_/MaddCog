package com.maddcog.android.liveGameSummary.composables

import androidx.compose.foundation.Canvas
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.size
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.StrokeCap
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.Dp
import com.maddcog.android.R
import com.maddcog.android.baseui.ui.theme.Spacing_12
import com.maddcog.android.baseui.ui.theme.Text_40
import com.maddcog.android.liveGameSummary.breakTimer.BreakTimerFragment.Companion.SECONDS_MINUTES
import kotlinx.coroutines.delay
import java.text.DecimalFormat

@Composable
fun ComposeTimer(
    size: Dp,
    totalTime: Long,
    inactiveBarColor: Color,
    activeBarColor: Color,
    modifier: Modifier = Modifier,
    initialValue: Float = 1f,
    strokeWidth: Dp = Spacing_12
) {
    var value by remember {
        mutableStateOf(initialValue)
    }
    var currentTime by remember {
        mutableStateOf(totalTime)
    }
    var isTimerRunning by remember {
        mutableStateOf(false)
    }

    isTimerRunning = true

    val min = (currentTime / SECONDS_MINUTES.times(1000)) % SECONDS_MINUTES
    val sec = (currentTime / 1000) % SECONDS_MINUTES
    val f = DecimalFormat(stringResource(id = R.string.timer_pattern))

    LaunchedEffect(key1 = currentTime, key2 = isTimerRunning) {
        if (currentTime > 0 && isTimerRunning) {
            delay(100L)
            currentTime -= 100L
            value = currentTime / totalTime.toFloat()
        }
    }
    Box(
        contentAlignment = Alignment.Center,
        modifier = modifier.fillMaxWidth()
    ) {
        Canvas(modifier = modifier.size(size)) {
            drawCircle(
                color = inactiveBarColor,
                radius = size.toPx() / 2,
                style = Stroke(width = strokeWidth.toPx(), cap = StrokeCap.Butt)
            )
            drawArc(
                color = activeBarColor,
                startAngle = -90f,
                sweepAngle = 360f * value,
                useCenter = false,
                size = Size(size.toPx(), size.toPx()),
                style = Stroke(strokeWidth.toPx(), cap = StrokeCap.Round)
            )
        }
        Text(
            text = stringResource(id = R.string.timer_string, f.format(min), f.format(sec)),
            fontSize = Text_40,
            fontWeight = FontWeight.Bold,
            color = Color.White
        )
    }
}