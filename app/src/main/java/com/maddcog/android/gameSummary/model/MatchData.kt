package com.maddcog.android.gameSummary.model

import com.maddcog.android.data.extensions.roundNumbers
import com.maddcog.android.domain.entities.UserGame
import com.maddcog.android.domain.entities.activity.MyPlayer
import com.maddcog.android.domain.entities.activity.TeamMember
import com.maddcog.android.domain.entities.activity.UserActivity
import com.maddcog.android.domain.entities.activity.UserActivityData
import com.maddcog.android.gameSummary.extensions.FATIGUE
import com.maddcog.android.gameSummary.extensions.FLOW
import com.maddcog.android.gameSummary.extensions.GRADE
import com.maddcog.android.gameSummary.extensions.YOU
import com.maddcog.android.gameSummary.extensions.toMentalType

data class MatchData(
    val userActivity: UserActivity,
    val paramsData: Map<String, List<PlayerScreenSummary>>,
    val currentGameData: UserGame?,
)

data class PlayerScreenSummary(
    val username: String,
    val metric: String,
    val image: String,
)

fun UserActivityData.toMatchData(): MatchData {
    return MatchData(
        userActivity = this.userActivity,
        paramsData = getParamsData(
            paramsList = getParamsList(this.userActivity),
            members = this.userActivity.match.team,
            myPlayer = this.userActivity.match.myPlayer,
        ),
        currentGameData = this.profile?.userGames?.firstOrNull { it.gameNumber == this.userActivity.activityType.id }
    )
}

fun getParamsData(
    paramsList: List<String>,
    members: List<TeamMember>,
    myPlayer: MyPlayer,
): Map<String, List<PlayerScreenSummary>> {
    val paramsMap = mutableMapOf<String, List<PlayerScreenSummary>>()

    val gradeMapList = mutableListOf<PlayerScreenSummary>()
    members.forEach {
        gradeMapList.add(
            PlayerScreenSummary(
                username = it.gamerName,
                image = it.characterImage,
                metric = it.grade.roundNumbers().toString()
            )
        )
    }
    gradeMapList.add(
        PlayerScreenSummary(
            username = YOU,
            image = myPlayer.characterImage,
            metric = myPlayer.grade.roundNumbers().toString()
        )
    )

    val fatigueMapList = mutableListOf<PlayerScreenSummary>()
    members.forEach {
        fatigueMapList.add(
            PlayerScreenSummary(
                username = it.gamerName,
                image = it.characterImage,
                metric = it.fatigueText.toMentalType().name
            )
        )
    }
    fatigueMapList.add(
        PlayerScreenSummary(
            username = YOU,
            image = myPlayer.characterImage,
            metric = myPlayer.fatigueText.toMentalType().name
        )
    )

    val flowMapList = mutableListOf<PlayerScreenSummary>()
    members.forEach {
        flowMapList.add(
            PlayerScreenSummary(
                username = it.gamerName,
                image = it.characterImage,
                metric = it.flowText.toMentalType().name
            )
        )
    }

    flowMapList.add(
        PlayerScreenSummary(
            username = YOU,
            image = myPlayer.characterImage,
            metric = myPlayer.flowText.toMentalType().name
        )
    )

    paramsMap[GRADE] = gradeMapList.sortedByDescending { it.metric }
    paramsMap[FATIGUE] = fatigueMapList.sortedByDescending { it.metric }
    paramsMap[FLOW] = flowMapList.sortedByDescending { it.metric }

    paramsList.forEach { param ->
        val membersList = mutableListOf<PlayerScreenSummary>()
        members.forEach {
            membersList.add(
                PlayerScreenSummary(
                    username = it.gamerName,
                    image = it.characterImage,
                    metric = it.performanceParams.first { memberParam -> memberParam.name == param }.value.roundNumbers().toString()
                )
            )
        }
        membersList.add(
            PlayerScreenSummary(
                username = YOU,
                image = myPlayer.characterImage,
                metric = myPlayer.performanceParams.first { it.name == param }.value.roundNumbers().toString()
            )
        )
        paramsMap[param] = membersList.sortedByDescending { it.metric.toDouble() }
    }

    return paramsMap
}

fun getParamsList(userActivity: UserActivity): List<String> {
    val metricList = mutableListOf<String>()
    val performanceParamsList = mutableListOf<String>()

    userActivity.match.myPlayer.performanceParams.forEach { param ->
        performanceParamsList.add(param.name)
    }

    performanceParamsList.forEach {
        if (!metricList.contains(it)) {
            metricList.add(it)
        }
    }

    return metricList
}