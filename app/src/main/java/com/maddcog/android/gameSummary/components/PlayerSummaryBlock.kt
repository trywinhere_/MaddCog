package com.maddcog.android.gameSummary.components

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.material3.Card
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.BlendMode
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.layout.layoutId
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import coil.compose.AsyncImage
import com.maddcog.android.baseui.ui.theme.BlackSemiTransparent
import com.maddcog.android.baseui.ui.theme.BoxDarkGrey
import com.maddcog.android.baseui.ui.theme.Int_1
import com.maddcog.android.baseui.ui.theme.Spacing_2
import com.maddcog.android.baseui.ui.theme.Spacing_50
import com.maddcog.android.baseui.ui.theme.Spacing_6
import com.maddcog.android.baseui.ui.theme.Spacing_60
import com.maddcog.android.baseui.ui.theme.Text_10
import com.maddcog.android.baseui.ui.theme.Text_18
import com.maddcog.android.baseui.ui.theme.White
import com.maddcog.android.gameSummary.extensions.YOU

@Composable
fun PlayerSummaryBlock(
    imageUrl: String,
    metricValue: String,
    username: String,
    metricColor: Color,
    isYou: Boolean,
) {
    Column(
        modifier = Modifier
            .width(Spacing_60),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Card(
            modifier = Modifier.wrapContentSize(),
            shape = RoundedCornerShape(Spacing_50),
            border = if (isYou) BorderStroke(Spacing_2, White) else null
        ) {
            AsyncImage(
                model = imageUrl,
                contentDescription = null,
                modifier = Modifier
                    .clip(RoundedCornerShape(Spacing_50))
                    .layoutId("character_image")
                    .height(Spacing_50)
                    .aspectRatio(1f)
                    .background(BoxDarkGrey),
                contentScale = ContentScale.Crop,
                colorFilter = if (username != YOU) ColorFilter.tint(
                    BlackSemiTransparent,
                    BlendMode.Darken
                ) else null
            )
        }
        Spacer(modifier = Modifier.height(Spacing_6))
        Text(
            text = metricValue,
            style = TextStyle(
                color = metricColor,
                fontWeight = FontWeight.Bold,
                fontSize = Text_18
            )
        )
        Spacer(modifier = Modifier.height(Spacing_6))
        Text(
            text = username,
            style = TextStyle(
                color = Color.White,
                fontSize = Text_10,
            ),
            overflow = TextOverflow.Ellipsis,
            maxLines = Int_1
        )
    }
}