package com.maddcog.android.gameSummary.extensions

import android.content.Context
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.withStyle
import com.maddcog.android.R
import com.maddcog.android.baseui.ui.theme.CreepScoreText
import com.maddcog.android.baseui.ui.theme.DeathsChartColor
import com.maddcog.android.baseui.ui.theme.FatigueText
import com.maddcog.android.baseui.ui.theme.FlowText
import com.maddcog.android.baseui.ui.theme.GameGrade
import com.maddcog.android.baseui.ui.theme.GameStatChart
import com.maddcog.android.baseui.ui.theme.HintLightGrey
import com.maddcog.android.baseui.ui.theme.KdaText
import com.maddcog.android.baseui.ui.theme.KillParticipationText
import com.maddcog.android.baseui.ui.theme.KillsChartColor
import com.maddcog.android.baseui.ui.theme.Text_14
import com.maddcog.android.baseui.ui.theme.Text_16
import com.maddcog.android.baseui.ui.theme.White
import com.maddcog.android.domain.entities.MentalType

const val PERCENT = "%"
const val YOU = "You"
const val GRADE = "Grade"
const val FLOW = "Flow"
const val FATIGUE = "Fatigue"
const val MIN = " min"

const val HIGH = "high"
const val MID = "normal"
const val LOW = "low"

fun String.toMentalType(): MentalType {
    return when (this) {
        HIGH -> MentalType.HI
        MID -> MentalType.MID
        LOW -> MentalType.LOW
        else -> MentalType.NONE
    }
}

fun String.toMetricColor(context: Context): Color {
    return when (this) {
        GRADE -> GameGrade
        FLOW -> FlowText
        FATIGUE -> FatigueText
        context.resources.getString(R.string.kd) -> KdaText
        context.resources.getString(R.string.kda) -> KdaText
        context.resources.getString(R.string.kill_participation) -> KillParticipationText
        else -> CreepScoreText
    }
}

fun String.toEventColor(context: Context): Color {
    return when (this) {
        context.resources.getString(R.string.kills) -> KillsChartColor
        context.resources.getString(R.string.deaths) -> DeathsChartColor
        else -> GameStatChart
    }
}

fun createAnnotatedString(value: Int): AnnotatedString {
    return buildAnnotatedString {
        withStyle(
            style = SpanStyle(
                color = White,
                fontSize = Text_16,
                fontWeight = FontWeight.Bold,
            )
        ) {
            append(value.toString())
        }
        withStyle(
            style = SpanStyle(
                color = HintLightGrey,
                fontSize = Text_14
            )
        ) {
            append(MIN)
        }
    }
}