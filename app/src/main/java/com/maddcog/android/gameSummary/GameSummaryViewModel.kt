package com.maddcog.android.gameSummary

import androidx.compose.runtime.State
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.maddcog.android.baseui.model.Resource
import com.maddcog.android.baseui.presentation.BaseViewModel
import com.maddcog.android.baseui.ui.theme.UNEXPECTED_ERROR
import com.maddcog.android.data.bluetoothle.ConnectionState
import com.maddcog.android.data.bluetoothle.IBleReceiveManager
import com.maddcog.android.domain.usecase.GetUserActivityByIdUseCase
import com.maddcog.android.gameSummary.model.GameSummaryState
import com.maddcog.android.gameSummary.model.MatchData
import com.maddcog.android.gameSummary.model.toMatchData
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class GameSummaryViewModel @Inject constructor(
    private val useCase: GetUserActivityByIdUseCase,
    private val bleReceiveManager: IBleReceiveManager
) : BaseViewModel() {

    private val _errorState = MutableLiveData<Boolean>()
    val errorState: LiveData<Boolean>
        get() = _errorState

    private val _state = mutableStateOf(GameSummaryState())
    val state: State<GameSummaryState> = _state

    private var _match: MatchData? = null
    val match: MatchData
        get() = _match!!

    fun getGameSummary(userActivityId: String) {
        useCase(userActivityId).onEach { result ->
            when (result) {
                is Resource.Success -> {
                    _match = result.data?.toMatchData()
                    _state.value = GameSummaryState(matchData = _match)
                    _errorState.value = false
                }
                is Resource.Error -> {
                    _state.value = GameSummaryState(error = result.message ?: UNEXPECTED_ERROR)
                    _errorState.value = true
                }
                is Resource.Loading -> {
                    _state.value = GameSummaryState(isLoading = true)
                    _errorState.value = false
                }
            }
        }.launchIn(viewModelScope)
    }

    var connectionState by mutableStateOf<ConnectionState>(ConnectionState.Uninitialized)

    fun subscribeToChanges() {
        viewModelScope.launch {
            bleReceiveManager.data.collect { result ->
                when (result) {
                    is com.maddcog.android.domain.utils.ble.Resource.Success -> {
                        connectionState = result.data.connectionState
                    }

                    is com.maddcog.android.domain.utils.ble.Resource.Loading -> {
                        connectionState = ConnectionState.CurrentlyInitializing
                    }

                    is com.maddcog.android.domain.utils.ble.Resource.Error -> {
                        connectionState = ConnectionState.Uninitialized
                    }
                }
            }
        }
    }
}