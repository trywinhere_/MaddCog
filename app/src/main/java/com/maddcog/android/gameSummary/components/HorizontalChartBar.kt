package com.maddcog.android.gameSummary.components

import androidx.compose.foundation.Canvas
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.CornerRadius
import androidx.compose.ui.geometry.Rect
import androidx.compose.ui.geometry.RoundRect
import androidx.compose.ui.graphics.Path
import androidx.compose.ui.unit.dp
import com.maddcog.android.baseui.ui.theme.Black
import com.maddcog.android.baseui.ui.theme.GameStatChart
import com.maddcog.android.baseui.ui.theme.LineChartEmptyGrey
import com.maddcog.android.baseui.ui.theme.LineChartTickGrey
import kotlin.math.abs

@Composable
fun HorizontalChartBar(
    modifier: Modifier = Modifier,
    averageValue: Double,
    currentValue: Double,
    isScore: Boolean = false,
    isWin: Boolean = false
) {
    val emptyBarColor = LineChartEmptyGrey
    val filledBarColor = GameStatChart

    Canvas(modifier = modifier) {

        val canvasWidth = size.width
        val halfWidth = canvasWidth / 2
        val cornerRadius = CornerRadius(10f, 10f)

        fun drawScoreBar() {
            val filledPath = Path().apply {
                addRoundRect(
                    RoundRect(
                        rect = if (isWin) {
                            Rect(
                                left = halfWidth,
                                top = 0f,
                                right = halfWidth + halfWidth * ((currentValue - 3) / 3).toFloat(),
                                bottom = 8.dp.toPx()
                            )
                        } else {
                            Rect(
                                left = halfWidth * (abs((currentValue - 3)) / 3).toFloat(),
                                top = 0f,
                                right = halfWidth,
                                bottom = 8.dp.toPx()
                            )
                        },
                        topLeft = cornerRadius,
                        topRight = cornerRadius,
                        bottomLeft = cornerRadius,
                        bottomRight = cornerRadius,
                    )
                )
            }
            drawPath(filledPath, color = filledBarColor)
        }

        fun drawEmptyBar() {
            val emptyPath = Path().apply {
                addRoundRect(
                    RoundRect(
                        rect = Rect(
                            left = 0f,
                            top = 0f,
                            right = canvasWidth,
                            bottom = 8.dp.toPx()
                        ),
                        topLeft = cornerRadius,
                        topRight = cornerRadius,
                        bottomLeft = cornerRadius,
                        bottomRight = cornerRadius,
                    )
                )
            }
            drawPath(emptyPath, color = emptyBarColor)
        }

        fun drawCenterTick() {
            val tickShadowPath = Path().apply {
                addRoundRect(
                    RoundRect(
                        rect = Rect(
                            left = (canvasWidth / 2) - 12f,
                            top = 0 - 6f,
                            right = (canvasWidth / 2) + 12f,
                            bottom = 51f,
                        ),
                        bottomLeft = cornerRadius,
                        bottomRight = cornerRadius,
                    )
                )
            }
            drawPath(tickShadowPath, color = Black)
            val tickPath = Path().apply {
                addRoundRect(
                    RoundRect(
                        rect = Rect(
                            left = (canvasWidth / 2) - 6f,
                            top = 0f,
                            right = (canvasWidth / 2) + 6f,
                            bottom = 45f,
                        ),
                        bottomLeft = cornerRadius,
                        bottomRight = cornerRadius,
                    )
                )
            }
            drawPath(tickPath, color = LineChartTickGrey)
        }

        fun drawFilledBar() {
            // filled bar
            val aboveAverage = halfWidth + halfWidth * (averageValue / currentValue).toFloat()
            val belowAverage = halfWidth * (currentValue / averageValue).toFloat()
            val filledPath = Path().apply {
                addRoundRect(
                    RoundRect(
                        rect = Rect(
                            left = 0f,
                            top = 0f,
                            right = if (currentValue > averageValue) aboveAverage else belowAverage,
                            bottom = 8.dp.toPx()
                        ),
                        topLeft = cornerRadius,
                        topRight = cornerRadius,
                        bottomLeft = cornerRadius,
                        bottomRight = cornerRadius,
                    )
                )
            }
            drawPath(filledPath, color = filledBarColor)
        }

        if (!isScore) {
            drawEmptyBar()
            drawFilledBar()
            drawCenterTick()
        } else {
            drawEmptyBar()
            drawScoreBar()
            drawCenterTick()
        }
    }
}