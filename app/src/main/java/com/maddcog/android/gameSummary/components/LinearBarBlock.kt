package com.maddcog.android.gameSummary.components

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import com.maddcog.android.R
import com.maddcog.android.baseui.ui.theme.Spacing_10
import com.maddcog.android.baseui.ui.theme.Spacing_20
import com.maddcog.android.baseui.ui.theme.Spacing_30
import com.maddcog.android.baseui.ui.theme.TickGreyText14
import com.maddcog.android.baseui.ui.theme.WhiteText18Bold
import com.maddcog.android.gameSummary.extensions.PERCENT
import kotlin.math.roundToInt

@Composable
fun LinearBarBlock(
    modifier: Modifier = Modifier,
    metricName: String,
    metricValue: Double,
    metricAverage: Double,
) {

    val belowAverage = 100 - (metricValue / metricAverage * 100).roundToInt()
    val aboveAverage = 100 - (metricAverage / metricValue * 100).roundToInt()

    Column(modifier = modifier.fillMaxWidth()) {
        Row(
            modifier = Modifier
                .fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Text(text = metricName, style = WhiteText18Bold)
            Text(
                text = if (metricName == stringResource(id = R.string.kill_participation)) metricValue.roundToInt()
                    .toString() + PERCENT else metricValue.roundToInt().toString(),
                style = WhiteText18Bold
            )
        }
        Spacer(modifier = Modifier.height(Spacing_10))
        HorizontalChartBar(
            modifier = Modifier
                .fillMaxWidth()
                .wrapContentHeight(),
            averageValue = metricAverage,
            currentValue = metricValue,
        )
        Spacer(modifier = Modifier.height(Spacing_20))
        Row(
            modifier = Modifier
                .fillMaxWidth(),
            horizontalArrangement = Arrangement.Start
        ) {
            Text(
                text = if (metricAverage > metricValue) stringResource(
                    R.string.metric_below_average,
                    belowAverage
                )
                else if (metricAverage == metricValue) stringResource(R.string.equal_average) else stringResource(
                    R.string.metric_above_average,
                    aboveAverage
                ),
                style = TickGreyText14
            )
        }
        Spacer(modifier = Modifier.height(Spacing_30))
    }
}