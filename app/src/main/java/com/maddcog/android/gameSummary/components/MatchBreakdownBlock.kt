package com.maddcog.android.gameSummary.components

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import com.maddcog.android.baseui.extensions.toCapitalize
import com.maddcog.android.baseui.ui.theme.FatigueText
import com.maddcog.android.baseui.ui.theme.FlowBreakdownColor
import com.maddcog.android.baseui.ui.theme.Spacing_12
import com.maddcog.android.baseui.ui.theme.Spacing_20
import com.maddcog.android.baseui.ui.theme.Spacing_220
import com.maddcog.android.baseui.ui.theme.Spacing_30
import com.maddcog.android.data.extensions.toLocalName
import com.maddcog.android.domain.entities.activity.Match
import com.maddcog.android.domain.entities.activity.UserActivity
import com.maddcog.android.gameSummary.extensions.createAnnotatedString
import com.maddcog.android.gameSummary.extensions.toEventColor
import com.maddcog.android.trends.extensions.FATIGUE_LEGEND
import com.maddcog.android.trends.extensions.FLOW_LEGEND

@Composable
fun MatchBreakdownBlock(
    userActivity: UserActivity,
    match: Match,
) {
    val matchDuration = (match.end - match.start) / 60
    Column(
        Modifier
            .fillMaxWidth()
    ) {
        MatchChart(
            modifier = Modifier
                .fillMaxWidth()
                .height(Spacing_220)
                .padding(vertical = Spacing_20, horizontal = Spacing_30),
            matchStart = match.start,
            matchEnd = match.end,
            timelineEvents = match.timelineEvents,
            mentalMetricsList = userActivity.mentalPerformanceTimeline,
        )
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = Spacing_12),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Text(text = createAnnotatedString(0))
            Text(text = createAnnotatedString(matchDuration / 2))
            Text(text = createAnnotatedString(matchDuration))
        }
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = Spacing_12, vertical = Spacing_12),
            horizontalArrangement = Arrangement.SpaceEvenly
        ) {
            LegendItem(itemName = FATIGUE_LEGEND, itemColor = FatigueText)
            LegendItem(itemName = FLOW_LEGEND, itemColor = FlowBreakdownColor)
            match.timelineEvents.forEach {
                LegendItem(
                    itemName = it.name.toCapitalize(),
                    itemColor = it.name.toLocalName(
                        LocalContext.current
                    ).toEventColor(LocalContext.current)
                )
            }
        }
    }
}