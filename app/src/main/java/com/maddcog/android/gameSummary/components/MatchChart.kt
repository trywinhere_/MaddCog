package com.maddcog.android.gameSummary.components

import androidx.compose.foundation.Canvas
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Path
import androidx.compose.ui.graphics.StrokeCap
import androidx.compose.ui.graphics.asAndroidPath
import androidx.compose.ui.graphics.asComposePath
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import com.maddcog.android.baseui.ui.theme.FatigueText
import com.maddcog.android.baseui.ui.theme.FlowBreakdownColor
import com.maddcog.android.data.extensions.toLocalName
import com.maddcog.android.domain.entities.activity.MentalPerformanceTimeline
import com.maddcog.android.domain.entities.activity.TimelineEvent
import com.maddcog.android.gameSummary.extensions.toEventColor
import kotlin.math.roundToInt

@Composable
fun MatchChart(
    modifier: Modifier = Modifier,
    matchStart: Int,
    matchEnd: Int,
    timelineEvents: List<TimelineEvent>,
    mentalMetricsList: List<MentalPerformanceTimeline>,
) {

    val context = LocalContext.current

    val upperFatigueValue = remember(mentalMetricsList) {
        (mentalMetricsList.maxOfOrNull { it.fatigue }?.plus(1))?.roundToInt() ?: 0
    }

    val lowerFatigueValue = remember(mentalMetricsList) {
        mentalMetricsList.minOfOrNull { it.fatigue }?.toInt() ?: 0
    }

    val upperFlowValue = remember(mentalMetricsList) {
        (mentalMetricsList.maxOfOrNull { it.flow }?.plus(1))?.roundToInt() ?: 0
    }

    val lowerFlowValue = remember(mentalMetricsList) {
        mentalMetricsList.minOfOrNull { it.flow }?.toInt() ?: 0
    }

    Canvas(modifier = modifier) {

        val canvasWidth = size.width
        val height = size.height
        val matchDuration = matchEnd - matchStart

        fun drawFatigue() {
            // flow path
            var lastX = 0f
            val fatiguePath = Path().apply {
                for (i in mentalMetricsList.indices.filter { it % 3 == 0 }) {
                    val fatigue = mentalMetricsList[i].fatigue
                    val timestamp = mentalMetricsList[i].timestamp
                    val timestampRatio = (timestamp - matchStart)
                    val timestampProportion = (timestampRatio.toFloat() / matchDuration.toFloat())
                    val currentX = canvasWidth * timestampProportion

                    val nextTimestamp = mentalMetricsList.getOrNull(i + 1)?.timestamp ?: mentalMetricsList.last().timestamp
                    val nextTimestampRatio = (nextTimestamp - matchStart)
                    val nextTimestampProportion = (nextTimestampRatio.toFloat() / matchDuration.toFloat())
                    val nextX = canvasWidth * nextTimestampProportion

                    val nextFatigue = mentalMetricsList.getOrNull(i + 1)?.fatigue ?: mentalMetricsList.last().fatigue
                    val leftFatigueRatio = (fatigue - lowerFatigueValue) / (upperFatigueValue - lowerFatigueValue)
                    val rightFatigueRatio = (nextFatigue - lowerFatigueValue) / (upperFatigueValue - lowerFatigueValue)

                    val x1 = currentX
                    val x2 = nextX
                    val y1 = height - (leftFatigueRatio * height).toFloat()
                    val y2 = height - (rightFatigueRatio * height).toFloat()
                    if (i == 0) {
                        moveTo(x1, y1)
                    }
                    lastX = (x1 + x2) / 2f
                    quadraticBezierTo(
                        x1, y1, lastX, (y1 + y2) / 2f
                    )
                }
            }
            drawPath(
                path = fatiguePath,
                color = FatigueText,
                style = Stroke(
                    width = 3.dp.toPx(),
                    cap = StrokeCap.Round
                )
            )
        }

        fun drawFlow() {
            // flow path
            var lastX = 0f
            val flowPath = Path().apply {
                for (i in mentalMetricsList.indices) {
                    val flow = mentalMetricsList[i].flow
                    val timestamp = mentalMetricsList[i].timestamp
                    val timestampRatio = (timestamp - matchStart)
                    val timestampProportion = (timestampRatio.toFloat() / matchDuration.toFloat())
                    val currentX = canvasWidth * timestampProportion

                    val nextTimestamp = mentalMetricsList.getOrNull(i + 1)?.timestamp ?: mentalMetricsList.last().timestamp
                    val nextTimestampRatio = (nextTimestamp - matchStart)
                    val nextTimestampProportion = (nextTimestampRatio.toFloat() / matchDuration.toFloat())
                    val nextX = canvasWidth * nextTimestampProportion

                    val nextFlow = mentalMetricsList.getOrNull(i + 1)?.flow ?: mentalMetricsList.last().flow
                    val leftFlowRatio = (flow - lowerFlowValue) / (upperFlowValue - lowerFlowValue)
                    val rightFlowRatio = (nextFlow - lowerFlowValue) / (upperFlowValue - lowerFlowValue)

                    val x1 = currentX
                    val x2 = nextX
                    val y1 = height - (leftFlowRatio * height).toFloat()
                    val y2 = height - (rightFlowRatio * height).toFloat()
                    if (i == 0) {
                        moveTo(x1, y1)
                    }
                    lastX = (x1 + x2) / 2f
                    quadraticBezierTo(
                        x1, y1, lastX, (y1 + y2) / 2f
                    )
                }
            }

            val fillPath = android.graphics.Path(flowPath.asAndroidPath())
                .asComposePath()
                .apply {
                    lineTo(lastX, size.height)
                    lineTo(0f, size.height)
                    close()
                }

            drawPath(
                path = fillPath,
                color = FlowBreakdownColor
            )

            drawPath(
                path = flowPath,
                color = FlowBreakdownColor,
                style = Stroke(
                    width = 0.dp.toPx(),
                    cap = StrokeCap.Round
                )
            )
        }

        fun drawTimelineEvents() {
            timelineEvents.forEach { timeLineEvent ->
                timeLineEvent.timestamps.indices.forEach { i ->
                    val event = timeLineEvent.timestamps[i]
                    val ratio = (event - matchStart)
                    val proportion = (ratio.toFloat() / matchDuration.toFloat())
                    val currentX = canvasWidth * proportion
                    drawLine(
                        color = timeLineEvent.name.toLocalName(context).toEventColor(context),
                        start = Offset(x = currentX, 0f),
                        end = Offset(x = currentX, size.height),
                        strokeWidth = 3f,
                    )
                }
            }
        }
        drawFlow()
        drawFatigue()
        drawTimelineEvents()
    }
}