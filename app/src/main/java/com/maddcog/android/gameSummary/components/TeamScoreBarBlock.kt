package com.maddcog.android.gameSummary.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import com.maddcog.android.R
import com.maddcog.android.baseui.ui.theme.Spacing_10
import com.maddcog.android.baseui.ui.theme.Spacing_20
import com.maddcog.android.baseui.ui.theme.Spacing_30
import com.maddcog.android.baseui.ui.theme.TickGreyText14
import com.maddcog.android.baseui.ui.theme.WIN
import com.maddcog.android.data.extensions.toScoreText

@Composable
fun TeamScoreBarBlock(
    modifier: Modifier = Modifier,
    metricValue: Double,
    scoreText: String,
    outcome: String,
) {
    Column(modifier = modifier.fillMaxWidth()) {
        Row(
            modifier = Modifier
                .fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Image(
                modifier = Modifier.size(Spacing_30),
                painter = painterResource(
                    id = R.drawable.brain
                ),
                contentDescription = ""
            )
            Image(
                modifier = Modifier.size(Spacing_30),
                painter = painterResource(
                    id = R.drawable.skill
                ),
                contentDescription = ""
            )
        }
        Spacer(modifier = Modifier.height(Spacing_10))
        HorizontalChartBar(
            modifier = Modifier
                .fillMaxWidth()
                .wrapContentHeight(),
            averageValue = metricValue,
            currentValue = metricValue,
            isScore = true,
            isWin = outcome == WIN
        )
        Spacer(modifier = Modifier.height(Spacing_20))
        Row(
            modifier = Modifier
                .fillMaxWidth(),
            horizontalArrangement = Arrangement.Start
        ) {
            Text(
                text = stringResource(id = scoreText.toScoreText(outcome)),
                style = TickGreyText14
            )
        }
    }
}