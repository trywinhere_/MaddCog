package com.maddcog.android.gameSummary

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.ui.platform.ComposeView
import androidx.core.view.WindowCompat
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.maddcog.android.baseui.presentation.BaseFragment
import com.maddcog.android.baseui.presentation.BaseViewModel
import com.maddcog.android.baseui.ui.theme.MaddcogTheme
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class GameSummaryFragment : BaseFragment() {

    private val viewModel: GameSummaryViewModel by viewModels()
    private val args by navArgs<GameSummaryFragmentArgs>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.getGameSummary(args.userActivityId)
        viewModel.subscribeToChanges()
        WindowCompat.setDecorFitsSystemWindows(requireActivity().window, false)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = ComposeView(requireContext())
        view.setContent {
            MaddcogTheme(isTransparent = true) {
                GameSummaryScreen(
                    viewModel = viewModel,
                )
            }
        }

        return view
    }

    override fun onStop() {
        super.onStop()
        WindowCompat.setDecorFitsSystemWindows(requireActivity().window, true)
    }

    override fun withViewModel(): BaseViewModel = viewModel.apply {
        errorState.observe(viewLifecycleOwner) { isError ->
            if (isError) {
                showToastMessage(state.value.error)
                viewModel.navigateBack()
            }
        }
    }
}