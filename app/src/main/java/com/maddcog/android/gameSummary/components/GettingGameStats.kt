package com.maddcog.android.gameSummary.components

import androidx.compose.foundation.layout.Column
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import com.maddcog.android.R
import com.maddcog.android.baseui.ui.theme.TickGreyText14
import com.maddcog.android.baseui.ui.theme.WhiteText16Bold

@Composable
fun GettingGameStats(
    modifier: Modifier = Modifier,
) {
    Column(
        modifier = modifier,
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        Text(text = stringResource(id = R.string.getting_game_stats), style = WhiteText16Bold, textAlign = TextAlign.Center)
        Text(text = stringResource(id = R.string.getting_game_stats_30), style = TickGreyText14, textAlign = TextAlign.Center)
    }
}