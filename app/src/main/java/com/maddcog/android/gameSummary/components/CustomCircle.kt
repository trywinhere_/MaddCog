package com.maddcog.android.gameSummary.components

import androidx.compose.foundation.Canvas
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.StrokeCap
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.Dp
import com.maddcog.android.baseui.ui.theme.Number_for_angle
import com.maddcog.android.baseui.ui.theme.Number_for_div_for_angle
import com.maddcog.android.baseui.ui.theme.Spacing_7
import com.maddcog.android.baseui.ui.theme.Text_20
import com.maddcog.android.domain.entities.MentalType
import com.maddcog.android.domain.extensions.toDoubleValue

@Composable
fun CustomCircle(
    value: Double,
    text: String,
    elementSize: Dp,
    backgroundCircleColor: Color,
    thickness: Dp = Spacing_7,
) {

    val sweepAngles = value * Number_for_angle / Number_for_div_for_angle

    Box(Modifier.wrapContentWidth(), contentAlignment = Alignment.Center) {
        Canvas(
            modifier = Modifier
                .size(elementSize)
        ) {

            val canvasSize = size.height
            val arcRadius = elementSize.toPx()

            drawCircle(
                color = backgroundCircleColor,
                alpha = 0.2F,
                radius = arcRadius / 2,
                style = Stroke(width = thickness.toPx(), cap = StrokeCap.Butt)
            )

            drawArc(
                color = backgroundCircleColor,
                startAngle = -90f,
                sweepAngle = sweepAngles.toFloat(),
                useCenter = false,
                style = Stroke(width = thickness.toPx(), cap = StrokeCap.Round),
                size = Size(arcRadius, arcRadius),
                topLeft = Offset(
                    x = (elementSize.toPx() - arcRadius) / 2,
                    y = (elementSize.toPx() - arcRadius) / 2
                )
            )
        }
        Text(
            text = text,
            style = TextStyle(
                color = backgroundCircleColor,
                fontSize = Text_20,
            ),
        )
    }
}

@Composable
fun CustomMentalCircle(mentalType: MentalType, elementSize: Dp, backgroundCircleColor: Color, onDialClick: () -> Unit ) {

    val thickness: Dp = Spacing_7

    val sweepAngles = mentalType.toDoubleValue() * Number_for_angle / Number_for_div_for_angle

    Box(Modifier.wrapContentSize(), contentAlignment = Alignment.Center) {
        Canvas(
            modifier = Modifier
                .size(elementSize)
        ) {

            val arcRadius = elementSize.toPx()

            drawCircle(
                color = backgroundCircleColor,
                alpha = 0.2F,
                radius = arcRadius / 2,
                style = Stroke(width = thickness.toPx(), cap = StrokeCap.Butt)
            )

            drawArc(
                color = backgroundCircleColor,
                startAngle = -90f,
                sweepAngle = sweepAngles.toFloat(),
                useCenter = false,
                style = Stroke(width = thickness.toPx(), cap = StrokeCap.Round),
                size = Size(arcRadius, arcRadius),
                topLeft = Offset(
                    x = (elementSize.toPx() - arcRadius) / 2,
                    y = (elementSize.toPx() - arcRadius) / 2
                )
            )
        }
        Text(
            text = mentalType.name,
            style = TextStyle(
                color = backgroundCircleColor,
                fontSize = Text_20,
            ),
        )
    }
}