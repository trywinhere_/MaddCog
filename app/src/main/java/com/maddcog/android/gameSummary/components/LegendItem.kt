package com.maddcog.android.gameSummary.components

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import com.maddcog.android.baseui.ui.theme.Spacing_10
import com.maddcog.android.baseui.ui.theme.Spacing_20

@Composable
fun LegendItem(
    itemName: String,
    itemColor: Color,
) {
    Row(
        modifier = Modifier.wrapContentSize(),
    ) {
        Box(
            modifier = Modifier
                .padding(end = Spacing_10)
                .size(Spacing_20)
                .clip(CircleShape)
                .background(itemColor)
        )
        Text(text = itemName, color = Color.White)
    }
}