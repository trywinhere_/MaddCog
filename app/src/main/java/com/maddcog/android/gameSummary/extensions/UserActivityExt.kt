package com.maddcog.android.gameSummary.extensions

import android.content.Context
import com.maddcog.android.R
import com.maddcog.android.baseui.extensions.toCapitalize
import com.maddcog.android.domain.entities.activity.UserActivity
import com.maddcog.android.gameSummary.model.SummaryTileInfo

const val LOL = "League of Legends"
const val VALORANT = "Valorant"
const val CS_GO = "Counter-Strike: Global Offensive"

const val FIFA = "FIFA"
const val NFL = "Madden NFL"
const val ROCKET_LEAGUE = "Rocket League"
const val NBA = "NBA 2K"

const val COD_MW = "Call of Duty: Modern Warfare"
const val BF = "Battelfield V"
const val COD_W = "Call of Duty: Warzone"
const val FORTNITE = "Fortnite"
const val APEX = "Apex Legends"
const val OVERWATCH = "Overwatch 2"

const val TOM_CLANCY = "Tom Clancy's Division 2"
const val GENSHIN = "Genshin Impact"
const val MINECRAFT = "Minecraft"
const val ROBLOX = "Roblox"
const val DESTINY = "Destiny 2"
const val GTA = "GTA"
const val FINAL_FANTASY = "Final Fantasy"
const val CYBERPUNK = "Cyberpunk 2077"
const val HOGWARTS = "Hogwarts Legacy"
const val LOST_ARK = "Lost Ark"
const val RUST = "Rust"
const val VALHEIM = "Valheim"
const val ELDEN_RING = "Elden Ring"
const val ARK = "ARK"
const val RDR2 = "Red Dead Redemption 2"
const val GOW = "God of War"
const val AS = "Assassin's Creed"
const val MEDITATION = "Meditation"
const val WORK = "Work"
const val STUDY = "Study"

const val I_RACING = "iRacing"
const val FORZA = "Forza Horizon"
const val F_1 = "F1"

const val PUBG = "PUBG: Battlegrounds"

const val SMITE = "Smite"
const val HALO = "Halo Infinite"
const val DOTA = "Dota 2"
const val BF2042 = "Battlefield 2042"
const val COD_BO = "Call of Duty: Black Ops Cold War"
const val TF2 = "Team Fortress 2"


const val DASH = "-"
const val DEMO_ACTIVITY_NAME = "demoactivity"

fun String.toGameStatsList(): List<Int> {
    return when (this) {
        FIFA -> listOf<Int>(R.string.goals_for, R.string.goals_against)
        SMITE, LOL -> listOf(R.string.kda)
        COD_MW, TOM_CLANCY, BF, HALO, COD_W, TF2, CS_GO, VALORANT -> listOf(R.string.kd)
        PUBG -> listOf(R.string.kills)
        NFL, NBA -> listOf(R.string.points_for, R.string.points_against)
        ROCKET_LEAGUE -> listOf(R.string.player_points, R.string.goals_for, R.string.goals_against)
        FORTNITE, APEX, OVERWATCH -> listOf(R.string.kills, R.string.deaths)
        DOTA -> listOf(R.string.kda)
        BF2042, COD_BO -> listOf(R.string.kd, R.string.score)
        else -> listOf()
    }
}

fun String.toMainStatType(): MainStatType {
    return when(this) {
        FIFA, SMITE, NFL, HALO, ROCKET_LEAGUE, OVERWATCH, DOTA, NBA, TOM_CLANCY, COD_BO, BF2042, TF2, LOL, CS_GO, VALORANT -> MainStatType.WIN_LOSS
        COD_MW, PUBG, BF, COD_W, FORTNITE, APEX, FORZA, F_1, I_RACING -> MainStatType.POSITION
        else -> MainStatType.NONE
    }
}

fun UserActivity.toTeammatesList(context: Context): String {
    val playerName = this.match.myPlayer.gamerName
    val teamString = this.match.team.joinToString { it.gamerName.lowercase() }
    return when (this.activityType.name) {
        LOL, VALORANT -> context.resources.getString(
            R.string.team_names_list,
            playerName,
            teamString
        )
        else -> playerName
    }
}

fun UserActivity.toSummaryTile(context: Context): List<SummaryTileInfo> {
    val paramsList = this.match.myPlayer.performanceParams
    return when (this.activityType.name) {
        CS_GO -> listOf(
            SummaryTileInfo(
                this.match.myPlayer.outcome.toCapitalize(),
                this.match.mapName,
            ),
            SummaryTileInfo(
                this.match.tileMetricText,
                this.match.tileMetricDescription
            ),
        )
        F_1, I_RACING, FORZA -> listOf(
            SummaryTileInfo(
                this.match.myPlayer.position.toString(),
                context.resources.getString(R.string.position),
            )
        )
        FIFA, NFL, ROCKET_LEAGUE, NBA -> listOf(
            SummaryTileInfo(
                this.match.myPlayer.outcome.toCapitalize(),
                this.match.myPlayer.gamerName,
            ),
            SummaryTileInfo(
                paramsList.firstOrNull()?.value?.toInt().toString() +
                        DASH +
                        paramsList.lastOrNull()?.value?.toInt().toString(),
                context.resources.getString(R.string.score),
            )
        )
        COD_MW, BF, COD_W, FORTNITE, APEX -> listOf(
            SummaryTileInfo(
                this.match.myPlayer.position.toString(),
                context.resources.getString(R.string.position),
            ),
            SummaryTileInfo(
                paramsList.firstOrNull { it.name == context.resources.getString(R.string.kd) }?.value.toString(),
                paramsList.firstOrNull { it.name == context.resources.getString(R.string.kd) }?.name.toString(),
            )
        )
        PUBG -> listOf(
            SummaryTileInfo(
                this.match.myPlayer.position.toString(),
                context.resources.getString(R.string.position),
            ),
            SummaryTileInfo(
                paramsList.firstOrNull()?.value.toString(),
                paramsList.firstOrNull()?.name.toString(),
            )
        )
        TOM_CLANCY, GENSHIN, MINECRAFT,
        ROBLOX, DESTINY, GTA, FINAL_FANTASY,
        CYBERPUNK, HOGWARTS, LOST_ARK, RUST,
        VALHEIM, ELDEN_RING, ARK, RDR2, GOW,
        AS, MEDITATION, WORK, STUDY -> emptyList()

        else -> listOf(
            SummaryTileInfo(
                this.match.myPlayer.outcome.toCapitalize(),
                this.match.myPlayer.characterName,
            ),
            SummaryTileInfo(
                paramsList.firstOrNull()?.value.toString(),
                paramsList.firstOrNull()?.name.toString(),
            ),
        )
    }
}

enum class MainStatType {
    WIN_LOSS, POSITION, NONE
}