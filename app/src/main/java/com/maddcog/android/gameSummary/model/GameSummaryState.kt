package com.maddcog.android.gameSummary.model

data class GameSummaryState(
    val isLoading: Boolean = false,
    val matchData: MatchData? = null,
    val error: String = "",
)