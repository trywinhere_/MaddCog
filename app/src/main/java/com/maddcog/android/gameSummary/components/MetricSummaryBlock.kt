package com.maddcog.android.gameSummary.components

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import com.maddcog.android.R
import com.maddcog.android.baseui.ui.theme.BoxDarkGrey
import com.maddcog.android.baseui.ui.theme.Spacing_12
import com.maddcog.android.baseui.ui.theme.Spacing_16
import com.maddcog.android.baseui.ui.theme.Spacing_20
import com.maddcog.android.baseui.ui.theme.Spacing_60
import com.maddcog.android.baseui.ui.theme.WhiteText16Bold
import com.maddcog.android.data.extensions.toLocalName
import com.maddcog.android.domain.entities.MentalType
import com.maddcog.android.gameSummary.extensions.PERCENT
import com.maddcog.android.gameSummary.extensions.YOU
import com.maddcog.android.gameSummary.extensions.toMetricColor
import com.maddcog.android.gameSummary.model.PlayerScreenSummary

@Composable
fun MetricSummaryBlock(
    metricName: String,
    playersList: List<PlayerScreenSummary>,
) {
    Column(
        Modifier
            .fillMaxWidth()
            .background(BoxDarkGrey)
    ) {
        Spacer(modifier = Modifier.height(Spacing_16))
        Text(
            text = metricName.toLocalName(LocalContext.current),
            style = WhiteText16Bold,
            modifier = Modifier.padding(start = Spacing_12)
        )
        Spacer(modifier = Modifier.height(Spacing_16))
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceEvenly
        ) {
            playersList.forEach {
                if (it.metric != MentalType.NONE.name)
                    PlayerSummaryBlock(
                        imageUrl = it.image,
                        metricValue = if (metricName != stringResource(id = R.string.kill_participation)) it.metric else it.metric + PERCENT,
                        username = it.username,
                        metricColor = metricName.toMetricColor(LocalContext.current),
                        it.username == YOU
                    )
            }
            repeat(5 - playersList.filter { it.metric != MentalType.NONE.name }.size) {
                Box(Modifier.size(Spacing_60))
            }
        }
        Spacer(modifier = Modifier.height(Spacing_20))
    }
}