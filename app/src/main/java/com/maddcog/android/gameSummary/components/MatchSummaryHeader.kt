package com.maddcog.android.gameSummary.components

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.statusBarsPadding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.BlendMode
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.layout.layoutId
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import coil.compose.AsyncImage
import com.maddcog.android.R
import com.maddcog.android.baseui.ui.theme.GameSummaryBannerTint
import com.maddcog.android.baseui.ui.theme.MATERIAL_DEFAULT_HEIGHT
import com.maddcog.android.baseui.ui.theme.Spacing_18
import com.maddcog.android.baseui.ui.theme.Spacing_5
import com.maddcog.android.baseui.ui.theme.Spacing_50
import com.maddcog.android.baseui.ui.theme.WhiteText14Bold
import com.maddcog.android.baseui.ui.theme.WhiteText24Bold
import com.maddcog.android.data.extensions.toLocalName
import com.maddcog.android.domain.entities.activity.UserActivity
import com.maddcog.android.gameSummary.extensions.toSummaryTile
import com.maddcog.android.gameSummary.extensions.toTeammatesList

@Composable
fun MatchSummaryHeader(
    modifier: Modifier = Modifier,
    userActivity: UserActivity,
) {

    val dayOfWeek = userActivity.timestamp.dayOfWeek().asText
    val dayOfMonth = userActivity.timestamp.dayOfMonth().asText
    val monthOfYear = userActivity.timestamp.monthOfYear().asText
    val matchDuration = (userActivity.match.end - userActivity.match.start) / 60

    Box(
        modifier = modifier
            .fillMaxWidth()
            .aspectRatio(1f)
    ) {
        AsyncImage(
            modifier = Modifier.fillMaxSize(),
            model = userActivity.activityType.imageBig,
            contentDescription = "",
            contentScale = ContentScale.Crop,
            colorFilter = ColorFilter.tint(GameSummaryBannerTint, BlendMode.Darken)
        )
        Column(
            modifier = Modifier.fillMaxSize(),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.SpaceEvenly
        ) {
            Text(
                text = userActivity.activityType.name,
                style = WhiteText24Bold,
                textAlign = TextAlign.Center,
                modifier = Modifier
                    .statusBarsPadding()
                    .padding(top = MATERIAL_DEFAULT_HEIGHT)
            )
            Text(
                text = if (!userActivity.isForTest) stringResource(
                    id = R.string.game_summary_date,
                    dayOfWeek,
                    dayOfMonth,
                    monthOfYear,
                    matchDuration
                ) else stringResource(
                    id = R.string.demoactivity_date_text,
                    matchDuration
                ),
                style = WhiteText14Bold,
                textAlign = TextAlign.Center
            )
            Row(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.Center
            ) {
                userActivity.toSummaryTile(LocalContext.current).forEach {
                    SummaryHeaderDoubleText(
                        upperText = it.upperText.toLocalName(LocalContext.current),
                        downText = it.lowerText.toLocalName(LocalContext.current),
                    )
                }
            }
            Text(
                modifier = Modifier.padding(horizontal = Spacing_18),
                text = userActivity.toTeammatesList(LocalContext.current),
                style = WhiteText14Bold,
                textAlign = TextAlign.Center
            )
            Row(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.Center
            ) {
                userActivity.match.team.forEach {
                    AsyncImage(
                        modifier = Modifier
                            .clip(RoundedCornerShape(Spacing_50))
                            .layoutId("team_member_image")
                            .height(Spacing_50)
                            .aspectRatio(1f)
                            .padding(horizontal = Spacing_5),
                        model = it.characterImage,
                        contentDescription = "",
                        contentScale = ContentScale.Crop,
                    )
                }
            }
        }
    }
}