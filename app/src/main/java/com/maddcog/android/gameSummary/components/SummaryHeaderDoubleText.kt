package com.maddcog.android.gameSummary.components

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import com.maddcog.android.baseui.ui.theme.Spacing_20
import com.maddcog.android.baseui.ui.theme.TickGreyText14
import com.maddcog.android.baseui.ui.theme.WhiteText16Bold

@Composable
fun SummaryHeaderDoubleText(
    upperText: String,
    downText: String,
) {
    Column(
        modifier = Modifier
            .wrapContentWidth().padding(Spacing_20),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(text = upperText, style = WhiteText16Bold)
        Text(text = downText, style = TickGreyText14)
    }
}