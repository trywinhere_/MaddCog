package com.maddcog.android.gameSummary

import android.os.Bundle
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.statusBarsPadding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import com.maddcog.android.R
import com.maddcog.android.baseui.components.BasicTopAppBar
import com.maddcog.android.baseui.ui.theme.*
import com.maddcog.android.data.bluetoothle.ConnectionState
import com.maddcog.android.data.extensions.roundNumbers
import com.maddcog.android.data.extensions.toLocalName
import com.maddcog.android.gameSummary.components.CustomCircle
import com.maddcog.android.gameSummary.components.CustomMentalCircle
import com.maddcog.android.gameSummary.components.LinearBarBlock
import com.maddcog.android.gameSummary.components.MatchBreakdownBlock
import com.maddcog.android.gameSummary.components.MatchSummaryHeader
import com.maddcog.android.gameSummary.components.MetricSummaryBlock
import com.maddcog.android.gameSummary.components.TeamScoreBarBlock
import com.maddcog.android.gameSummary.extensions.toMentalType
import com.maddcog.android.settings.components.BoldSectionHeader

@Composable
fun GameSummaryScreen(
    viewModel: GameSummaryViewModel,

    ) {
    val firebaseAnalytics: FirebaseAnalytics = Firebase.analytics
    val state = viewModel.state.value.matchData
    val avgParamsList = state?.currentGameData?.userAverages
    val activityNameEvent = state?.currentGameData?.gameName

    firebaseAnalytics.logEvent(
        "Activity_summary",
        Bundle().apply {
            this.putString(
                ACTIVITY_SUMMARY, "activity_type_name $activityNameEvent"
            )
        }
    )
    val bleConnectionState = viewModel.connectionState

    Column() {
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .padding(bottom = MATERIAL_DEFAULT_HEIGHT)
        ) {
            LazyColumn(
                modifier = Modifier
                    .background(Color.Black)
                    .fillMaxSize()
            ) {

                if (!viewModel.state.value.isLoading) {
                    item {
                        state?.userActivity?.let {
                            MatchSummaryHeader(userActivity = it)
                        }
                        Spacer(modifier = Modifier.height(Spacing_8))
                        BoldSectionHeader(
                            modifier = Modifier.padding(horizontal = Spacing_12),
                            name = R.string.match_summary_header
                        )
                        Spacer(modifier = Modifier.height(Spacing_30))
                        state?.userActivity?.let {
                            Row(
                                modifier = Modifier.fillMaxWidth(),
                                horizontalArrangement = Arrangement.Start
                            ) {
                                Spacer(modifier = Modifier.width(Spacing_30))
                                CustomCircle(
                                    value = it.match.myPlayer.grade.roundNumbers(),
                                    text = it.match.myPlayer.grade.roundNumbers().toString(),
                                    elementSize = Spacing_70,
                                    GameGrade
                                )
                                Spacer(modifier = Modifier.width(Spacing_30))
                                CustomMentalCircle(
                                    mentalType = it.match.myPlayer.fatigueText.toMentalType(),
                                    elementSize = Spacing_70,
                                    FatigueText,
                                    onDialClick = {
                                        if(bleConnectionState == ConnectionState.Connected) {
                                            firebaseAnalytics.logEvent(
                                                "live_activity_show_graph_fatigue",
                                                Bundle().apply {
                                                    this.putString(
                                                        LIVE_ACTIVITY_SHOW_GRAPH,
                                                        "live_activity_show_graph_fatigue $activityNameEvent fatigue"
                                                    )
                                                }
                                            )
                                        } else {
                                            firebaseAnalytics.logEvent(
                                                "activity_summary_show_graph_fatigue",
                                                Bundle().apply {
                                                    this.putString(
                                                        ACTIVITY_SUMMARY_SHOW_GRAPH,
                                                        "activity_summary_show_graph_fatigue $activityNameEvent fatigue"
                                                    )
                                                }
                                            )
                                        }
                                    }
                                )
                                Spacer(modifier = Modifier.width(Spacing_30))
                                CustomMentalCircle(
                                    mentalType = it.match.myPlayer.flowText.toMentalType(),
                                    elementSize = Spacing_70, FlowText,
                                    onDialClick = {
                                        if(bleConnectionState == ConnectionState.Connected) {
                                            firebaseAnalytics.logEvent(
                                                "live_activity_show_graph_flow",
                                                Bundle().apply {
                                                    this.putString(
                                                        LIVE_ACTIVITY_SHOW_GRAPH,
                                                        "live_activity_show_graph_flow $activityNameEvent flow"
                                                    )
                                                }
                                            )
                                        } else {
                                            firebaseAnalytics.logEvent(
                                                "activity_summary_show_graph_flow",
                                                Bundle().apply {
                                                    this.putString(
                                                        ACTIVITY_SUMMARY_SHOW_GRAPH,
                                                        "activity_summary_show_graph_flow $activityNameEvent flow"
                                                    )
                                                }
                                            )
                                        }
                                    }
                                )
                            }
                        }
                        Spacer(modifier = Modifier.height(Spacing_30))
                        state?.userActivity?.match?.myPlayer?.let {
                            TeamScoreBarBlock(
                                modifier = Modifier.padding(horizontal = Spacing_12),
                                metricValue = it.score.toDouble(),
                                scoreText = it.scoreText,
                                outcome = it.outcome
                            )
                        }
                        Spacer(modifier = Modifier.height(Spacing_30))
                        state?.userActivity?.match?.myPlayer?.performanceParams?.forEach {
                            LinearBarBlock(
                                modifier = Modifier.padding(horizontal = Spacing_12),
                                metricName = it.name.toLocalName(LocalContext.current),
                                metricValue = it.value,
                                metricAverage = avgParamsList?.find { avg ->
                                    avg.name == it.name
                                }?.avg ?: 0.0
                            )
                        }
                        BoldSectionHeader(
                            modifier = Modifier.padding(horizontal = Spacing_12),
                            name = R.string.match_breakdown_header
                        )
                        state?.userActivity?.match?.let {
                            MatchBreakdownBlock(
                                match = it,
                                userActivity = state.userActivity
                            )
                        }
                        Spacer(modifier = Modifier.height(Spacing_16))
                        BoldSectionHeader(
                            modifier = Modifier.padding(horizontal = Spacing_12),
                            name = R.string.team_summary_header
                        )
                        state?.paramsData?.forEach {
                            Spacer(modifier = Modifier.height(Spacing_12))
                            MetricSummaryBlock(metricName = it.key, playersList = it.value)
                        }
                    }
                }
            }

            BasicTopAppBar(
                title = R.string.match,
                color = Color.Transparent.copy(alpha = 0f),
                modifier = Modifier.statusBarsPadding()
            ) {
                viewModel.navigateBack()
            }

            if (viewModel.state.value.isLoading) {
                CircularProgressIndicator(
                    modifier = Modifier
                        .height(Spacing_40)
                        .width(Spacing_40)
                        .align(Alignment.Center),
                    color = Color.Red
                )
            }
        }
    }
}