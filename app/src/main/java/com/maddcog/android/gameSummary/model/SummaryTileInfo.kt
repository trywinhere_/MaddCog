package com.maddcog.android.gameSummary.model

data class SummaryTileInfo(
    val upperText: String,
    val lowerText: String,
)