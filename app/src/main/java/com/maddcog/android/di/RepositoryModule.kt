package com.maddcog.android.di

import com.maddcog.android.domain.api.*
import com.maddcog.android.domain.repository.*
import com.maddcog.android.livegamedata.aimgame.AimGameRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
abstract class RepositoryModule {

    @Binds
    @Singleton
    abstract fun bindAuthRepository(impl: AuthRepositoryImpl): IAuthRepository

    @Binds
    @Singleton
    abstract fun bindDashboardRepository(impl: DashboardRepositoryImpl): IDashboardRepository

    @Binds
    @Singleton
    abstract fun bindUserDataRepository(impl: UserDataRepositoryImpl): IUserDataRepository

    @Binds
    @Singleton
    abstract fun bindAimGameRepository(impl: AimGameRepositoryImpl): IAimGameRepository

    @Binds
    @Singleton
    abstract fun bindLiveGameDataRepository(impl: LiveGameDataRepositoryImpl): ILiveGameDataRepository
}