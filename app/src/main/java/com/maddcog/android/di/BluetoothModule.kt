package com.maddcog.android.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothManager
import android.content.Context
import com.maddcog.android.data.bluetoothle.BLEReceiveManager
import com.maddcog.android.data.bluetoothle.IBleReceiveManager
import com.maddcog.android.domain.api.ISharedPreferencesStorage
import com.maddcog.android.data.sensordatarecorder.ISensorDataRecorder
import com.maddcog.android.data.sensordatarecorder.IStorage
import com.maddcog.android.data.sensordatarecorder.RawDataStorage
import com.maddcog.android.data.sensordatarecorder.SensorDataRecorder

@Module
@InstallIn(SingletonComponent::class)
object BluetoothModule {

    @Provides
    @Singleton
    fun provideBluetoothAdapter(@ApplicationContext context: Context): BluetoothAdapter {
        val manager = context.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        return manager.adapter
    }

    @Provides
    @Singleton
    fun provideBleReceiveManager(
        sensorDataRecorder: ISensorDataRecorder,
        @ApplicationContext context: Context,
        bluetoothAdapter: BluetoothAdapter,
        preferences: ISharedPreferencesStorage,
    ): IBleReceiveManager {
        return BLEReceiveManager(sensorDataRecorder, bluetoothAdapter, context, preferences)
    }

    @Provides
    @Singleton
    fun provideSensorDataRecorder(
        rawDataStorage: IStorage,
    ): ISensorDataRecorder = SensorDataRecorder(rawDataStorage)

    @Provides
    @Singleton
    fun provideRawDataStorage(): IStorage = RawDataStorage()
}