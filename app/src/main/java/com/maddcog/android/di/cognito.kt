package com.maddcog.android.di

import android.content.Context
import com.amazonaws.ClientConfiguration
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserPool
import com.amazonaws.regions.Regions
import com.maddcog.android.R
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object CognitoUserPool {

    @Provides
    @Singleton
    fun providesCognitoUserPool(
        @ApplicationContext context: Context
    ): CognitoUserPool = CognitoUserPool(
        context,
        context.resources.getString(R.string.user_pool_id),
        context.resources.getString(R.string.client_id),
        "",
        ClientConfiguration(),
        Regions.US_WEST_2
    )
}