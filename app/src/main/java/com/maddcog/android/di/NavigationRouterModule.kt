package com.maddcog.android.di

import android.content.Context
import com.maddcog.android.app.navigation.*
import com.maddcog.android.auth.IAuthRouter
import com.maddcog.android.dashboard.IDashboardRouter
import com.maddcog.android.gameSummary.IGameSummaryRouter
import com.maddcog.android.liveGameSummary.ILiveGameSummaryRouter
import com.maddcog.android.livegamedata.ILiveGameDataRouter
import com.maddcog.android.settings.ISettingsRouter
import com.maddcog.android.trends.ITrendsDetailsRouter
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class NavigationRouterModule {

    @Provides
    @Singleton
    fun provideAuthRouter(@ApplicationContext context: Context): IAuthRouter =
        AuthRouterImpl(context)

    @Provides
    @Singleton
    fun provideSettingsRouter(@ApplicationContext context: Context): ISettingsRouter =
        SettingsRouterImpl(context)

    @Provides
    @Singleton
    fun provideDashboardsRouter(@ApplicationContext context: Context): IDashboardRouter =
        DashboardsRouterImpl(context)

    @Provides
    @Singleton
    fun provideTrendsDetailsRouter(@ApplicationContext context: Context): ITrendsDetailsRouter =
        TrendsDetailsRouterImpl(context)

    @Provides
    @Singleton
    fun provideLiveGameDataRouter(@ApplicationContext context: Context): ILiveGameDataRouter =
        LiveGameDataRouterImpl(context)

    @Provides
    @Singleton
    fun provideGameSummaryRouter(@ApplicationContext context: Context): IGameSummaryRouter =
        GameSummaryRouterImpl(context)

    @Provides
    @Singleton
    fun provideLiveGameSummaryRouter(@ApplicationContext context: Context): ILiveGameSummaryRouter =
        LiveGameSummaryRouterImpl(context)
}