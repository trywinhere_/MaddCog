package com.maddcog.android.di

import android.content.Context
import com.maddcog.android.data.storage.ResourceProviderImpl
import com.maddcog.android.data.storage.SharedPreferencesStorageImpl
import com.maddcog.android.domain.api.IResourcesProvider
import com.maddcog.android.domain.api.ISharedPreferencesStorage
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class StorageModule {

    @Singleton
    @Provides
    fun provideSharedPrefStorage(@ApplicationContext context: Context): ISharedPreferencesStorage {
        return SharedPreferencesStorageImpl(context)
    }

    @Provides
    @Singleton
    fun provideResourceProvider(
        @ApplicationContext context: Context,
    ): IResourcesProvider = ResourceProviderImpl(context)
}