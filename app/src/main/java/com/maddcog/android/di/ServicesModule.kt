package com.maddcog.android.di

import com.maddcog.android.data.network.MaddcogApi
import com.maddcog.android.data.network.services.ActivityListApiServiceImpl
import com.maddcog.android.data.network.services.ProfileApiServiceImpl
import com.maddcog.android.data.network.services.SensorDataApiServiceImpl
import com.maddcog.android.data.network.services.TeamsApiServiceImpl
import com.maddcog.android.domain.api.IActivitiesApiService
import com.maddcog.android.domain.api.IProfileApiService
import com.maddcog.android.domain.api.ISensorDataApiService
import com.maddcog.android.domain.api.ISharedPreferencesStorage
import com.maddcog.android.domain.api.ITeamsApiService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class ServicesModule {

    @Provides
    @Singleton
    fun provideActivitiesListApiService(
        api: MaddcogApi,
    ): IActivitiesApiService =
        ActivityListApiServiceImpl(api)

    @Provides
    @Singleton
    fun provideProfileApiService(
        api: MaddcogApi,
        storage: ISharedPreferencesStorage,
    ): IProfileApiService =
        ProfileApiServiceImpl(api, storage)

    @Provides
    @Singleton
    fun provideTeamsApiService(
        api: MaddcogApi,
    ): ITeamsApiService =
        TeamsApiServiceImpl(api)

    @Provides
    @Singleton
    fun provideSensorApiService(
        api: MaddcogApi,
    ): ISensorDataApiService =
        SensorDataApiServiceImpl(api)
}