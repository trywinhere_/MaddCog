package com.maddcog.android.data.sensordatarecorder

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.maddcog.android.data.network.dto.rawActivity.RawMentalPerformance
import com.maddcog.android.data.sensordatarecorder.utils.EEG
import com.maddcog.android.data.sensordatarecorder.utils.HR
import com.maddcog.android.data.sensordatarecorder.utils.SampleEEG
import com.maddcog.android.data.sensordatarecorder.utils.SampleHR
import com.maddcog.android.data.sensordatarecorder.utils.SensorDataActivity
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import javax.inject.Inject

class RawDataStorage @Inject constructor() : IStorage {

    private val _batteryLevel = MutableStateFlow(0)
    override val batteryLevel: StateFlow<Int> get() = _batteryLevel.asStateFlow()

    private val _mentalPerformanceQuantity = MutableStateFlow(0)
    override val mentalPerformanceQuantity: StateFlow<Int> get() = _mentalPerformanceQuantity.asStateFlow()

    private val _lastMentalPerformance = MutableStateFlow(RawMentalPerformance(0.0, 0.0, 0.0, 0))
    override val lastMentalPerformance: StateFlow<RawMentalPerformance> get() = _lastMentalPerformance.asStateFlow()


    var rawDataActivity = SensorDataActivity()

    var mentalPerformanceList: MutableList<RawMentalPerformance> = mutableListOf()

    override suspend fun getRawActivity(): SensorDataActivity {
        return rawDataActivity
    }

    override suspend fun getHrData(): HashMap<String, List<HR>> {
        return rawDataActivity.hrRawData
    }

    override suspend fun getEegData(): HashMap<String, List<EEG>> {
        return rawDataActivity.eegRawData
    }

    override suspend fun addHrData(identifier: String, data: SampleHR) {
        rawDataActivity.addHRdata(identifier, data)
    }

    override suspend fun addEegData(identifier: String, data: SampleEEG) {
        rawDataActivity.addEEGdata(identifier, data)
    }

    override suspend fun clear() {
        rawDataActivity = SensorDataActivity()
        mentalPerformanceList.clear()
    }

    override suspend fun saveMentalPerformance(mentalPerformance: RawMentalPerformance) {
        mentalPerformanceList.add(mentalPerformance)
        _mentalPerformanceQuantity.value = mentalPerformanceList.size
        _lastMentalPerformance.value = mentalPerformance
    }

    override suspend fun getMentalPerformanceList(): List<RawMentalPerformance> {
        return mentalPerformanceList
    }

    override suspend fun saveBatteryLevel(currentLevel: Int) {
        _batteryLevel.value = currentLevel
    }

    override suspend fun getBatteryLevel(): Int {
        return batteryLevel.value
    }
}