package com.maddcog.android.data.storage

import android.content.Context
import android.content.SharedPreferences
import com.maddcog.android.baseui.extensions.orNull
import com.maddcog.android.data.storage.entities.ActivityItemJson
import com.maddcog.android.data.storage.entities.TeamJson
import com.maddcog.android.data.storage.entities.UserActivityJson
import com.maddcog.android.data.storage.entities.UserProfileJson
import com.maddcog.android.data.storage.entities.toData
import com.maddcog.android.data.storage.entities.toDomain
import com.maddcog.android.domain.api.ISharedPreferencesStorage
import com.maddcog.android.domain.entities.ActivityItem
import com.maddcog.android.domain.entities.RegionType
import com.maddcog.android.domain.entities.activity.UserActivity
import com.maddcog.android.domain.entities.team.Team
import com.maddcog.android.domain.entities.user.UserProfile
import com.maddcog.android.settings.presentation.selectLanguage.model.Language
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import javax.inject.Inject

class SharedPreferencesStorageImpl @Inject constructor(private val context: Context) :
    ISharedPreferencesStorage {

    private val sharedPref: SharedPreferences by lazy {
        context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
    }

    companion object {
        const val SHARED_PREF_NAME = "shared_pref_name"
        const val KEY_SHOW_SIGNIN_SCREEN = "key_show_welcome_screen"
        const val KEY_SAVE_EMAIL = "key_save_email"
        const val KEY_SAVE_PASSWORD = "key_save_password"
        const val KEY_SAVE_TOKEN = "key_save_token"
        const val KEY_SAVE_USER_INFO = "key_save_user_info"
        const val KEY_COGNITO_TOKEN = "key_cognito_token"
        const val PREF_USER_PROFILE = "key_save_profile"
        const val PREF_USER_NAME = "key_save_username"
        const val KEY_SIGN_IN_LOGGINING_IN = "key_sign_in_loggining_in"
        const val KEY_NOTIFICATIONS = "key_notifications"
        const val KEY_GOOGLE_SIN_IN = "key_google_sin_in"
        const val KEY_GOOGLE_ACT = "key_google_act"
        const val KEY_GAME_NAME = "key_game_name"
        const val KEY_GAME_URL = "key_game_url"
        const val KEY_TAGS = "key_tags_name"
        const val KEY_TEAM_NAME = "key_team_name"
        const val KEY_TEAMS = "key_teams_list"
        const val KEY_ACTIVITY_ITEMS = "key_activity_item_list"
        const val KEY_USER_ACTIVITIES = "key_user_activity_list"
        const val KEY_DEVICE = "key_saved_device"
        const val KEY_NEW_GAME_SESSION = "key_new_game_session"
        const val KEY_NEW_GAME_UUID = "key_new_game_uuid"
    }

    override fun clearOnLogout(): Boolean {
        return sharedPref.edit().clear().commit()
    }

    override fun saveUserName(name: String) {
        sharedPref
            .edit()
            .putString(PREF_USER_NAME, name)
            .apply()
    }

    override fun getLanguageList(): List<Language> {
        return Language.values().toList()
    }

    override fun getRegionList(): List<RegionType> {
        return RegionType.values().toList()
    }

    override fun getUserName(): String? {
        return sharedPref.getString(PREF_USER_NAME, toString())
    }

    override fun saveCognitoToken(token: String) {
        sharedPref
            .edit()
            .putString(KEY_COGNITO_TOKEN, token)
            .apply()
    }

    override fun getCognitoToken(): String? {
        return sharedPref.getString(KEY_COGNITO_TOKEN, toString())
    }

    override fun saveToken(token: String) {
        sharedPref
            .edit()
            .putString(KEY_SAVE_TOKEN, token)
            .apply()
    }

    override fun getToken(): String? {
        return sharedPref.getString(KEY_SAVE_TOKEN, toString())
    }

    override fun saveEmail(email: String) {
        sharedPref
            .edit()
            .putString(KEY_SAVE_EMAIL, email)
            .apply()
    }

    override fun getEmail(): String? {
        return sharedPref.getString(KEY_SAVE_EMAIL, toString())
    }

    override fun savePassword(password: String) {
        sharedPref
            .edit()
            .putString(KEY_SAVE_PASSWORD, password)
            .apply()
    }

    override fun getPassword(): String? {
        return sharedPref.getString(KEY_SAVE_PASSWORD, toString())
    }

    override fun saveStateSignInScreen(showing: Boolean) {
        sharedPref
            .edit()
            .putBoolean(KEY_SHOW_SIGNIN_SCREEN, showing)
            .apply()
    }

    override fun getStateSignInScreen(): Boolean {
        return sharedPref.getBoolean(KEY_SHOW_SIGNIN_SCREEN, true)
    }

    override fun registerSharedPrefsListener(changeListener: SharedPreferences.OnSharedPreferenceChangeListener) {
        sharedPref.registerOnSharedPreferenceChangeListener(changeListener)
    }

    override fun unregisterSharedPrefsListener(changeListener: SharedPreferences.OnSharedPreferenceChangeListener) {
        sharedPref.unregisterOnSharedPreferenceChangeListener(changeListener)
    }

    override fun saveTeamsList(teams: List<Team>) {
        val encodeToStringList = teams.map { Json.encodeToString(it.toData()) }
        sharedPref.edit().putStringSet(KEY_TEAMS, encodeToStringList.toSet()).apply()
    }

    override fun getTeamsList(): List<Team>? {
        return orNull {
            sharedPref
                .getStringSet(KEY_TEAMS, setOf())
                ?.takeIf { it.isNotEmpty() }
                ?.let {
                    it.toList().map { raw ->
                        Json.decodeFromString(TeamJson.serializer(), raw).toDomain()
                    }
                }
        }
    }

    override fun saveNewSessionInfo(activityItem: ActivityItem) {
        val encodedToString = Json.encodeToString(activityItem.toData())
        sharedPref.edit().putString(KEY_NEW_GAME_SESSION, encodedToString).apply()
    }

    override fun getNewSessionInfo(): ActivityItem? {
        return orNull {
            sharedPref.getString(KEY_NEW_GAME_SESSION, null)
                ?.takeIf { it.isNotEmpty() }
                ?.let { raw -> Json.decodeFromString(ActivityItemJson.serializer(), raw).toDomain() }
        }
    }

    override fun saveNewSessionUUID(uuid: String) {
        sharedPref
            .edit()
            .putString(KEY_NEW_GAME_UUID, uuid)
            .apply()
    }

    override fun getNewSessionUUID(): String? {
        return sharedPref.getString(KEY_NEW_GAME_UUID, "")
    }

    override fun saveActivityItemList(activityItems: List<ActivityItem>) {
        val encodedToStringList = activityItems.map { Json.encodeToString(it.toData()) }
        sharedPref.edit().putStringSet(KEY_ACTIVITY_ITEMS, encodedToStringList.toSet()).apply()
    }

    override fun getActivityItemList(): List<ActivityItem>? {
        return orNull {
            sharedPref
                .getStringSet(KEY_ACTIVITY_ITEMS, setOf())
                ?.takeIf { it.isNotEmpty() }
                ?.let {
                    it.toList().map { raw ->
                        Json.decodeFromString(ActivityItemJson.serializer(), raw).toDomain()
                    }
                }
        }
    }

    override fun saveUserActivityList(userActivityList: List<UserActivity>) {
        val encodedToStringList = userActivityList.map { Json.encodeToString(it.toData()) }
        sharedPref.edit().putStringSet(KEY_USER_ACTIVITIES, encodedToStringList.toSet()).apply()
    }

    override fun getUserActivityList(): List<UserActivity>? {
        return orNull {
            sharedPref
                .getStringSet(KEY_USER_ACTIVITIES, setOf())
                ?.takeIf { it.isNotEmpty() }
                ?.let {
                    it.toList().map { raw ->
                        Json.decodeFromString(UserActivityJson.serializer(), raw).toDomain()
                    }
                }
        }
    }

    override suspend fun saveUserProfile(profile: UserProfile) {
        val encodeToString = Json.encodeToString(profile.toData())
        sharedPref.edit().putString(PREF_USER_PROFILE, encodeToString).apply()
    }

    override suspend fun getUserProfile(): UserProfile? {
        return orNull {
            sharedPref
                .getString(PREF_USER_PROFILE, null)
                ?.takeIf { it.isNotEmpty() }
                ?.let {
                    Json.decodeFromString(UserProfileJson.serializer(), it).toDomain()
                }
        }
    }

    override fun saveStateSignInInLogginingIn(showing: Boolean) {
        sharedPref
            .edit()
            .putBoolean(KEY_SIGN_IN_LOGGINING_IN, showing)
            .apply()
    }

    override fun getStateSignInInLogginingIn(): Boolean {
        return sharedPref.getBoolean(KEY_SIGN_IN_LOGGINING_IN, true)
    }

    override fun saveDeviceStatus(isConnected: Boolean) {
        sharedPref.edit().putBoolean(KEY_DEVICE, isConnected).apply()
    }

    override fun getDeviceStatus(): Boolean {
        return sharedPref.getBoolean(KEY_DEVICE, false)
    }

    override fun saveStateNotifications(showing: Boolean) {
        sharedPref
            .edit()
            .putBoolean(KEY_NOTIFICATIONS, showing)
            .apply()
    }

    override fun getStateNotifications(): Boolean {
        return sharedPref.getBoolean(KEY_NOTIFICATIONS, true)
    }

    override fun saveStateGoogleSignIn(showing: Boolean) {
        sharedPref
            .edit()
            .putBoolean(KEY_GOOGLE_SIN_IN, showing)
            .apply()
    }

    override fun getStateGoogleSignIn(): Boolean {
        return sharedPref.getBoolean(KEY_GOOGLE_SIN_IN, true)
    }

    override fun saveGoogleSignIn(act: Boolean) {
        sharedPref
            .edit()
            .putBoolean(KEY_GOOGLE_ACT, act)
            .apply()
    }

    override fun getGoogleSignIn(): Boolean {
        return sharedPref.getBoolean(KEY_GOOGLE_ACT, false)
    }

    override fun saveGameName(gameName: String) {
        sharedPref
            .edit()
            .putString(KEY_GAME_NAME, gameName)
            .apply()
    }

    override fun getGameName(): String? {
        return sharedPref.getString(KEY_GAME_NAME, "")
    }

    override fun saveTags(tags: Set<String>) {
        sharedPref
            .edit()
            .putStringSet(KEY_TAGS, tags)
            .apply()
    }

    override fun getTags(): MutableSet<String>? {
        return sharedPref.getStringSet(KEY_TAGS, setOf())
    }

    override fun saveTeamName(teamName: String) {
        sharedPref
            .edit()
            .putString(KEY_TEAM_NAME, teamName)
            .apply()
    }

    override fun getTeamName(): String? {
        return sharedPref.getString(KEY_TEAM_NAME, "")
    }

    override fun saveGameUrl(gameUrl: String) {
        sharedPref
            .edit()
            .putString(KEY_GAME_URL, gameUrl)
            .apply()
    }

    override fun getGameUrl(): String? {
        return sharedPref.getString(KEY_GAME_URL, "")
    }
}