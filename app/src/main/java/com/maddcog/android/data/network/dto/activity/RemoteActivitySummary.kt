package com.maddcog.android.data.network.dto.activity

import com.google.gson.annotations.SerializedName
import kotlinx.serialization.Serializable

@Serializable
data class RemoteActivitySummary(
    @SerializedName("fatigue_zone_high")
    val fatigueZoneHigh: Double?,
    @SerializedName("fatigue_zone_normal")
    val fatigueZoneNormal: Double?,
    @SerializedName("fatigue_zone_low")
    val fatigueZoneLow: Double?,
    @SerializedName("flow_zone_normal")
    val flowZoneNormal: Double?,
    @SerializedName("flow_zone_high")
    val flowZoneHigh: Double?,
    @SerializedName("fatigue_end_avg")
    val fatigueEndAvg: Double?,
    @SerializedName("flow_avg")
    val flowAvg: Double?,
    @SerializedName("flow_zone_low")
    val flowZoneLow: Double?,
    @SerializedName("fatigue_avg")
    val fatigueAvg: Double?,
)