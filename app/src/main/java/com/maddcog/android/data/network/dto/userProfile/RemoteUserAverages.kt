package com.maddcog.android.data.network.dto.userProfile

import com.google.gson.annotations.SerializedName
import kotlinx.serialization.Serializable

@Serializable
data class RemoteUserAverages(
    @SerializedName("avg")
    val avg: Double?,
    @SerializedName("median")
    val median: Double?,
    @SerializedName("name")
    val name: String?,
)