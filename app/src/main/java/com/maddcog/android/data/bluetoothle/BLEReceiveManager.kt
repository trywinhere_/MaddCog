package com.maddcog.android.data.bluetoothle

import android.annotation.SuppressLint
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothGatt
import android.bluetooth.BluetoothGattCallback
import android.bluetooth.BluetoothGattCharacteristic
import android.bluetooth.BluetoothGattDescriptor
import android.bluetooth.BluetoothProfile
import android.bluetooth.le.ScanCallback
import android.bluetooth.le.ScanResult
import android.bluetooth.le.ScanSettings
import android.content.Context
import android.content.ContentValues.TAG
import android.util.Log
import com.maddcog.android.data.sensordatarecorder.ISensorDataRecorder
import com.maddcog.android.domain.api.ISharedPreferencesStorage
import com.maddcog.android.domain.utils.ble.Resource
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject

@SuppressLint("MissingPermission")
class BLEReceiveManager @Inject constructor(
    private val sensorDataRecorder: ISensorDataRecorder,
    private val bluetoothAdapter: BluetoothAdapter,
    private val context: Context,
    private val preferences: ISharedPreferencesStorage,
) : IBleReceiveManager {

    var count = 0
    var batteryCount = 0
    private val DEVICE_NAME = "Game Band"

    val MADDCOG_HRV_SERVICE = "5000df9c-9a41-47ac-a82a-7c301d509580"
    val MADDCOG_HRV_CHAR = "dab33654-9a44-4107-bb28-3abc12f52688"

    val MADDCOG_EEG_SERVICE = "b62ea99a-5371-4cab-9ab9-7f338623b1a3"
    val MADDCOG_EEG_CHAR = "1688c50a-1344-4358-a43c-8bed435a11c7"

    val MADDCOG_HEARTRATE_SERVICE = "0000180d-0000-1000-8000-00805f9b34fb"
    val MADDCOG_HEARTRATE_CHAR = "00002a37-0000-1000-8000-00805f9b34fb"

    val MADDCOG_BATTERY_SERVICE = "0000180f-0000-1000-8000-00805f9b34fb"
    val MADDCOG_BATTERY_CHAR = "00002a19-0000-1000-8000-00805f9b34fb"


    private val _data = MutableStateFlow<Resource<GameBandResult>>(Resource.Loading<GameBandResult>())
    override val data: StateFlow<Resource<GameBandResult>> get() = _data.asStateFlow()

    private val _dataAlerts = MutableStateFlow<ConnectionState>(ConnectionState.NotDefined)
    override val dataAlerts: StateFlow<ConnectionState> get() = _dataAlerts.asStateFlow()

    private val bleScanner by lazy {
        bluetoothAdapter.bluetoothLeScanner
    }

    private val scanSettings = ScanSettings.Builder()
        .setScanMode(ScanSettings.SCAN_MODE_BALANCED)
        .build()

    private var bleGatt: BluetoothGatt? = null

    private var isScanning = false

    private var gameBandDevice: BluetoothDevice? = null

    private val coroutineScope = CoroutineScope(Dispatchers.IO)

    private var currentConnectionAttempt = 1
    private var MAXIMUM_CONNECTION_ATTEMPT = 5

    private val scanCallback = object : ScanCallback() {
        override fun onScanResult(callbackType: Int, result: ScanResult) {
            if (result.device.name == DEVICE_NAME) {
                _dataAlerts.value = ConnectionState.DeviceFound
                _data.value = Resource.Success(data = GameBandResult(ConnectionState.DeviceFound))
                preferences.saveDeviceStatus(true)

                if (isScanning) {
                    gameBandDevice = result.device
                    isScanning = false
                    bleScanner.stopScan(this)
                }
            }
        }
    }

    private val reconnectScanCallback = object : ScanCallback() {
        override fun onScanResult(callbackType: Int, result: ScanResult) {
            if (result.device.name == DEVICE_NAME) {
                _dataAlerts.value = ConnectionState.DeviceFound
                _data.value = Resource.Success(data = GameBandResult(ConnectionState.DeviceFound))

                if (isScanning) {
                    gameBandDevice = result.device
                    isScanning = false
                    bleScanner.stopScan(this)
                    gameBandDevice?.connectGatt(context, false, gattCallback)
                }
            }
        }
    }

    override fun connectToGameBand() {
        gameBandDevice?.connectGatt(context, false, gattCallback)
    }

    private val gattCallback = object : BluetoothGattCallback() {
        override fun onConnectionStateChange(gatt: BluetoothGatt, status: Int, newState: Int) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                if (newState == BluetoothProfile.STATE_CONNECTED) {
                    coroutineScope.launch {
                        _dataAlerts.value = ConnectionState.Connected
                        _data.value = Resource.Success(data = GameBandResult(connectionState = ConnectionState.Connected))
                    }
                    bleGatt = gatt
                    preferences.saveDeviceStatus(true)
                    gatt.discoverServices()
                } else if (newState == BluetoothProfile.STATE_DISCONNECTED || newState == BluetoothProfile.STATE_DISCONNECTING) {
                    coroutineScope.launch {
                        _dataAlerts.value = ConnectionState.Disconnected
                        _data.value = Resource.Success(data = GameBandResult(ConnectionState.Disconnected))
                    }
                    preferences.saveDeviceStatus(false)
                    gatt.close()
                } else {
                    gatt.close()
                    currentConnectionAttempt += 1
                    if (currentConnectionAttempt <= MAXIMUM_CONNECTION_ATTEMPT) {
                        startReceiving()
                    } else {
                        coroutineScope.launch {
                            _dataAlerts.value = ConnectionState.CannotFoundDevice
                            _data.value = Resource.Error(errorMessage = "Could not connect to device")
                        }
                    }
                }
            } else if (status == BluetoothGatt.GATT_FAILURE) {
                coroutineScope.launch {
                    _dataAlerts.value = ConnectionState.CannotFoundDevice
                    _data.value = Resource.Error(errorMessage = "Could not connect to device")
                }
            }
        }

        override fun onServicesDiscovered(gatt: BluetoothGatt, status: Int) {
            with(gatt) {
                printGattTable()
                gatt.requestMtu(517)
            }
        }

        override fun onMtuChanged(gatt: BluetoothGatt, mtu: Int, status: Int) {
            val characteristicsList = listOf(
                findCharacteristic(MADDCOG_EEG_SERVICE, MADDCOG_EEG_CHAR),
                findCharacteristic(MADDCOG_HRV_SERVICE, MADDCOG_HRV_CHAR),
            )
            val filteredList = characteristicsList.filterNotNull()
            if (filteredList.isEmpty()) {
                coroutineScope.launch {
                    _dataAlerts.value = ConnectionState.CannotFoundDevice
                    _data.value = Resource.Error(errorMessage = "Could not find characteristics")
                }
            }
            setHrvNotifications(gatt)
        }

        override fun onCharacteristicChanged(
            gatt: BluetoothGatt,
            characteristic: BluetoothGattCharacteristic
        ) {
            with(characteristic) {
                if (batteryCount < 10) {
                    batteryCount += 1
                } else {
                    batteryCount = 0
                    readBatteryCharacteristic()
                }
                when (this.uuid) {
                    UUID.fromString(MADDCOG_HRV_CHAR) -> {
                        //XX XX XX XX XX XX
                        val hrValue = extractHeartRate(this)
                        Log.d("MADDCOG_HRV", hrValue.toString())
                        val gameBandResult = GameBandResult(connectionState = ConnectionState.Connected)

                        coroutineScope.launch {
                            _dataAlerts.value = ConnectionState.Connected
                            sensorDataRecorder.parseHeartRateMaddcog(value)
                            _data.value = Resource.Success(data = gameBandResult)
                        }
                    }

                    UUID.fromString(MADDCOG_EEG_CHAR) -> {
                        //XX XX XX XX XX XX
                        val eegValue = value.toHexString()
                        Log.d("MADDCOG_EEG", eegValue)
                        val gameBandResult = GameBandResult(connectionState = ConnectionState.Connected)

                        coroutineScope.launch {
                            _dataAlerts.value = ConnectionState.Connected
                            sensorDataRecorder.parseEEGMaddcog(value)
                            _data.value = Resource.Success(data = gameBandResult)
                        }
                    }
                    else -> {}
                }
            }
        }

        override fun onCharacteristicRead(
            gatt: BluetoothGatt,
            characteristic: BluetoothGattCharacteristic,
            status: Int
        ) {
            with(characteristic) {
                when (status) {
                    BluetoothGatt.GATT_SUCCESS -> {
                        when (this.uuid) {
                            UUID.fromString(MADDCOG_BATTERY_CHAR) -> {
                                //XX XX XX XX XX XX
                                val gameBandResult = GameBandResult(connectionState = ConnectionState.Connected)
                                coroutineScope.launch {
                                    val battery = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0)
                                    sensorDataRecorder.parseBatteryMaddcog(battery)
                                    _dataAlerts.value = ConnectionState.Connected
                                    _data.value = Resource.Success(data = gameBandResult)
                                }
                            }
                        }
                        Log.i("BluetoothGattCallback", "MADDCOG_BATTERY $uuid:\n${value.toHexString()}")
                    }
                    BluetoothGatt.GATT_READ_NOT_PERMITTED -> { Log.e("BluetoothGattCallback", "Read not permitted for $uuid!") }
                    else -> { Log.e("BluetoothGattCallback", "Characteristic read failed for $uuid, error: $status") }
                }
            }
        }

        override fun onDescriptorWrite(
            gatt: BluetoothGatt,
            descriptor: BluetoothGattDescriptor?,
            status: Int
        ) {
            super.onDescriptorWrite(gatt, descriptor, status)
            when (count) {
                0 -> {
                    count = 1
                    setEegNotifications(gatt)
                }
                1 -> {
                    count = 2
                    setHeartRateNotifications(gatt)
                }
                2 -> {
                    count = 3
                    setBatteryNotifications(gatt)
                }
            }
        }
    }

    private fun setEegNotifications(gatt: BluetoothGatt) {
        val eegChar = gatt.getService(UUID.fromString(MADDCOG_EEG_SERVICE))
            .getCharacteristic(UUID.fromString(MADDCOG_EEG_CHAR))
        gatt.setCharacteristicNotification(eegChar, true)
        eegChar.writeType = BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT
        val descriptor = eegChar.getDescriptor(DEFAULT_DESCRIPTOR)
        if (descriptor != null) {
            descriptor.value = BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE
            gatt.writeDescriptor(descriptor)
        }
    }

    private fun setHrvNotifications(gatt: BluetoothGatt) {
        val eegChar = gatt.getService(UUID.fromString(MADDCOG_HRV_SERVICE))
            .getCharacteristic(UUID.fromString(MADDCOG_HRV_CHAR))
        gatt.setCharacteristicNotification(eegChar, true)
        eegChar.writeType = BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT
        val descriptor = eegChar.getDescriptor(DEFAULT_DESCRIPTOR)
        if (descriptor != null) {
            descriptor.value = BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE
            gatt.writeDescriptor(descriptor)
        }
    }

    private fun setHeartRateNotifications(gatt: BluetoothGatt) {
        val eegChar = gatt.getService(UUID.fromString(MADDCOG_HEARTRATE_SERVICE))
            .getCharacteristic(UUID.fromString(MADDCOG_HEARTRATE_CHAR))
        gatt.setCharacteristicNotification(eegChar, true)
        eegChar.writeType = BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT
        val descriptor = eegChar.getDescriptor(DEFAULT_DESCRIPTOR)
        if (descriptor != null) {
            descriptor.value = BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE
            gatt.writeDescriptor(descriptor)
        }
    }

    private fun setBatteryNotifications(gatt: BluetoothGatt) {
        val batteryChar = gatt.getService(UUID.fromString(MADDCOG_BATTERY_SERVICE))
            .getCharacteristic(UUID.fromString(MADDCOG_BATTERY_CHAR))
        gatt.setCharacteristicNotification(batteryChar, true)
        batteryChar.writeType = BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT
        val descriptor = batteryChar.getDescriptor(DEFAULT_DESCRIPTOR)
        if (descriptor != null) {
            descriptor.value = BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE
            gatt.writeDescriptor(descriptor)
        }
    }

    private fun findCharacteristic(
        serviceUUID: String,
        characteristicsUUID: String
    ): BluetoothGattCharacteristic? {
        return bleGatt?.services?.find { service ->
            service.uuid.toString() == serviceUUID
        }?.characteristics?.find { characteristics ->
            characteristics.uuid.toString() == characteristicsUUID
        }
    }

    override fun startSearchForReconnect() {
        isScanning = true
        bleScanner.startScan(null, scanSettings, reconnectScanCallback)
    }

    override fun startReceiving() {
        coroutineScope.launch {
            _dataAlerts.value = ConnectionState.CurrentlyInitializing
            _data.value = Resource.Loading(message = "Scanning BLE devices")
        }
        isScanning = true
        bleScanner.startScan(null, scanSettings, scanCallback)
    }

    override fun readBatteryCharacteristic() {
        bleGatt?.let { gatt ->
            gatt.readCharacteristic(gatt.getService(UUID.fromString(MADDCOG_BATTERY_SERVICE)).getCharacteristic(UUID.fromString(MADDCOG_BATTERY_CHAR)))
        }
    }

    override fun reconnect() {
        bleGatt?.connect()
    }

    override fun disconnect() {
        bleGatt?.disconnect()
        preferences.saveDeviceStatus(false)
    }

    override fun closeConnection() {
        bleScanner.stopScan(scanCallback)
        val characteristicsList = listOf(
            findCharacteristic(MADDCOG_EEG_SERVICE, MADDCOG_EEG_CHAR),
            findCharacteristic(MADDCOG_HRV_SERVICE, MADDCOG_HRV_CHAR),
            findCharacteristic(MADDCOG_HEARTRATE_SERVICE, MADDCOG_HEARTRATE_CHAR),
            findCharacteristic(MADDCOG_BATTERY_SERVICE, MADDCOG_BATTERY_CHAR),
        )
        characteristicsList.forEach { characteristic ->
            if (characteristic != null) {
                disconnectCharacteristic(characteristic)
            }
        }
        preferences.saveDeviceStatus(false)
        bleGatt?.close()
    }

    private fun disconnectCharacteristic(characteristic: BluetoothGattCharacteristic) {
        val cccdUuid = UUID.fromString(CCCD_DESCRIPTOR_UUID)
        characteristic.getDescriptor(cccdUuid)?.let { cccdDescriptor ->
            if (bleGatt?.setCharacteristicNotification(characteristic, false) == false) {
                Log.d("ReceiveManager", "set characteristics notification failed")
                return
            }
            writeDescription(cccdDescriptor, BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE)
        }
    }


    private fun writeDescription(descriptor: BluetoothGattDescriptor, payload: ByteArray) {
        bleGatt?.let { gatt ->
            descriptor.value = payload
            gatt.writeDescriptor(descriptor)
        } ?: error("Not connected to a BLE device!")
    }

    private fun extractHeartRate(
        characteristic: BluetoothGattCharacteristic
    ): Double {
        val flag = characteristic.properties
        Log.d(TAG, "Heart rate flag: $flag")
        var format = -1
        // Heart rate bit number format
        if (flag and 0x01 != 0) {
            format = BluetoothGattCharacteristic.FORMAT_UINT16
            Log.d(TAG, "Heart rate format UINT16.")
        } else {
            format = BluetoothGattCharacteristic.FORMAT_UINT8
            Log.d(TAG, "Heart rate format UINT8.")
        }
        val heartRate = characteristic.getIntValue(format, 1)
        Log.d(TAG, String.format("Received heart rate: %d", heartRate))
        return heartRate.toDouble()
    }
}