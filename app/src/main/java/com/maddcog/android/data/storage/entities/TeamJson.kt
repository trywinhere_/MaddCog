package com.maddcog.android.data.storage.entities

import com.maddcog.android.domain.entities.team.Team
import com.maddcog.android.domain.entities.team.TeamMembers
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class TeamJson(
    @SerialName("teamMembers")
    val teamMembers: ArrayList<TeamMembersJson>,
    @SerialName("teamName")
    val teamName: String,
    @SerialName("teamId")
    val teamId: String,
    @SerialName("exchange")
    val exchange: String,
)

@Serializable
data class TeamMembersJson(
    val username: String?,
    val status: Int?,
    val isOwner: Boolean?,
    val imageUrl: String?,
    val teamUserId: String?,
)

fun TeamMembersJson.toDomain() = TeamMembers(
    username = username,
    status = status,
    owner = isOwner,
    imageUrl = imageUrl,
    teamUserId = teamUserId,
)

fun TeamMembers.toData() = TeamMembersJson(
    username = username,
    status = status,
    isOwner = owner,
    imageUrl = imageUrl,
    teamUserId = teamUserId,
)

fun ArrayList<TeamMembersJson>.toDomain(): ArrayList<TeamMembers> {
    val result = arrayListOf<TeamMembers>()
    for (i in this.indices) {
        result.add(
            TeamMembers(
                username = this[i].username,
                status = this[i].status,
                owner = this[i].isOwner,
                imageUrl = this[i].imageUrl,
                teamUserId = this[i].teamUserId
            )
        )
    }
    return result
}

fun ArrayList<TeamMembers>.toData(): ArrayList<TeamMembersJson> {
    val result = arrayListOf<TeamMembersJson>()
    for (i in this.indices) {
        result.add(
            TeamMembersJson(
                username = this[i].username,
                status = this[i].status,
                isOwner = this[i].owner,
                imageUrl = this[i].imageUrl,
                teamUserId = this[i].teamUserId
            )
        )
    }
    return result
}

fun TeamJson.toDomain() = Team(
    teamMembers = teamMembers.toDomain(),
    teamName = teamName,
    teamId = teamId,
    exchange = exchange,
)

fun Team.toData() = TeamJson(
    teamMembers = teamMembers.toData(),
    teamName = teamName.toString(),
    teamId = teamId.toString(),
    exchange = exchange.toString(),
)