package com.maddcog.android.data.network.dto.activity

import com.google.gson.annotations.SerializedName
import kotlinx.serialization.Serializable

@Serializable
data class RemoteTeamMember(
    @SerializedName("character_image")
    val characterImage: String?,
    @SerializedName("character_name")
    val characterName: String?,
    @SerializedName("fatigue")
    val fatigue: Double?,
    @SerializedName("fatigue_text")
    val fatigueText: String?,
    @SerializedName("flow")
    val flow: Double?,
    @SerializedName("flow_text")
    val flowText: String?,
    @SerializedName("gamer_image")
    val gamerImage: String?,
    @SerializedName("gamer_name")
    val gamerName: String?,
    @SerializedName("grade")
    val grade: Double?,
    @SerializedName("performance_params")
    val performanceParams: List<RemoteTeamMemberParam>?
)