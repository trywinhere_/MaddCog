package com.maddcog.android.data.storage.entities

import com.maddcog.android.domain.entities.user.FlowSd
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class FlowSdJson(
    @SerialName("count")
    val count: Int? = null,
    @SerialName("sd")
    val sd: Double? = null,
    @SerialName("mean")
    val mean: Double? = null
)

fun FlowSdJson.toDomain() = FlowSd(
    count = count,
    sd = sd,
    mean = mean
)

fun FlowSd.toData() = FlowSdJson(
    count = count,
    sd = sd,
    mean = mean
)