package com.maddcog.android.data.sensordatarecorder.utils

data class EEG(
    var timestamp: Int,
    var delta: Int,
    var theta: Int,
    var lowAlpha: Int,
    var highAlpha: Int,
    var lowBeta: Int,
    var highBeta: Int,
    var lowGamma: Int,
    var midGamma: Int,
    var signal: Int,
)