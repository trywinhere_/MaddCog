package com.maddcog.android.data.sensordatarecorder.utils

data class SensorDataActivity(
    var activityId: String? = null,
    var eegRawData: HashMap<String, List<EEG>> = hashMapOf(),
    var hrRawData: HashMap<String, List<HR>> = hashMapOf(),
    var events: String? = null,
) {
    fun addHRdata(identifier: String, data: SampleHR) {
        val hr = HR(
            timestamp = data.timestamp,
            rr = data.rr.toInt(),
            rrRaw = data.rrRaw.toInt()
        )
        val oldList = hrRawData[identifier]?.toMutableList()
        oldList?.add(hr)
    }

    fun addEEGdata(identifier: String, data: SampleEEG) {
        val eeg = EEG(
            timestamp = data.timestamp!!,
            delta = data.delta!!,
            theta = data.theta!!,
            lowAlpha = data.lowAlpha!!,
            highAlpha = data.highAlpha!!,
            lowBeta = data.lowBeta!!,
            highBeta = data.highBeta!!,
            lowGamma = data.lowGamma!!, midGamma = data.midGamma!!, signal = data.signal!!
        )
        val oldList = eegRawData[identifier]?.toMutableList()
        oldList?.add(eeg)
    }
}