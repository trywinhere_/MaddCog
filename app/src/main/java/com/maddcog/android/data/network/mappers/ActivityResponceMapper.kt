package com.maddcog.android.data.network.mappers

import com.maddcog.android.baseui.ui.theme.WIN
import com.maddcog.android.data.extensions.NO_NAME
import com.maddcog.android.data.network.dto.activity.RemoteActivity
import com.maddcog.android.data.network.dto.activity.RemoteActivitySummary
import com.maddcog.android.data.network.dto.activity.RemoteMatch
import com.maddcog.android.data.network.dto.activity.RemoteMentalPerformance
import com.maddcog.android.data.network.dto.activity.RemoteMyPlayer
import com.maddcog.android.data.network.dto.activity.RemotePerformanceParam
import com.maddcog.android.data.network.dto.activity.RemoteTeamMember
import com.maddcog.android.data.network.dto.activity.RemoteTeamMemberParam
import com.maddcog.android.data.network.dto.activity.RemoteTimelineEvent
import com.maddcog.android.domain.entities.activity.ActivitySummary
import com.maddcog.android.domain.entities.activity.Match
import com.maddcog.android.domain.entities.activity.MentalPerformanceTimeline
import com.maddcog.android.domain.entities.activity.MyPlayer
import com.maddcog.android.domain.entities.activity.PerformanceParam
import com.maddcog.android.domain.entities.activity.TeamMember
import com.maddcog.android.domain.entities.activity.TeamMemberParam
import com.maddcog.android.domain.entities.activity.TimelineEvent
import com.maddcog.android.domain.entities.activity.UserActivity
import com.maddcog.android.gameSummary.extensions.DEMO_ACTIVITY_NAME
import org.joda.time.DateTime

fun RemoteActivity.toDomain(): UserActivity {
    return UserActivity(
        activityId = this.activityId,
        duration = this.duration ?: 0,
        mentalPerformanceTimeline = this.mentalPerformanceTimeline?.map { it.toDomain() }
            ?: emptyList(),
        activityType = this.activityType.toDomain(),
        timestamp = DateTime(this.timestamp * 1000),
        activitySummary = this.activitySummary.toDomain(),
        match = this.match.toDomain(),
        tags = this.tags ?: emptyList(),
        isForTest = this.activityId.contains(DEMO_ACTIVITY_NAME)
    )
}

fun RemoteMentalPerformance.toDomain(): MentalPerformanceTimeline {
    return MentalPerformanceTimeline(
        fatigue = this.fatigue ?: 0.0,
        flow = this.flow ?: 0.0,
        timestamp = this.timestamp ?: 0,
    )
}

fun RemoteActivitySummary.toDomain(): ActivitySummary {
    return ActivitySummary(
        fatigueZoneHigh = this.fatigueZoneHigh ?: 0.0,
        fatigueZoneNormal = this.fatigueZoneNormal ?: 0.0,
        flowZoneNormal = this.flowZoneNormal ?: 0.0,
        fatigueZoneLow = this.fatigueZoneLow ?: 0.0,
        flowZoneHigh = this.flowZoneHigh ?: 0.0,
        fatigueEndAvg = this.fatigueEndAvg ?: 0.0,
        flowAvg = this.flowAvg ?: 0.0,
        flowZoneLow = this.flowZoneLow ?: 0.0,
        fatigueAvg = this.fatigueAvg ?: 0.0
    )
}

fun RemoteMatch.toDomain(): Match {
    return Match(
        start = this.start ?: 0,
        end = this.end ?: 0,
        gameMode = this.gameMode ?: "",
        mapName = this.mapName ?: "",
        stayedTillGameOver = this.stayedTillGameOver ?: false,
        teamCtScore = this.teamCtScore ?: 0,
        tileMetricDescription = this.tileMetricDescription ?: "",
        tileMetricText = this.tileMetricText ?: "",
        teamTScore = this.teamTScore ?: 0,
        tileImage = this.tileImage ?: "",
        myPlayer = this.myPlayer.toDomain(),
        team = this.team?.map { it.toDomain() } ?: emptyList(),
        timelineEvents = this.timelineEvents?.map { it.toDomain() } ?: emptyList(),
    )
}

fun RemoteTimelineEvent.toDomain(): TimelineEvent {
    return TimelineEvent(
        name = this.name ?: "",
        timestamps = this.timestamps ?: emptyList(),
    )
}

fun RemoteMyPlayer.toDomain(): MyPlayer {
    return MyPlayer(
        outcome = this.outcome ?: "",
        gamerName = this.gamerName ?: NO_NAME,
        gamerImage = this.gamerImage ?: "",
        characterName = this.characterName ?: NO_NAME,
        characterImage = this.characterImage ?: "",
        grade = this.grade ?: 0.0,
        score = this.score ?: if (this.outcome == WIN) 6 else 0,
        scoreText = this.scoreText ?: "",
        flow = this.flow ?: 0.0,
        flowText = this.flowText ?: "",
        fatigue = this.fatigue ?: 0.0,
        fatigueText = this.fatigueText ?: "",
        fatigueEnd = this.fatigueEnd ?: 0.0,
        performanceParams = this.performanceParams?.map { it.toDomain() } ?: emptyList(),
        position = this.position ?: 0,
    )
}

fun RemotePerformanceParam.toDomain(): PerformanceParam {
    return PerformanceParam(
        name = this.name ?: "",
        value = this.value ?: 0.0,
        order = this.order ?: 0,
    )
}

fun RemoteTeamMember.toDomain(): TeamMember {
    return TeamMember(
        gamerName = this.gamerName ?: "",
        gamerImage = this.gamerImage ?: "",
        characterName = this.characterName ?: NO_NAME,
        characterImage = this.characterImage ?: "",
        flow = this.flow ?: 0.0,
        flowText = this.flowText ?: "",
        fatigue = this.fatigue ?: 0.0,
        fatigueText = this.fatigueText ?: "",
        grade = this.grade ?: 0.0,
        performanceParams = this.performanceParams?.map { it.toDomain() } ?: emptyList()
    )
}

fun RemoteTeamMemberParam.toDomain(): TeamMemberParam {
    return TeamMemberParam(
        name = this.name ?: "",
        value = this.value ?: 0.0,
    )
}