package com.maddcog.android.data.extensions

import android.content.Context
import com.maddcog.android.R
import com.maddcog.android.baseui.ui.theme.LOSS
import com.maddcog.android.baseui.ui.theme.WIN
import com.maddcog.android.trends.extensions.GAME_GRADE
import com.maddcog.android.trends.extensions.MENTAL_PERFORMANCE
import com.maddcog.android.trends.extensions.WIN_RATE
import com.maddcog.android.trends.utils.FiltersListHandler.Companion.ALL
import com.maddcog.android.trends.utils.FiltersListHandler.Companion.ALL_ACTIVITIES
import com.maddcog.android.trends.utils.FiltersListHandler.Companion.DURING_GAME
import com.maddcog.android.trends.utils.FiltersListHandler.Companion.HOURLY
import com.maddcog.android.trends.utils.FiltersListHandler.Companion.LAST_MONTH
import com.maddcog.android.trends.utils.FiltersListHandler.Companion.LAST_WEEK
import com.maddcog.android.trends.utils.FiltersListHandler.Companion.PER_GAME

const val NO_NAME = "N/A"

const val KDA = "kda"
const val KD = "kd"
const val KILL_PARTICIPATION = "kill_participation"
const val KILLS = "kills"
const val KILL = "kill"
const val DEATHS = "deaths"
const val SHOT_ACCURACY = "shot_accuracy"
const val SCORE_PER_MINUTE = "score_per_minute"
const val CREEP_SCORE_PER_MINUTE = "creep_score_per_min"
const val SCORE_ROUND = "score_per_round"
const val SCORE_FOR = "score_for"
const val SCORE_AGAINST = "score_against"
const val CREEP_SCORE = "creep_score"
const val GOALS_FOR = "goals_for"
const val GOALS_AGAINST = "goals_against"
const val POINTS_FOR = "points_for"
const val POINTS_AGAINST = "points_against"
const val PLAYER_POINTS = "player_points"
const val AVG_POSITION = "avg_position"

const val SCORE_6_TEAM = "6_team_higher"
const val SCORE_6_SKILL = "6_skill_higher"
const val SCORE_6_EQUAL = "6_equal"
const val SCORE_5_4_TEAM = "5_4_team_higher"
const val SCORE_5_4_SKILL = "5_4_skill_higher"
const val SCORE_5_4_EQUAL = "5_4_equal"
const val SCORE_3_TEAM = "3_team_higher"
const val SCORE_3_SKILL = "3_skill_higher"
const val SCORE_3_EQUAL = "3_equal"
const val SCORE_2_1_GENERAL = "2_1_general"
const val SCORE_0_GENERAL = "0_general"

const val TRENDS_NEW_USER = "trends_new_user"
const val TRENDS_MENTAL_PERFORMANCE_LOW = "trends_mental_performance_low"
const val TRENDS_MENTAL_PERFORMANCE_MODERATE = "trends_mental_performance_moderate"
const val TRENDS_MENTAL_PERFORMANCE_HIGH = "trends_mental_performance_high"

const val DASHBOARD_NEW_USER = "dashboard_new_user"
const val DASHBOARD_INSIGHT_FOCUS = "dashboard_insight_focus_avg_z"
const val DASHBOARD_INSIGHT_FLOW_AVG = "dashboard_insight_flow_avg_z"
const val DASHBOARD_INSIGHT_FATIGUE = "dashboard_insight_fatigue_start_z"
const val DASHBOARD_INSIGHT_FLOW_SD = "dashboard_insight_flow_sd_z"

fun String.toLocalName(context: Context): String {
    return when (this) {
        LAST_MONTH -> context.resources.getString(R.string.last_month)
        LAST_WEEK -> context.resources.getString(R.string.last_week)
        HOURLY -> context.resources.getString(R.string.hourly)
        PER_GAME -> context.resources.getString(R.string.per_game)
        DURING_GAME -> context.resources.getString(R.string.during_game)
        MENTAL_PERFORMANCE -> context.resources.getString(R.string.mental_performance)
        WIN_RATE -> context.resources.getString(R.string.win_rate)
        GAME_GRADE -> context.resources.getString(R.string.game_grade)
        ALL -> context.resources.getString(R.string.all_filter)
        ALL_ACTIVITIES -> context.resources.getString(R.string.all_activities)
        KILLS -> context.resources.getString(R.string.kills)
        KILL -> context.resources.getString(R.string.kill)
        KDA -> context.resources.getString(R.string.kda)
        KD -> context.resources.getString(R.string.kd)
        KILL_PARTICIPATION -> context.resources.getString(R.string.kill_participation)
        DEATHS -> context.resources.getString(R.string.deaths)
        SHOT_ACCURACY -> context.resources.getString(R.string.shot_accuracy)
        SCORE_PER_MINUTE -> context.resources.getString(R.string.score_per_minute)
        CREEP_SCORE_PER_MINUTE -> context.resources.getString(R.string.creep_score_per_min)
        SCORE_ROUND -> context.resources.getString(R.string.score_per_round)
        SCORE_FOR -> context.resources.getString(R.string.score_for)
        SCORE_AGAINST -> context.resources.getString(R.string.score_against)
        CREEP_SCORE -> context.resources.getString(R.string.creep_score)
        GOALS_FOR -> context.resources.getString(R.string.goals_for)
        GOALS_AGAINST -> context.resources.getString(R.string.goals_against)
        POINTS_FOR -> context.resources.getString(R.string.points_for)
        POINTS_AGAINST -> context.resources.getString(R.string.points_against)
        PLAYER_POINTS -> context.resources.getString(R.string.player_points)
        AVG_POSITION -> context.resources.getString(R.string.avg_position)
        else -> this
    }
}

fun String.toScoreText(outcome: String): Int {
    return when (this + outcome) {
        SCORE_6_TEAM + WIN -> R.string.win_6_team
        SCORE_6_TEAM + LOSS -> R.string.loss_6_team
        SCORE_6_SKILL + WIN -> R.string.win_6_team
        SCORE_6_SKILL + LOSS -> R.string.loss_6_skill
        SCORE_6_EQUAL + WIN -> R.string.win_6_equal
        SCORE_6_EQUAL + LOSS -> R.string.loss_6_equal
        SCORE_5_4_TEAM + WIN -> R.string.win_5_4_team
        SCORE_5_4_TEAM + LOSS -> R.string.loss_5_4_team
        SCORE_5_4_SKILL + WIN -> R.string.win_5_4_skill
        SCORE_5_4_SKILL + LOSS -> R.string.loss_5_4_skill
        SCORE_5_4_EQUAL + WIN -> R.string.win_5_4_equal
        SCORE_5_4_EQUAL + LOSS -> R.string.loss_5_4_equal
        SCORE_3_TEAM + WIN -> R.string.win_3_team
        SCORE_3_TEAM + LOSS -> R.string.loss_3_team
        SCORE_3_SKILL + WIN -> R.string.win_3_skill
        SCORE_3_SKILL + LOSS -> R.string.loss_3_skill
        SCORE_3_EQUAL + WIN -> R.string.win_3_equal
        SCORE_3_EQUAL + LOSS -> R.string.loss_3_equal
        SCORE_2_1_GENERAL + WIN -> R.string.win_2_1_general
        SCORE_2_1_GENERAL + LOSS -> R.string.loss_2_1_general
        SCORE_0_GENERAL + WIN -> R.string.win_0_general
        SCORE_0_GENERAL + LOSS -> R.string.loss_0_general
        else -> R.string.empty_string
    }
}

fun String?.getTrendsInsightText(value: Int?, context: Context): String {
    return when (this) {
        TRENDS_NEW_USER -> context.resources.getString(R.string.trends_mental_performance_new_user)
        TRENDS_MENTAL_PERFORMANCE_LOW -> context.resources.getString(R.string.trends_mental_performance_low)
        TRENDS_MENTAL_PERFORMANCE_MODERATE -> context.resources.getString(
            R.string.trends_mental_performance_moderate,
            value
        )
        TRENDS_MENTAL_PERFORMANCE_HIGH -> context.resources.getString(
            R.string.trends_mental_performance_high_full,
            value
        )
        else -> context.resources.getString(R.string.trends_mental_performance_new_user)
    }
}

fun String?.getDashboardInsightText(context: Context): String {
    return when (this) {
        DASHBOARD_NEW_USER -> context.resources.getString(R.string.dashboard_new_user)
        DASHBOARD_INSIGHT_FATIGUE -> context.resources.getString(R.string.dashboard_insight_fatigue_start_z)
        DASHBOARD_INSIGHT_FLOW_AVG -> context.resources.getString(R.string.dashboard_insight_flow_avg_z)
        DASHBOARD_INSIGHT_FLOW_SD -> context.resources.getString(R.string.dashboard_insight_flow_sd_z)
        DASHBOARD_INSIGHT_FOCUS -> context.resources.getString(R.string.dashboard_insight_focus_avg_z)
        else -> context.resources.getString(R.string.dashboard_new_user)
    }
}