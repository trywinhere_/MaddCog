package com.maddcog.android.data.storage.entities

import com.maddcog.android.domain.entities.user.Mf5min
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class Mf5minJson(
    @SerialName("count")
    var count: Int? = null,
    @SerialName("sd")
    var sd: Double? = null,
    @SerialName("startingSd")
    var startingSd: Double? = null,
    @SerialName("mean")
    var mean: Double? = null,
    @SerialName("startingMean")
    var startingMean: Double? = null
)

fun Mf5minJson.toDomain() = Mf5min(
    count = count,
    sd = sd,
    startingSd = startingSd,
    startingMean = startingMean,
    mean = mean
)

fun Mf5min.toData() = Mf5minJson(
    count = count,
    sd = sd,
    startingSd = startingSd,
    startingMean = startingMean,
    mean = mean
)