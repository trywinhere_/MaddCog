package com.maddcog.android.data.network.dto.raw

data class Event(
    val event_type: Int,
    val percentage_correct: Int,
    val response_time: Double,
    val time_since_last: Int,
    val timestamp: Int,
    val value: Double
)