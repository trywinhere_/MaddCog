package com.maddcog.android.data.network.dto.activity

import com.google.gson.annotations.SerializedName
import kotlinx.serialization.Serializable

@Serializable
data class RemoteMatch(
    @SerializedName("start")
    val start: Int?,
    @SerializedName("end")
    val end: Int?,
    @SerializedName("game_mode")
    val gameMode: String?,
    @SerializedName("map_name")
    val mapName: String?,
    @SerializedName("my_player")
    val myPlayer: RemoteMyPlayer,
    @SerializedName("stayed_till_game_over")
    val stayedTillGameOver: Boolean?,
    @SerializedName("team")
    val team: List<RemoteTeamMember>?,
    @SerializedName("team_ct_score")
    val teamCtScore: Int?,
    @SerializedName("team_t_score")
    val teamTScore: Int?,
    @SerializedName("tile_image")
    val tileImage: String?,
    @SerializedName("tile_metric_description")
    val tileMetricDescription: String?,
    @SerializedName("tile_metric_text")
    val tileMetricText: String?,
    @SerializedName("timeline_events")
    val timelineEvents: List<RemoteTimelineEvent>?
)