package com.maddcog.android.data.sensordatarecorder.utils

import org.jtransforms.fft.FloatFFT_1D
import kotlin.math.pow
import kotlin.math.log2

object FftAnalyzer {
    fun fftAnalyzer(frameOfSamples: FloatArray): DoubleArray {
        val frameCount = frameOfSamples.size
        val copyFrameOfSample = frameOfSamples.copyOf()
        val winCoeffs = DoubleArray(frameCount)

        for (i in 0 until frameCount) {
            winCoeffs[i] = 1.0 - (((2.0 * i) / frameCount) - 1.0).pow(2.0)
        }

        for (k in 0 until frameCount) {
            copyFrameOfSample[k] = frameOfSamples[k] * winCoeffs[k].toFloat()
        }

        var scaleFactor = 0.0
        for (coeff in winCoeffs) {
            scaleFactor += coeff
        }
        scaleFactor /= winCoeffs.size
        scaleFactor = 1.0 / scaleFactor

        val reals = copyFrameOfSample.copyOf()
        val imags = FloatArray(frameCount)

        val log2Size = log2(frameCount.toFloat()).toInt()

        val fft = FloatFFT_1D(log2Size.toLong()) //TODO ??
        fft.complexForward(reals) //TODO ??

        val result = DoubleArray(frameCount)
        for (i in 0 until frameCount) {
            result[i] = (reals[i] * reals[i] + imags[i] * imags[i]) / 256.0 * scaleFactor
        }

        return result
    }
}