package com.maddcog.android.data.model

class StateViewModel(
    val loading: Boolean = false,
    val error: Boolean = false
)