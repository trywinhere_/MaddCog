package com.maddcog.android.data.network.services

import com.maddcog.android.data.network.MaddcogApi
import com.maddcog.android.data.network.mappers.toDomain
import com.maddcog.android.domain.api.ITeamsApiService
import com.maddcog.android.domain.entities.team.Team

class TeamsApiServiceImpl(
    private val api: MaddcogApi
) : ITeamsApiService {

    override suspend fun getTeams(): List<Team> {
        return api.getTeams().map {
            it.toDomain()
        }
    }

    override suspend fun leaveTeam(teamId: String): Boolean {
        return try {
            api.leaveTeam(teamId)
            true
        } catch (e: Exception) {
            return false
        }
    }

    override suspend fun removeTeamMate(teamId: String): Boolean {
        return try {
            api.removeTeamMate(teamId)
            true
        } catch (e: Exception) {
            return false
        }
    }

    override suspend fun inviteUser(userEmail: String): Boolean {
        return try {
            api.inviteUser(userEmail)
            true
        } catch (e: Exception) {
            return false
        }
    }
}