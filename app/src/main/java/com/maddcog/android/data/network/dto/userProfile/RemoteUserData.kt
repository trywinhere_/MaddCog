package com.maddcog.android.data.network.dto.userProfile

import com.google.gson.annotations.SerializedName
import kotlinx.serialization.Serializable

@Serializable
data class RemoteUserData(
    @SerializedName("fatigue_percentiles") var fatiguePercentiles: RemoteFatiguePercentiles? = RemoteFatiguePercentiles(),
    @SerializedName("fatigue_pred") var fatiguePred: RemoteFatiguePred? = RemoteFatiguePred(),
    @SerializedName("fatigue_start") var fatigueStart: RemoteFatigueStart? = RemoteFatigueStart(),
    @SerializedName("flow") var flow: RemoteFlow? = RemoteFlow(),
    @SerializedName("flow_sd") var flowSd: RemoteFlowSd? = RemoteFlowSd(),
    @SerializedName("focus") var focus: RemoteFocus? = RemoteFocus(),
    @SerializedName("hb5min") var hb5min: RemoteHb5min? = RemoteHb5min(),
    @SerializedName("lfhf5min") var lfhf5min: RemoteLfhf5min? = RemoteLfhf5min(),
    @SerializedName("mf5min") var mf5min: RemoteMf5min? = RemoteMf5min(),
    @SerializedName("tablog5min") var tablog5min: RemoteTablog5min? = RemoteTablog5min(),
    @SerializedName("tplog5min") var tplog5min: RemoteTplog5min? = RemoteTplog5min(),
    @SerializedName("trends_insight") var trendsInsight: String,
    @SerializedName("trends_insight_value") var trendsInsightValue: Int,
    @SerializedName("user_insight") var userInsight: String,
    @SerializedName("wtalphaavgcurt") var wtalphaavgcurt: RemoteWtalphaavgcurt? = RemoteWtalphaavgcurt(),
    @SerializedName("wtbetaavgcurt") var wtbetaavgcurt: RemoteWtbetaavgcurt? = RemoteWtbetaavgcurt()
)