package com.maddcog.android.data.network.dto

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class DeleteToken(
    @SerialName("cognito_access_token")
    val cognito_access_token: String?
)