package com.maddcog.android.data.network.dto.raw

data class EegRaw(
    val delta: Int,
    val highalpha: Int,
    val highbeta: Int,
    val lowalpha: Int,
    val lowbeta: Int,
    val lowgamma: Int,
    val midgamma: Int,
    val signal: Int,
    val theta: Int,
    val timestamp: Int
)