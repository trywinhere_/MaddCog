package com.maddcog.android.data.storage.entities

import com.maddcog.android.domain.entities.user.FatigueStart
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
class FatigueStartJson(
    @SerialName("count")
    val count: Int? = null,
    @SerialName("sd")
    val sd: Double? = null,
    @SerialName("mean")
    val mean: Double? = null
)

fun FatigueStartJson.toDomain() = FatigueStart(
    count = count,
    sd = sd,
    mean = mean,
)

fun FatigueStart.toData() = FatigueStartJson(
    count = count,
    sd = sd,
    mean = mean,
)