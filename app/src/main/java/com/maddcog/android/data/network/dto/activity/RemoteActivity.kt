package com.maddcog.android.data.network.dto.activity

import com.google.gson.annotations.SerializedName
import com.maddcog.android.data.network.dto.activityType.ActivityTypeResponseItem
import kotlinx.serialization.Serializable

@Serializable
data class RemoteActivity(
    @SerializedName("activity_id")
    val activityId: String,
    @SerializedName("duration")
    val duration: Int?,
    @SerializedName("mental_performance_timeline")
    val mentalPerformanceTimeline: List<RemoteMentalPerformance>?,
    @SerializedName("activity_type")
    val activityType: ActivityTypeResponseItem,
    @SerializedName("timestamp")
    val timestamp: Long,
    @SerializedName("activity_summary")
    val activitySummary: RemoteActivitySummary,
    @SerializedName("match")
    val match: RemoteMatch,
    @SerializedName("tags")
    val tags: List<String>?,
)