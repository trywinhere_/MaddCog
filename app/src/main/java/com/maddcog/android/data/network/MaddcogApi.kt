package com.maddcog.android.data.network

import com.maddcog.android.data.network.dto.DeleteGame
import com.maddcog.android.data.network.dto.DeleteToken
import com.maddcog.android.data.network.dto.activity.ActivityResponse
import com.maddcog.android.data.network.dto.activity.RemoteActivity
import com.maddcog.android.data.network.dto.activityType.ActivityTypeResponse
import com.maddcog.android.data.network.dto.raw.SensorSendData
import com.maddcog.android.data.network.dto.rawActivity.RawUserActivity
import com.maddcog.android.data.network.dto.team.TeamResponse
import com.maddcog.android.data.network.dto.userProfile.RemoteProfile
import com.maddcog.android.domain.entities.AimGame
import com.maddcog.android.domain.entities.Sensors
import retrofit2.Response
import retrofit2.http.*

interface MaddcogApi {

    @GET("users/me/teams")
    suspend fun getTeams(): TeamResponse

    @GET("/activities")
    suspend fun getUserActivities(): ActivityResponse

    @GET("/activities/{activity_id}")
    suspend fun loadUserActivityById(@Path("activity_id") id: String): RemoteActivity

    @GET("activities/activity-types")
    suspend fun loadAllActivities(): ActivityTypeResponse

    @GET("users/me")
    suspend fun loadUserProfile(): RemoteProfile

    @HTTP(method = "DELETE", path = "users/me", hasBody = true)
    suspend fun deleteUser(@Body deleteToken: DeleteToken)

    @HTTP(method = "DELETE", path = "users/me/games", hasBody = true)
    suspend fun deleteConnectedGame(@Body deleteGame: DeleteGame)

    @HTTP(method = "PUT", path = "users/me/games", hasBody = true)
    suspend fun connectGame(@Body deleteGame: DeleteGame)

    @DELETE("/activities/{activity_id}")
    suspend fun deleteActivity(@Path("activity_id") id: String)

    @GET("users/me/aim-game")
    suspend fun getAimGameScores(): Response<AimGame>

    @HTTP(method = "PUT", path = "sensors", hasBody = true)
    suspend fun addSensors(@Body sensors: Sensors)

    @DELETE("/teams/{team_user_id}")
    suspend fun leaveTeam(@Path("team_user_id") id: String)

    @DELETE("/teams/{team_user_id}/delete-mate")
    suspend fun removeTeamMate(@Path("team_user_id") id: String)

    @POST("/teams/find-user-by-email")
    suspend fun inviteUser(@Path("user_email") userEmail: String)

    @HTTP(method = "POST", path = "raw", hasBody = true)
    suspend fun sendSensorRawData(@Body sensorData: SensorSendData)

    @HTTP(method = "POST", path = "activities", hasBody = true)
    suspend fun sendRawActivity(@Body rawActivity: RawUserActivity)
}
