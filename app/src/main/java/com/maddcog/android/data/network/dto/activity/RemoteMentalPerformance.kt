package com.maddcog.android.data.network.dto.activity

import com.google.gson.annotations.SerializedName
import kotlinx.serialization.Serializable

@Serializable
data class RemoteMentalPerformance(
    @SerializedName("fatigue")
    val fatigue: Double?,
    @SerializedName("flow")
    val flow: Double?,
    @SerializedName("timestamp")
    val timestamp: Int?,
)