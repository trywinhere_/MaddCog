package com.maddcog.android.data.storage.entities

import com.maddcog.android.domain.entities.user.Lfhf5min
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class Lfhf5minJson(
    @SerialName("count")
    var count: Int? = null,
    @SerialName("sd")
    var sd: Double? = null,
    @SerialName("startingSd")
    var startingSd: Double? = null,
    @SerialName("mean")
    var mean: Double? = null,
    @SerialName("startingMean")
    var startingMean: Double? = null
)

fun Lfhf5minJson.toDomain() = Lfhf5min(
    count = count,
    sd = sd,
    startingSd = startingSd,
    startingMean = startingMean,
    mean = mean
)

fun Lfhf5min.toData() = Lfhf5minJson(
    count = count,
    sd = sd,
    startingSd = startingSd,
    startingMean = startingMean,
    mean = mean
)