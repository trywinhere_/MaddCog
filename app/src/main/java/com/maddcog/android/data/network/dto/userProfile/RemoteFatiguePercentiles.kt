package com.maddcog.android.data.network.dto.userProfile

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class RemoteFatiguePercentiles(
    @SerialName("high") var high: Int? = null,
    @SerialName("low") var low: Int? = null,
    @SerialName("max") var max: Int? = null,
    @SerialName("base") var base: Int? = null
)