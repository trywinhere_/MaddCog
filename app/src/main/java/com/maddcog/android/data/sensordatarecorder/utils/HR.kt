package com.maddcog.android.data.sensordatarecorder.utils

data class HR(
    var timestamp: Int,
    var rr: Int,
    var rrRaw: Int,
)