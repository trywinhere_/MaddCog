package com.maddcog.android.data.network.dto.activity

import com.google.gson.annotations.SerializedName
import kotlinx.serialization.Serializable

@Serializable
data class RemoteTimelineEvent(
    @SerializedName("name")
    val name: String?,
    @SerializedName("timestamps")
    val timestamps: List<Int>?
)