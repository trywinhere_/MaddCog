package com.maddcog.android.data.storage.entities

import com.maddcog.android.domain.entities.user.FatiguePred
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
class FatiguePredJson(
    @SerialName("count")
    val count: Int? = null,
    @SerialName("min")
    val min: Double? = null,
    @SerialName("startingP2")
    val startingP2: Double? = null,
    @SerialName("max")
    val max: Double? = null,
    @SerialName("startingP1")
    val startingP1: Double? = null,
    @SerialName("countVal")
    val countVal: ArrayList<Int> = arrayListOf()
)

fun FatiguePredJson.toDomain() = FatiguePred(
    count = count,
    min = min,
    startingP2 = startingP2,
    max = max,
    startingP1 = startingP1,
    countVal = countVal
)

fun FatiguePred.toData() = FatiguePredJson(
    count = count,
    min = min,
    startingP2 = startingP2,
    max = max,
    startingP1 = startingP1,
    countVal = countVal
)