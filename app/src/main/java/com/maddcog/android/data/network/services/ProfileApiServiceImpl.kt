package com.maddcog.android.data.network.services

import com.maddcog.android.data.network.MaddcogApi
import com.maddcog.android.data.network.dto.DeleteGame
import com.maddcog.android.data.network.dto.DeleteToken
import com.maddcog.android.data.network.mappers.toDomain
import com.maddcog.android.domain.api.IProfileApiService
import com.maddcog.android.domain.api.ISharedPreferencesStorage
import com.maddcog.android.domain.entities.UserGame
import com.maddcog.android.domain.entities.user.UserProfile

class ProfileApiServiceImpl(
    private val api: MaddcogApi,
    private val storage: ISharedPreferencesStorage,
) : IProfileApiService {

    override suspend fun loadUserProfile(): UserProfile? {
        return api.loadUserProfile().toDomain()
    }

    override suspend fun deleteUser() {
        val deleteToken = storage.getCognitoToken()
        api.deleteUser(DeleteToken(deleteToken))
    }

    override suspend fun deleteConnectedGame(game: UserGame) {
        api.deleteConnectedGame(
            DeleteGame(
                gamer_name = game.userName ?: "",
                region = game.region,
                game = game.gameNumber,
            )
        )
    }

    override suspend fun saveConnection(number: Int, username: String, region: String) {
        api.connectGame(
            DeleteGame(
                gamer_name = username,
                region = region,
                game = number,
            )
        )
    }
}