package com.maddcog.android.data.sensordatarecorder.utils

import java.util.concurrent.CopyOnWriteArrayList
import kotlin.math.pow

object Utils {
    fun rollingAverage(list: CopyOnWriteArrayList<Pair<Int, Double>>, timestamp: Int, duration: Int): Pair<Double, List<Pair<Int, Double>>> {
        var rollingAverage = 0.0
        val newList: MutableList<Pair<Int, Double>> = mutableListOf()
        var count = 0

        for ((t, v) in list) {
            if ((timestamp - t) <= duration) {
                if ((timestamp - t) >= 0) {
                    rollingAverage += v
                    count += 1
                }
                newList.add(Pair(t, v))
            }
        }

        if (count > 1) {
            rollingAverage /= count.toDouble()
        }

        return Pair(rollingAverage, newList)
    }

    fun computeWT(list: List<Pair<Int, Double>>): Pair<Double, List<Pair<Int, Double>>> {
        var wtValue = 0.0
        var count = 0.0
        val newList = mutableListOf<Pair<Int, Double>>()

        for ((t, v) in list) {
            wtValue += (count + 1) * v
            if (count > 0) {
                newList.add(t to v)
            }
            count += 1
        }

        wtValue = (wtValue / 15.0).pow(1.0 / 3.0)

        return wtValue to newList
    }

    fun newMean(oldMean: Double, newValue: Double, oldSize: Int): Double {
        return if (oldSize > 0) {
            (oldMean + newValue - oldMean) / (oldSize.toDouble() + 1)
        } else {
            newValue
        }
    }

    fun newStDev(
        oldStDev: Double,
        newValue: Double,
        oldMean: Double,
        newMean: Double,
        oldSize: Int
    ): Double {
        return if (
            (oldSize > 0) && ((oldSize.toDouble() - 1) * oldStDev.pow(2.0) + (newValue - newMean) * (newValue - oldMean)) / oldSize.toDouble() > 0
        ) {
            kotlin.math.sqrt(((oldSize.toDouble() - 1) * oldStDev.pow(2.0) + (newValue - newMean) * (newValue - oldMean)) / oldSize.toDouble())
        } else {
            0.0
        }
    }
}