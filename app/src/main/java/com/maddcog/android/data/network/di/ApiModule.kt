package com.maddcog.android.data.network.di

import com.maddcog.android.BuildConfig
import com.maddcog.android.data.network.MaddcogApi
import com.maddcog.android.data.network.interceptors.loggingInterceptor
import com.maddcog.android.domain.api.ISharedPreferencesStorage
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object ApiModule {
    @Provides
    @Singleton
    fun okhttp(storage: ISharedPreferencesStorage): OkHttpClient = OkHttpClient.Builder()
        .apply {
            addInterceptor(loggingInterceptor())
            addInterceptor { chain ->
                val request = chain.request().newBuilder()
                    .addHeader("Authorization", storage.getToken().toString())
                    .build()
                chain.proceed(request)
            }
        }
        .build()

    @Provides
    @Singleton
    fun retrofit(client: OkHttpClient): Retrofit = Retrofit.Builder()
        .addConverterFactory(GsonConverterFactory.create())
        .baseUrl(BuildConfig.BASE_URL)
        .client(client)
        .build()

    @Provides
    @Singleton
    fun provideApiService(
        storage: ISharedPreferencesStorage
    ): MaddcogApi {
        return retrofit(okhttp(storage))
            .create(MaddcogApi::class.java)
    }
}