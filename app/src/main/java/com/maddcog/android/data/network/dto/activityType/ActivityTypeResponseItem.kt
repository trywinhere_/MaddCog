package com.maddcog.android.data.network.dto.activityType

import com.google.gson.annotations.SerializedName
import kotlinx.serialization.Serializable

@Serializable
data class ActivityTypeResponseItem(
    @SerializedName("activity_type_id")
    val activityTypeId: Int?,
    @SerializedName("activity_type_name")
    val activityTypeName: String?,
    @SerializedName("game_type")
    val gameType: String?,
    @SerializedName("has_grade")
    val hasGrade: Boolean,
    @SerializedName("has_team_score")
    val hasTeamScore: Boolean,
    @SerializedName("image_rectangle")
    val imageRectangle: String?,
    @SerializedName("image_square")
    val imageSquare: String?,
    @SerializedName("maddcog_integrated")
    val maddcogIntegrated: String?,
    @SerializedName("maddcog_owned")
    val maddcogOwned: Boolean?,
)