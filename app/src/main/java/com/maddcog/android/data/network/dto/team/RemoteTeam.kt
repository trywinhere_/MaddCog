package com.maddcog.android.data.network.dto.team

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class RemoteTeam(
    @SerialName("team_members") var teamMembers: ArrayList<RemoteTeamMembers> = arrayListOf(),
    @SerialName("team_name") var teamName: String,
    @SerialName("team_id") var teamId: String,
    @SerialName("exchange") var exchange: String
)