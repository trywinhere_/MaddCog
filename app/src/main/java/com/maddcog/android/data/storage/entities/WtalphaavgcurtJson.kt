package com.maddcog.android.data.storage.entities

import com.maddcog.android.domain.entities.user.Wtalphaavgcurt
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class WtalphaavgcurtJson(
    @SerialName("count")
    var count: Int? = null,
    @SerialName("sd")
    var sd: Double? = null,
    @SerialName("startingSd")
    var startingSd: Double? = null,
    @SerialName("mean")
    var mean: Double? = null,
    @SerialName("startingMean")
    var startingMean: Double? = null
)

fun WtalphaavgcurtJson.toDomain() = Wtalphaavgcurt(
    count = count,
    sd = sd,
    startingSd = startingSd,
    startingMean = startingMean,
    mean = mean
)

fun Wtalphaavgcurt.toData() = WtalphaavgcurtJson(
    count = count,
    sd = sd,
    startingSd = startingSd,
    startingMean = startingMean,
    mean = mean
)