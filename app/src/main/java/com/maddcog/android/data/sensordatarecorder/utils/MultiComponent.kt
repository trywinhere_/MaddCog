package com.maddcog.android.data.sensordatarecorder.utils

data class MultiComponent(
    var first: Any? = null,
    var second: Any? = null,
    var third: Any? = null,
    var fourth: Any? = null,
    var fifth: Any? = null
)