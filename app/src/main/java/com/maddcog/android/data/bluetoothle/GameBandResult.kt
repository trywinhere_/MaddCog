package com.maddcog.android.data.bluetoothle

data class GameBandResult(
    val connectionState: ConnectionState
)