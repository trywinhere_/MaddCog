package com.maddcog.android.data.network.dto.raw

data class HrRawData(
    val sensorData: Map<String, List<HrRaw>>
)