package com.maddcog.android.data.network.dto.raw

data class HrRaw(
    val rr: Int,
    val rrraw: Int,
    val timestamp: Int
)