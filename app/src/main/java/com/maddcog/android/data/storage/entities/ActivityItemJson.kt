package com.maddcog.android.data.storage.entities

import com.maddcog.android.domain.entities.ActivityItem
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class ActivityItemJson(
    @SerialName("id")
    val id: Int,
    @SerialName("name")
    var name: String,
    @SerialName("gameType")
    val gameType: String,
    @SerialName("hasGrade")
    val hasGrade: Boolean,
    @SerialName("hasTeamScore")
    val hasTeamScore: Boolean,
    @SerialName("imageBig")
    val imageBig: String,
    @SerialName("imageSmall")
    val imageSmall: String,
    @SerialName("maddcogIntegrated")
    val maddcogIntegrated: String,
    @SerialName("maddcogOwned")
    val maddcogOwned: Boolean,
)

fun ActivityItemJson.toDomain() = ActivityItem(
    id = id,
    name = name,
    gameType = gameType,
    hasGrade = hasGrade,
    hasTeamScore = hasTeamScore,
    imageBig = imageBig,
    imageSmall = imageSmall,
    maddcogIntegrated = maddcogIntegrated,
    maddcogOwned = maddcogOwned,
)

fun ActivityItem.toData() = ActivityItemJson(
    id = id,
    name = name,
    gameType = gameType,
    hasGrade = hasGrade,
    hasTeamScore = hasTeamScore,
    imageBig = imageBig,
    imageSmall = imageSmall,
    maddcogIntegrated = maddcogIntegrated,
    maddcogOwned = maddcogOwned,
)