package com.maddcog.android.data.network.dto.rawActivity

data class RawActivityType(
    val activity_type_id: Int,
    val activity_type_name: String,
    val image_rectangle: String,
    val image_square: String
)