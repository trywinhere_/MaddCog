package com.maddcog.android.data.bluetoothle

import com.maddcog.android.domain.utils.ble.Resource
import kotlinx.coroutines.flow.StateFlow

interface IBleReceiveManager {
    val data: StateFlow<Resource<GameBandResult>>
    val dataAlerts: StateFlow<ConnectionState>
    fun connectToGameBand()
    fun reconnect()
    fun disconnect()
    fun startReceiving()
    fun closeConnection()
    fun readBatteryCharacteristic()
    fun startSearchForReconnect()
}