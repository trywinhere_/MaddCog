package com.maddcog.android.data.network.dto.activity

import com.google.gson.annotations.SerializedName
import kotlinx.serialization.Serializable

@Serializable
data class RemoteTeamMemberParam(
    @SerializedName("name")
    val name: String?,
    @SerializedName("value")
    val value: Double?
)