package com.maddcog.android.data.network.mappers

import com.maddcog.android.data.network.dto.activityType.ActivityTypeResponseItem
import com.maddcog.android.data.network.dto.team.RemoteTeam
import com.maddcog.android.data.network.dto.team.RemoteTeamMembers
import com.maddcog.android.data.network.dto.userProfile.*
import com.maddcog.android.data.network.dto.userProfile.RemoteProfile
import com.maddcog.android.data.network.dto.userProfile.RemoteUserGame
import com.maddcog.android.domain.entities.ActivityItem
import com.maddcog.android.domain.entities.UserGame
import com.maddcog.android.domain.entities.team.TeamMembers
import com.maddcog.android.domain.entities.team.Team
import com.maddcog.android.domain.entities.user.*

fun ActivityTypeResponseItem.toDomain(): ActivityItem {
    return ActivityItem(
        id = this.activityTypeId ?: 0,
        name = this.activityTypeName ?: "Some game",
        imageBig = this.imageRectangle ?: "",
        imageSmall = this.imageSquare ?: "",
        gameType = this.gameType ?: "",
        hasGrade = this.hasGrade ?: false,
        hasTeamScore = this.hasTeamScore ?: false,
        maddcogIntegrated = this.maddcogIntegrated ?: "",
        maddcogOwned = this.maddcogOwned ?: false,
    )
}

fun RemoteProfile.toDomain(): UserProfile? {
    return this.userData?.toDomain()?.let {
        UserProfile(
            id = this.userId ?: "0",
            email = this.email ?: "",
            name = this.name ?: "",
            userGames = this.userGames?.toDomain() ?: emptyList(),
            endpoint = this.endpoint ?: "",
            userData = it
        )
    }
}

fun List<RemoteUserGame>.toDomain(): List<UserGame> {
    val result = mutableListOf<UserGame>()
    for (i in this.indices) {
        result.add(
            UserGame(
                generatedId = i.toString(),
                gameNumber = this[i].game ?: 0,
                region = this[i].region ?: "",
                userName = this[i].gamerName,
                gameName = this[i].gameName ?: "",
                userAverages = this[i].userPerformanceParams?.map { it.toDomain() } ?: emptyList()
            )
        )
    }
    return result
}

fun RemoteUserAverages.toDomain(): UserAverage {
    return UserAverage(
        name = this.name ?: "",
        avg = this.avg ?: 0.0,
        median = this.median ?: 0.0,
    )
}

fun RemoteUserData.toDomain(): UserData {
    return UserData(
        fatiguePercentiles = this.fatiguePercentiles?.toDomain(),
        fatiguePred = this.fatiguePred?.toDomain(),
        fatigueStart = this.fatigueStart?.toDomain(),
        flow = this.flow?.toDomain(),
        flowSd = this.flowSd?.toDomain(),
        focus = this.focus?.toDomain(),
        hb5min = this.hb5min?.toDomain(),
        lfhf5min = this.lfhf5min?.toDomain(),
        mf5min = this.mf5min?.toDomain(),
        tablog5min = this.tablog5min?.toDomain(),
        tplog5min = this.tplog5min?.toDomain(),
        trendsInsight = this.trendsInsight,
        trendsInsightValue = this.trendsInsightValue,
        userInsight = this.userInsight,
        wtalphaavgcurt = this.wtalphaavgcurt?.toDomain(),
        wtbetaavgcurt = this.wtbetaavgcurt?.toDomain()
    )
}

fun RemoteFatiguePercentiles.toDomain(): FatiguePercentiles {
    return FatiguePercentiles(
        high = this.high,
        low = this.low,
        max = this.max,
        base = this.base
    )
}

fun RemoteFatiguePred.toDomain(): FatiguePred {
    return FatiguePred(
        count = this.count,
        min = this.min,
        startingP2 = this.starting_p2,
        max = this.max,
        startingP1 = this.starting_p1,
        countVal = this.count_val
    )
}

fun RemoteFatigueStart.toDomain(): FatigueStart {
    return FatigueStart(
        count = this.count,
        sd = this.sd,
        mean = this.mean
    )
}

fun RemoteFlow.toDomain(): Flow {
    return Flow(
        count = this.count,
        sd = this.sd,
        mean = this.mean
    )
}

fun RemoteFlowSd.toDomain(): FlowSd {
    return FlowSd(
        count = this.count,
        sd = this.sd,
        mean = this.mean
    )
}

fun RemoteFocus.toDomain(): Focus {
    return Focus(
        count = this.count,
        sd = this.sd,
        mean = this.mean
    )
}

fun RemoteHb5min.toDomain(): Hb5min {
    return Hb5min(
        count = this.count,
        sd = this.sd,
        startingSd = this.starting_sd,
        startingMean = this.starting_mean,
        mean = this.mean
    )
}

fun RemoteLfhf5min.toDomain(): Lfhf5min {
    return Lfhf5min(
        count = this.count,
        sd = this.sd,
        startingSd = this.starting_sd,
        startingMean = this.starting_mean,
        mean = this.mean
    )
}

fun RemoteMf5min.toDomain(): Mf5min {
    return Mf5min(
        count = this.count,
        sd = this.sd,
        startingSd = this.starting_sd,
        startingMean = this.starting_mean,
        mean = this.mean
    )
}

fun RemoteTablog5min.toDomain(): Tablog5min {
    return Tablog5min(
        count = this.count,
        sd = this.sd,
        startingSd = this.starting_sd,
        startingMean = this.starting_mean,
        mean = this.mean
    )
}

fun RemoteTplog5min.toDomain(): Tplog5min {
    return Tplog5min(
        count = this.count,
        sd = this.sd,
        startingSd = this.starting_sd,
        startingMean = this.starting_mean,
        mean = this.mean
    )
}

fun RemoteWtalphaavgcurt.toDomain(): Wtalphaavgcurt {
    return Wtalphaavgcurt(
        count = this.count,
        sd = this.sd,
        startingSd = this.starting_sd,
        startingMean = this.starting_mean,
        mean = this.mean
    )
}

fun RemoteWtbetaavgcurt.toDomain(): Wtbetaavgcurt {
    return Wtbetaavgcurt(
        count = this.count,
        sd = this.sd,
        startingSd = this.starting_sd,
        startingMean = this.starting_mean,
        mean = this.mean
    )
}

fun ArrayList<RemoteTeamMembers>.toDomain(): ArrayList<TeamMembers> {
    val result = arrayListOf<TeamMembers>()
    for (i in this.indices) {
        result.add(
            TeamMembers(
                username = this[i].username,
                status = this[i].status,
                owner = this[i].owner,
                imageUrl = this[i].imageUrl,
                teamUserId = this[i].teamUserId
            )
        )
    }
    return result
}

fun RemoteTeam.toDomain(): Team {
    return Team(
        teamName = this.teamName,
        teamId = this.teamId,
        exchange = this.exchange,
        teamMembers = this.teamMembers.toDomain()
    )
}