package com.maddcog.android.data.utils

import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import org.joda.time.LocalDate
import org.joda.time.Period
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.DateTimeFormatter
import java.util.Locale
import javax.inject.Inject
import kotlin.math.round

class DateFormatter @Inject constructor() {

    companion object {
        private const val RESEND_CODE_FORMAT_TIME = "%01d:%02d"
        private const val TIME_FORMAT = "%02d:%02d"
        private const val SECOND_IN_MINUTE = 60
        private const val SECOND_IN_HOUR = 3600

        val dateTextFormat: DateTimeFormatter =
            DateTimeFormat.forPattern("dd MMM").withLocale(Locale.getDefault())

        val dateTimeFormat: DateTimeFormatter =
            DateTimeFormat.forPattern("dd.MM.yyyy HH:mm").withLocale(
                Locale.getDefault()
            )
        val timeDateFormatSlash: DateTimeFormatter =
            DateTimeFormat.forPattern("HH:mm dd/MM/yyyy").withLocale(
                Locale.getDefault()
            )
        val dateTimeFormatWithSlash: DateTimeFormatter =
            DateTimeFormat.forPattern("dd/MM/yyyy h:mm a").withLocale(
                Locale.getDefault()
            )
        val dateFormat: DateTimeFormatter = DateTimeFormat.forPattern("dd.MM.yyyy").withLocale(
            Locale.getDefault()
        )
        val dateFormatSlash: DateTimeFormatter = DateTimeFormat.forPattern("dd/MM/yyyy").withLocale(
            Locale.getDefault()
        )
        val dateFormatWithFullMonth: DateTimeFormatter =
            DateTimeFormat.forPattern("dd MMM yyyy").withLocale(
                Locale.getDefault()
            )

        val dateFormatWithFullMonthFirst: DateTimeFormatter =
            DateTimeFormat.forPattern("MMM dd yyyy").withLocale(
                Locale.getDefault()
            )
        val timeFormat: DateTimeFormatter = DateTimeFormat.forPattern("HH:mm").withLocale(
            Locale.getDefault()
        )
        val timeFormatHalfOfDay: DateTimeFormatter =
            DateTimeFormat.forPattern("HH:mm aa").withLocale(
                Locale.getDefault()
            )
        val dayFormat: DateTimeFormatter = DateTimeFormat.forPattern("EEE").withLocale(
            Locale.forLanguageTag("en")
        )
    }

    fun getDiscountDateTime(dateTime: DateTime): String {
        return getConverter(dateTime, dateTimeFormat)
    }

    fun getDateTimeSlash(dateTime: DateTime): String {
        return getConverter(dateTime, dateTimeFormatWithSlash)
    }

    fun getUTCDate(): Long {
        return DateTime.now().withZone(DateTimeZone.UTC).millis
    }

    fun getTimeDateSlashStr(dateTime: DateTime): String {
        return getConverter(dateTime, timeDateFormatSlash)
    }

    fun getOnlyHoursDate(string: String): DateTime {
        return timeFormat.parseDateTime(string)
    }

    fun getOnlyDateStr(dateTime: DateTime): String {
        return getConverter(dateTime, dateFormat)
    }

    fun getOnlyDateSlashStr(dateTime: DateTime): String {
        return getConverter(dateTime, dateFormatSlash)
    }

    fun getOnlyDateWithFullMonthStr(dateTime: DateTime): String {
        return getConverter(dateTime, dateFormatWithFullMonth)
    }

    fun getOnlyDateWithFullMonthFirstStr(dateTime: DateTime): String {
        return getConverter(dateTime, dateFormatWithFullMonthFirst)
    }

    fun getOnlyTextDateStr(dateTime: DateTime): String {
        return getConverter(dateTime, dateTextFormat)
    }

    fun getOnlyHoursStr(dateTime: DateTime): String {
        return getConverter(dateTime, timeFormat)
    }

    fun getOnlyHoursHalfOfDayStr(dateTime: DateTime): String {
        return getConverter(dateTime, timeFormatHalfOfDay)
    }

    fun getDayOfTheWeek(dateTime: DateTime): String {
        return getConverter(dateTime, dayFormat)
    }

    private fun getConverter(dateTime: DateTime, formatter: DateTimeFormatter): String =
        dateTime.toString(formatter)

    fun secondsToString(second: Long): String {
        return RESEND_CODE_FORMAT_TIME.format(
            second / SECOND_IN_MINUTE,
            second % SECOND_IN_MINUTE
        )
    }

    fun hoursToString(second: Long): String {
        return TIME_FORMAT.format(
            second / SECOND_IN_HOUR,
            round(second.toDouble() % SECOND_IN_HOUR / SECOND_IN_MINUTE).toInt()
        )
    }

    fun getDaysDifference(endDate: DateTime): String {
        val period = Period(DateTime.now(), endDate)
        return period.days.toString()
    }

    fun getHoursDifference(endDate: DateTime): String {
        val period = Period(DateTime.now(), endDate)
        return period.hours.toString()
    }

    fun getMinutesDifference(endDate: DateTime): String {
        val period = Period(DateTime.now(), endDate)
        return period.minutes.toString()
    }

    fun isToday(dateToCheck: DateTime): Boolean {
        return LocalDate.now().compareTo(LocalDate(dateToCheck)) == 0
    }

    fun isYesterday(dateToCheck: DateTime): Boolean {
        return LocalDate.now().minusDays(1).compareTo(LocalDate(dateToCheck)) == 0
    }
}