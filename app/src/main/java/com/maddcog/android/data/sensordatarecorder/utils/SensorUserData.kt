package com.maddcog.android.data.sensordatarecorder.utils

import android.util.Log
import java.util.concurrent.CopyOnWriteArrayList
import kotlin.math.max
import kotlin.math.min
import kotlin.math.roundToInt

data class SensorUserData(
    var userData: UserData? = UserData()
) {
    data class UserData(
        var lfhf5min: Feature? = Feature(startingMean = 6.78, startingSd = 2.96),
        var hb5min: Feature? = Feature(startingMean = 11181.00, startingSd = 8151.00),
        var mf5min: Feature? = Feature(startingMean = 9.5, startingSd = 2.98),
        var tablog5min: Feature? = Feature(startingMean = 1.6, startingSd = 0.06),
        var tplog5min: Feature? = Feature(startingMean = 55.86, startingSd = 4.34),
        var fatiguePred: Prediction? = Prediction(startingP1 = -0.24, startingP2 = 1.6),
        var wtbetaavgcurt: FeatureWT? = FeatureWT(startingMean = 0.62, startingSd = 0.065),
        var wtalphaavgcurt: FeatureWT? = FeatureWT(startingMean = 0.674, startingSd = 0.055),
        var trendsInsightValue: Double? = 0.0,
        var trendsInsight: String? = "",
        var userInsight: String? = "",
    ) {
        data class Feature(
            var mean: Double = 0.0,
            var sd: Double = 0.0,
            var count: Int = 0,
            var startingMean: Double = 0.0,
            var startingSd: Double = 0.0,
            var rollingList: CopyOnWriteArrayList<Pair<Int, Double>>? = CopyOnWriteArrayList(),
            var rollingValue: Double? = 0.0,
            var duration: Int? = 0,
            var value: Double? = 0.0,
            var newMean: Double? = 0.0,
            var newSd: Double? = 0.0,
            var zScore: Double? = 0.0,
        )

        data class Prediction(
            var min: Double = 0.0,
            var max: Double = 0.0,
            var count: Int = 0,
            var countVal: List<Int> = listOf(),
            var startingP1: Double = 0.0,
            var startingP2: Double = 0.0,
            var rollingList: CopyOnWriteArrayList<Pair<Int, Double>>? = CopyOnWriteArrayList(),
            var rollingValue: Double? = 0.0,
            var duration: Int? = 0,
            var value: Double? = 0.0,
            var percentile1Value: Double? = 0.0,
            var percentile2Value: Double? = 0.0,
            var score: Double? = 0.0,
            var percentile1: Double? = 0.0,
            var percentile2: Double? = 0.0,
        )

        data class FeatureWT(
            var mean: Double = 0.0,
            var sd: Double = 0.0,
            var count: Int = 0,
            var startingMean: Double = 0.0,
            var startingSd: Double = 0.0,
            var rollingList: CopyOnWriteArrayList<Pair<Int, Double>>? = CopyOnWriteArrayList(),
            var rollingValue: Double? = 0.0,
            var duration: Int? = 0,
            var value: Double? = 0.0,
            var newMean: Double? = 0.0,
            var newSd: Double? = 0.0,
            var zScore: Double? = 0.0,
        )
    }
}

fun SensorUserData.UserData.Feature.getZScore(newValue: Double, timestamp: Int): Double? {
    value = newValue

    if (rollingList == null) {
        rollingList = CopyOnWriteArrayList()
    }
    rollingList?.add(Pair(timestamp, newValue))
    rollingList?.let {
        val result = Utils.rollingAverage(it, timestamp, duration ?: 300)
        rollingValue = result.first
        rollingList = CopyOnWriteArrayList(result.second)
    }

    val oldmean = mean
    mean = Utils.newMean(oldMean = mean, newValue = rollingValue!!, oldSize = count)
    sd = Utils.newStDev(
        oldStDev = sd,
        newValue = rollingValue!!,
        oldMean = oldmean,
        newMean = mean,
        oldSize = count
    )

    count += 1
    if (count < 10800) {
        newMean = (startingMean * (10800.0 - count) / 10800.0) + (mean * (count.toDouble() / 10800.0))
        newSd = (startingSd * (10800.0 - count) / 10800.0) + (sd * (count.toDouble() / 10800.0))
    } else {
        newMean = mean
        newSd = sd
    }

    newSd?.let { newSd ->
        if (newSd > 0) {
            zScore = (rollingValue!! - newMean!!) / newSd
        }
    }

    return zScore
}

fun SensorUserData.UserData.FeatureWT.getZScore(newValue: Double, timestamp: Int): Double? {
    value = newValue
    if (rollingList == null) {
        rollingList = CopyOnWriteArrayList()
    }
    rollingList?.add(Pair(timestamp, newValue))

    rollingList?.let { list ->
        if (list.size == 5) {
            val (rv, rl) = Utils.computeWT(list)
            rollingValue = rv
            rollingList = CopyOnWriteArrayList(rl)

            val oldmean = mean
            mean = Utils.newMean(oldMean = mean, newValue = rollingValue!!, oldSize = count)
            sd = Utils.newStDev(
                oldStDev = sd,
                newValue = rollingValue!!,
                oldMean = oldmean,
                newMean = mean,
                oldSize = count
            )

            count += 1
            if (count < 10800) {
                newMean = (startingMean * (10800.0 - count.toDouble()) / 10800.0) + (mean * (count.toDouble() / 10800.0))
                newSd = (startingSd * (10800.0 - count.toDouble()) / 10800.0) + (sd * (count.toDouble() / 10800.0))
            } else {
                newMean = mean
                newSd = sd
            }

            newSd?.let { newSd ->
                if (newSd > 0) {
                    zScore = min(max(50.0 + (((rollingValue!! - newMean!!) / newSd) * 22), 0.0), 100.0)

                }
            }
        }
    }

    return zScore
}

fun SensorUserData.UserData.Prediction.getScore(newValue: Double, timestamp: Int, minus: Boolean): Double? {
    value = newValue
    if (rollingList == null) {
        rollingList = CopyOnWriteArrayList()
    }
    rollingList?.add(Pair(timestamp, newValue))
    rollingList?.let {
        val result = Utils.rollingAverage(it, timestamp, duration ?: 300)
        rollingValue = result.first
        rollingList = CopyOnWriteArrayList(result.second)
    }

    if (count == 0) {
        countVal = listOf(1)
        min = (rollingValue!! * 100).roundToInt() / 100.0
        max = (rollingValue!! * 100).roundToInt() / 100.0
        count += 1
    } else {
        if (((min - rollingValue!!) * 100).roundToInt() >= 1) {
            for (i in 1 until ((min - rollingValue!!) * 100).roundToInt()) {
                countVal = listOf(0) + countVal
            }
            countVal = listOf(1) + countVal
            min = (rollingValue!! * 100).roundToInt() / 100.0
        } else if (((rollingValue!! - max) * 100).roundToInt() >= 1) {
            for (i in 1 until ((rollingValue!! - max) * 100).roundToInt()) {
                countVal = countVal + listOf(0)
            }
            countVal = countVal + listOf(1)
            max = (rollingValue!! * 100).roundToInt() / 100.0
        } else {
            countVal = countVal.toMutableList().apply {
                val min = min((rollingValue!! - min).roundToInt(), countVal.size - 1)
                val max = max(0, min)
                set(
                    max(0, min(((rollingValue!! - min) * 100).roundToInt(), countVal.size - 1)),
                    countVal[max] + 1
                )
            }
        }
        count += 1
    }

    var sumValueCount = 0.0
    val countPercentile1 = (percentile1 ?: 0.01) * count
    for (i in countVal.indices) {
        sumValueCount += countVal[i].toDouble()
        if (sumValueCount >= countPercentile1) {
            percentile1Value = min + i * 0.01
            break
        }
    }
    if (count < 100) {
        percentile1Value = startingP1
    } else if (count < 10800) {
        percentile1Value =
            (count / 10800.0) * percentile1Value!! + ((10800.0 - count) / 10800.0) * startingP1
    }

    val countPercentile2 = (percentile2 ?: 0.99) * count.toDouble()
    sumValueCount = count.toDouble()
    for (i in countVal.indices) {
        val index = countVal.size - i - 1
        if (index in countVal.indices) {
            sumValueCount -= countVal[index].toDouble()
            if (sumValueCount <= countPercentile2) {
                percentile2Value = max - i.toDouble() * 0.01
                break
            }
        } else {
            Log.e("getScore", "Invalid index: $index, countVal.size: ${countVal.size}")
        }
    }
    if (count < 100) {
        percentile2Value = startingP2
    } else if (count < 10800) {
        percentile2Value =
            (count.toDouble() / 10800.0) * percentile2Value!! + ((10800.0 - count.toDouble()) / 10800.0) * startingP2
    }
    var predNorm = 0.0
    if (percentile2Value != percentile1Value) {
        predNorm = (rollingValue!! - percentile1Value!!) / (percentile2Value!! - percentile1Value!!)
    }
    if (minus) {
        score = min(max(100 * (1 - predNorm), 0.0), 100.0)
    } else {
        score = min(max(100 * predNorm, 0.0), 100.0)
    }
    return score
}