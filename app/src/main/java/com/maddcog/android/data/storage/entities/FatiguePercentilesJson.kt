package com.maddcog.android.data.storage.entities

import com.maddcog.android.domain.entities.user.FatiguePercentiles
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
class FatiguePercentilesJson(
    @SerialName("high")
    val high: Int? = null,
    @SerialName("low")
    val low: Int? = null,
    @SerialName("max")
    val max: Int? = null,
    @SerialName("base")
    val base: Int? = null
)

fun FatiguePercentilesJson.toDomain() = FatiguePercentiles(
    high = high,
    low = low,
    max = max,
    base = base
)

fun FatiguePercentiles.toData() = FatiguePercentilesJson(
    high = high,
    low = low,
    max = max,
    base = base
)