package com.maddcog.android.data.extensions

import kotlin.math.roundToInt

fun Double.roundNumbers(): Double {
    val number3digits = this.times(1000.0).let { it.roundToInt() }
        .div(1000.0)
    val number2digits = number3digits.times(100.0).let { it.roundToInt() }
        .div(100.0)
    val solution = number2digits.times(10.0).let { it.roundToInt() }.div(10.0)
    return solution
}