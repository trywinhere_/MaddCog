package com.maddcog.android.data.storage

import android.content.Context
import androidx.annotation.IntegerRes
import androidx.annotation.StringRes
import com.maddcog.android.domain.api.IResourcesProvider

class ResourceProviderImpl(
    private val context: Context,
) : IResourcesProvider {
    override fun getString(@StringRes id: Int): String = context.getString(id)

    override fun getStringFormatted(@StringRes id: Int, vararg args: Any): String =
        context.getString(id, *args)

    override fun getInteger(@IntegerRes id: Int): Int = context.resources.getInteger(id)
}