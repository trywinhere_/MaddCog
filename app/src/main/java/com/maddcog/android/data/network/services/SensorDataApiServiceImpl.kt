package com.maddcog.android.data.network.services

import com.maddcog.android.data.network.MaddcogApi
import com.maddcog.android.data.network.dto.raw.EegRaw
import com.maddcog.android.data.network.dto.raw.EegRawData
import com.maddcog.android.data.network.dto.raw.Event
import com.maddcog.android.data.network.dto.raw.HrRaw
import com.maddcog.android.data.network.dto.raw.HrRawData
import com.maddcog.android.data.network.dto.raw.SensorSendData
import com.maddcog.android.data.network.dto.rawActivity.RawActivitySummary
import com.maddcog.android.data.network.dto.rawActivity.RawActivityType
import com.maddcog.android.data.network.dto.rawActivity.RawMentalPerformance
import com.maddcog.android.data.network.dto.rawActivity.RawUserActivity
import com.maddcog.android.domain.api.ISensorDataApiService
import com.maddcog.android.domain.entities.ActivityItem
import com.maddcog.android.gameStats.GameStats

class SensorDataApiServiceImpl(
    private val api: MaddcogApi,
) : ISensorDataApiService {

    val MADDCOG_HRV_CHAR = "dab33654-9a44-4107-bb28-3abc12f52688"
    val MADDCOG_EEG_CHAR = "1688c50a-1344-4358-a43c-8bed435a11c7"

    override suspend fun sendSensorData() {
        api.sendSensorRawData(
            fakeSensorData
        )
    }

    override suspend fun sendRawActivity() {
        api.sendRawActivity(fakeRawActivity)
    }

    override suspend fun sendNewGameSession(gameStats: GameStats, mentalList: List<RawMentalPerformance>, activityItem: ActivityItem, uuid: String) {
        val gameStart = mentalList.maxOf { it.timestamp }
        val gameEnd = mentalList.minOf { it.timestamp }

        val newSession = RawUserActivity(
            activity_id = uuid,
            duration = gameEnd - gameStart,
            activity_summary = fakeActivitySummary,
            activity_type = RawActivityType(
                activity_type_id = activityItem.id,
                activity_type_name = activityItem.name,
                image_square = activityItem.imageSmall,
                image_rectangle = activityItem.imageBig
            ),
            mental_performance_timeline = mentalList,
            timestamp = gameStart,
        )

        api.sendRawActivity(newSession)
    }
}

private val mentalPerformanceList = mutableListOf<RawMentalPerformance>().apply {
    for (i in 0..305) {
        add(
            RawMentalPerformance(
                fatigue = if (i % 2 == 0) 60.0 + (i.toDouble() / 150.0) else 60 - (i.toDouble() / 150.0),
                flow = if (i % 2 == 0) 50.0 + (i.toDouble() / 150.0) else 50 - (i.toDouble() / 150.0),
                focus = if (i % 2 == 0) 75.0 + (i.toDouble() / 150.0) else 75 - (i.toDouble() / 150.0),
                timestamp = 1631917182 + i
            )
        )
    }
}

val fakeActivityType = RawActivityType(
    activity_type_id = 34,
    activity_type_name = "League of Legends",
    image_rectangle = "d1eki5k4rk47j4.cloudfront.net/LeagueofLegends683x400.jpg",
    image_square = "d1eki5k4rk47j4.cloudfront.net/LeagueofLegends400x512.jpg",
)

val fakeActivitySummary = RawActivitySummary(
    fatigue_avg = 57.74,
    fatigue_zone_high = 87.42,
    fatigue_zone_low = 59.71,
    fatigue_zone_normal = 62.91,
    flow_avg = 45.48,
    flow_zone_high = 24.5,
    flow_zone_low = 37.58,
    flow_zone_normal = 37.92,
)

val fakeRawActivity = RawUserActivity(
    activity_id = "bad04bbb-ebcb-41a6-afb8-5299f9a2dbbe",
    duration = 1421,
    timestamp = 1631917182,
    activity_type = RawActivityType(
        activity_type_id = 34,
        activity_type_name = "League of Legends",
        image_rectangle = "d1eki5k4rk47j4.cloudfront.net/LeagueofLegends683x400.jpg",
        image_square = "d1eki5k4rk47j4.cloudfront.net/LeagueofLegends400x512.jpg",
    ),
    activity_summary = RawActivitySummary(
        fatigue_avg = 57.74,
        fatigue_zone_high = 87.42,
        fatigue_zone_low = 59.71,
        fatigue_zone_normal = 62.91,
        flow_avg = 45.48,
        flow_zone_high = 24.5,
        flow_zone_low = 37.58,
        flow_zone_normal = 37.92,
    ),
    mental_performance_timeline = mentalPerformanceList
)

val fakeSensorData = SensorSendData(
    activity_id = "bad04bbb-ebcb-41a6-afb8-5299f9a2dbbe",
    eeg_raw_data = EegRawData(
        sensorData = mapOf(
            Pair(
                first = "30C1D682-D69B-D2D3-1AE6-BE623890345C",
                second = listOf(
                    EegRaw(
                        midgamma = 604,
                        signal = 0,
                        theta = 4687,
                        lowgamma = 1489,
                        highbeta = 7863,
                        highalpha = 831,
                        lowalpha = 1573,
                        timestamp = 1585587667,
                        delta = 12091,
                        lowbeta = 8213,
                    ),
                    EegRaw(
                        midgamma = 2337,
                        signal = 0,
                        theta = 15924,
                        lowgamma = 23368,
                        highbeta = 11712,
                        highalpha = 7367,
                        lowalpha = 7426,
                        timestamp = 1585587668,
                        delta = 613381,
                        lowbeta = 29021,
                    ),
                    EegRaw(
                        midgamma = 404,
                        signal = 0,
                        theta = 6131,
                        lowgamma = 2247,
                        highbeta = 3216,
                        highalpha = 1409,
                        lowalpha = 1284,
                        timestamp = 1585587669,
                        delta = 244005,
                        lowbeta = 2711,
                    )
                )
            )
        )
    ),
    hr_raw_data = HrRawData(
        sensorData = mapOf(
            Pair(
                first = "5472657C-54AA-FCE0-7A65-8221255E58F5",
                second = listOf(
                    HrRaw(
                        rrraw = 1071,
                        rr = 1071,
                        timestamp = 1585587666,
                    ),
                    HrRaw(
                        rrraw = 1109,
                        rr = 1109,
                        timestamp = 1585587667,
                    ),
                    HrRaw(
                        rrraw = 1119,
                        rr = 1119,
                        timestamp = 1585587670,
                    ),
                )
            )
        )
    ),
    events = listOf(
        Event(
            event_type = 2,
            percentage_correct = 98,
            response_time = 25.3,
            time_since_last = 560,
            timestamp = 1585587799,
            value = 12.3,
        ),
        Event(
            event_type = 2,
            percentage_correct = 98,
            response_time = 25.4,
            time_since_last = 260,
            timestamp = 1585587800,
            value = 12.5,
        ),
    ),
)