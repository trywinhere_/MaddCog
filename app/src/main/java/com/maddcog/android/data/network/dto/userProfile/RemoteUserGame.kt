package com.maddcog.android.data.network.dto.userProfile

import com.google.gson.annotations.SerializedName
import kotlinx.serialization.Serializable

@Serializable
data class RemoteUserGame(
    @SerializedName("game")
    val game: Int?,
    @SerializedName("game_name")
    val gameName: String?,
    @SerializedName("gamer_name")
    val gamerName: String?,
    @SerializedName("region")
    val region: String?,
    @SerializedName("user_performance_params")
    val userPerformanceParams: List<RemoteUserAverages>?,
)