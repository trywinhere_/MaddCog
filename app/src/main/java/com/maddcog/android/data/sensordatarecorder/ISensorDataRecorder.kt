package com.maddcog.android.data.sensordatarecorder

import com.maddcog.android.data.sensordatarecorder.utils.SampleEEG
import kotlinx.coroutines.flow.StateFlow

interface ISensorDataRecorder {

    val dataQuality: StateFlow<SensorDataQuality>
    val signalLevel: StateFlow<Int>
    val goodEEGcount: StateFlow<Int>
    val badEEGcount: StateFlow<Int>

    suspend fun parseEEGMaddcog(data: ByteArray?)

    suspend fun parseHeartRateMaddcog(data: ByteArray?)

    suspend fun parseBatteryMaddcog(level: Int?)

    suspend fun computeMetrics(sampleEEG: SampleEEG)
}