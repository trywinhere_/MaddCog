package com.maddcog.android.data.network.dto.activity

import com.google.gson.annotations.SerializedName
import kotlinx.serialization.Serializable

@Serializable
data class RemoteMyPlayer(
    @SerializedName("character_name")
    val characterName: String?,
    @SerializedName("character_image")
    val characterImage: String?,
    @SerializedName("fatigue")
    val fatigue: Double?,
    @SerializedName("fatigue_text")
    val fatigueText: String?,
    @SerializedName("fatigue_end")
    val fatigueEnd: Double?,
    @SerializedName("flow")
    val flow: Double?,
    @SerializedName("flow_text")
    val flowText: String?,
    @SerializedName("gamer_name")
    val gamerName: String?,
    @SerializedName("gamer_image")
    val gamerImage: String?,
    @SerializedName("grade")
    val grade: Double?,
    @SerializedName("outcome")
    val outcome: String?,
    @SerializedName("performance_params")
    val performanceParams: List<RemotePerformanceParam>?,
    @SerializedName("position")
    val position: Int?,
    @SerializedName("score")
    val score: Int?,
    @SerializedName("score_text")
    val scoreText: String?,
)