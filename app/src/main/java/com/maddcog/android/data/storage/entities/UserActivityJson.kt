package com.maddcog.android.data.storage.entities

import com.maddcog.android.domain.entities.activity.ActivitySummary
import com.maddcog.android.domain.entities.activity.Match
import com.maddcog.android.domain.entities.activity.MentalPerformanceTimeline
import com.maddcog.android.domain.entities.activity.MyPlayer
import com.maddcog.android.domain.entities.activity.PerformanceParam
import com.maddcog.android.domain.entities.activity.TeamMember
import com.maddcog.android.domain.entities.activity.TeamMemberParam
import com.maddcog.android.domain.entities.activity.TimelineEvent
import com.maddcog.android.domain.entities.activity.UserActivity
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import org.joda.time.DateTime

@Serializable
data class UserActivityJson(
    @SerialName("activityId")
    val activityId: String,
    @SerialName("duration")
    val duration: Int,
    @SerialName("mentalPerformanceTimeline")
    val mentalPerformanceTimeline: List<MentalPerformanceTimelineJson>,
    @SerialName("activityType")
    val activityType: ActivityItemJson,
    @SerialName("timestamp")
    val timestamp: String,
    @SerialName("activitySummary")
    val activitySummary: ActivitySummaryJson,
    @SerialName("match")
    var match: MatchJson,
    @SerialName("tags")
    var tags: List<String>,
    @SerialName("isForTest")
    val isForTest: Boolean,
)

fun UserActivityJson.toDomain() = UserActivity(
    activityId = activityId,
    duration = duration,
    mentalPerformanceTimeline = mentalPerformanceTimeline.map { it.toDomain() },
    activityType = activityType.toDomain(),
    timestamp = DateTime.parse(timestamp),
    activitySummary = activitySummary.toDomain(),
    match = match.toDomain(),
    tags = tags,
    isForTest = isForTest,
)

fun UserActivity.toData() = UserActivityJson(
    activityId = activityId,
    duration = duration,
    mentalPerformanceTimeline = mentalPerformanceTimeline.map { it.toData() },
    activityType = activityType.toData(),
    timestamp = timestamp.toString(),
    activitySummary = activitySummary.toData(),
    match = match.toData(),
    tags = tags,
    isForTest = isForTest,
)

@Serializable
data class MentalPerformanceTimelineJson(
    @SerialName("fatigue")
    val fatigue: Double,
    @SerialName("flow")
    val flow: Double,
    @SerialName("timestamp")
    val timestamp: Int,
)

fun MentalPerformanceTimelineJson.toDomain() = MentalPerformanceTimeline(
    fatigue = fatigue,
    flow = flow,
    timestamp = timestamp,
)

fun MentalPerformanceTimeline.toData() = MentalPerformanceTimelineJson(
    fatigue = fatigue,
    flow = flow,
    timestamp = timestamp,
)

@Serializable
data class ActivitySummaryJson(
    @SerialName("fatigueZoneHigh")
    var fatigueZoneHigh: Double,
    @SerialName("fatigueZoneNormal")
    val fatigueZoneNormal: Double,
    @SerialName("fatigueZoneLow")
    val fatigueZoneLow: Double,
    @SerialName("flowZoneNormal")
    val flowZoneNormal: Double,
    @SerialName("flowZoneHigh")
    var flowZoneHigh: Double,
    @SerialName("fatigueEndAvg")
    val fatigueEndAvg: Double,
    @SerialName("flowAvg")
    val flowAvg: Double,
    @SerialName("flowZoneLow")
    val flowZoneLow: Double,
    @SerialName("fatigueAvg")
    val fatigueAvg: Double,
)

fun ActivitySummaryJson.toDomain() = ActivitySummary(
    fatigueZoneHigh = fatigueZoneHigh,
    fatigueZoneNormal = fatigueZoneNormal,
    fatigueZoneLow = fatigueZoneLow,
    flowZoneNormal = flowZoneNormal,
    flowZoneHigh = flowZoneHigh,
    fatigueEndAvg = fatigueEndAvg,
    flowAvg = flowAvg,
    flowZoneLow = flowZoneLow,
    fatigueAvg = fatigueAvg,
)

fun ActivitySummary.toData() = ActivitySummaryJson(
    fatigueZoneHigh = fatigueZoneHigh,
    fatigueZoneNormal = fatigueZoneNormal,
    fatigueZoneLow = fatigueZoneLow,
    flowZoneNormal = flowZoneNormal,
    flowZoneHigh = flowZoneHigh,
    fatigueEndAvg = fatigueEndAvg,
    flowAvg = flowAvg,
    flowZoneLow = flowZoneLow,
    fatigueAvg = fatigueAvg,
)

@Serializable
data class MatchJson(
    @SerialName("start")
    val start: Int,
    @SerialName("end")
    val end: Int,
    @SerialName("gameMode")
    val gameMode: String,
    @SerialName("mapName")
    val mapName: String,
    @SerialName("myPlayer")
    var myPlayer: MyPlayerJson,
    @SerialName("stayedTillGameOver")
    val stayedTillGameOver: Boolean,
    @SerialName("team")
    val team: List<TeamMemberJson>,
    @SerialName("teamCtScore")
    val teamCtScore: Int,
    @SerialName("teamTScore")
    val teamTScore: Int,
    @SerialName("tileImage")
    val tileImage: String,
    @SerialName("tileMetricDescription")
    val tileMetricDescription: String,
    @SerialName("tileMetricText")
    val tileMetricText: String,
    @SerialName("timelineEvents")
    val timelineEvents: List<TimelineEventJson>,
)

fun MatchJson.toDomain() = Match(
    start = start,
    end = end,
    gameMode = gameMode,
    mapName = mapName,
    myPlayer = myPlayer.toDomain(),
    stayedTillGameOver = stayedTillGameOver,
    team = team.map { it.toDomain() },
    teamCtScore = teamCtScore,
    teamTScore = teamTScore,
    tileImage = tileImage,
    tileMetricDescription = tileMetricDescription,
    tileMetricText = tileMetricText,
    timelineEvents = timelineEvents.map { it.toDomain() },
)

fun Match.toData() = MatchJson(
    start = start,
    end = end,
    gameMode = gameMode,
    mapName = mapName,
    myPlayer = myPlayer.toData(),
    stayedTillGameOver = stayedTillGameOver,
    team = team.map { it.toData() },
    teamCtScore = teamCtScore,
    teamTScore = teamTScore,
    tileImage = tileImage,
    tileMetricDescription = tileMetricDescription,
    tileMetricText = tileMetricText,
    timelineEvents = timelineEvents.map { it.toData() },
)

@Serializable
data class TeamMemberJson(
    @SerialName("characterName")
    val characterName: String,
    @SerialName("characterImage")
    val characterImage: String,
    @SerialName("fatigue")
    val fatigue: Double,
    @SerialName("fatigueText")
    val fatigueText: String,
    @SerialName("flow")
    val flow: Double,
    @SerialName("flowText")
    val flowText: String,
    @SerialName("gamerImage")
    val gamerImage: String,
    @SerialName("gamerName")
    val gamerName: String,
    @SerialName("grade")
    val grade: Double,
    @SerialName("performanceParams")
    val performanceParams: List<TeamMemberParamJson>,
)

fun TeamMemberJson.toDomain() = TeamMember(
    characterName = characterName,
    characterImage = characterImage,
    fatigue = fatigue,
    fatigueText = fatigueText,
    flow = flow,
    flowText = flowText,
    gamerImage = gamerImage,
    gamerName = gamerName,
    grade = grade,
    performanceParams = performanceParams.map { it.toDomain() }
)

fun TeamMember.toData() = TeamMemberJson(
    characterName = characterName,
    characterImage = characterImage,
    fatigue = fatigue,
    fatigueText = fatigueText,
    flow = flow,
    flowText = flowText,
    gamerImage = gamerImage,
    gamerName = gamerName,
    grade = grade,
    performanceParams = performanceParams.map { it.toData() }
)

@Serializable
data class TeamMemberParamJson(
    @SerialName("name")
    val name: String,
    @SerialName("value")
    val value: Double,
)

fun TeamMemberParamJson.toDomain() = TeamMemberParam(
    name = name,
    value = value,
)

fun TeamMemberParam.toData() = TeamMemberParamJson(
    name = name,
    value = value,
)

@Serializable
data class TimelineEventJson(
    @SerialName("name")
    val name: String,
    @SerialName("timestamps")
    val timestamps: List<Int>,
)

fun TimelineEventJson.toDomain() = TimelineEvent(
    name = name,
    timestamps = timestamps,
)

fun TimelineEvent.toData() = TimelineEventJson(
    name = name,
    timestamps = timestamps,
)

@Serializable
data class MyPlayerJson(
    @SerialName("characterName")
    val characterName: String,
    @SerialName("characterImage")
    val characterImage: String,
    @SerialName("fatigue")
    val fatigue: Double,
    @SerialName("fatigueText")
    val fatigueText: String,
    @SerialName("fatigueEnd")
    val fatigueEnd: Double,
    @SerialName("flow")
    val flow: Double,
    @SerialName("flowText")
    val flowText: String,
    @SerialName("gamerName")
    val gamerName: String,
    @SerialName("gamerImage")
    val gamerImage: String,
    @SerialName("grade")
    var grade: Double,
    @SerialName("outcome")
    var outcome: String,
    @SerialName("performanceParams")
    val performanceParams: List<PerformanceParamJson>,
    @SerialName("position")
    val position: Int,
    @SerialName("score")
    val score: Int,
    @SerialName("scoreText")
    val scoreText: String,
)

fun MyPlayerJson.toDomain() = MyPlayer(
    characterName = characterName,
    characterImage = characterImage,
    fatigue = fatigue,
    fatigueText = fatigueText,
    fatigueEnd = fatigueEnd,
    flow = flow,
    flowText = flowText,
    gamerName = gamerName,
    gamerImage = gamerImage,
    grade = grade,
    outcome = outcome,
    performanceParams = performanceParams.map { it.toDomain() },
    position = position,
    score = score,
    scoreText = scoreText,
)

fun MyPlayer.toData() = MyPlayerJson(
    characterName = characterName,
    characterImage = characterImage,
    fatigue = fatigue,
    fatigueText = fatigueText,
    fatigueEnd = fatigueEnd,
    flow = flow,
    flowText = flowText,
    gamerName = gamerName,
    gamerImage = gamerImage,
    grade = grade,
    outcome = outcome,
    performanceParams = performanceParams.map { it.toData() },
    position = position,
    score = score,
    scoreText = scoreText,
)

@Serializable
data class PerformanceParamJson(
    @SerialName("name")
    val name: String,
    @SerialName("value")
    val value: Double,
    @SerialName("order")
    val order: Int,
)

fun PerformanceParamJson.toDomain() = PerformanceParam(
    name = name,
    value = value,
    order = order,
)

fun PerformanceParam.toData() = PerformanceParamJson(
    name = name,
    value = value,
    order = order,
)