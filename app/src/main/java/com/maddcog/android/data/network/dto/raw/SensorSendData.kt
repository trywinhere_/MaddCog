package com.maddcog.android.data.network.dto.raw

data class SensorSendData(
    val activity_id: String,
    val eeg_raw_data: EegRawData,
    val events: List<Event>,
    val hr_raw_data: HrRawData
)