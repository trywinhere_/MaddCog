package com.maddcog.android.data.storage.entities

import com.maddcog.android.domain.entities.user.UserData
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
class UserDataJson(
    @SerialName("fatiguePercentiles")
    val fatiguePercentiles: FatiguePercentilesJson? = FatiguePercentilesJson(),
    @SerialName("fatiguePred")
    val fatiguePred: FatiguePredJson? = FatiguePredJson(),
    @SerialName("fatigueStart")
    val fatigueStart: FatigueStartJson? = FatigueStartJson(),
    @SerialName("flow")
    val flow: FlowJson? = FlowJson(),
    @SerialName("flowSd")
    val flowSd: FlowSdJson? = FlowSdJson(),
    @SerialName("focus")
    val focus: FocusJson? = FocusJson(),
    @SerialName("hb5min")
    val hb5min: Hb5minJson? = Hb5minJson(),
    @SerialName("lfhf5min")
    val lfhf5min: Lfhf5minJson? = Lfhf5minJson(),
    @SerialName("mf5min")
    val mf5min: Mf5minJson? = Mf5minJson(),
    @SerialName("tablog5min")
    val tablog5min: Tablog5minJson? = Tablog5minJson(),
    @SerialName("tplog5min")
    val tplog5min: Tplog5minJson? = Tplog5minJson(),
    @SerialName("trendsInsight")
    val trendsInsight: String,
    @SerialName("trendsInsightValue")
    val trendsInsightValue: Int,
    @SerialName("userInsight")
    val userInsight: String,
    @SerialName("wtalphaavgcurt")
    val wtalphaavgcurt: WtalphaavgcurtJson? = WtalphaavgcurtJson(),
    @SerialName("wtbetaavgcurt")
    val wtbetaavgcurt: WtbetaavgcurtJson? = WtbetaavgcurtJson()
)

fun UserDataJson.toDomain() = UserData(
    fatiguePercentiles = fatiguePercentiles?.toDomain(),
    fatiguePred = fatiguePred?.toDomain(),
    fatigueStart = fatigueStart?.toDomain(),
    flow = flow?.toDomain(),
    flowSd = flowSd?.toDomain(),
    focus = focus?.toDomain(),
    hb5min = hb5min?.toDomain(),
    lfhf5min = lfhf5min?.toDomain(),
    mf5min = mf5min?.toDomain(),
    tablog5min = tablog5min?.toDomain(),
    tplog5min = tplog5min?.toDomain(),
    trendsInsight = trendsInsight,
    trendsInsightValue = trendsInsightValue,
    userInsight = userInsight,
    wtalphaavgcurt = wtalphaavgcurt?.toDomain(),
    wtbetaavgcurt = wtbetaavgcurt?.toDomain()
)

fun UserData.toData() = UserDataJson(
    fatiguePercentiles = fatiguePercentiles?.toData(),
    fatiguePred = fatiguePred?.toData(),
    fatigueStart = fatigueStart?.toData(),
    flow = flow?.toData(),
    flowSd = flowSd?.toData(),
    focus = focus?.toData(),
    hb5min = hb5min?.toData(),
    lfhf5min = lfhf5min?.toData(),
    mf5min = mf5min?.toData(),
    tablog5min = tablog5min?.toData(),
    tplog5min = tplog5min?.toData(),
    trendsInsight = trendsInsight,
    trendsInsightValue = trendsInsightValue,
    userInsight = userInsight,
    wtalphaavgcurt = wtalphaavgcurt?.toData(),
    wtbetaavgcurt = wtbetaavgcurt?.toData()
)