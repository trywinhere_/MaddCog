package com.maddcog.android.data.network.services

import com.maddcog.android.data.network.MaddcogApi
import com.maddcog.android.data.network.mappers.toDomain
import com.maddcog.android.domain.api.IActivitiesApiService
import com.maddcog.android.domain.entities.ActivityItem
import com.maddcog.android.domain.entities.activity.UserActivity

open class ActivityListApiServiceImpl(
    private val api: MaddcogApi,
) : IActivitiesApiService {

    override suspend fun loadAllActivitiesList(): List<ActivityItem> {
        return api.loadAllActivities().map {
            it.toDomain()
        }
    }

    override suspend fun getUserActivities(): List<UserActivity> {
        return api.getUserActivities().activities.sortedBy { it.timestamp }.map { it.toDomain() }
    }

    override suspend fun getUserActivityById(userActivityId: String): UserActivity {
        return api.loadUserActivityById(userActivityId).toDomain()
    }
}