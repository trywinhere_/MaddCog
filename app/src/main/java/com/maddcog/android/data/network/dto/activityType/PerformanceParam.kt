package com.maddcog.android.data.network.dto.activityType

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class PerformanceParam(
    @SerialName("name")
    val name: String
)