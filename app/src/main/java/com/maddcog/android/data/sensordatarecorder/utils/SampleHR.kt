package com.maddcog.android.data.sensordatarecorder.utils

import kotlin.math.abs

data class SampleHR(
    var value: Quintuple<Int, Int, Int, Double, Int>? = null
) {
    private var previousValues: MutableList<Quintuple<Int, Int, Int, Double, Int>> = mutableListOf()
    private var skipCount = 0
    var countBadRrSample = 0
    var countGoodRrSample = 0
    var timestamp = 0
    var rr: Double = -1.0
    var rrRaw: Double = -1.0
    var confidence: Int = -1
    var hr: Int = -1
    var status: String = "unknown"

    private fun <A, B, C, D, E> quintupleOf(
        a: A,
        b: B,
        c: C,
        d: D,
        e: E
    ): Quintuple<A, B, C, D, E> =
        Quintuple(a, b, c, d, e)

    private fun calculateMedianRR(values: List<Quintuple<Int, Int, Int, Double, Int>>): Double {
        val sorted = values.sortedBy { it.fourth }
        return if (sorted.size % 2 == 0) {
            (sorted[sorted.size / 2 - 1].fourth + sorted[sorted.size / 2].fourth) / 2
        } else {
            sorted[sorted.size / 2].fourth
        }
    }

    private fun processValue(newValue: Quintuple<Int, Int, Int, Double, Int>) {
        val (t, h, s, v, c) = newValue
        value = newValue

        when {
            previousValues.size == 0 && (v > 2000 || v < 400) ->
                previousValues.add(quintupleOf(t, h, s, 1000.0, c))
            previousValues.size == 1 && (v > 2000 || v < 400) -> {
                val (_, h0, s0, v0, c0) = previousValues[0]
                previousValues.add(quintupleOf(t, h0, s0, v0, c0))
            }
            else -> previousValues.add(newValue)
        }


        if (previousValues.size == 5) {
            val (t2, h2, s2, v2, c2) = previousValues[2]
            rrRaw = v2
            var error = false
            if (v2 > 2000 || v2 < 400) {
                previousValues.removeAt(2)
                val medianRr = calculateMedianRR(listOf(previousValues[0], previousValues[1]))
                previousValues.add(2, quintupleOf(t2, h2, s2, medianRr, c2))
                countBadRrSample += 1
                error = true
            }
            val medianRr = calculateMedianRR(previousValues)
            if (abs(v2 - medianRr) > 0.25 * medianRr || (c2 != -1 && c2 <= 25)) {
                previousValues.removeAt(2)
                val previousValuesMedian = previousValues.toMutableList()
                val (_, _, _, _, cIndex2) = previousValues[2]
                val (_, _, _, _, cIndex3) = previousValues[3]
                if (cIndex2 <= 25) {
                    previousValuesMedian.removeAt(2)
                    if (cIndex3 <= 25) {
                        previousValuesMedian.removeAt(2)
                    }
                } else if (cIndex3 <= 25) {
                    previousValuesMedian.removeAt(3)
                }
                val newMedianRr = calculateMedianRR(previousValuesMedian)
                previousValues.add(2, quintupleOf(t2, h2, s2, newMedianRr, c2))
                if (!error) {
                    countBadRrSample += 1
                    error = true
                }
                skipCount = 2
            } else {
                skipCount = maxOf(skipCount - 1, 0)
                countGoodRrSample += 1
            }
            val (t2f, h2f, s2f, v2f, c2f) = previousValues[2]
            rr = v2f
            timestamp = t2f
            confidence = c2f
            hr = h2f
            status = when (s2f) {
                0, -1 -> "unknown"
                1 -> "off_skin"
                2 -> "on_some_subject"
                3 -> "on_skin"
                else -> status
            }
            previousValues.removeFirst()
        }
    }

    var newValue: Quintuple<Int, Int, Int, Double, Int>?
        get() = value?.let { quintupleOf(it.first, it.second, it.third, it.fourth, it.fifth) }
        set(value) {
            value?.let { processValue(it) }
        }

    data class Quintuple<A, B, C, D, E>(
        val first: A,
        val second: B,
        val third: C,
        val fourth: D,
        val fifth: E
    )
}