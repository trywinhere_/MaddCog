package com.maddcog.android.data.network.dto

data class DeleteGame(
    val gamer_name: String,
    val region: String?,
    val game: Int,
)