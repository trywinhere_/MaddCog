package com.maddcog.android.data.network.dto.userProfile

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class RemoteFlowSd(
    @SerialName("count") var count: Int? = null,
    @SerialName("sd") var sd: Double? = null,
    @SerialName("mean") var mean: Double? = null
)