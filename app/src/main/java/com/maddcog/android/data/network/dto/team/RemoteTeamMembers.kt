package com.maddcog.android.data.network.dto.team

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class RemoteTeamMembers(
    @SerialName("team_user_name") var username: String,
    @SerialName("status") var status: Int,
    @SerialName("owner") var owner: Boolean,
    @SerialName("team_user_image") var imageUrl: String,
    @SerialName("team_user_id") var teamUserId: String
)