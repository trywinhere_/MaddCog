package com.maddcog.android.data.sensordatarecorder.utils

data class SampleEEG(
    var signal: Int? = null,
    var delta: Int? = null,
    var theta: Int? = null,
    var lowAlpha: Int? = null,
    var highAlpha: Int? = null,
    var lowBeta: Int? = null,
    var highBeta: Int? = null,
    var lowGamma: Int? = null,
    var midGamma: Int? = null,
    var timestamp: Int? = null,
    var isReady: Boolean = false,
)