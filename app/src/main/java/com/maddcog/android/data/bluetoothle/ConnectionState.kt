package com.maddcog.android.data.bluetoothle

sealed interface ConnectionState {
    object DeviceFound: ConnectionState
    object Connected: ConnectionState
    object Disconnected: ConnectionState
    object Uninitialized: ConnectionState
    object CurrentlyInitializing: ConnectionState
    object CannotFoundDevice: ConnectionState
    object NotDefined: ConnectionState
}