package com.maddcog.android.data.network.dto.rawActivity

data class RawUserActivity(
    val activity_id: String,
    val activity_summary: RawActivitySummary,
    val activity_type: RawActivityType,
    val duration: Int,
    val mental_performance_timeline: List<RawMentalPerformance>,
    val timestamp: Int
)