package com.maddcog.android.data.storage.entities

import com.maddcog.android.domain.entities.user.Focus
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
class FocusJson(
    @SerialName("count")
    val count: Int? = null,
    @SerialName("sd")
    val sd: Double? = null,
    @SerialName("mean")
    val mean: Double? = null
)

fun FocusJson.toDomain() = Focus(
    count = count,
    sd = sd,
    mean = mean
)

fun Focus.toData() = FocusJson(
    count = count,
    sd = sd,
    mean = mean
)