package com.maddcog.android.data.sensordatarecorder

import com.maddcog.android.data.network.dto.rawActivity.RawMentalPerformance
import com.maddcog.android.data.sensordatarecorder.utils.EEG
import com.maddcog.android.data.sensordatarecorder.utils.HR
import com.maddcog.android.data.sensordatarecorder.utils.SampleEEG
import com.maddcog.android.data.sensordatarecorder.utils.SampleHR
import com.maddcog.android.data.sensordatarecorder.utils.SensorDataActivity
import kotlinx.coroutines.flow.StateFlow

interface IStorage {

    val batteryLevel: StateFlow<Int>
    val mentalPerformanceQuantity: StateFlow<Int>
    val lastMentalPerformance: StateFlow<RawMentalPerformance>

    suspend fun getRawActivity(): SensorDataActivity

    suspend fun getHrData(): HashMap<String, List<HR>>

    suspend fun getEegData(): HashMap<String, List<EEG>>

    suspend fun addHrData(identifier: String, data: SampleHR)

    suspend fun addEegData(identifier: String, data: SampleEEG)

    suspend fun clear()

    suspend fun saveMentalPerformance(mentalPerformance: RawMentalPerformance)

    suspend fun getMentalPerformanceList(): List<RawMentalPerformance>

    suspend fun saveBatteryLevel(currentLevel: Int)

    suspend fun getBatteryLevel(): Int
}