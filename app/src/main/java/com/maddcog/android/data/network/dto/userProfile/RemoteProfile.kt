package com.maddcog.android.data.network.dto.userProfile

import com.google.gson.annotations.SerializedName
import kotlinx.serialization.Serializable

@Serializable
data class RemoteProfile(
    @SerializedName("created")
    val created: Int?,
    @SerializedName("email")
    val email: String?,
    @SerializedName("endpoint")
    val endpoint: String?,
    @SerializedName("name")
    val name: String?,
    @SerializedName("user_data")
    val userData: RemoteUserData? = null,
    @SerializedName("user_games")
    val userGames: List<RemoteUserGame>?,
    @SerializedName("user_id")
    val userId: String?
)