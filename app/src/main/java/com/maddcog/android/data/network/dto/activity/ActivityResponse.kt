package com.maddcog.android.data.network.dto.activity

import com.google.gson.annotations.SerializedName
import kotlinx.serialization.Serializable

@Serializable
data class ActivityResponse(
    @SerializedName("activities")
    val activities: List<RemoteActivity>
)