package com.maddcog.android.data.network.dto.userProfile

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class RemoteFatiguePred(
    @SerialName("count") var count: Int? = null,
    @SerialName("min") var min: Double? = null,
    @SerialName("starting_p2") var starting_p2: Double? = null,
    @SerialName("max") var max: Double? = null,
    @SerialName("starting_p1") var starting_p1: Double? = null,
    @SerialName("count_val") var count_val: ArrayList<Int> = arrayListOf()
)