package com.maddcog.android.data.sensordatarecorder

import com.maddcog.android.data.bluetoothle.toHexString
import com.maddcog.android.data.network.dto.rawActivity.RawMentalPerformance
import com.maddcog.android.data.sensordatarecorder.utils.FftAnalyzer
import com.maddcog.android.data.sensordatarecorder.utils.SampleEEG
import com.maddcog.android.data.sensordatarecorder.utils.SampleHR
import com.maddcog.android.data.sensordatarecorder.utils.SensorUserData
import com.maddcog.android.data.sensordatarecorder.utils.getScore
import com.maddcog.android.data.sensordatarecorder.utils.getZScore
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import java.util.*
import javax.inject.Inject
import kotlin.math.ln

class SensorDataRecorder @Inject constructor(
    private val rawDataStorage: IStorage,
) : ISensorDataRecorder {

    private val _dataQuality = MutableStateFlow(SensorDataQuality.NotDefined)
    override val dataQuality: StateFlow<SensorDataQuality> get() = _dataQuality.asStateFlow()

    private val _signalLevel = MutableStateFlow(-1)
    override val signalLevel: StateFlow<Int> get() = _signalLevel.asStateFlow()

    private val _goodEEGcount = MutableStateFlow(-1)
    override val goodEEGcount: StateFlow<Int> get() = _goodEEGcount.asStateFlow()

    private val _badEEGcount = MutableStateFlow(-1)
    override val badEEGcount: StateFlow<Int> get() = _badEEGcount.asStateFlow()

    val MADDCOG_HRV_CHAR = "dab33654-9a44-4107-bb28-3abc12f52688"
    val MADDCOG_EEG_CHAR = "1688c50a-1344-4358-a43c-8bed435a11c7"

    /** EEG test data + all necessary methods */
    var currentSampleEeg = mutableMapOf<String, SampleEEG>()
    var user = SensorUserData()
    var recordSensorsData = true
    var fatigue: Double? = null
    var focus: Double? = null
    var flow: Double? = null
    val peripheral = "EEG_TEST"
    var countGoodEeg = 0
    var countBadEeg = 0
    var receivedGoodEeg = false
    var hrArray = arrayListOf<ByteArray>()

    init {
        currentSampleEeg[peripheral] = SampleEEG()
    }

    override suspend fun parseEEGMaddcog(data: ByteArray?) {

        val parsedEeg = data?.toHexString()?.let {
            it.split(" ").map { char ->
                if (char == "0x00") {
                    0
                } else {
                    char.toInt(16)
                }
            }
        }

        val data2 = parsedEeg?.let { it[2] }
        parsedEeg?.let { data ->
            if (data.size >= 20) {
                when (data[2]) {
                    234 -> {
                        if (data[5] == 2) {
                            currentSampleEeg[peripheral]?.let {
                                it.signal = data[6]
                                _signalLevel.value = data[6]
                            }
                        }
                    }
                    235 -> {
                        var dataUint: MutableList<UInt> = mutableListOf()
                        if (data[4] == 6) {
                            dataUint = mutableListOf()
                            dataUint.add(data[5].toUInt())
                            dataUint.add(data[6].toUInt())
                            dataUint.add(data[7].toUInt())
                            val delta =
                                (dataUint[0] shl 16) or (dataUint[1] shl 8) or dataUint[2]
                            currentSampleEeg[peripheral]?.let {
                                it.delta = delta.toInt()
                            }
                        }
                        if (data[8] == 7) {
                            dataUint = mutableListOf()
                            dataUint.add(data[9].toUInt())
                            dataUint.add(data[10].toUInt())
                            dataUint.add(data[11].toUInt())
                            val theta =
                                (dataUint[0] shl 16) or (dataUint[1] shl 8) or dataUint[2]
                            currentSampleEeg[peripheral]?.let {
                                it.theta = theta.toInt()
                            }
                        }
                        if (data[12] == 8) {
                            dataUint = mutableListOf()
                            dataUint.add(data[13].toUInt())
                            dataUint.add(data[14].toUInt())
                            dataUint.add(data[15].toUInt())
                            val lowAlpha =
                                (dataUint[0] shl 16) or (dataUint[1] shl 8) or dataUint[2]
                            currentSampleEeg[peripheral]?.let {
                                it.lowAlpha = lowAlpha.toInt()
                            }
                        }
                        if (data[16] == 9) {
                            dataUint = mutableListOf()
                            dataUint.add(data[17].toUInt())
                            dataUint.add(data[18].toUInt())
                            dataUint.add(data[19].toUInt())
                            val highAlpha =
                                (dataUint[0] shl 16) or (dataUint[1] shl 8) or dataUint[2]
                            currentSampleEeg[peripheral]?.let {
                                it.highAlpha = highAlpha.toInt()
                            }
                        }
                    }
                    236 -> {
                        var dataUint: MutableList<UInt> = mutableListOf()
                        if (data[4] == 10) {
                            dataUint = mutableListOf()
                            dataUint.add(data[5].toUInt())
                            dataUint.add(data[6].toUInt())
                            dataUint.add(data[7].toUInt())
                            val lowBeta =
                                (dataUint[0] shl 16) or (dataUint[1] shl 8) or dataUint[2]
                            currentSampleEeg[peripheral]?.let {
                                it.lowBeta = lowBeta.toInt()
                            }
                        }
                        if (data[8] == 11) {
                            dataUint = mutableListOf()
                            dataUint.add(data[9].toUInt())
                            dataUint.add(data[10].toUInt())
                            dataUint.add(data[11].toUInt())
                            val highBeta =
                                (dataUint[0] shl 16) or (dataUint[1] shl 8) or dataUint[2]
                            currentSampleEeg[peripheral]?.let {
                                it.highBeta = highBeta.toInt()
                            }
                        }
                        if (data[12] == 12) {
                            dataUint = mutableListOf()
                            dataUint.add(data[13].toUInt())
                            dataUint.add(data[14].toUInt())
                            dataUint.add(data[15].toUInt())
                            val lowGamma =
                                (dataUint[0] shl 16) or (dataUint[1] shl 8) or dataUint[2]
                            currentSampleEeg[peripheral]?.let {
                                it.lowGamma = lowGamma.toInt()
                            }
                        }
                        if (data[16] == 13) {
                            dataUint = mutableListOf()
                            dataUint.add(data[17].toUInt())
                            dataUint.add(data[18].toUInt())
                            dataUint.add(data[19].toUInt())
                            val midGamma =
                                (dataUint[0] shl 16) or (dataUint[1] shl 8) or dataUint[2]
                            currentSampleEeg[peripheral]?.let {
                                it.midGamma = midGamma.toInt()
                            }
                        }
                    }
                    else -> {
                        println("Something going wrong parse EEG")
                    }
                }
            }
            saveDataCheck(peripheral)
        }
    }

    private fun dataQualityCheck(good: Int, bad: Int, currentTime: Int) {
        _goodEEGcount.value = good
        _badEEGcount.value = bad
        if (good >= 5) {
            _dataQuality.value = SensorDataQuality.Good
            receivedGoodEeg = true
        }
        if (bad >= 10) {
            _dataQuality.value = SensorDataQuality.EegError
            receivedGoodEeg = false
        }
        if (currentTime - lastRrTimestamp > 5) {
            receivedGoodHrData = false
            _dataQuality.value = SensorDataQuality.HrError
        } else {
            receivedGoodHrData = true
        }
    }

    private suspend fun saveDataCheck(peripheral: String) {
        val currentEeg = currentSampleEeg[peripheral]
        val currentTime = (Date().time / 1000L).toInt()

        currentEeg?.signal?.let { signal ->
            if (signal <= 26) {
                countGoodEeg += 1
                countBadEeg = 0
                dataQualityCheck(countGoodEeg, countBadEeg, currentTime)
            } else {
                countGoodEeg = 0
                countBadEeg += 1
                dataQualityCheck(countGoodEeg, countBadEeg, currentTime)
            }
        }

        currentSampleEeg[peripheral]?.let { sampleEEG ->
            sampleEEG.isReady =
                sampleEEG.signal != null && sampleEEG.delta != null && sampleEEG.theta != null && sampleEEG.lowAlpha != null && sampleEEG.highAlpha != null && sampleEEG.lowBeta != null && sampleEEG.highBeta != null && sampleEEG.lowGamma != null && sampleEEG.midGamma != null
            if (sampleEEG.isReady) {
                val timestamp = (Date().time / 1000L).toInt()
                sampleEEG.timestamp = timestamp
                if (receivedGoodEeg) {
                    rawDataStorage.addEegData(MADDCOG_EEG_CHAR, sampleEEG)
                    computeMetrics(sampleEEG)
                    currentSampleEeg[peripheral] = SampleEEG()
                }
            }
        }
    }

    override suspend fun computeMetrics(sampleEEG: SampleEEG) {
        val sum = sampleEEG.delta!!.toDouble() + sampleEEG.theta!!.toDouble() +
                sampleEEG.lowAlpha!!.toDouble() + sampleEEG.highAlpha!!.toDouble() +
                sampleEEG.lowBeta!!.toDouble() + sampleEEG.highBeta!!.toDouble() +
                sampleEEG.lowGamma!!.toDouble() + sampleEEG.midGamma!!.toDouble()
        val dataIsCorrect = sampleEEG.signal!!.toDouble() <= 26.0 && (sum > 1000.0)

        if (dataIsCorrect) {
            var sumwaves = sampleEEG.delta!!.toDouble() + sampleEEG.theta!!.toDouble() +
                    sampleEEG.lowAlpha!!.toDouble() + sampleEEG.highAlpha!!.toDouble() +
                    sampleEEG.lowBeta!!.toDouble() + sampleEEG.highBeta!!.toDouble()

            var sumallwaves = sampleEEG.delta!!.toDouble() + sampleEEG.theta!!.toDouble() +
                    sampleEEG.lowAlpha!!.toDouble() + sampleEEG.highAlpha!!.toDouble() +
                    sampleEEG.lowBeta!!.toDouble() + sampleEEG.highBeta!!.toDouble() +
                    sampleEEG.lowGamma!!.toDouble() + sampleEEG.midGamma!!.toDouble()

            var sumwavesdelta = sampleEEG.theta!!.toDouble() +
                    sampleEEG.lowAlpha!!.toDouble() + sampleEEG.highAlpha!!.toDouble() +
                    sampleEEG.lowBeta!!.toDouble() + sampleEEG.highBeta!!.toDouble()

            if (sumwaves > 0 && sumallwaves > 0 && sumwavesdelta > 0 &&
                sampleEEG.lowBeta!! > 0 && sampleEEG.highBeta!! > 0 &&
                sampleEEG.midGamma!! > 0 && sampleEEG.delta!! > 0 &&
                sampleEEG.theta!! > 0 && sampleEEG.lowAlpha!! > 0 &&
                sampleEEG.highAlpha!! > 0 && sampleEEG.lowGamma!! > 0
            ) {

                var hb5minZ = user.userData?.hb5min?.getZScore(
                    newValue = sampleEEG.highBeta!!.toDouble(),
                    timestamp = sampleEEG.timestamp!!
                )
                var mf5minZ = user.userData?.hb5min?.getZScore(
                    newValue = (
                            1.625 * sampleEEG.delta!!.toDouble() +
                                    4.125 * sampleEEG.theta!!.toDouble() +
                                    8.375 * sampleEEG.lowAlpha!!.toDouble() +
                                    10.875 * sampleEEG.highAlpha!!.toDouble() +
                                    14.875 * sampleEEG.lowBeta!!.toDouble() +
                                    23.875 * sampleEEG.highBeta!!.toDouble() +
                                    35.375 * sampleEEG.lowGamma!!.toDouble() +
                                    45.375 * sampleEEG.midGamma!!.toDouble()
                            ) / sumallwaves,
                    timestamp = sampleEEG.timestamp!!
                )
                var tablog5minZ = user.userData?.tablog5min?.getZScore(
                    newValue = ((ln(sampleEEG.theta!!.toFloat()) + ln(sampleEEG.lowAlpha!!.toFloat()) + ln(
                        sampleEEG.highAlpha!!.toFloat()
                    )) / (ln(sampleEEG.lowBeta!!.toFloat()) + ln(sampleEEG.highBeta!!.toFloat()))).toDouble(),
                    timestamp = sampleEEG.timestamp!!
                )
                var tplog5minZ = user.userData?.tplog5min?.getZScore(
                    newValue = (ln(sampleEEG.delta!!.toFloat()) + ln(sampleEEG.theta!!.toFloat()) + ln(
                        sampleEEG.lowAlpha!!.toFloat()
                    ) + ln(sampleEEG.highAlpha!!.toFloat()) + ln(sampleEEG.lowBeta!!.toFloat()) + ln(
                        sampleEEG.highBeta!!.toFloat()
                    )).toDouble(),
                    timestamp = sampleEEG.timestamp!!
                )

                /** Fatigue */
                if (hb5minZ != null && tplog5minZ != null && mf5minZ != null && tablog5minZ != null && user.userData?.lfhf5min?.zScore != null) {
                    var fatiguePred =
                        0.492 + 0.2606 * user.userData?.lfhf5min!!.zScore!! + 0.0475 * hb5minZ + 0.2555 * tplog5minZ + 0.1439 * tablog5minZ + 0.0958 * mf5minZ
                    fatigue = user.userData!!.fatiguePred?.getScore(
                        newValue = fatiguePred,
                        timestamp = sampleEEG.timestamp!!,
                        minus = false
                    )
                    if (user.userData!!.fatiguePred?.count!! < 3600) {
                        fatigue = fatigue?.coerceIn(25.0, 75.0) ?: 25.0
                    } else if (user.userData!!.fatiguePred?.count!! < 7200) {
                        fatigue = fatigue?.coerceIn(18.0, 82.0) ?: 18.0
                    } else if (user.userData!!.fatiguePred?.count!! < 10800) {
                        fatigue = fatigue?.coerceIn(10.0, 90.0) ?: 10.0
                    }
                }

                /** Focus */
                focus = user.userData!!.wtbetaavgcurt?.getZScore(
                    newValue = (sampleEEG.lowBeta!! + sampleEEG.highBeta!!).toDouble() / sumwavesdelta,
                    timestamp = sampleEEG.timestamp!!
                )

                /** Flow */
                flow = user.userData!!.wtalphaavgcurt?.getZScore(
                    newValue = (sampleEEG.lowAlpha!! + sampleEEG.highAlpha!!).toDouble() / sumwavesdelta,
                    timestamp = sampleEEG.timestamp!!
                )

                if (flow != null && fatigue != null && focus != null) {
                    rawDataStorage.saveMentalPerformance(
                        RawMentalPerformance(
                            fatigue = fatigue ?: 0.0,
                            flow = flow ?: 0.0,
                            focus = focus ?: 0.0,
                            timestamp = (Date().time / 1000L).toInt()
                        )
                    )
                    flow = 0.0
                    fatigue = 0.0
                    focus = 0.0
                }
            }
        }
    }
    /**  */

    /** HR test data + all necessary methods */

    var sample = SampleHR()
    var currentSampleHR = mutableMapOf<String, SampleHR>()
    var DebugHRcsvFile = StringBuilder()
    var lastRrTimestamp = 0
    var referenceTimestampCountBadRrSample = 0
    var receivedGoodHrData = false
    var sessionNumberHRError = 0
    var rollingRR256 = arrayListOf<Pair<Int?, Double?>>()
    var fftCount = 0
    var DebugLFHFcsvFile = StringBuilder()

    private fun fullfillHrArray() {
        var codeByteArray1 = byteArrayOf(71, 3, 68, 3, 100, 0, 0, 0, 0, 0, 0, 0, 0)
        var codeByteArray2 = byteArrayOf(72, 3, 101, 3, 100, 0, 0, 0, 0, 0, 0, 0, 0)
        var codeByteArray3 = byteArrayOf(72, 3, 121, 3, 100, 0, 0, 0, 0, 0, 0, 0, 0)
        var codeByteArray4 = byteArrayOf(72, 3, 106, 3, 100, 0, 0, 0, 0, 0, 0, 0, 0)
        var codeByteArray5 = byteArrayOf(72, 3, 84, 3, 100, 94, 3, 100, 0, 0, 0, 0, 0)
        hrArray.add(codeByteArray1)
        hrArray.add(codeByteArray2)
        hrArray.add(codeByteArray3)
        hrArray.add(codeByteArray4)
        hrArray.add(codeByteArray5)
    }

    override suspend fun parseHeartRateMaddcog(data: ByteArray?) {
        var hrValue = 0
        var rrs: MutableList<SampleHR.Quintuple<Int, Int, Int, Int, Int>> = mutableListOf()
        val timestamp = (Date().time / 1000L).toInt()

        data?.let { data ->
            if (data.size >= 2) {
                hrValue = data[0].toInt()
                val status = data[1].toInt()
                var offset = 2

                if (offset < data.size) {
                    val rrValueRaw = data[offset].toInt() or (data[offset + 1].toInt() shl 8)
                    val rrValue = ((rrValueRaw.toDouble() / 1024.0) * 1000.0).toInt()
                    val rrConfidence = data[offset + 2].toInt()
                    rrs.add(SampleHR.Quintuple(hrValue, status, rrValue, rrConfidence, timestamp))
                }
            }
        }

        if (rrs.isNotEmpty()) {
            onHeartRateReceived(rrs, "peripheral!!")
        }
    }

    override suspend fun parseBatteryMaddcog(level: Int?) {
        level?.let {
            rawDataStorage.saveBatteryLevel(it)
        }
    }


    suspend fun onHeartRateReceived(
        rrs: List<SampleHR.Quintuple<Int, Int, Int, Int, Int>>,
        peripheral: String
    ) {
        rrs.forEach { (hr, status, rr, confidence, timestamp) ->
            sample.newValue = SampleHR.Quintuple(
                timestamp.toInt(),
                hr.toInt(),
                status.toInt(),
                rr.toDouble(),
                confidence.toInt(),
            )
            currentSampleHR[peripheral] = sample

            currentSampleHR[peripheral]?.let { currentSample ->
                if (currentSample.rr != -1.0) {
                    DebugHRcsvFile.append(
                        "$timestamp,${currentSample.hr},${currentSample.status},${
                            "%.1f".format(
                                currentSample.rrRaw
                            )
                        },${currentSample.confidence},${"%.1f".format(currentSample.rr)}\n"
                    )
                }

                lastRrTimestamp = timestamp

                if ((referenceTimestampCountBadRrSample == 0) || ((timestamp - referenceTimestampCountBadRrSample) > 30)) {
                    referenceTimestampCountBadRrSample = timestamp
                    currentSample.countBadRrSample = 0
                }

                if (currentSample.countGoodRrSample == 5) {
                    receivedGoodHrData = true
                }

                if (currentSample.countBadRrSample == 20) {
                    receivedGoodHrData = false

                    sessionNumberHRError += 1
                    referenceTimestampCountBadRrSample = 0
                    currentSample.countBadRrSample = 0
                    currentSample.countGoodRrSample = 0
                }

                if (currentSample.rr != -1.0) {
                    rollingRR256.add(Pair(currentSample.timestamp, currentSample.rr))

                    if (rollingRR256.size == 257) {
                        rollingRR256.removeFirst()
                    }

                    if (rollingRR256.size == 256) {
                        if (fftCount % 2 == 0) {
                            val signal: FloatArray =
                                rollingRR256.map { it.second?.toFloat() ?: 0.0f }.toFloatArray()
                            val spectrum = FftAnalyzer.fftAnalyzer(signal)

                            var vlf = 0.0
                            var lf = 0.0
                            var hf = 0.0
                            val step = 1.0 / 256.0
                            var frequency = step

                            for (j in 1 until spectrum.size) {
                                when {
                                    frequency < 0.04 -> {
                                        vlf += (spectrum[j - 1] + spectrum[j]) / 2 * step
                                    }
                                    frequency < 0.15 -> {
                                        lf += (spectrum[j - 1] + spectrum[j]) / 2 * step
                                    }
                                    frequency < 0.4 -> {
                                        hf += (spectrum[j - 1] + spectrum[j]) / 2 * step
                                    }
                                    else -> {
                                        break
                                    }
                                }
                                frequency += step
                            }

                            fftCount = 1

                            currentSampleHR[peripheral]?.let { currentSample ->
                                user.userData?.lfhf5min?.getZScore(
                                    newValue = lf / hf, timestamp = currentSample.timestamp
                                )

                                DebugLFHFcsvFile.append(
                                    "${currentSample.timestamp},${
                                        "%.1f".format(
                                            currentSample.rrRaw
                                        )
                                    },${"%.1f".format(currentSample.rr)},${"%.4f".format(lf / hf)},${
                                        "%.4f".format(
                                            user.userData?.lfhf5min?.rollingValue
                                        )
                                    },${"%.4f".format(user.userData?.lfhf5min?.mean)},${
                                        "%.4f".format(
                                            user.userData?.lfhf5min?.sd
                                        )
                                    },${"%.4f".format(user.userData?.lfhf5min?.zScore)}\n"
                                )

                            }
                        } else {
                            fftCount += 1
                        }
                    }
                }
            }

            if (true) {
                currentSampleHR[peripheral]?.let { currentSample ->
                    if (currentSample.rr != -1.0) {
                        rawDataStorage.addHrData(MADDCOG_HRV_CHAR, currentSample)
                    }
                }
            }
        }
    }

    /**  */
}

enum class SensorDataQuality {
    NotDefined, EegError, HrError, Good, DeviceError
}