package com.maddcog.android.data.network.dto.userProfile

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class RemoteWtbetaavgcurt(
    @SerialName("count") var count: Int? = null,
    @SerialName("sd") var sd: Double? = null,
    @SerialName("starting_sd") var starting_sd: Double? = null,
    @SerialName("mean") var mean: Double? = null,
    @SerialName("starting_mean") var starting_mean: Double? = null
)