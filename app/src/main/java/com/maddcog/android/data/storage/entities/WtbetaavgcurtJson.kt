package com.maddcog.android.data.storage.entities

import com.maddcog.android.domain.entities.user.Wtbetaavgcurt
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class WtbetaavgcurtJson(
    @SerialName("count")
    var count: Int? = null,
    @SerialName("sd")
    var sd: Double? = null,
    @SerialName("startingSd")
    var startingSd: Double? = null,
    @SerialName("mean")
    var mean: Double? = null,
    @SerialName("startingMean")
    var startingMean: Double? = null
)

fun WtbetaavgcurtJson.toDomain() = Wtbetaavgcurt(
    count = count,
    sd = sd,
    startingSd = startingSd,
    startingMean = startingMean,
    mean = mean
)

fun Wtbetaavgcurt.toData() = WtbetaavgcurtJson(
    count = count,
    sd = sd,
    startingSd = startingSd,
    startingMean = startingMean,
    mean = mean
)