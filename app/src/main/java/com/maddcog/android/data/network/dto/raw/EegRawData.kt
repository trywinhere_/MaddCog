package com.maddcog.android.data.network.dto.raw

data class EegRawData(
    val sensorData: Map<String, List<EegRaw>>
)