package com.maddcog.android.data.network.dto.rawActivity

data class RawMentalPerformance(
    val fatigue: Double,
    val flow: Double,
    val focus: Double,
    val timestamp: Int
)