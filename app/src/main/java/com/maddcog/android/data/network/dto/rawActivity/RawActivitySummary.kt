package com.maddcog.android.data.network.dto.rawActivity

data class RawActivitySummary(
    val fatigue_avg: Double,
    val fatigue_zone_high: Double,
    val fatigue_zone_low: Double,
    val fatigue_zone_normal: Double,
    val flow_avg: Double,
    val flow_zone_high: Double,
    val flow_zone_low: Double,
    val flow_zone_normal: Double
)