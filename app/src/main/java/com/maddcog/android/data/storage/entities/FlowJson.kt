package com.maddcog.android.data.storage.entities

import com.maddcog.android.domain.entities.user.Flow
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
class FlowJson(
    @SerialName("count")
    val count: Int? = null,
    @SerialName("sd")
    val sd: Double? = null,
    @SerialName("mean")
    val mean: Double? = null
)

fun FlowJson.toDomain() = Flow(
    count = count,
    sd = sd,
    mean = mean,
)

fun Flow.toData() = FlowJson(
    count = count,
    sd = sd,
    mean = mean,
)