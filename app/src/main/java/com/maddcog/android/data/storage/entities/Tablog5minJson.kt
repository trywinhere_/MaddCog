package com.maddcog.android.data.storage.entities

import com.maddcog.android.domain.entities.user.Tablog5min
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class Tablog5minJson(
    @SerialName("count")
    val count: Int? = null,
    @SerialName("sd")
    val sd: Double? = null,
    @SerialName("startingSd")
    val startingSd: Double? = null,
    @SerialName("mean")
    val mean: Double? = null,
    @SerialName("startingMean")
    val startingMean: Double? = null
)

fun Tablog5minJson.toDomain() = Tablog5min(
    count = count,
    sd = sd,
    startingSd = startingSd,
    startingMean = startingMean,
    mean = mean
)

fun Tablog5min.toData() = Tablog5minJson(
    count = count,
    sd = sd,
    startingSd = startingSd,
    startingMean = startingMean,
    mean = mean
)