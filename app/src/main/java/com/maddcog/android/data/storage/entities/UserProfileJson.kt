package com.maddcog.android.data.storage.entities

import com.maddcog.android.domain.entities.UserGame
import com.maddcog.android.domain.entities.user.UserAverage
import com.maddcog.android.domain.entities.user.UserProfile
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
class UserProfileJson(
    @SerialName("id")
    var id: String,
    @SerialName("name")
    var name: String,
    @SerialName("email")
    var email: String,
    @SerialName("userGames")
    var userGames: List<UserGameJson>,
    @SerialName("endpoint")
    val endpoint: String,
    @SerialName("userData")
    val userData: UserDataJson,
)

@Serializable
data class UserGameJson(
    @SerialName("generatedId")
    val generatedId: String,
    @SerialName("gameNumber")
    val gameNumber: Int,
    @SerialName("userName")
    val userName: String,
    @SerialName("region")
    val region: String,
    @SerialName("gameName")
    val gameName: String,
    @SerialName("userAverages")
    val userAverages: List<UserAverageJson>,
)

@Serializable
data class UserAverageJson(
    @SerialName("name")
    val name: String,
    @SerialName("avg")
    val avg: Double,
    @SerialName("median")
    val median: Double,
)

fun UserProfileJson.toDomain() = UserProfile(
    id = id,
    name = name,
    email = email,
    userGames = userGames.map { it.toDomain() },
    endpoint = endpoint,
    userData = userData.toDomain()
)

fun UserProfile.toData() = UserProfileJson(
    id = id,
    name = name,
    email = email,
    userGames = userGames.map { it.toData() },
    endpoint = endpoint,
    userData = userData.toData()
)

fun UserGameJson.toDomain() = UserGame(
    generatedId = generatedId,
    gameNumber = gameNumber,
    userName = userName,
    region = region,
    gameName = gameName,
    userAverages = userAverages.map { it.toDomain() }
)

fun UserGame.toData() = UserGameJson(
    generatedId = generatedId,
    gameNumber = gameNumber,
    userName = userName ?: "",
    region = region,
    gameName = gameName ?: "",
    userAverages = userAverages.map { it.toData() }
)

fun UserAverageJson.toDomain() = UserAverage(
    name = name,
    avg = avg,
    median = median,
)

fun UserAverage.toData() = UserAverageJson(
    name = name,
    avg = avg,
    median = median,
)