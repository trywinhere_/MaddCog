package com.maddcog.android.data.network.dto.userProfile

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class RemoteUserAvgs(
    @SerialName("kda") var kda: Double? = null,
    @SerialName("kill_participation") var killParticipation: Double? = null,
    @SerialName("creep_score_min") var creepScoreMin: Double? = null,
    @SerialName("win") var win: Int? = null,
    @SerialName("grade") var grade: Double? = null
)