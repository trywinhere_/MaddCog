plugins {
    id 'com.android.application'
    id 'org.jetbrains.kotlin.android'
    id 'kotlin-kapt'
    id 'kotlin-parcelize'
    id 'kotlinx-serialization'
    id "androidx.navigation.safeargs.kotlin"
    id 'dagger.hilt.android.plugin'
    id 'com.google.gms.google-services'
    id 'com.google.firebase.crashlytics'
}
apply from: '../quality/ktlint_prefs.gradle'
apply from: '../dependencies.gradle'

android {
    namespace 'com.maddcog.android'
    compileSdk 33

    defaultConfig {
        applicationId "com.maddcog.android"
        minSdk 28
        targetSdk 33
        versionCode 1
        versionName "1.0"

        testInstrumentationRunner "androidx.test.runner.AndroidJUnitRunner"
        vectorDrawables {
            useSupportLibrary true
        }
    }

    buildTypes {
        release {
            minifyEnabled false
            proguardFiles getDefaultProguardFile('proguard-android-optimize.txt'), 'proguard-rules.pro'
            manifestPlaceholders.usesCleartextTraffic = false
            buildConfigField "String", "BASE_URL", '"https://v6.develop.maddcog.io/"'
        }
        debug {
            manifestPlaceholders.usesCleartextTraffic = true
            buildConfigField "String", "BASE_URL", '"https://v6.develop.maddcog.io/"'
        }
    }
    compileOptions {
        coreLibraryDesugaringEnabled true
        sourceCompatibility JavaVersion.VERSION_11
        targetCompatibility JavaVersion.VERSION_11
    }

    java {
        toolchain {
            languageVersion.set(JavaLanguageVersion.of(11))
        }
    }

    kotlinOptions {
        jvmTarget = '11'
        freeCompilerArgs += ["-Xopt-in=kotlin.RequiresOptIn"]
    }
    buildFeatures {
        compose true
        viewBinding true
    }
    composeOptions {
        kotlinCompilerExtensionVersion compose_compiler_version
    }
    packagingOptions {
        resources {
            excludes += '/META-INF/{AL2.0,LGPL2.1}'
        }
    }

    lintOptions {
        lintConfig file("$project.rootDir/quality/lint/lint.xml")
    }

    hilt {
        enableTransformForLocalTests = true
    }
    testOptions {
        unitTests.all {
            useJUnitPlatform()
        }
    }

}

dependencies {
//    Google services
    implementation 'com.google.firebase:firebase-crashlytics-ktx:18.3.3'
    implementation 'com.google.firebase:firebase-analytics-ktx:21.2.0'
    implementation 'com.google.firebase:firebase-messaging:23.1.1'

    //Permissions
    implementation "com.google.accompanist:accompanist-permissions:0.21.1-beta"

//    The Amplify Auth
    implementation amplify
    implementation amplifyframework_core
    coreLibraryDesugaring desugar_jdk_libs

    implementation android_core
    implementation lifecycle_viewmodel
    implementation google_gson
    implementation google_retrofit
    implementation okhttp
    implementation squareup_retrofit
    implementation okhttp_interceptor

    // Android
    implementation appcompat
    implementation constraintlayout
    implementation activity_ktx
    implementation fragment_ktx
    implementation legacy_support
    implementation material
    implementation  activity_compose

    implementation compose_ui
    implementation compose_material
    implementation compose_tooling_preview
    implementation lifecycle_viewmodel_compose
    implementation livedata
    implementation compose_3
    implementation compose_material_window_size
    implementation compose_coil
    implementation coil_gif
    implementation accompanist_placeholder
    implementation compose_ui_utils
    implementation compose_foundation
    implementation compose_animation
    implementation compose_animation_core
    implementation compose_compiler
    implementation compose_runtime
    implementation compose_ui_tooling
    implementation flow_row
    implementation compose_icons

    implementation constraintlayout_compose

    implementation navigation_compose

    // Android Navigation
    implementation navigation_fragment_ktx
    implementation navigation_ui_ktx

    implementation androidx_lifecycle
    implementation androidx_lifecycle_livedata
    implementation androidx_lifecycle_tuntime
    implementation androidx_lifecycle_common
    implementation androidx_lifecycle_ext

    //Dagger Hilt
    kapt hilt_compiler
    implementation hilt
    implementation hilt_work

    implementation accompanist_systemuicontroller
    implementation exoplayer

    // Glide
    implementation bumptech_glide
    kapt bumptech_glide_version_compiler
    implementation bumptech_glide_okhttp

    //JSON
    implementation serialization

//    Browser
    implementation browser

//    AWS
    implementation aws_mobile_client
    implementation aws_android_sdk
    implementation aws_android_sdk_google_auth
    implementation aws_android_sdk_auth_userpool
    implementation aws_android_sdk_cognito
    implementation android_support
    implementation appcompat_support
    implementation amazonaws_auth_ui
    implementation api_gateaway
    implementation sdk_pinpoint

    implementation google_play_services_auth
    implementation work

    implementation joda_time

    implementation compose_insets
    
    testImplementation junit

    androidTestImplementation android_x_test_junit
    androidTestImplementation espresso_core
    androidTestImplementation compose_ui_test
    debugImplementation compose_ui_tooling
    debugImplementation compose_ui_test_manifest
    testImplementation koin_test
    testImplementation koin_test_junit_4
    testImplementation mokito_inline
    testImplementation mokito_core
    testImplementation turbine
    testImplementation truth
    testImplementation coroutines_test

    implementation 'com.github.wendykierp:JTransforms:3.1'


    //Junit5
    // Required -- JUnit 4 framework
    testImplementation(platform("org.junit:junit-bom:5.9.2"))
    // Optional -- Robolectric environment
    testImplementation "androidx.test:core:1.5.0"
    // Optional -- mockito-kotlin
    testImplementation "org.mockito.kotlin:mockito-kotlin:4.1.0"
    // Optional -- Mockk framework
    testImplementation "io.mockk:mockk:1.10.0"

    testImplementation 'org.junit.jupiter:junit-jupiter'
    implementation 'androidx.test.ext:junit-ktx:1.1.5'

}